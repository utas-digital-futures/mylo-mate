import archiver from 'archiver';
import request from 'superagent';
import path from 'path';
import fs from 'fs';

export interface Authorization {
  access_token: string;
  token_type: 'Bearer';
  expires_in: number;
  refresh_token: string;
}

class UploadError extends Error {
  constructor(msg: string, public response: any, public request: any) {
    super(msg);
  }
}

interface ChromeWebStoreConfig {
  clientId: string;
  clientSecret: string;
  refresh_token: string;
  extensionId: string;
}

export class ChromeWebStore {
  constructor(private config: ChromeWebStoreConfig) {}

  async getCredentials(): Promise<Authorization> {
    const { clientId, clientSecret, refresh_token } = this.config;

    const res = await request.post('https://oauth2.googleapis.com/token').type('form').send({
      client_id: clientId,
      client_secret: clientSecret,
      refresh_token,
      grant_type: 'refresh_token',
    });

    return res.body;
  }

  async upload(filepath: string, token: Authorization) {
    const { extensionId } = this.config;
    const res = await request
      .put(`https://www.googleapis.com/upload/chromewebstore/v1.1/items/${extensionId}`)
      .set('x-goog-api-version', '2')
      .auth(token.access_token, { type: 'bearer' })
      .attach('package', filepath);

    if (res.body.uploadState == 'SUCCESS') return true;
    else throw new UploadError('Error uploading package', res.body, res);
  }

  async publish(token: Authorization) {
    const { extensionId } = this.config;
    const res = await request
      .post(`https://www.googleapis.com/chromewebstore/v1.1/items/${extensionId}/publish`)
      .set('x-goog-api-version', '2')
      .set('Content-Length', '0')
      .auth(token.access_token, { type: 'bearer' });

    return res.body;
  }

  async republish(filepath: string) {
    console.log('Fetching refreshed credentials...');
    const token = await this.getCredentials();
    try {
      console.log('Uploading the zipped payload to the Chrome Web Store...');
      const up = await this.upload(filepath, token);
      if (!up) return false;

      console.log('Marking the payload as published...');
      await this.publish(token);

      return true;
    } catch (e) {
      console.error(e);
      throw e;
      /** return false; */
    }
  }
}

class ExtendedDate extends Date {
  /** Get the number of raw seconds since midnight. */
  getSecondsSinceMidnight() {
    return Math.floor(((new Date(this) as any) - new Date(this).setHours(0, 0, 0, 0)) / 1000);
  }

  /** Test if this is a leap year. */
  isLeapYear() {
    var year = this.getFullYear();
    if ((year & 3) != 0) return false;
    return year % 100 != 0 || year % 400 == 0;
  }

  /** Get the day of the year. Accounts for daylight savings */
  dayOfYear() {
    var dayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
    var mn = this.getMonth();
    var dn = this.getDate();
    var dayOfYear = dayCount[mn] + dn;
    if (mn > 1 && this.isLeapYear()) dayOfYear++;
    dayOfYear++;

    return dayOfYear.toString().padStart(3, '0');
  }
}

/*******************************
 *                             *
 *  Lets start the execution!  *
 *                             *
 *******************************/

const date = new ExtendedDate();

const { REFRESH_TOKEN, CLIENT_ID, CLIENT_SECRET, EXTENSION_ID } = process.env;

const chromestore = new ChromeWebStore({
  clientId: CLIENT_ID as string,
  clientSecret: CLIENT_SECRET as string,
  refresh_token: REFRESH_TOKEN as string,
  extensionId: EXTENSION_ID as string,
});

console.log('Updating the manifest.json file to beta-ify it...');

const manifest = JSON.parse(fs.readFileSync('./src/manifest.json', 'utf8'));
const build_id = `${date.getFullYear() - 2000}${date.dayOfYear()}.${Math.round(date.getSecondsSinceMidnight() / 2)}`;

const [major, minor, hotfix] = manifest.version.split('.');

manifest.version = `${major}.${minor}.${build_id}`;
manifest.version_name = `${major}.${minor}.${hotfix} beta ${build_id}`;
manifest.name += ' Beta';
manifest.short_name += ' Beta';
manifest.permissions.concat('management');

async function execute() {
  try {
    const zip = await new Promise<string>((resolve, reject) => {
      const fn = `mylo-mate-${Date.now()}.zip`;
      const res = () => resolve(fn);
      console.log('Zipping the src/ directory into %s...', fn);

      const output = fs.createWriteStream(path.join(__dirname, fn));
      const zip = archiver('zip', { zlib: { level: 9 } });

      output.on('close', res);
      output.on('end', res);

      zip.on('error', reject);
      zip.on('warning', e => {
        if (e.code === 'ENOENT') console.log('Warning: ', e);
        else reject(e);
      });

      zip.pipe(output);
      zip.glob('**/!(*manifest*)', { cwd: 'src' });
      zip.append(JSON.stringify(manifest), { name: 'manifest.json' });
      zip.finalize();
    });

    const success = await chromestore.republish(zip);
    if (success) console.log('%s beta %s updated successfully', manifest.name, build_id);
    else console.log('Something went wrong updating %s', manifest.name);
  } catch (e) {
    console.error('An error occurred uploading the updated extension.', e);
  }
}

execute()
  .then(() => process.exit(0))
  .catch(() => process.exit(1));
