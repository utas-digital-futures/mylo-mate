const path = require('path');
const archiver = require('archiver');
const fs = require('fs');
const exec = require('child_process').exec;

const folder = 'src';
const zipName = 'extension.zip';
const manifest = require(`./${folder}/manifest.json`);
const webstore = './node_modules/.bin/webstore';

const betaPermissions = ['management'];

Date.prototype.getSecondsSinceMidnight = function() {
  return Math.floor(
    (new Date(this) - new Date(this).setHours(0, 0, 0, 0)) / 1000
  );
};
Date.prototype.isLeapYear = function() {
  var year = this.getFullYear();
  if ((year & 3) != 0) return false;
  return year % 100 != 0 || year % 400 == 0;
};

// Get Day of Year
Date.prototype.getDOY = function() {
  var dayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
  var mn = this.getMonth();
  var dn = this.getDate();
  var dayOfYear = dayCount[mn] + dn;
  if (mn > 1 && this.isLeapYear()) dayOfYear++;
  dayOfYear++;
  return dayOfYear < 10
    ? `00${dayOfYear}`
    : dayOfYear < 100
    ? `0${dayOfYear}`
    : dayOfYear;
};

const d = new Date();

const v = manifest.version;
const [major, minor] = v.split('.');
const beta = `${d.getFullYear() - 2000}${d.getDOY()}.${Math.round(
  d.getSecondsSinceMidnight() / 2
)}`;
manifest.version = `${major}.${minor}.${beta}`;
manifest.version_name = `${v} beta ${beta}`;
manifest.name += ' Beta';
manifest.short_name += ' Beta';
manifest.permissions = betaPermissions.concat(manifest.permissions);

const REFRESH_TOKEN = process.env.REFRESH_TOKEN;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const CLIENT_ID = process.env.CLIENT_ID;
const EXTENSION_ID = 'maianiginahgggelicglpcgcpjoefofi';

const async_exec = (cmd, log) =>
  new Promise((resolve, reject) => {
    exec(cmd, (err, stdout, stderr) => {
      if (log) {
        console.log(`stdout: ${stdout.trim()}`);
        console.log(`stderr: ${stderr.trim()}`);
      }
      if (err !== null) {
        reject(err);
        process.exit(1);
      } else resolve(true);
    });
  });

const Command = {
  Upload: `${webstore} upload --source ./${zipName} --extension-id ${EXTENSION_ID} --client-id ${CLIENT_ID} --client-secret ${CLIENT_SECRET} --refresh-token ${REFRESH_TOKEN}`,
  Publish: `${webstore} publish --extension-id ${EXTENSION_ID} --client-id ${CLIENT_ID} --client-secret ${CLIENT_SECRET} --refresh-token ${REFRESH_TOKEN}`
};

class Package {
  static async Zip(dir, name) {
    return new Promise((resolve, reject) => {
      const output = fs.createWriteStream(path.join(__dirname, name));
      const zip = archiver('zip', {
        zlib: { level: 9 }
      });

      output.on('close', resolve);
      output.on('end', resolve);
      zip.on('warning', e => {
        if (e.code === 'ENOENT') console.log('Warning: ', e);
        else reject(e);
      });
      zip.on('error', reject);

      zip.pipe(output);
      zip.glob('**/!(*manifest*)', { cwd: dir });
      zip.append(JSON.stringify(manifest), { name: 'manifest.json' });
      zip.finalize();
    });
  }
  static async Upload(log) {
    return await async_exec(Command.Upload, log);
  }
  static async Publish(log) {
    return await async_exec(Command.Publish, log);
  }
}

(async function() {
  try {
    await Package.Zip(folder, zipName);
    console.log('Zip file "%s" created from "%s/"', zipName, folder);

    await Package.Upload(true);
    console.log('Package uploaded to the Chrome Webstore successfully.');

    await Package.Publish(true);
    console.log('Package published successfully.');
  } catch (e) {
    console.log('Error: ', e);
  }
})();
