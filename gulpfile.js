console.time('Loading plugins');
const fs = require('fs');
const path = require('path');
const gulp = require('gulp');
const through = require('through2');
const commentparser = require('comment-parser');
console.timeEnd('Loading plugins');

function GenerateDocs(filename, template) {
  let comments = [];
  let latestFile;

  function parseFile(file, enc, cb) {
    latestFile = file;
    const res = commentparser(file.contents.toString());
    const matching = res.filter(comment => comment.source.includes('@problem') && comment.source.includes('@solution'));
    if (matching.length > 0) comments.push({ file: file.path, comments: matching });
    cb();
  }

  const getTag = (block, name) => {
    const tag = block.tags.find(tag => tag.tag == name);
    return tag ? `${tag.name} ${tag.description}` : '';
  };

  const modify = str =>
    str.replace(
      /(?<!\\)(?:\\{2})*`((?:(?<!\\)(?:\\{2})*\\`|[^`])+)(?<!\\)(?:\\{2})*`/gi,
      (target, match) => `<code>${match}</code>`
    );

  function buildDocs(cb) {
    const pool = {};
    comments.forEach(file => {
      try {
        file.comments.forEach(block => {
          const affects = modify(getTag(block, 'affects'));
          const problem = modify(getTag(block, 'problem'));
          const solution = modify(getTag(block, 'solution'));
          const target = modify(getTag(block, 'target'));
          const impact = modify(getTag(block, 'impact'));
          const savings = modify(getTag(block, 'savings'));

          const entries = pool[affects] || [];
          entries.push({
            path: file.file,
            description: file.description,
            affects,
            problem,
            solution,
            target,
            impact,
            savings,
          });
          pool[affects] = entries;
        });
      } catch (e) {
        console.log('Error parsing comment', file);
      }
    });

    const normalize = dir => dir.replace(path.join(__dirname, 'src/js/'), '');

    const html = template || `<html><head></head><body><%-CONTENT-%></body></html>`;
    let content = '<div class="accordion js-accordion-container" role="tablist">';

    Object.entries(pool)
      .sort((a, b) => a[0].localeCompare(b[0]))
      .forEach(([area, comments], index) => {
        const key = `problem-${index}-${Date.now()}`;
        content += `
        <section class="accordion--panel js-accordion-panel is-inactive" id="acc-${key}" aria-selected="false">
          <header role="tab" class="accordion--heading js-accordion-heading">
            <div class="accordion--title u-h4-small"><a href="#${key}" class="accordion--link">${area}</a></div>
          </header>
          <div role="tabpanel" id="${key}" class="accordion--body js-accordion-body" style="height: 0px; overflow: hidden;">
            <div class="accordion--content"> 
              <table class="table table__full">
                <thead>
                  <tr>
                    <th>Problem</th>
                    <th>Solution</th>
                    <th>Target</th>
                    <th>Impact</th>
                    <th>Time Saved</th>
                  </tr>
                </thead>
                <tbody>
                  ${comments
                    .map(
                      ({ path, problem, solution, target, impact, savings }) =>
                        `<tr>
                    <td title="Recorded in ${normalize(path)}">${problem}</td>
                    <td>${solution}</td>
                    <td>${target}</td>
                    <td>${impact}</td>
                    <td>${savings || 'Unknown'}</td>
                  </tr>`
                    )
                    .join('\n')}
                </tbody>
              </table>
            </div>
          </div>
        </section>`;
      });
    content += '</div>';

    /** Write data stream out as single file. */
    const data = Buffer.from(html.replace('<%-CONTENT-%>', content));
    const file = latestFile.clone({ contents: false });
    file.path = path.join(file.base, filename);
    file.contents = data;
    this.push(file);
    cb();
  }

  return through.obj(parseFile, buildDocs);
}

function docs() {
  return gulp
    .src(['./src/js/**/*.js', './src/js/**/*.mjs'])
    .pipe(GenerateDocs('impact-table.html', fs.readFileSync('./impact-table-template.html', 'utf-8')))
    .pipe(gulp.dest('./public/'));
}

exports.docs = docs;
// exports.watchDocs = function () {
//   gulp.watch(['./src/js/**/*.js', './src/js/**/*.mjs'], { ignoreInitial: false, usePolling: true }, docs);
// };
