(async () => {
  // TODO: Improve clone speed somehow?

  const orgid = window.location.pathname.match(/le\/content\/([0-9]+)\/home/i)[1];

  const ReorderTopic = async (orig, clone) => {
    // Store the values as FormData
    const body = new URLSearchParams({
      requestId: Date.now() % 10,
      d2l_referrer: window.localStorage.getItem('XSRF.Token'),
      isXhr: true,
      dropPosition: 1,
      dropTargetKey: `D2L.LE.Content.ContentObject.TopicCO-${orig}`,
      dropObjectKey: `D2L.LE.Content.ContentObject.TopicCO-${clone}`,
    });

    // Send the request, and return the resulting promise.
    return fetch(`/d2l/le/content/${orgid}/reorder/ReorderObject`, { method: 'post', body });
  };

  const CloneTopic = async topic => {
    const module = topic.ParentModuleId;
    const src = await api(`/d2l/api/le/1.57/${orgid}/content/topics/${topic.Id}/file?stream=1`, {
      type: 'text',
      first: true,
    });

    const parts = new URL(topic.Url, window.location.origin).pathname.split('/');
    const file = parts.pop();
    const pathbase = parts.join('/');

    const mdata = {
      Url: `${pathbase}/${file.replace(/\.html/gi, '')}-copy.html`,
      Title: `${topic.Title.trim()} (copy)`,
      ShortTitle: null,
      Type: 1,
      TopicType: 1,
      StartDate: null,
      EndDate: null,
      DueDate: null,
      IsHidden: true,
      IsLocked: false,
      OpenAsExternalResource: null,
      Description: null,
      MajorUpdate: null,
      MajorUpdateText: null,
      ResetCompletionTracking: null,
    };

    const body = new FormData();
    body.append('', new Blob([JSON.stringify(mdata)], { type: 'application/json' }));
    body.append('', new Blob([src], { type: 'text/html' }), file.replace(/\.html/gi, ''));

    const req = new Request(`/d2l/api/le/1.57/${orgid}/content/modules/${module}/structure/?renameFileIfExists=1`, {
      headers: { Authorization: `Bearer ${await GetOAuth2Token()}` },
      method: 'post',
      body,
    });

    const type = req.headers.get('content-type');
    req.headers.set('content-type', type.replace('/form-data', '/mixed'));

    return fetch(req).then(d => d.json());
  };

  const item = document.createElement('d2l-menu-item');
  item.setAttribute('aria-disabled', 'false');
  item.setAttribute('aria-label', 'Create a new copy of this page');
  item.setAttribute('text', 'Copy Page');
  item.setAttribute('role', 'menuitem');
  item.tabindex = -1;
  item.onclick = async e => {
    const topicId = e.target.closest('[data-placeholderkey]').getAttribute('data-placeholderkey');

    const topic = await api(`/d2l/api/le/1.57/${orgid}/content/topics/${topicId}`, { first: true });

    const response = await CloneTopic(topic);
    await ReorderTopic(topic.Id, response.Id);

    window.location.search = `?itemIdentifier=D2L.LE.Content.ContentObject.ModuleCO-${topic.ParentModuleId}`;
  };

  const onchange = ([record]) => {
    const dropdown = Array.from(record.addedNodes).find(el => el.matches?.('d2l-dropdown-menu'))?.children[0];
    if (!dropdown) return;

    if (!dropdown.querySelector('d2l-menu-item[text*="edit web page" i]')) return;

    dropdown.children[0].insertAdjacentElement('afterend', item);
  };

  const observer = new MutationObserver(onchange);
  const addDropdownItems = () => {
    const dropdownParent = document.querySelector('.d2l-datalist d2l-dropdown-context-menu').closest('ul.d2l-datalist');
    observer.observe(dropdownParent, { childList: true, subtree: true });
  };

  document.addEventListener('d2l-topic-changed', addDropdownItems);
  addDropdownItems();
})();
