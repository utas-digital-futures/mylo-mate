/**
 * @affects MyLO Table of Contents
 * @problem When opening some resources while impersonating a student they may function incorrectly, particularly LTI links such as Echo360 resources.
 * @solution When impersonating a student, LTI link clicks now warn that the user is impersonating a student, requiring confirmation to continue.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  function iterate(base) {
    let output = [],
      start = base;
    const loop = node => {
      if (node.Modules.length > 0) node.Modules.forEach(loop);
      if (node.Topics.length > 0)
        output = output.concat(
          node.Topics.map(topic => {
            topic.IsHidden = topic.IsHidden || node.IsHidden;
            return topic;
          })
        );
    };
    if (Array.isArray(start)) start.forEach(loop);
    else loop(start);
    return output;
  }

  const warning =
    'You are currently navigating MyLO in Student View. Please note this may have an effect on exteral LTI platforms.\n\nDo you wish to continue?';

  const [, orgid] = window.location.pathname.match(/content\/([0-9]+)\/Home/i);
  const me = await api(`/d2l/api/lp/1.24/enrollments/myenrollments/${orgid}/access`, { first: true, fresh: true });

  if (me.Access.ClasslistRoleName == 'Student View') {
    const toc = await api(`/d2l/api/le/1.28/${orgid}/content/toc?ignoreModuleDateRestrictions=true`, { first: true });
    const files = await Promise.all(iterate(toc.Modules));

    document.addEventListener(
      'click',
      e => {
        if (e.target.matches('a.d2l-link')) {
          const href = e.target.href;
          const [, id] = href.match(/viewContent\/([0-9]+)\/View/i);
          const topic = files.find(file => file.Identifier === id);

          if (topic && topic.ActivityType === 7 && !confirm(warning)) {
            e.preventDefault();
            e.stopImmediatePropagation();
            e.stopPropagation();
          }
        }
      },
      true
    );
  }
})();
