/**
 * @affects MyLO Table of Contents
 * @problem Content start, due and end dates are incorrect when content is migrated from a previous delivery, and it takes a long time to remove those incorrect date restrictions.
 * @solution A new tool automatically examines the content and removes all date restrictions.
 * @target Support staff
 * @impact Moderate
 */

(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  const store = await FetchMMSettings();
  if (!store.mylo.ShowDateRestrictionRemover) return;

  const orgid = document.querySelector('a[href^="/d2l/home"]').href.split('/').pop();

  const addButton = () => {
    const tree = document.querySelector('ul#ContentPluginTree');
    const item = document.createElement('li');
    item.classList.add('d2l-le-TreeAccordionItem', 'd2l-le-TreeAccordionItem-Root', 'mm-remove-dates');

    const a = document.createElement('a');
    a.classList.add('d2l-le-TreeAccordionItem-anchor', 'vui-heading-4', 'd2l-link');
    a.setAttribute('mm-icon-type', 'action-button');
    a.setAttribute('mm-icon-offset', '-7:12');
    a.title = 'Remove the Start, Due and End dates from content modules.';

    const wrapper = document.createElement('div');
    wrapper.classList.add('d2l-le-TreeAccordionItem-wrapper', 'd2l-body-compact');

    const icon = document.createElement('d2l-icon');
    icon.classList.add('d2l-tree-browser-icon');
    icon.setAttribute('icon', 'tier2:manage-dates');

    const text = document.createElement('div');
    text.classList.add('d2l-inline');
    text.innerHTML = `<div class="d2l-textblock">Clear Content Dates</div>`;

    const clear = document.createElement('div');
    clear.classList.add('d2l-clear');

    wrapper.append(icon, text, clear);
    a.append(wrapper);
    item.append(a);
    tree.append(item);

    a.addEventListener('click', async e => {
      a.disabled = true;

      try {
        const toc = await api(`/d2l/api/le/1.42/${orgid}/content/toc`, { first: true, fresh: true });
        function iterate(base) {
          let output = [],
            start = base;
          const loop = node => {
            output.push(node);
            if (node.Modules.length > 0) {
              node.Modules.forEach(loop);
            }
            if (node.Topics.length > 0) output = output.concat(node.Topics);
          };
          if (Array.isArray(start)) start.forEach(loop);
          else loop(start);
          return output;
        }

        const module = document.querySelector('div[id^="placeholderid"]').id.replace('placeholderid', '');

        const flat = iterate(toc.Modules);
        const restricted = flat.filter(item => item.StartDateTime || item.EndDateTime);

        if (restricted.length == 0) return alert('No modules found with date restrictions.');

        if (
          confirm(
            `This will remove the date restrictions from ${restricted.length} item${
              restricted.length == 1 ? '' : 's'
            }. Do you wish to continue?`
          )
        ) {
          let results = await Promise.all(restricted.map(RemoveDates));
          const okay = results.filter(res => res.status == 200);
          const notokay = results.filter(res => res.status != 200);
          alert(
            `${okay.length} item${okay.length == 1 ? '' : 's'} successfully updated. ${notokay.length} item${
              notokay.length == 1 ? '' : 's'
            } encountered errors updating.`
          );
          window.location.search = `?itemIdentifier=D2L.LE.Content.ContentObject.ModuleCO-${module}`;
        }
      } catch (e) {
        console.warn(e);
      }

      a.disabled = false;
    });
  };

  const RemoveDates = item => {
    const params = {
      requestId: Date.now() % 10,
      d2l_referrer: window.localStorage.getItem('XSRF.Token'),
      isXhr: true,
      ShowEditControls: 'True',
      DoUpdate: 'True',
      IsHidden: 'False',
      DisplayInCalendar: 'False',
      HasConditionSet: 'False',
      ConditionSetId: '',
      ConditionSetOperatorId: 1,

      HasStartDate: 'False',
      StartDateSelector$year: 2021,
      StartDateSelector$month: 9,
      StartDateSelector$day: 22,
      StartDateSelector$hour: 15,
      StartDateSelector$minute: 0,
      StartDateSelector$hasTime: 1,
      StartDateSelector$isEnabled: 0,
      StartDateTypeValue: 'SubmissionRestricted',

      HasDueDate: 'False',
      DueDateSelector$year: 2021,
      DueDateSelector$month: 9,
      DueDateSelector$day: 23,
      DueDateSelector$hour: 15,
      DueDateSelector$minute: 0,
      DueDateSelector$hasTime: 1,
      DueDateSelector$isEnabled: 0,

      HasEndDate: 'False',
      EndDateSelector$year: 2021,
      EndDateSelector$month: 9,
      EndDateSelector$day: 24,
      EndDateSelector$hour: 15,
      EndDateSelector$minute: 0,
      EndDateSelector$hasTime: 1,
      EndDateSelector$isEnabled: 1,
      EndDateTypeValue: 'AccessRestricted',
    };

    const url = item.ModuleId
      ? `/d2l/le/content/${orgid}/module/${item.ModuleId}/EditModuleRestrictions`
      : `/d2l/le/content/${orgid}/updateresource/ContentFile/${item.TopicId}/UpdateRestrictions?topicId=${item.TopicId}&activityId=`;

    console.log(url, params);

    return fetch(url, {
      method: 'post',
      body: new URLSearchParams(params),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });
  };

  let mo = new MutationObserver(() => {
    if (!document.querySelector('#ContentModuleTree .mm-remove-dates')) addButton();
  });
  mo.observe(document.querySelector('#ContentModuleTree'), { childList: true, subtree: true });
  addButton();
})();
