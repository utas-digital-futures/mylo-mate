/** Add the "Use D2L Basic Template" button */

const id = GetOrgId();
const basicTemplateSrc = '/shared/content-template/v3.0/pages/';
async function GetUnitSettings(id, asURLData = false) {
  const dom = new DOMParser().parseFromString(
    await fetch(`/d2l/le/content/${id}/settings/Open`).then(d => d.text()),
    'text/html'
  );
  const form = dom.querySelector('form');
  const fd = new URLSearchParams(new FormData(form));
  if (asURLData) return fd;

  const items = {};
  for (const [k, v] of fd) {
    items[k] = v;
  }
  return items;
}

async function SaveUnitSettings(id, changes) {
  const fd = await GetUnitSettings(id, true);
  for (const [k, v] of Object.entries(changes)) {
    fd.set(k, v);
  }

  fd.append('requestId', Date.now() % 10);
  fd.append('d2l_referrer', window.localStorage.getItem('XSRF.Token'));
  fd.append('isXhr', true);

  return fetch(`/d2l/le/content/${id}/settings/Save`, { method: 'post', body: fd });
}

GetUnitSettings(id).then(async config => {
  if (config.defaultTemplatePathSelector == '' || config.defaultTemplateIsEnabled != '1') {
    await SaveUnitSettings(id, {
      defaultTemplateIsEnabled: '1',
      defaultTemplatePathSelector: basicTemplateSrc,
    });
    console.log('📃 Template updated to %s', basicTemplateSrc);
  }
});
