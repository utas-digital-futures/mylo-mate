/**
 * @affects MyLO
 * @problem Some commonly used tools aren't easily available to users (such as accessing the Unit Outline or Manage Files area).
 * @solution Add shortcut icons to these commonly accessed tools to the navigation bar.
 * @target Academic and support staff
 * @impact High
 * @savings 30 seconds per use
 */

/**
 * @affects MyLO
 * @problem When in a unit, information about the delivery period is no longer available.
 * @solution
 *  Add the delivery period and unit type (such as `Semester 1 2020, Award Unit`) as a subtitle beneath the unit name.
 *  Also add a coloured marker next to the unit title, to indicate its current delivery state (Future | Current | Exam | Closed).
 * @target Academic and support staff
 * @impact High
 * @savings Unmeasureable
 */

/**
 * @affects MyLO
 * @problem Regularly accessed tool properties require several clicks and page loads to access.
 * @solution Add quicklinks to tools such as Quizzes and Assignments.
 * @target Academic and support staff
 * @impact High
 * @savings 2 minutes per unit
 */

/** @namespace MyLO */
(async function () {
  // Prevents the script from being run more than once.
  if (window.MyLOContentButtons) return;
  window.MyLOContentButtons = true;

  const _ = (r, all) => (all ? document.querySelectorAll(r) : document.querySelector(r));
  await until(() => document.body.getAttribute('mm-abort') != null);

  const icon = {
    none: 'none',
    pdf: 'pdflogo.jpg',
    html: 'htmllogo.png',
    docx: 'docxlogo.png',
    content: 'htmllogo.png',
    internet: 'internetlogo.png',
  };

  // If we're viewing the unit as a student (Impersonating View Student), then hide all of our features.
  const isStudent = document.body.getAttribute('mm-abort') == 'true';
  if (isStudent) return;

  const OER = window.location.hostname == 'oer.utas.edu.au';

  if (window.location.pathname.startsWith('/d2l/le/manageCourses/search')) return;
  if (window.location.pathname.includes('common/popup')) return;
  if (!document.querySelector('a[href*="/d2l/home"]')) return;

  const id = document.querySelector('a[href*="/d2l/home"]').href.split('/').pop();

  let items = await FetchMMSettings();
  let UnitData = await api(`/d2l/api/lp/1.22/courses/${id}`, { fresh: true, first: true });

  // Akari unit outline information
  const akari = await fetch('/shared/Images/Info_widget/AkariUnitOutlines_StageOneUnitSetup.js')
    .then(d => d.text())
    .then(txt => Array.from(txt.matchAll(/({.*})/gi)).map(match => JSON.parse(match[0])));

  const akariPeriods = {
    S1: 'Semester%201',
    S2: 'Semester%202',
    S3: 'Semester%203',
    S4: 'Semester%204',
    S5: 'Semester%205',
    A1: 'Trimester%201',
    A2: 'Trimester%202',
    A3: 'Trimester%203',
    T1: 'Term%201',
    T2: 'Term%202',
    T3: 'Term%203',
    T4: 'Term%204',
    YR: 'Full%20Year',
    OTHER: 'Other',
  };

  const root = document.createElement('div');
  root.classList.add('d2l-navigation-s-item', 'less-rpad');
  root.id = 'unit-links';
  root.role = 'list-item';
  document
    .querySelectorAll('.d2l-navigation-s-main-wrapper > .d2l-navigation-s-item:not(.d2l-navigation-s-more)')
    .at(-2)
    .insertAdjacentElement('afterend', root);

  let info = {};
  if (!OER) {
    const codeSplit = UnitData.Code.split('_');
    const outlineFormats = ['none', 'pdf', 'html', 'docx', 'content', 'internet'];

    if (codeSplit[0] == 'AW') {
      //get units from title prefix e.g. 'BMA101 BMA102 BMA103 three bma units'
      info.unitMembers = UnitData.Name.match(/[A-Z]{3}[0-9]{3}/g);

      while (codeSplit.length < 9) codeSplit.push('0');
      //Award code
      //AW_{0}_{1}_{2}_{3}_{4}_{5}_{6}_{7}
      info.unitType = 'Award';
      info.schoolCode = codeSplit[1];
      info.semester = codeSplit[2];
      info.requestId = codeSplit[3];
      info.myMedia = codeSplit[4] == '0' ? 'No' : 'Yes';
      info.timeTable = codeSplit[5] == '0' ? 'No' : 'Yes';

      info.unitOutlines = parseInt(codeSplit[6]);
      // Unit Outline codes are:
      //	0: 'Single Outline',
      //	1: 'One per Unit Code',
      //	2: 'OnCampus/OffCampus'

      info.useAkariOutlines =
        parseInt(codeSplit[8]) === 5 ||
        info.unitMembers.some(code =>
          akari.find(({ unitCode, studyPeriod }) => code === unitCode && studyPeriod === info.semester),
        );

      info.readingLists = codeSplit[7] == '0' ? 'No' : 'Yes';
      info.formatOutlines = outlineFormats[parseInt(codeSplit[8])];

      if (info.semester) {
        info.year = '20' + info.semester.substring(0, 2);
        info.semesterCode = info.semester.substring(2, 4);
      }

      const outlineURLs = getUnitOutlineURLs();

      const link = outlineURLs.length == 1 ? buildUnitOutlineSingle(outlineURLs[0]) : buildUnitOutlineDropdown(outlineURLs);
      root.prepend(link);
    }
  }

  if (!OER) {
    // Add unit codes to the header of daylight.
    const title = document.querySelector('.d2l-navigation-s-link');
    if (title.parentNode.querySelector('small')) title.parentNode.querySelector('small').remove();

    title.style.overflow = 'hidden';
    title.style['-webkit-line-clamp'] = '2';
    title.style['-webkit-box-orient'] = 'vertical';
    title.style.display = '-webkit-box';

    const code = UnitData.Code;

    const regex = /_([0-9]{2})(YR|(S|A|T)[0-9]{1})_/;
    const result = code.match(regex);

    const container = document.createElement('div');
    const subtitle = document.createElement('small');

    subtitle.style.fontSize = 'small';

    document.querySelector('.d2l-navigation-s-header-logo-area').style.overflow = 'visible';

    container.classList.add('mylo-mate-header-title');

    title.parentNode.insertBefore(container, title);
    title.style.fontSize = '1.25rem';

    container.appendChild(title);
    container.appendChild(subtitle);
    document.dispatchEvent(new CustomEvent('mm-subtitle-added'));

    document.body.setAttribute('mm-unit-code', code);

    if (!window.location.href.includes('d2ldev1.utas.edu.au')) {
      // Subtitle logic.
      let details = {
        type: '', // Award, non-award or sandpit
        year: '', // Year the unit ran
      };

      if (code.startsWith('NA_SP_')) {
        subtitle.innerText = 'Sandpit';
      } else if (code.startsWith('TP_')) {
        subtitle.innerText = 'Template Unit';
      } else if (code.startsWith('AW_') || code.startsWith('NA_')) {
        if (code.startsWith('AW_')) details.type = 'Award';
        else if (code.startsWith('NA_')) details.type = 'Non-Award';

        details.year = '20' + result[1];

        const isFullYear = result[2].trim().toLowerCase() === 'yr';
        const periodLabel = isFullYear ? `Full Year ${details.year}` : UnitData.Semester.Name;

        subtitle.innerText = `${periodLabel}, ${details.type} Unit`;
      }
    }
  }

  // Add Advanced Search Options link if on Manage Groups page
  if (document.querySelector('.d2l-heading h1')?.innerText == 'Manage Groups') {
    const button = document.createElement('d2l-button-subtle');
    button.setAttribute('mm-icon-type', 'action-button');
    button.setAttribute('mm-icon-offset', '4:10');
    button.setAttribute('mm-view-as-student', 'false');
    button.setAttribute('icon', 'd2l-tier1:group');
    button.setAttribute('text', 'Advanced Search Options');

    button.onclick = () => {
      const href = document.querySelector('.d2l-navigation-s-link').href.split('/').pop();
      document.location = `/d2l/customization/DemographicBasedGroupCreation/${href}/Home`;
    };

    document.querySelector('.d2l-page-header .d2l-button-subtle-group')?.prepend(button);
  }

  if (!document.querySelector('header a[href*="/d2l/lp/manageFiles/main.d2l"]:not(.d2l-menu-item-link)')) {
    const button = getNavButtonCode(
      id,
      'Manage Files',
      '/d2l/lp/manageFiles/main.d2l?ou=',
      '',
      '/d2l/img/0/CMC.Main.actFiles.png?v=10.7.3.7486-189',
      'navbar-manage-files',
    );
    root.append(button);
  }

  if (!document.querySelector('header a[href*="/d2l/lp/cmc/main.d2l"]:not(.d2l-menu-item-link)')) {
    const button = getNavButtonCode(
      id,
      'Unit Admin',
      '/d2l/lp/cmc/main.d2l?ou=',
      '',
      '/d2l/img/0/CMC.Main.tbTools.png?v=10.7.3.7486-189',
      'navbar-unit-admin',
    );
    root.append(button);
  }

  // There is no group list
  if (!document.querySelector('header a[href^="/d2l/lms/group/group_list.d2l?ou="]')) {
    const button = getNavButtonCode(
      id,
      'Groups',
      '/d2l/lms/group/group_list.d2l?ou=',
      '',
      '/d2l/img/0/CMC.Main.actGroups.png?v=10.7.10.10018-151',
      'navbar-groups',
    );

    root.append(button);
  }

  // There is no classlist link
  if (!document.querySelector('header a[href^="/d2l/lms/classlist/classlist.d2l?ou="]')) {
    const button = getNavButtonCode(
      id,
      'Classlist',
      '/d2l/lms/classlist/classlist.d2l?ou=',
      '',
      '/d2l/img/0/CmcLE.CmcLinks.iconClasslist.png?v=20.22.8.18777',
      'navbar-classlist',
    );

    root.append(button);
  }

  const insertAdjacentElements = (el, position, ...elements) => {
    elements.reverse();
    for (const e of elements) {
      el.insertAdjacentElement(position, e);
    }
  };

  const quicklink = (id, title, href, text, addMicon) => {
    const a = document.createElement('a');
    a.setAttribute('data-testid', id);
    a.classList.add('d2l-link');
    a.title = title;
    a.href = href;
    a.innerText = `[${text}]`;

    if (addMicon) {
      a.setAttribute('mm-icon-render', 'icon');
      a.setAttribute('mm-icon-type', 'action-navigation');
      a.setAttribute('mm-icon-offset', '-5:0');
    }

    return a;
  };

  if (items.mylo.enhancements) {
    if (window.location.pathname == '/d2l/lms/quizzing/admin/quizzes_manage.d2l') {
      document.querySelectorAll('[href*="/d2l/lms/quizzing/admin/modify/quiz_newedit_properties.d2l"]').forEach(el => {
        const search = new URL(el.href).search; //window.location.search;
        const name = el.innerText;

        const r = quicklink(
          'quiz-restrictions',
          `Restrictions for ${name}`,
          `/d2l/lms/quizzing/admin/modify/quiz_newedit_restrictions.d2l${search}`,
          'r',
          true,
        );

        const a = quicklink(
          'quiz-assessment',
          `Assessment for ${name}`,
          `/d2l/lms/quizzing/admin/modify/quiz_newedit_assessment.d2l${search}`,
          'a',
        );

        const s = quicklink(
          'quiz-submissions',
          `Submission Views for ${name}`,
          `/d2l/lms/quizzing/admin/modify/quiz_newedit_subviews.d2l${search}`,
          's',
        );

        const g = quicklink('quiz-grade', `Grade ${name}`, `/d2l/lms/quizzing/admin/mark/quiz_mark_users.d2l${search}`, 'g');

        const wrapper = document.createElement('span');
        wrapper.style.marginLeft = '1em';
        wrapper.append(r, a, s, g);

        el.insertAdjacentElement('afterend', wrapper);
      });
    }
  }

  if (items.mylo.overrideGrades) {
    const link = document.querySelector('a[href*="/d2l/lms/grades/index.d2l"]');
    link.href = '/d2l/lms/grades/admin/manage/gradeslist.d2l' + new URL(link.href).search;
  }

  const makeDraftTextItalic = () =>
    document.querySelectorAll('#D2L_LE_Content_TreeBrowser div.d2l-textblock').forEach(block => {
      if (block.textContent == 'Draft') {
        block.style.fontStyle = 'italic';
        block.setAttribute('data-testid', 'italic-draft-text');
      }
    });

  if (document.querySelector('#ContentModuleTree')) {
    let draftObserver = new MutationObserver(makeDraftTextItalic);
    draftObserver.observe(document.querySelector('#ContentModuleTree'), {
      childList: true,
    });
    makeDraftTextItalic();
  }

  function getAkariUnitOutline(code, label) {
    const semester = info.semester;
    const period = semester.substring(2);

    const urlSections = [
      // The base URL for the Akari unit outlines
      'https://outlines.utas.edu.au',

      // The year that this unit is being delivered
      info.year,

      // The first three letters of the unit code (ie. ABC or CXA)
      code.substring(0, 3),

      // The full unit code (ie. ABC123)
      code,

      // The akari path, keyed from the units delivery period (ie. S1, T4, A2 etc).
      // If it doesn't exist, we will use the default 'Other' address.
      akariPeriods[period] ?? akariPeriods['OTHER'],

      // A cachebuster to make sure that the user is always seeing a fresh page.
      `?ts=${Date.now()}`,
    ];

    return {
      label: label ?? code,
      path: urlSections.join('/'),
      icon: icon.internet,
    };
  }

  function getInternalUnitOutline(code, label) {
    const path = UnitData.Path;
    const format = info.formatOutlines;

    return {
      label: label ?? code,
      path: `${path}UnitOutline/${code ? `${code}_` : ''}UnitOutline.${format}`,
      icon: icon[format],
    };
  }

  function getUnitOutlineURLs() {
    // This is a single-unit-outline unit.
    if (info.unitOutlines === 0)
      return [info.useAkariOutlines ? getAkariUnitOutline(info.unitMembers[0]) : getInternalUnitOutline()];
    // This is a unit outline per code unit.
    else if (info.unitOutlines === 1)
      return info.unitMembers.map(code =>
        info.useAkariOutlines ? getAkariUnitOutline(code) : getInternalUnitOutline(code),
      );
    // This is an on-campus/off-campus unit.
    else {
      const getOutline = info.useAkariOutlines ? getAkariUnitOutline : getInternalUnitOutline;
      return [getOutline('OnCampus', 'On Campus'), getOutline('OffCampus', 'Off Campus')];
    }
  }

  /** Returns a single icon link to the Unit Outline. */
  function buildUnitOutlineSingle(entry) {
    const container = document.createElement('div');
    container.classList.add('d2l-navigation-s-item', 'less-rpad');
    container.setAttribute('role', 'listitem');

    container.innerHTML = `
			<a class="d2l-navigation-s-link" target="_blank" href="${entry.path}">
				<span style="width:16px; display:inline-block;">
					<img style="vertical-align:middle;width:20px;" src="/shared/Images/Info_widget/${entry.icon}" title="Unit Outline" alt="Unit Outline">
				</span>
			</a>`;

    return container;
  }

  /** Returns a dropdown with links to multiple Unit Outlines. */
  function buildUnitOutlineDropdown(entries) {
    const dropdown = document.createElement('d2l-dropdown');
    dropdown.innerHTML = `
      <span style="margin-right:8px; display:inline-block; cursor:pointer;" class="d2l-dropdown-opener">
        <img style="vertical-align:middle;width:20px;" src="/shared/Images/Info_widget/${
          entries[0].icon
        }" title="Unit Outline" alt="Unit Outline">
        <d2l-icon icon="d2l-tier1:chevron-down"></d2l-icon>
      </span>
      <d2l-dropdown-content align="end">
        <ul class="unit-outlines">
          ${entries.map(entry => `<li><a href="${entry.path}" target="_blank">${entry.label}</a></li>`).join('')}
        </ul>
      </d2l-dropdown-content>
      <style>
        span.d2l-dropdown-opener:hover d2l-icon {
          color: #005694;
        }
      </style>
    `;

    return dropdown;
  }

  /**
   * Returns a pre-rendered HTML string for a button, for use in the Daylight theme.
   *
   * @param {string} ou The unit code number
   * @param {string} title The hover title of the icon
   * @param {string} lnk1 The link attached before the OU
   * @param {string} lnk2 The link attached after the OU
   * @param {string} ico The icon image link.
   * @returns {string} The final HTML string.
   * @memberof MyLO#
   */
  function getNavButtonCode(ou, title, lnk1, lnk2, ico, testid) {
    const el = document.createElement('div');
    el.classList.add('d2l-navigation-s-item', 'less-rpad');
    el.setAttribute('data-testid', testid);

    const a = document.createElement('a');
    a.classList.add('d2l-navigation-s-link');
    a.href = lnk1 + ou + lnk2;

    const span = document.createElement('span');
    span.style.width = '16px';
    span.style.display = 'inline-block';

    const img = document.createElement('img');
    img.style.verticalAlign = 'middle';
    img.title = title;
    img.alt = title;
    img.src = ico;

    span.append(img);
    a.append(span);
    el.append(a);
    return el;
  }
})();
