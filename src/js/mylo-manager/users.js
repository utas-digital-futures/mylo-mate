/**
 * @affects MyLO Manager
 * @problem When setting up new units we usually need to add the same group of support staff to each delivery, a very time consuming process of searching and adding each user individually.
 * @solution
 *  Add a MyLO Manager user group feature, where a groups of staff and their roles in a unit can be stored.
 *  The user group can then be added to the Staff tab of a MyLO Manager unit with a single click.
 * @target Support staff
 * @impact High
 * @savings 20 minutes per unit managed
 */

(async function () {
  const store = await new Promise(res => chrome.storage.sync.get(null, res));
  const manager = new UserGroups();
  const table = document.querySelector('#grdStaff tbody');
  const quickgroups = document.createElement('fieldset');

  quickgroups.innerHTML = `<legend mm-icon-type="view-data">Add Predefined Group</legend>
  <p role="quickadd-users">To create a predefined group, use the Manage Groups button below.</p>
    <table class="Borderless">
      <tbody>
      <tr>
        <td colspan="2">
          Select a group
        </td>
      </tr>
      <tr>
        <td>
          <select name="quick-usergroups" id="quick-usergroups" style="min-width: 20em;"></select>
        </td>
        <td align="right">
          <a class="fm-button-icon-left ui-corner-all ui-state-default fm-button" id="btnStaffGroupAdd"><span class="ui-icon-arrowthick-1-n ui-icon"></span>Add Group to Unit</a>
          <a class="fm-button-icon-left ui-corner-all ui-state-default fm-button" id="btnStaffUsersAdd"><span class="ui-icon-arrowthick-1-n ui-icon"></span>Add Username List to Unit...</a>
          <a class="fm-button-icon-left ui-corner-all ui-state-default fm-button" id="btnGroupsEdit"><span class="fa-icon fa-cogs"></span>Manage Groups</a>
        </td>
      </tr>
    </tbody>
  </table>`;
  document.querySelector('fieldset.UnitRequestTabPage').append(quickgroups);

  const FillGroups = async () => {
    const select = quickgroups.querySelector('#quick-usergroups');
    const selected = select.value;

    const groups = await manager.get();

    select.disabled = groups.length == 0;
    quickgroups.querySelector('[role="quickadd-users"]').classList[groups.length == 0 ? 'add' : 'remove']('hidden');
    quickgroups.querySelector('#btnStaffGroupAdd').disabled = groups.length == 0;

    const defaultOption = document.createElement('option');
    defaultOption.disabled = true;
    defaultOption.value = -1;
    defaultOption.innerText = 'Select a group..';
    defaultOption.style.color = 'grey';

    const options = groups.map(group => {
      let option = document.createElement('option');
      if (group.Favourite) {
        option.selected = true;
      }
      option.value = group.ID;
      option.innerText = group.Name;
      return option;
    });

    select.innerHTML = '';
    select.append(defaultOption, ...options);
    if (selected) {
      select.selected = selected;
    }
  };

  await FillGroups();
  manager.addEventListener('update', FillGroups);

  const AddUsers = (users, override = store.manager.overrideRoles) => {
    const UnitID = window.location.pathname.split('/').pop();
    const existing = Array.from(table.querySelectorAll('input[name$=".UserName"]')).map(i => i.value);

    return Promise.all(
      users
        .filter(user => override || !existing.includes(user.Username))
        .map(async user => {
          try {
            const body = {
              StaffID: user.StaffId || user.StaffID,
              PreferredName: user.PreferredName,
              Surname: user.Surname,
              Username: user.Username,
              Department: user.Schools,
            };

            const req = await fetch(`/UnitRequest/AddStaff/${UnitID}`, { method: 'POST', body: JSONtoSearchParams(body) });
            const res = await req.text();

            const exists = table.querySelector(`input[name$=".UserName"][value="${user.Username}"]`) != null;
            const row = exists
              ? table.querySelector(`input[name$=".UserName"][value="${user.Username}"]`).closest('tr')
              : document.createElement('tr');
            row.innerHTML = res;

            // Set the MyLO Role
            const role = row.querySelector(`select[name$=".MyLORole"] option[value="${user.Role || 'Support'}"]`);
            if (role) role.selected = true;

            // Set the Echo360 role
            const echo = row.querySelector(`select[name$=".EchoInstructorLevel"] option[value="${user.EchoRole || 0}"]`);
            if (echo) echo.selected = true;

            table.append(row);
          } catch (e) {
            console.error(e);
          }
        })
    );
  };

  const AddUsersFromGroup = async () => {
    const selected = quickgroups.querySelector('#quick-usergroups').value;
    if (selected == '-1') return;

    const group = await manager.get(selected);
    if (!group) {
      console.warn('Something went wrong getting the group.');
      return;
    }

    return AddUsers(group.Users);
  };

  const AddUsersFromUsername = async () => {
    const response = await OptionsUI.promptUserAdd(
      'Add Users',
      'Insert a list of usernames to add, separated by a comma.<br>Example: <pre>username1, username2, username3</pre>',
      'Usernames to add',
      '',
      'Role for these users: ',
      store.manager.overrideRoles
    );

    if (!response) return;

    const usernames = response.users
      .split(',')
      .map(v => v.trim())
      .filter(v => v.length > 0);
    const role = response.role;
    const users = await Promise.all(usernames.map(UserGroups.SearchForUser)).then(users => {
      users.forEach(user => (user.Role = role));
      return users;
    });

    AddUsers(users, response.forceRole);
  };

  document.querySelector('#btnStaffGroupAdd').addEventListener('click', AddUsersFromGroup);
  document.querySelector('#btnStaffUsersAdd').addEventListener('click', AddUsersFromUsername);
  document
    .querySelector('#btnGroupsEdit')
    .addEventListener('click', () => chrome.runtime.sendMessage({ open: 'options', data: { tab: 'user-groups' } }));
})();
