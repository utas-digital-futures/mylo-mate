(async function() {
  // Runs the search action when enter is pressed in these fields.
  const search = e => {
    if (e.keyCode === 13) {
      document.querySelector('#btnDialogSearch').dispatchEvent(new Event('click'));
    }
  };

  document.querySelector('#txtDialogUserName').addEventListener('keyup', search);
  document.querySelector('#txtDialogStaffId').addEventListener('keyup', search);
  document.querySelector('#txtDialogSurname').addEventListener('keyup', search);
  document.querySelector('#txtDialogGivenName').addEventListener('keyup', search);

  document.querySelector('#staffSearchDialog').parentNode.id = 'staffSearchDialogContainer';

  document.addEventListener('GridReady', () => {
    if (document.querySelectorAll('#select-all').length === 0) {
      const header = document.querySelector('#jqgh_grdDialogResults_Select');
      header.innerHTML = '<input type="checkbox" id="select-all"> ' + header.innerHTML.replace('Select', '');
    }

    document.querySelector('#select-all').addEventListener('click', e => {
      e.stopPropagation ? e.stopPropagation() : (e.cancelBubble = true);
      document
        .querySelector('#grdDialogResults')
        .querySelectorAll('tr:not(:first-child)')
        .forEach(row => (row.querySelector('input[type="checkbox"]').checked = e.target.checked));
    });

    document
      .querySelector('#grdDialogResults')
      .querySelectorAll('tr:not(:first-child) input[type="checkbox"]')
      .forEach(checkbox => {
        checkbox.addEventListener('click', e => {
          e.stopPropagation ? e.stopPropagation() : (e.cancelBubble = true);
          if (!e.target.checked) document.querySelector('#select-all').checked = false;
        });
      });
  });
})();
