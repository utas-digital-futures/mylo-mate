(async () => {
  const unitType = document.querySelector('input[name="UnitType"]').value;
  if (unitType != 'Award') return;

  const fieldset = document.querySelector('.UnitRequestTabPage');
  const message = document.createElement('p');

  const WarningIconColor = 'darkorange';
  const WarningMessages = {
    Echo360: `an Echo360 <strong>Instructor</strong> if known, and an Echo360 space is required for this unit`,
    UnitCoordinator: `a <strong>Unit Coordinator</strong> role`,
  };

  fieldset.prepend(message);

  const GenerateMessage = () => {
    const echo360users = Array.from(document.querySelectorAll('select[name$=".EchoInstructorLevel"]'))
      .map(v => v.value)
      .filter(v => v == '2');
    const unitCoordinators = Array.from(document.querySelectorAll('select[name$=".MyLORole"]'))
      .map(v => v.value)
      .filter(v => v == 'UnitCoordinator');

    const parts = [];

    if (unitCoordinators.length == 0) parts.push(WarningMessages.UnitCoordinator);
    if (echo360users.length == 0) parts.push(WarningMessages.Echo360);

    const msg =
      unitCoordinators.length > 0 && echo360users.length > 0
        ? '&nbsp;'
        : `
  <span style="color:${WarningIconColor};" class="fas fa-exclamation-triangle"></span>
  Please select ${parts.join(' and ')}. This information can be added later.`;
    message.innerHTML = msg;
  };

  fieldset.querySelector('#grdStaff').addEventListener('change', GenerateMessage);
  GenerateMessage();
})();
