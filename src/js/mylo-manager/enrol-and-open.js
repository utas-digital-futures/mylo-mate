/**
 * @affects MyLO Manager
 * @problem With the Faculty Support role in MyLO Manager you can enrol yourself with a few clicks and a long wait and page refresh.
 * @solution Add an `Enrol and open` link in MyLO Manager that runs the enrol procedure and opens the unit in a new tab.
 * @target Support staff
 * @impact Low
 */

(async function () {
  $('#grdMyUnits').on('jqGridLoadComplete', () => {
    const rows = document.querySelectorAll(
      'td[aria-describedby="grdMyUnits_Status"][title="Active"]+td[aria-describedby="grdMyUnits_MyRole"][title="faculty support" i]'
    );

    rows.forEach(td => {
      const row = td.closest('tr');
      const id = row.getAttribute('id');

      const link = document.createElement('a');
      link.setAttribute('data-unit-id', id);
      link.classList.add('enrol-and-open');
      link.title = 'Enrol as Support and open unit page';
      link.innerText = '[enrol and open]';
      td.append(link);
    });
  });

  document.addEventListener('click', async e => {
    if (e.target.matches('a.enrol-and-open')) {
      const id = e.target.getAttribute('data-unit-id');
      if (id) {
        let pendingText = document.createElement('span');
        pendingText.className = e.target.className;
        pendingText.innerText = 'Processing...';
        e.target.replaceWith(pendingText);

        const orgid = document.querySelector(`tr[id="${id}"] td[aria-describedby="grdMyUnits_OrgUnitID"]`).title;
        const data = new URLSearchParams();
        data.append('Action', 'Enrol');
        data.append('RequestID', id);
        await fetch('/Home/ProcessEnrolUnenrol', { method: 'POST', body: data });
        window.open(`https://mylo.utas.edu.au/d2l/home/${orgid}`, '_blank');
        $('#grdMyUnits').trigger('reloadGrid');
        // Pending Text should remove itself when the grid reloads.
        // Maybe it's worth keeping it there until that happens, so that users know what's happening.
        // Otherwise, if we remove it early, it'll show "Faculty Support" still while implying we're done.
        // Which, technically is true, but it's a poor end experience.

        // pendingText.remove();
      }
    }
  });
})();
