/**
 * @affects MyLO Manager
 * @problem MyLO uses colour to convey meaning in certain areas (e.g. unit status). This information can be missed by staff who have certain visual impairments.
 * @solution Add an optional accessibility mode which overrides the default colours to make them more accessible.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  const store = await FetchMMSettings();
  if (store.other.makeAccessible && !document.body.classList.contains('mm-access-friendly'))
    document.body.classList.add('mm-access-friendly');
})();
