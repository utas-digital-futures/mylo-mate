(async () => {
  // Prevents the script from being run more than once.
  if (window.guardactive) return;
  window.guardactive = true;

  // Find the "Department" dropdown elements
  var options = document.querySelectorAll('#cboDialogDepartment option');
  var maxLen = 80;
  options.forEach(o => {
    const text = o.innerText.trim();
    // For each dropdown element, check the length of the text.
    //	If the text is longer than our predefined max-length (80), take the first (maxlength/2)-3 letters off,
    //	add an ellipses (...), and attach the last (maxlength/2)-1 letters from the end.
    //
    //	In order to maintain integrity of data, the original title remains accessible via a tooltip upon hover (using 'title').
    o.title = text;
    if (text.length > maxLen) {
      o.innerText = `${text.substr(0, maxLen / 2 - 3)}...${text.substr((maxLen / 2) * -1)}`;
    }
  });

  // Hijacking a little space here for user-list comms.
  document.addEventListener('USERCONTENT_RPC', e => {
    const d = e.detail;
    if (d.flag === 'GET') {
      let p = [];
      const users = $('#grdDialogResults').jqGrid().getDataIDs();
      for (let user of users) {
        const r = $('#grdDialogResults').jqGrid().getRowData(user);
        r.checked = document.querySelector(`#chkSelect${user}`).checked;
        p.push(r);
      }

      document.dispatchEvent(
        new CustomEvent('USERGROUPS_RPC', {
          detail: {
            payload: p,
            ts: d.ts,
          },
        })
      );
    } else if (d.flag === 'SET') {
      $('#grdDialogResults').jqGrid().clearGridData();
      d.payload.forEach(data => {
        $('#grdDialogResults').addRowData(data.Username, {
          PreferredName: data.PreferredName,
          Surname: data.Surname,
          Username: data.Username,
          StaffID: data.StaffID,
          Schools: data.Schools,
          Select: `<input type="checkbox" checked="checked" class="ui-corner-all" id="chkSelect${data.Username}"/>`,
        });
      });
    }
  });

  document.querySelector('#grdStaff').classList.add('sortable');

  $('#grdDialogResults').on('jqGridGridComplete', function () {
    document.dispatchEvent(new CustomEvent('GridReady'));
  });
})();
