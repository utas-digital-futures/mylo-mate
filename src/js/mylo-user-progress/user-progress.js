(async function() {   
	
	const affectedElements = ["ProvidersSettingsData$ChecklistParam","ProvidersSettingsData`4$ChecklistParam","ProvidersSettingsData`7$ChecklistParam",
    "ProgressReportInformationSettingsData$DisplayBoxPlotsChecklistParam","ProgressReportInformationSettingsData$DisplayFinalGradeRangeChecklistParam"];
    const actionButton = document.createElement('button');
	actionButton.classList.add('d2l-button');
	actionButton.innerHTML = "Apply Common Hide Settings and Save";
	actionButton.style.float = "right";
	actionButton.setAttribute('mm-icon-type', 'action-select');
	actionButton.setAttribute('mm-icon-offset', '-6:0');
	
	const dialogIcon = document.createElement('d2l-icon');
	dialogIcon.setAttribute('icon', 'd2l-tier1:help');
	dialogIcon.classList.add('mm-more-info-icon');
	dialogIcon.title = "More information";

	let style = document.createElement('style');
	style.innerHTML = `
	.mm-more-info-icon { float: right; margin-top: 10px; cursor: pointer; }
	.mm-more-info-icon:hover { color:black; }`;
	document.head.appendChild(style);

	let checked_state = true;
	let all_unchecked = affectedElements.map(c => document.querySelector(`input[type="checkbox"][id="${c}"]`)).filter(cb => cb.checked).length == 0;
	if(all_unchecked) { actionButton.innerHTML = 'Retract Common Hide Settings and Save'; checked_state = false; }

	actionButton.addEventListener('click', () => {
		affectedElements.forEach(c => { let cb = document.querySelector(`input[type="checkbox"][id="${c}"]`); if (cb.checked == checked_state) cb.click(); });
		document.querySelector('d2l-floating-buttons button[primary]').click();
	}); 
	
	actionButton.addEventListener('mouseenter', e => {
		affectedElements.forEach(c => document.querySelector(`input[type="checkbox"][id="${c}"]`).parentElement.classList.add('MM_highlight'));
	})
	
	actionButton.addEventListener('mouseleave', e => {
		affectedElements.forEach(c => document.querySelector(`input[type="checkbox"][id="${c}"]`).parentElement.classList.remove('MM_highlight'));
	})
	
	document.querySelector('div.d2l-page-main h1').appendChild(dialogIcon);
	document.querySelector('div.d2l-page-main h1').appendChild(actionButton);

	const ajax = (url) => new Promise((res, rej) => { let xhr = new XMLHttpRequest(); xhr.open('GET', url); xhr.addEventListener('loadend', e => { if(xhr.statusText == 'OK') res(xhr.response); else { rej([url,xhr,e]); } }); xhr.send(); });

	let dialog = document.createElement('dialog');
	dialog.innerHTML = await ajax(chrome.runtime.getURL('js/mylo-user-progress/modal-content.html'));
	
	let close = document.createElement('span');
	close.innerText = 'x';
	close.classList.add('dialog-close');
	close.title = 'Close dialog';
	close.addEventListener('click', e => dialog.close());

	dialog.appendChild(close);
	document.body.appendChild(dialog);

	dialogIcon.addEventListener('click', (e) => { dialog.showModal(); });
})();