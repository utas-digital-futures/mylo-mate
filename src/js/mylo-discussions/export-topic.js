/**
 * @affects Discussions
 * @problem There is no easy way to extract discussion topic content for use outside MyLO.
 * @solution Add a button to the top of each discussion topic which saves all posts to a spreadsheet.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  const store = await new Promise(res => chrome.storage.sync.get(null, res));
  if (!store.mylo.ShowDiscussionPostExporter) return;

  await until(() => window.top.document.body.getAttribute('mm-abort') != null);
  if (window.top.document.body.getAttribute('mm-abort') == 'true') return;
  let header_row = document.querySelector('.d2l-grid-actions-container d2l-overflow-group');
  if (!header_row) return;

  let loader = document.createElement('mm-loader');
  loader.setAttribute('indeterminate', true);
  loader.setAttribute('width', 20);
  loader.style.margin = '0px -10px -5px 10px';
  loader.style.display = 'inline-block';
  loader.setAttribute('hidden', true);

  let export_posts = document.createElement('d2l-button-subtle');
  export_posts.setAttribute('data-testid', 'export-topic');
  export_posts.setAttribute('icon', 'tier1:download');
  export_posts.setAttribute('text', 'Export Posts');
  export_posts.setAttribute('title', 'Export all posts in this topic to a spreadsheet');
  export_posts.setAttribute('mm-icon-type', 'action-button');
  export_posts.setAttribute('mm-icon-offset', '6:6');
  export_posts.setAttribute('disabled', true);
  export_posts.append(loader);

  header_row.append(export_posts);

  let { ou, tid, fid } = window.location.searchParams;
  if (!fid || fid === '0') {
    fid = await (async () => {
      if (!window.location.searchParams['mm-forum-id']) {
        const forums = await api(`/d2l/api/le/1.28/${ou}/discussions/forums/`);
        const topics = await Promise.all(
          forums.map(async forum => {
            const t = await api(`/d2l/api/le/1.28/${ou}/discussions/forums/${forum.ForumId}/topics/`);
            return t ? t.find(topic => topic.TopicId == tid) : null;
          })
        );
        const f = topics.filter(t => t != null)[0];
        return f ? f.ForumId : null;
      }
      return window.location.searchParams['mm-forum-id'];
    })();
  }

  const exportTopic = async () => {
    try {
      export_posts.setAttribute('disabled', true);
      loader.removeAttribute('hidden');

      let headings = ['Posting User', 'Subject', 'Message', 'Date Posted', 'Deleted', 'Reply Count'];
      let data = [];

      const [unit, topic, posts] = await Promise.all([
        api(`/d2l/api/lp/1.25/enrollments/myenrollments/${ou}`, { first: true, fresh: true }),
        api(`/d2l/api/le/1.41/${ou}/discussions/forums/${fid}/topics/${tid}`, { first: true, fresh: true }),
        api(`/d2l/api/le/1.41/${ou}/discussions/forums/${fid}/topics/${tid}/posts/`, { fresh: true }),
      ]);

      const toPlainText = text => {
        let container = document.createElement('div');
        container.innerHTML = text;
        return container.innerText.replace(/"/gi, '""');
      };

      posts.forEach(post => {
        data.push([
          post.PostingUserDisplayName,
          toPlainText(post.Subject),
          toPlainText(post.Message.Html),
          post.DatePosted,
          post.IsDeleted ? 'Yes' : 'No',
          post.ReplyPostIds.length,
        ]);
      });

      let headers = `data:text/csv;charset=utf-8,${headings.join(',')}\r\n`;
      let body = data.map(row => row.map(v => `"${v}"`).join(',')).join('\r\n');

      // console.log(body);

      const filename = `Topic Export - ${unit.OrgUnit.Name} -- ${topic.Name}.csv`;

      let link = document.createElement('a');
      link.setAttribute('href', encodeURI(headers + body));
      link.setAttribute('download', filename);
      link.click();
    } catch (e) {
      console.error(e);
      alert('Something went wrong exporting the posts.');
    } finally {
      loader.setAttribute('hidden', true);
      export_posts.removeAttribute('disabled');
    }
  };

  export_posts.addEventListener('click', exportTopic);
  export_posts.removeAttribute('disabled');
})();
