/**
 * @affects Discussions
 * @problem Restoring deleted posts is a tedious process that can only be carried out one by one.
 * @solution Add a button that allows multiple deleted posts to be selected and restored in bulk.
 * @target Academic and support staff
 * @impact Low
 */

(async function () {
  await until(() => window.top.document.body.getAttribute('mm-abort') != null);
  if (window.top.document.body.getAttribute('mm-abort') == 'true') return;

  const timer = time => new Promise(res => setTimeout(res, time));

  async function undeletePosts() {
    let checked = Array.from(document.querySelectorAll('input[type="checkbox"][name="GRID_messages_cb"]:checked'))
      .filter(cb => cb.parentNode.parentNode.querySelector('input[name^="hidden_isdeleted_"]').value == 'True')
      .map(cb => {
        let parts = cb.parentNode.parentNode.querySelector('a[onclick*="SelectPost("]').id.split('_');
        return {
          topicId: parts[1],
          postId: parts[2],
        };
      });

    if (checked.length == 0) {
      alert('No deleted posts were selected.');
      return;
    }

    restore_messages.setAttribute('disabled', true);

    await Promise.all(
      checked.map(
        ({ topicId, postId }) =>
          new Promise((res, rej) => {
            let frame = document.createElement('iframe');
            frame.style.position = 'absolute';
            frame.style.left = '-10000000px';
            frame.addEventListener('load', async () => {
              frame.contentWindow.UndeletePost(postId, topicId);
              await timer(1000);
              frame.remove();
              res();
            });
            frame.src = `/d2l/lms/discussions/messageLists/message_preview.d2l?ou=${window.location.searchParams.ou}&postId=${postId}&topicId=${topicId}&isPopup=1&actId=0&viewIsPoppedOut=True&d2l_body_type=5`;
            document.body.appendChild(frame);
          })
      )
    );

    restore_messages.removeAttribute('disabled');
  }

  const delete_button = document.querySelector(
    '.d2l-grid-actions-container d2l-overflow-group d2l-button-subtle[text="Delete"]'
  );

  const restore_messages = document.createElement('d2l-button-subtle');
  restore_messages.setAttribute('data-testid', 'restore-deleted-posts');
  restore_messages.setAttribute('icon', 'd2l-tier1:unsaved');
  restore_messages.setAttribute('text', 'Restore');
  restore_messages.setAttribute('title', 'Restore selected deleted posts');
  restore_messages.setAttribute('mm-icon-type', 'action-button');
  restore_messages.setAttribute('mm-icon-offset', '6:6');
  restore_messages.addEventListener('click', undeletePosts);

  delete_button?.insertAdjacentElement('afterend', restore_messages);
})();
