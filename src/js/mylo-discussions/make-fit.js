(function () {

	/*
	 * This is currently under development. It will be uncommented in the commit when it is at a stage that it won't actively crash the extension and/or page.
	 *  -- Connor
	 */

	if (window.MakeFit) return;
	window.MakeFit = true;

	function val(strings) {
		let v = {};
		let props = strings[0].replace(/(\([^]*?\))/g, (match) => { let i = Object.keys(v).length; v['Object' + i] = match; return `(Object${i})`; });

		let values = props.split('.');
		let r = values.map((p, i, a) => {
			if (i == 0) return p;
			else {
				let l = [];
				for (let j = 0; j < i; j++) { l.push(a[j].replace(/\(Object([0-9]+)\)/g, (match, p1) => v['Object' + p1])); }
				l.push(p.replace(/\(Object([0-9]+)\)/g, (match, p1) => v['Object' + p1]));
				return l.join('.');
			}
		});

		let lastValue = null;
		for (let i = 0; i < r.length; i++) {
			let prop = r[i];
			let f = Function('"use strict";return(' + prop + ')')();
			let o = typeof f == 'function' ? f() : f;
			if (o == undefined || o == null) {
				lastValue = null;
				break;
			}
			else lastValue = o;
		}
		return lastValue;
	}

	if (document.querySelector('.d2l-max-width')) document.querySelector('.d2l-max-width').classList.remove('d2l-max-width');
	document.querySelectorAll('d2l-sticky-element.d2l-scroll-wrapper').forEach(node => node.remove());

	// if (val`document.querySelector('iframe').contentDocument.querySelector('frame#FRAME_right').contentDocument.querySelector('frame#FRAME_list').contentDocument` != null) {
	// 	document.querySelector('iframe').contentDocument.querySelector('frame#FRAME_right').contentDocument.querySelector('frame#FRAME_list').contentDocument.querySelector('#wrapper').style.overflow = 'visible';
	// }
})();