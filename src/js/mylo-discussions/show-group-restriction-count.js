(async () => {
  const ou = window.location.href.split('/discussions/')[0].split('/').pop();

  const rows = Array.from(document.querySelectorAll('div[id^="topicDetailsPlaceholderId"]'));
  const hasRestrictions = rows
    .filter(row => /section restrictions/i.test(row.innerText))
    .map(row => {
      const details = row.children[0].attributes[0];
      const fid = details.name.split('-')[2];
      const tid = details.value;

      return { row, fid, tid };
    });

  const groupcategories = await api(`/d2l/api/lp/1.38/${ou}/groupcategories/`);
  hasRestrictions.forEach(async ({ row, fid, tid }) => {
    const topic = await api(`/d2l/api/le/1.61/${ou}/discussions/forums/${fid}/topics/${tid}`, { fresh: true, first: true });
    // const restrictions = await api(`/d2l/api/le/1.61/${ou}/discussions/forums/${fid}/topics/${tid}/groupRestrictions/`, {
    //   fresh: true,
    // });

    //const count = restrictions.length;
    const associatedGroup = groupcategories.find(cat => cat.GroupCategoryId == topic.GroupTypeId);

    const label = row.querySelector('span[title*="group or section topic" i]');
    //label.innerText = `Group/section restrictions (restricted to ${count} group${count === 1 ? '' : 's'}).`;
    label.innerText = `Group/section restrictions (restricted to "${associatedGroup.Name}").`;
    label.title = `${label.title}\nRestricted to "${associatedGroup.Name}"`;

    //if (count > 0) {
    // const groups = await Promise.all(
    //   restrictions.map(async res => {
    //     const category = groupcategories.find(cat => cat.Groups.includes(res.GroupId));
    //     const group = await api(
    //       `/d2l/api/lp/1.38/${ou}/groupcategories/${category.GroupCategoryId}/groups/${res.GroupId}`,
    //       { first: true }
    //     );

    //     return group.Name;
    //   })
    // );

    //       label.title = `${label.title}
    // Limited to following groups:
    // ${groups.map(name => ` - ${name}`).join('\n')}`;
    //}
  });
})();
