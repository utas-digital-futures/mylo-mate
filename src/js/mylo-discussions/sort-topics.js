(async () => {
  const sortTopics = e => {
    const id = e.target.dataset.forum;
    const sels = Array.from(document.querySelectorAll(`select[name="Topics${id}_n"]`));

    sels
      .map(select => {
        const title = select.closest('tr').querySelector('th').innerText;
        return { title, select };
      })
      .sort((a, b) => a.title.localeCompare(b.title))
      .forEach(({ select }, index) => {
        select.options[index].selected = true;
      });
  };

  document.querySelectorAll('select[name="Forums_n"]').forEach(select => {
    const heading = select.closest('tr').querySelector('th');
    const id = select.options[0].value.substr(3);
    const sorter = document.createElement('d2l-button-icon');
    sorter.title = 'Sort topics alphabetically';
    sorter.dataset.forum = id;
    sorter.onclick = sortTopics;
    sorter.setAttribute('icon', 'tier1:sort-type');

    const sels = document.querySelectorAll(`select[name="Topics${id}_n"]`);
    if (sels.length < 2) return;

    heading.append(sorter);
  });
})();
