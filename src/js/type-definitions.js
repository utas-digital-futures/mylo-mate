/** @namespace Types */

/**
 * The object wrapper for downloads from Echo360.
 * @typedef {Object} Types#VideoDownloads
 * @property {string} unit The unit ID
 * @property {string} offering The current offering of this unit (e.g. 2017_S1-ALL)
 * @property {array} videos The array of videos to download. Stored in `[{title:'', url: ''}]` format.
 */

/**
 * The object wrapper for holding tab data. 
 * @typedef {Object} Types#TabData
 * @property {string} id The ID of the element.
 * @property {array} urls An array of URLs to match against.
 * @property {array} blacklist An array of URLs to never match against.
 * @property {array} injected An array of scripts to inject into the DOM.
 * @property {array} content An array of scripts to run as content scripts.
 */

 /**
 * The object wrapper for holding tab data. 
 * @typedef {Object} Types#User
 * @property {string} name The users preferred name
 * @property {string} surname The users surname
 * @property {string} id The users Staff ID
 * @property {string} user The users username
 * @property {string} schools The schools that the user is a part of
 * @property {string} role The users default role
 */

