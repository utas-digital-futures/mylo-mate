/**
 * @affects Groups
 * @problem Renaming groups one at a time is a very time-consuming, tedious process.
 * @solution
 *  Introduce automation to group renaming, offering a prefix, suffix and replace functions (with optional counters).
 *  These features allow staff to accomplish the task in 30 seconds, instead of half a day.
 * @target Academic and support staff
 * @impact High
 * @savings 5 minutes per group
 */

(async function () {
  if (window.BulkEditToolsCreated) return;
  window.BulkEditToolsCreated = true;

  const button = ({ text, icon, title = '', parent = null, classes = [], testId = '' }) => {
    let btn = document.createElement('d2l-button-subtle');
    btn.title = title;
    btn.setAttribute('data-testid', testId);
    btn.setAttribute('icon', icon);
    btn.setAttribute('text', text);
    btn.setAttribute('mm-icon-type', 'action-button');
    btn.setAttribute('mm-icon-offset', '6:6');
    btn.classList.add(...classes);
    parent.appendChild(btn);
    return btn;
  };

  const noRowsSelected = `No rows selected. Please select at least 1 row to use this tool.`;

  if (!document.querySelector('[text="Delete"]')) return;
  const parent = document.querySelector('[text="Delete"]').parentElement;
  // D2L edit btn.
  let bulk_edit = button({
    text: 'Bulk Edit',
    testId: 'bulk-edit',
    title: 'Bulk edit group names',
    icon: 'd2l-tier1:edit-bulk',
    parent,
    classes: ['mm-btn'],
  });

  let prefix = button({
    text: 'Prefix Selected',
    testId: 'prefix-text',
    title: 'Prefix the selected group names with text',
    icon: 'd2l-tier1:arrow-thin-left',
    parent,
    classes: ['mm-btn', 'mm-hidden', 'edit-only'],
  });

  let suffix = button({
    text: 'Suffix Selected',
    testId: 'suffix-text',
    title: 'Append text to the names of each of the selected groups',
    icon: 'd2l-tier1:arrow-thin-right',
    parent,
    classes: ['mm-btn', 'mm-hidden', 'edit-only'],
  });

  let replace = button({
    text: 'Replace Selected',
    testId: 'replace-text',
    title: 'Replace the selected group names with text',
    icon: 'd2l-tier1:role-switch',
    parent,
    classes: ['mm-btn', 'mm-hidden', 'edit-only'],
  });

  // Adds the floating save/cancel bar
  let save_bar = document.createElement('d2l-floating-buttons');
  save_bar.setAttribute('data-testid', 'group-save-button-bar');
  document.querySelector('.d2l-grid-container').appendChild(save_bar);

  let save_btn = document.createElement('button');
  save_btn.setAttribute('data-testid', 'save-group-rename');
  save_btn.innerHTML =
    '<mm-loader hidden width="18" style="margin:0;" background="#006fbf"></mm-loader><span class="text">Save</span>';
  save_btn.classList.add('d2l-button');
  save_btn.setAttribute('primary', true);

  let cancel_btn = document.createElement('button');
  cancel_btn.setAttribute('data-testid', 'cancel-group-rename');
  cancel_btn.innerText = 'Cancel';
  cancel_btn.classList.add('d2l-button');

  let progress = document.createElement('span');
  progress.classList.add('progress-marker');

  save_bar.setAttribute('mm-visible', false);
  save_bar.appendChild(save_btn);
  save_bar.appendChild(cancel_btn);
  save_bar.appendChild(progress);

  save_btn._saving = false;
  save_btn.saving = val => {
    if (val == null) return save_btn._saving;
    else {
      save_btn._saving = val;
      const s = save_btn;
      s.querySelector('mm-loader')[!val ? 'setAttribute' : 'removeAttribute']('hidden', true);
      s.querySelector('.text')[val ? 'setAttribute' : 'removeAttribute']('hidden', true);
    }
  };

  // Adds the input fields, ands hides them by default.
  let items = [];
  document.querySelectorAll('a[href^="group_edit.d2l?ou="]').forEach(link => {
    let input = document.createElement('input');
    input.className = 'd_edt vui-input d2l-edit';
    input.style.width = '500px';
    input.value = link.innerText;
    input.setAttribute('mm-visible', false);
    link.setAttribute('mm-visible', true);
    link.parentNode.appendChild(input);
    items.push({
      link,
      input,
    });
  });

  // The bail-out function, that resets visibilities. This is used when clicking 'Cancel', or when saving without any changes made
  const cancel = () => {
    items.forEach(({ link, input }) => {
      link.setAttribute('mm-visible', true);
      // link.parentNode.parentNode.querySelector( 'input[type="checkbox"]' )
      // 	.disabled = false;
      input.setAttribute('mm-visible', false);
      input.disabled = false;
    });
    save_bar.setAttribute('mm-visible', false);
    document.body.classList.remove('mm-group-edit');
  };

  // Toggles the visibility of the input fields and links when the bulk edit button is clicked.
  bulk_edit.addEventListener('click', e => {
    if (items[0].link.getAttribute('mm-visible') == 'true') {
      items.forEach(({ link, input }) => {
        input.value = link.innerText;

        link.setAttribute('mm-visible', false);
        // link.parentNode.parentNode.querySelector( 'input[type="checkbox"]' )
        // 	.disabled = true;
        input.setAttribute('mm-visible', true);
      });
      save_bar.setAttribute('mm-visible', true);
      document.body.classList.add('mm-group-edit');
    }
  });

  // const asyncForEach = async (array, callback) => {
  //   let res = [];
  //   for (let i = 0; i < array.length; i++) {
  //     res.push(await callback(array[i], i, array));
  //   }
  //   return res;
  // };
  // const changesPerCycle = 2;

  let itemsUpdated = 0;
  let itemsTotal = 0;
  const updateProgress = () => {
    itemsUpdated += 1;
    progress.innerText = `${itemsUpdated} of ${itemsTotal} updated...`;
  };

  const clearProgress = () => (progress.innerText = ``);

  // When 'Save' is clicked
  save_btn.addEventListener('click', async e => {
    let changed = items.filter(({ link, input }) => input.value != link.innerText);
    if (changed.length == 0) cancel();
    else {
      items.forEach(({ link, input }) => (input.disabled = true));
      save_btn.saving(true);
      save_btn.disabled = true;
      cancel_btn.disabled = true;

      itemsUpdated = -1;
      itemsTotal = changed.length;
      updateProgress();

      const categoryId = window.location.searchParams.categoryId || document.querySelector('select[name="category"]').value;

      await Promise.all(
        changed.map(async ({ link, input }) => {
          const dom = new DOMParser().parseFromString(await fetch(link.href).then(d => d.text()), 'text/html');
          const code = dom.querySelector('input[name="code"]').value;

          let body = new FormData();
          body.append('d2l_action', 'Update');
          body.append('d2l_actionparam', '');
          body.append('d2l_hitCode', window.localStorage.getItem('XSRF.HitCodeSeed') + ((new Date().getTime() + 1e8) % 1e8));
          body.append('categoryId', categoryId);
          body.append('d2l_rf', '');
          body.append('name', input.value);
          body.append('d2l_referrer', window.localStorage.getItem('XSRF.Token'));
          body.append('code', code);

          await fetch(dom.querySelector('form').action, { method: 'post', body });
          updateProgress();
        })
      );

      const { ou } = window.location.searchParams;
      const groups = await api(`/d2l/api/lp/1.25/${ou}/groupcategories/${categoryId}/groups/`, { fresh: true });

      items.forEach(({ link, input }) => {
        const groupId = new URL(link.href).searchParams.get('groupId');
        const group = groups.find(group => group.GroupId == groupId);
        link.innerText = group.Name;
        input.value = group.Name;

        link.setAttribute('mm-visible', true);
        link.parentNode.parentNode.querySelector('input[type="checkbox"]').disabled = false;
        input.setAttribute('mm-visible', false);
        input.disabled = false;
      });

      save_btn.disabled = false;
      cancel_btn.disabled = false;

      save_btn.saving(false);
      save_bar.setAttribute('mm-visible', false);
      document.body.classList.remove('mm-group-edit');
      clearProgress();
    }
  });

  // When 'Cancel' is clicked
  cancel_btn.addEventListener('click', cancel);

  const getCheckedRows = () =>
    items.filter(({ link }) => link.parentNode.parentNode.querySelector('input[type="checkbox"]').checked);

  const pad = (v, total) => (v + '').padStart((total + '').length, '0');

  const numberPattern = /\[#\]/g;

  prefix.addEventListener('click', () => {
    let checked = getCheckedRows();
    if (checked.length == 0) {
      alert(noRowsSelected);
      return;
    }

    let val = prompt(
      'Text to prefix the titles of the selected groups with. Use [#] to indicate the selection number, e.g. Group [#]'
    );

    checked.forEach((item, i) => {
      let v = val.replace(numberPattern, pad(i + 1, checked.length));
      item.input.value = v + item.input.value;
    });
  });

  suffix.addEventListener('click', () => {
    let checked = getCheckedRows();
    if (checked.length == 0) {
      alert(noRowsSelected);
      return;
    }

    let val = prompt(
      'Text to append to the titles of the selected groups. Use [#] to indicate the selection number, e.g. Group [#]'
    );

    checked.forEach((item, i) => {
      let v = val.replace(numberPattern, pad(i + 1, checked.length));
      item.input.value += v;
    });
  });

  replace.addEventListener('click', () => {
    let checked = getCheckedRows();
    if (checked.length == 0) {
      alert(noRowsSelected);
      return;
    }

    let val = prompt(
      'Text to replace the titles of the selected groups with. Use [#] to indicate the selection number, e.g. Group [#]'
    );

    checked.forEach((item, i) => {
      let v = val.replace(numberPattern, pad(i + 1, checked.length));
      item.input.value = v;
    });
  });
})();
