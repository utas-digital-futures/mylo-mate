( async function() {
	if( window.location.searchParams[ 'mm-rename' ] ) {
		let newName = decodeURIComponent( window.location.searchParams[ 'mm-rename' ] );
		document.querySelector( 'input[name="name"]' )
			.value = newName;
		const click = () => document.querySelector( 'd2l-floating-buttons button[primary]' )
			.click();

		click();
		setInterval( click, 2000 );
	}
} )();