class APIErrorResponse extends Error {}

(async function () {
  const store = await new Promise(res => chrome.storage.sync.get(null, res));
  if (!store.other.AdvancedGroupEnhancements) return;

  const id = window.location.searchParams.ou;
  const root = document.querySelector('a[href*="category_newedit.d2l"]');
  if (!root) return;

  const category = new URL(root.href).searchParams.get('categoryId');

  const button = ({ text, icon, title = '', parent = null, classes = [], loader = false, testId = '' }) => {
    let btn = document.createElement('d2l-button-subtle');
    btn.title = title;
    btn.setAttribute('icon', icon);
    btn.setAttribute('text', text);
    btn.setAttribute('data-testid', testId);
    btn.setAttribute('mm-icon-type', 'action-button');
    btn.setAttribute('mm-icon-offset', '6:6');
    btn.classList.add(...classes);
    if (loader) {
      let l = document.createElement('mm-loader');
      l.setAttribute('width', 20);
      btn.appendChild(l);
    }
    parent.appendChild(btn);
    return btn;
  };

  const checkbox = (text, title, checked = false) => {
    const label = document.createElement('label');

    const span = document.createElement('span');
    span.innerText = text;
    if (title) span.title = title;

    const cb = document.createElement('input');
    cb.type = 'checkbox';
    cb.checked = checked;

    label.append(cb, span);
    return { label, text: span, checkbox: cb };
  };

  const radio = options => {
    const id = Date.now();
    const wrapper = document.createElement('div');
    wrapper.classList.add('radio-collection');
    options.forEach(({ text, title, selected, value }) => {
      const label = document.createElement('label');

      const span = document.createElement('span');
      span.innerText = text;
      if (title) span.title = title;

      const cb = document.createElement('input');
      cb.type = 'radio';
      cb.checked = selected || false;
      cb.name = id;
      cb.value = value;

      label.append(cb, span);
      wrapper.append(label);
    });
    return {
      wrapper,
      checked() {
        return (wrapper.querySelector('input:checked') || { value: null }).value;
      },
    };
  };

  /** Shuffle an array of data. */
  function shuffle(data) {
    let a = [].concat(data);
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  if (!document.querySelector('[text="Delete"]')) return;
  const parent = document.querySelector('[text="Delete"]').parentElement;

  // D2L edit btn.
  let split = button({
    text: 'Split Group',
    title: 'Split a group into two or more new groups',
    icon: 'tier1:group',
    parent,
    classes: ['mm-btn', 'mm-inc-loader'],
    testId: 'split-groups',
  });
  split.toggleAttribute('disabled', true);

  const loader = document.createElement('mm-fullscreen-loading-modal');
  loader.setAttribute('text', 'Loading...');
  loader.setAttribute('indeterminate', true);
  loader.setAttribute('preserve', true);
  document.body.append(loader);

  const container = document.querySelector('.d2l-grid-container table[type="list"]');
  container.addEventListener('change', e => {
    split.toggleAttribute('disabled', container.querySelectorAll('input[name="gridGroups_cb"]:checked').length != 1);
  });

  const dialog = document.createElement('mm-dialog');
  dialog.setAttribute('heading', 'Split Group Enrolments');
  dialog.setAttribute('preserve', true);
  dialog.setAttribute('width', '600px');
  dialog.hidden = true;
  document.body.append(dialog);

  const classlist = await api(`/d2l/api/le/1.46/${id}/classlist/`, { fresh: true });

  const description = document.createElement('div');

  const affectedStudents = document.createElement('dl');
  affectedStudents.classList.add('list');

  const groupCount = document.createElement('input');
  groupCount.type = 'number';
  groupCount.value = '2';
  groupCount.classList.add('d_edt', 'vui-input', 'd2l-edit', 'group-count');

  const type = radio([
    { text: 'Groups of #', value: 'groups-of-x', selected: true },
    { text: '# of Groups', value: 'x-groups' },
  ]);
  const move = checkbox('Move users', null, false);

  const wrapper = document.createElement('div');
  wrapper.classList.add('mm-group-option-flags');
  wrapper.append(type.wrapper, groupCount, move.label);

  affectedStudents.addEventListener('click', e => {
    if (e.target.matches('dt')) {
      affectedStudents.toggleAttribute('collapsed');
    }
  });
  dialog.append(description, wrapper, affectedStudents);

  dialog.hidden = false;

  const IsStudent = id => classlist.find(stu => stu.Identifier == id) != null;

  let group = null;
  let enrollments = [];

  const UpdateDescription = () => {
    const action = type.checked();
    const input = groupCount.value;

    description.innerText = `Select OK to distribute the ${enrollments.length} users in "${group.Name}" ${
      action == 'groups-of-x' ? `among new groups with up to ${input} users each.` : `equally among ${input} new groups.`
    }`;
  };

  type.wrapper.addEventListener('change', UpdateDescription);
  groupCount.addEventListener('change', UpdateDescription);

  split.addEventListener('click', async e => {
    description.innerHTML = `<mm-loader center></mm-loader><p style="text-align:center;">Loading...</p>`;

    const from = container.querySelector('input[name="gridGroups_cb"]:checked').value.split('_')[1];
    group = await api(`/d2l/api/lp/1.28/${id}/groupcategories/${category}/groups/${from}`, { first: true, fresh: true });
    enrollments = group.Enrollments.filter(IsStudent);

    if (enrollments.length < 2) {
      return alert(`The selected group has less than 2 users, so it can't be split.`);
    }

    UpdateDescription();

    move.text.innerText = `Remove users from ${group.Name}.`;
    move.label.title = `After splitting the users into new groups, remove users from ${group.Name}, leaving an empty group.`;

    affectedStudents.innerHTML = '';

    const style = document.createElement('style');
    style.innerHTML = `
    .radio-collection label:not(:first-child) {
      margin-left: 10px;
    }

    .group-count {
      width: 20%;
    }

    .mm-group-option-flags,
    .radio-collection {
      margin: 20px 0;
    }

    .mm-group-option-flags label span {
      padding-left: 10px;
    }

    optgroup[data-visible-children="0"] {
      display: none;
    }

    .student-list {
      list-style: none;
      padding: 0;
      margin: 0;
    }

    .list[collapsed] .student-list {
      display: none;
    }

    .list {
      margin-bottom: 0;
    }

    .list dt {
      cursor: pointer;
    }

    .list dt:before {
      display: inline-block;
      content: '- ';
      padding-right: 10px;
    }

    .list[collapsed] dt:before {
      content: '+ ';
    }
    `;

    const desc = document.createElement('dt');
    desc.innerText = 'List of students';

    const students = document.createElement('dd');
    const list = document.createElement('ul');
    list.classList.add('student-list');

    const col = enrollments
      .map(id => classlist.find(student => student.Identifier == id))
      .filter(v => v)
      .sort((a, b) => {
        const j = a.LastName.toLowerCase();
        const k = b.LastName.toLowerCase();
        return j < k ? -1 : j > k ? 1 : 0;
      });

    col.forEach(({ FirstName, LastName, OrgDefinedId }) => {
      const li = document.createElement('li');
      li.innerText = `${FirstName} ${LastName} (${OrgDefinedId})`;
      list.append(li);
    });

    students.append(list);
    affectedStudents.append(style, desc, students);
    affectedStudents.toggleAttribute('collapsed', true);

    dialog.setAttribute('open', true);
  });

  dialog.addEventListener('ok', async e => {
    split.setAttribute('disabled', '');

    try {
      const action = type.checked();
      const from = container.querySelector('input[name="gridGroups_cb"]:checked').value.split('_')[1];

      const moving = move.checkbox.checked;
      const students = enrollments.length;

      let counter = 0;
      const afterEach = () => loader.setAttribute('value', ++counter);

      const MakeGroup = async name => {
        const res = await apisend(
          `/d2l/api/lp/1.28/${id}/groupcategories/${category}/groups/`,
          {
            Name: name,
            Code: `${Date.now()}__${name.replace(/[\\:*?“”<>|‘ #,%&]/g, '')}`,
            Description: { Content: '', Type: 'Text' },
          },
          { afterEach, verb: 'POST' }
        );

        if (!res.ok) throw new APIErrorResponse(res);
        else return res.json();
      };

      const EnrollStudents = (users, group) =>
        apisend(`/d2l/api/lp/1.28/${id}/groupcategories/${category}/groups/${group}/enrollments/`, users, {
          verb: 'POST',
          asSinglePayload: false,
          afterEach,
        });

      const UnenrollStudent = (user, group) =>
        apisend(`/d2l/api/lp/1.28/${id}/groupcategories/${category}/groups/${group}/enrollments/${user}`, null, {
          verb: 'DELETE',
          afterEach,
        });

      const collection = shuffle(enrollments);

      const isStudentSizeCapped = action == 'groups-of-x';

      const input = parseInt(groupCount.value);

      const groups = isStudentSizeCapped ? Math.ceil(students / input) : input;
      const poolsize = isStudentSizeCapped ? input : Math.ceil(students / input);

      loader.setAttribute('text', `${moving ? 'Moving' : 'Adding'} ${students} ${plural(students, 'student', 'students')}.`);
      loader.removeAttribute('indeterminate');
      // Moving adds a second set of requests per student (one to enroll them, one to unenroll), so we account for that in the progress results.
      loader.setAttribute('max', students * (moving ? 2 : 1) + groups);
      loader.setAttribute('open', true);

      await Promise.all(
        Array(groups)
          .fill(null)
          .map(async (_, i) => {
            try {
              const students = collection.splice(0, poolsize).map(UserId => ({ UserId }));
              const { GroupId } = await MakeGroup(`${group.Name} (${i + 1})`);
              await EnrollStudents(students, GroupId);
            } catch (e) {
              console.warn('Something went wrong', e);
            }
          })
      );

      if (moving) {
        await Promise.all(enrollments.map(id => UnenrollStudent(id, from)));
      }
    } catch (e) {
      console.warn(e);
    } finally {
      // await new Promise(res => setTimeout(res, 100));
      split.removeAttribute('disabled');
      split.classList.remove('loading');
      window.location.reload();
    }
  });
})();
