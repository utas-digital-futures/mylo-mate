(async function () {
  const dropdown = document
    .querySelector('a[href*="category_newedit.d2l"]')
    .parentElement.querySelector('d2l-dropdown,d2l-dropdown-context-menu');
  const menuitem = document.createElement('d2l-menu-item');
  menuitem.setAttribute('text', 'Enrol Users - Selected Groups');
  menuitem.addEventListener('click', e => {
    const checked = Array.from(document.querySelectorAll('[name="gridGroups_cb"]:checked'));
    if (checked.length == 0) return;

    const url = new URL('/d2l/lms/group/group_enroll.d2l', window.location.origin);
    url.search = window.location.search;
    url.searchParams.set('categoryId', document.querySelector('select[name="category"]').value);
    url.searchParams.set('selectedGroups', checked.map(cb => cb.value.split('_')[1]).join(','));
    window.location.href = url.href;
  });

  dropdown.addEventListener('d2l-dropdown-open', e => {
    const checked = Array.from(document.querySelectorAll('[name="gridGroups_cb"]:checked'));
    menuitem.toggleAttribute('disabled', checked.length == 0);
    e.target.querySelector('d2l-menu').append(menuitem);
  });
})();
