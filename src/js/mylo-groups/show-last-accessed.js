/**
 * @affects Groups
 * @problem When adjusting the group enrollment of students it is useful to know when they last accessed MyLO.
 * @solution Add the last accessed date beneath each students name.
 * @target Academic and support staff
 * @impact Moderate
 */

// (async function () {
//   const rows = document.querySelectorAll('table[summary^="List of groups in this category"] tr:not(.d_gh)');

//   const OrgID = window.location.searchParams.ou;
//   const classlist = await api(`/d2l/api/le/1.36/${OrgID}/classlist/`);
//   const users = classlist.reduce((o, user) => {
//     o[user.Identifier] = user;
//     return o;
//   }, {});

//   rows.forEach(async row => {
//     const [, code] = /ShowGroups\(\s+?([0-9]+)\s+?\)/i.exec(
//       row.querySelector('th a[onclick*="ShowGroups" i]').getAttribute('onclick')
//     );
//     const user = users[code];

//     const wrapper = document.createElement('div');
//     wrapper.style.display = 'block';
//     wrapper.setAttribute('mm-icon-type', 'action-button');
//     wrapper.setAttribute('mm-icon-offset', '-7: 0');
//     row.querySelector('th').append(wrapper);

//     const access = document.createElement('span');
//     if (user.LastAccessed != null) {
//       const d = moment(user.LastAccessed);
//       row.setAttribute('data-last-accessed', d.unix());
//       access.innerText = `Accessed: ${d.format('DD/MM/YY')}`;
//       access.title = `Last accessed: ${d.format('DD MMMM, YYYY HH:MM')}`;
//     } else {
//       row.setAttribute('data-last-accessed', '-1');
//       access.innerText = 'Last accessed: Never';
//     }

//     wrapper.append(access);
//   });
// })();
