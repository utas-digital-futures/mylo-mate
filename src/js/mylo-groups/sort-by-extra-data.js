(async function () {
  const rows = document.querySelectorAll('table[summary^="List of groups in this category"] tr:not(.d_gh)');

  const settings = await FetchMMSettings();
  const advSort = settings.other.AdvancedGroupEnhancements;

  const OrgID = window.location.searchParams.ou;
  const classlist = await api(`/d2l/api/le/1.36/${OrgID}/classlist/`, { fresh: true });
  const users = classlist.reduce((o, user) => {
    o[user.Identifier] = user;
    return o;
  }, {});

  const style = document.createElement('style');
  style.innerHTML = `.mm-page-sorting {
    float: right;
    border: 1px solid var(--d2l-color-mica);
    padding: 5px;
    margin: 0 6px;
    border-radius: 5px;
  }
    
  .mm-page-sorting span {
      display: block;
      font-size: 90%;
      margin-top: -5px;
  }`;

  const GetDiscussionData = async studentId => {
    const url = `/d2l/le/classlist/userprogress/${studentId}/${OrgID}/Discussions/Details`;
    const page = new DOMParser().parseFromString(await fetch(url).then(d => d.text()), 'text/html');
    const fields = Array.from(page.querySelectorAll('.vui-change-tracker .d2l-textblock'));
    const [read, threads, replies] = fields.map(field => parseInt(field.innerText));
    return { read, threads, replies };
  };

  const groupChecked = () => {
    const table = document.querySelector('table[type="data"] tbody');

    if (groupByEnrolled.checked) {
      const cb = Array.from(
        table.querySelectorAll('td:not([style*="display: none;"]) input[onclick*="enrollmentchange" i]:checked')
      ).reverse();
      // console.log(cb);
      cb.forEach(cb => table.querySelector('tr[header]').insertAdjacentElement('afterend', cb.closest('tr')));
    } else {
      sortRows('index', true);
    }
  };

  const groupByEnrolled = document.createElement('input');
  groupByEnrolled.type = 'checkbox';
  groupByEnrolled.checked = true;
  groupByEnrolled.addEventListener('change', groupChecked);

  const label = document.createElement('label');
  label.innerText = 'Show Enrolled First';
  label.title = 'Groups the enrolled users at the top of the list';
  label.prepend(groupByEnrolled);

  const sortRows = (property, asc) => {
    const table = document.querySelector('table[type="data"] tbody');
    const attr = `data-${property}`;
    const rows = Array.from(table.querySelectorAll(`tr[${attr}]`));
    const sorted = rows.sort((a, b) => parseInt(a.getAttribute(attr)) - parseInt(b.getAttribute(attr)));
    table.append(...(asc ? sorted : sorted.reverse()));

    if (property != 'index') {
      table.querySelectorAll('tr').forEach((tr, i) => tr.setAttribute('data-index', i));
      groupChecked();
    }
  };

  const handleSortChange = e => {
    if (e.target.matches('[nosort]')) {
      e.target.closest('th').querySelector('d2l-table-col-sort-button:not([nosort])').toggleAttribute('nosort', true);
      e.target.removeAttribute('nosort');
    }

    const asc = e.target.getAttribute('data-d2l-table-next-sort-dir') == 'asc';
    e.target.setAttribute('data-d2l-table-next-sort-dir', asc ? 'desc' : 'asc');
    e.target.desc = !asc;

    sortRows(e.target.getAttribute('data-sort-field'), asc);
  };

  const wrapper = document.createElement('div');
  wrapper.classList.add('mm-page-sorting');
  wrapper.setAttribute('mm-icon-type', 'action-button');
  wrapper.setAttribute('mm-icon-offset', '-5:-6');

  const title = document.createElement('span');
  title.innerText = 'Page Sorting';
  title.title = 'These sorting options only apply to the current page of users';
  wrapper.append(title);

  const sortOnLastAccessed = document.createElement('d2l-table-col-sort-button');
  sortOnLastAccessed.setAttribute('data-sort-field', 'last-accessed');
  sortOnLastAccessed.setAttribute('data-d2l-table-next-sort-dir', 'asc');
  sortOnLastAccessed.toggleAttribute('nosort', true);
  sortOnLastAccessed.title = 'Sort by Last Accessed Date (this page only)';
  sortOnLastAccessed.innerText = 'Last Accessed Date';
  sortOnLastAccessed.addEventListener('click', handleSortChange);

  const comma = () => document.createTextNode(', ');
  const sortOnPostCount = document.createElement('d2l-table-col-sort-button');
  sortOnPostCount.setAttribute('data-sort-field', 'posts');
  sortOnPostCount.setAttribute('data-d2l-table-next-sort-dir', 'asc');
  sortOnPostCount.toggleAttribute('nosort', true);
  sortOnPostCount.title = 'Sort by Post Count (this page only)';
  sortOnPostCount.innerText = 'Post Count';
  sortOnPostCount.addEventListener('click', handleSortChange);
  wrapper.append(sortOnLastAccessed, comma(), sortOnPostCount, comma(), label);

  if (advSort) {
    document.querySelector('table[type="data"] tbody tr[header] th').append(style, wrapper);
  }

  rows.forEach(async row => {
    const [, code] = /ShowGroups\(\s+?([0-9]+)\s+?\)/i.exec(
      row.querySelector('th a[onclick*="ShowGroups" i]').getAttribute('onclick')
    );
    const user = users[code];

    const wrapper = document.createElement('div');
    wrapper.style.display = 'block';
    wrapper.setAttribute('mm-icon-type', 'action-button');
    wrapper.setAttribute('mm-icon-offset', '-7: 0');
    row.querySelector('th').append(wrapper);

    const access = document.createElement('span');
    if (user.LastAccessed != null) {
      const d = moment(user.LastAccessed);
      row.setAttribute('data-last-accessed', d.unix());
      access.innerText = `Accessed: ${d.format('DD/MM/YY')}`;
      access.title = `Last accessed: ${d.format('DD MMMM, YYYY HH:MM')}`;
    } else {
      row.setAttribute('data-last-accessed', '-1');
      access.innerText = 'Last accessed: Never';
    }

    wrapper.append(access);

    if (advSort) {
      const { threads, replies } = await GetDiscussionData(code);
      const activity = document.createElement('span');
      activity.style.display = 'inline-block';
      activity.style.marginLeft = '10px';
      activity.innerText = `(T${threads}, R${replies})`;
      activity.title = `This student has ${threads} ${plural(threads, 'thread', 'threads')} and ${replies} ${plural(
        replies,
        'reply',
        'replies'
      )}.`;

      if (threads + replies == 0) {
        wrapper.style.background = '#ccc';
      }

      row.setAttribute('data-posts', threads + replies);

      wrapper.append(activity);
    }
  });

  document.querySelectorAll('table[type="data"] tbody tr').forEach((tr, i) => tr.setAttribute('data-index', i));
  groupChecked();
})();
