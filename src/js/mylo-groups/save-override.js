/**
 * @affects Groups
 * @problem When handling Group User Enrolment, the `Save` button closes the group and exits, requiring the user to start again.
 * @solution Add a separate `Save` button that saves progress without refreshing or leaving the page.
 * @target Academic and support staff
 * @impact Moderate
 */
(async function () {
  const orig = document.querySelector('d2l-floating-buttons button[primary]');
  const cancel = document.querySelector('d2l-floating-buttons button:last-of-type');
  const newSave = orig.cloneNode();
  orig.innerText = 'Save and Close';
  orig.removeAttribute('primary');
  orig.parentElement.prepend(newSave);

  let loader = document.createElement('mm-loader');
  loader.setAttribute('indeterminate', true);
  loader.setAttribute('width', 20);
  loader.style.margin = '0px -10px -5px 10px';
  loader.style.display = 'inline-block';
  loader.setAttribute('hidden', true);

  const transition = 'opacity 1s linear';

  let savedMessage = document.createElement('p');
  savedMessage.innerText = 'Enrolments saved successfully.';
  savedMessage.style.margin = '0.4em 0';
  savedMessage.style.opacity = '0';
  savedMessage.style.float = 'left';
  savedMessage.style.transition = transition;
  cancel.insertAdjacentElement('afterend', savedMessage);

  newSave.id = '';
  newSave.innerText = 'Save';
  newSave.setAttribute('mm-icon-type', 'action-button');
  newSave.setAttribute('mm-icon-offset', '6:6');
  newSave.append(loader);

  newSave.addEventListener('click', async e => {
    newSave.disabled = true;
    loader.removeAttribute('hidden');

    try {
      // Add the necessary additional values to the Form element
      UI.GetByName('d2l_action').value = 'UpdateMultiple';
      UI.GetMultiEditManager().AddMultiEditToForm();
      UI.GetStateManager().SetSaveStateOnUnload(false);
      UI.GetStateManager().AddStateToForm();
      UI.GetControlMap().AddControlMapToForm();
      UI.GetByName('d2l_referrer').value = window.localStorage.getItem('XSRF.Token');

      // Bundle the form value up into a payload
      let data = new FormData(UI.form);

      // Send the payload to the action defined on the form
      await fetch(UI.form.action, { method: 'post', body: data });

      // When the response is received, show a notification.
      savedMessage.style.transition = '';
      savedMessage.style.opacity = 1;
      setTimeout(() => {
        savedMessage.style.transition = transition;
        savedMessage.style.opacity = 0;
      }, 5000);
    } catch (e) {
      alert('Something went wrong trying to save the enrolments. Please try Save and Close instead.');
      console.error(e);
    }

    loader.setAttribute('hidden', true);
    newSave.disabled = false;
  });

  // Make the save bar buttons sticky
  const wrapper = document.createElement('div');
  wrapper.style.maxWidth = '90vw';
  wrapper.style.margin = '0';
  wrapper.style.position = 'sticky';
  wrapper.style.left = '20px';
  wrapper.style.paddingBottom = '10px';

  const bar = orig.closest('d2l-floating-buttons');
  wrapper.append(...bar.children);
  bar.append(wrapper);
})();
