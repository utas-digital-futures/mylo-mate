const modal = document.createElement('mm-dialog');
modal.toggleAttribute('fullscreen', true);
modal.heading = 'Edit H5P Object';
modal.preserve = true;
modal.toggleAttribute('hide-button-row', true);
document.body.append(modal);

const editor = document.createElement('iframe');
editor.style.height = '99%';
editor.style.width = '100%';
editor.onload = e => {
  const handler = e => {
    if (e.data.msg == 'url-res') {
      editor.onsrcchange?.(e.data.src);
      window.removeEventListener('message', handler);
    }
  };

  window.addEventListener('message', handler);
  setTimeout(() => editor.contentWindow.postMessage({ msg: 'url' }, '*'), 100);
};

window.addEventListener('message', e => {
  const { src, msg } = e.data;
  if (msg == 'is-dialog') {
    e.source.postMessage({ msg: msg, result: document.querySelector('.d2l-dialog') != null }, '*');
  }

  if (msg == 'edit-h5p') {
    if (document.querySelector('.d2l-dialog')) return;

    editor.src = src;
    editor.onsrcchange = src => {
      if (!src.endsWith('/edit')) {
        setTimeout(() => {
          modal.open = false;
          refresh();
        }, 100);
      }
    };

    modal.append(editor);
    modal.open = true;

    const refresh = () => {
      editor.remove();
      e.source.postMessage({ msg: 'refresh' });
    };

    const save = e => {
      e.preventDefault();
      e.source.postMessage({ msg: 'save' });
    };

    const close = () => {
      editor.remove();
    };

    modal.addEventListener('ok', save, { once: true });
    modal.addEventListener('cancel', close, { once: true });
  }
});

modal.addEventListener('open', () => {
  document.querySelector('.editor-sidebar')?.toggleAttribute('collapsed', true);
});
modal.addEventListener('close', () => {
  document.querySelector('.editor-sidebar')?.toggleAttribute('collapsed', false);
});

Messaging.Listen((e, res) => {});
