let isDialog = false;
window.addEventListener('message', e => {
  switch (e.data.msg) {
    case 'url': {
      e.source.postMessage({ msg: 'url-res', src: window.location.href }, '*');
      break;
    }
    case 'refresh': {
      window.location.reload();
      break;
    }
    case 'save': {
      if (window.location.href.endsWith('/edit')) {
        document.querySelector('.content-actions [name="submitter"]')?.click();
      }
      break;
    }
    case 'is-dialog': {
      isDialog = e.data.result;
      break;
    }
  }
});

window.top.postMessage({ msg: 'is-dialog' }, '*');

document.addEventListener('click', async e => {
  if (isDialog) return;

  if (e.target.matches('a[href$="/edit" i]')) {
    e.preventDefault();
    window.top.postMessage({ msg: 'edit-h5p', src: e.target.href }, '*');
  }
});
