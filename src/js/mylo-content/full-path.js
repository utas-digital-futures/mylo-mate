/**
 * @affects Unit Content
 * @problem The file name at the bottom of a page doesn't include the directory it is stored in.
 * @solution Prepends the file's directory path to the start of the file name, so that it's obvious where the file is in the Manage Files area.
 * @target Academic and support staff
 * @impact Low
 */

(async function () {
  const store = await new Promise(res => chrome.storage.sync.get(null, res));
  if (!store.mylo.ShowFullFilePath) return;

  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  const [, OrgID, , TopicID] = window.location.pathname.match(/content\/([0-9]+)\/(viewContent|contentfile)\/([0-9]+)\//i);

  // This tries to get the module from the cache first. If it doesn't exist, refetch with a network request.
  const unit = (await GetUnitModule({ OrgID, TopicID })) || (await GetUnitModule({ OrgID, TopicID, Cached: false }));
  if (!unit || unit.IsBroken) return;
  if (!unit.Url.startsWith('/content')) return;

  const textzone = document.querySelector('#StatusPlaceholder');
  const path = unit.Url.split('/').slice(0, -1).join('/');
  const file = unit.Url.split('/').pop().trim();

  textzone.innerText = textzone.innerText.replace(file, unit.Url);

  const manageFilesLink = document.createElement('a');
  manageFilesLink.href = `/d2l/lp/manageFiles/main.d2l?ou=${OrgID}&Path=${path}/&File=${file}`;
  manageFilesLink.classList.add('d2l-link');
  manageFilesLink.title = 'Show in Manage Files';
  manageFilesLink.target = '_blank';
  manageFilesLink.style.marginLeft = '1em';
  manageFilesLink.setAttribute('mm-icon-type', 'action-button');
  manageFilesLink.setAttribute('mm-icon-offset', '-6:0');

  const img = document.createElement('img');
  img.src = '/d2l/img/0/CMC.Main.actFiles.png?v=10.7.3.7486-189';

  manageFilesLink.append(img);
  textzone.append(manageFilesLink);
})();
