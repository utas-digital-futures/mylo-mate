/**
 * @affects Unit Content
 * @problem Broken links can be hard to identify, and time consuming to check.
 * @solution
 *  Add an in-page link checker that automatically tests all links found in the page and reports any that have issues.
 *  It then highlights them, and can automatically scroll down to where the problem link is in the page.
 * @target Academic and support staff
 * @impact High
 * @savings 15 minutes per page
 */

(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  /** if (document.body.getAttribute('mm-abort') == 'true') return; */

  const wrap = el => {
    const wrapper = document.createElement('span');
    wrapper.setAttribute('mm-icon-type', 'action-select');
    wrapper.setAttribute('mm-icon-offset', '10:0');
    wrapper.append(el);
    return wrapper;
  };

  const [, OrgID, , TopicID] = window.location.pathname.match(/content\/([0-9]+)\/(viewContent|contentfile)\/([0-9]+)\//i);
  const unit = await GetUnitModule({ OrgID, TopicID, Cached: false });
  if (!unit || unit.IsBroken) return;

  // Check to see if the file is a local HTML file.
  // This stops us needing to check specific wording on page buttons.
  if (!(unit.Url.startsWith('/content/enforced') && (unit.Url.endsWith('.html') || unit.Url.endsWith('.htm')))) return;

  const store = await new Promise(res => chrome.storage.sync.get(null, res));
  let canCheckExternalPages = false;

  const style = document.createElement('style');
  style.innerHTML = `
		table#mm_content_checker_results_list { width: 100%; table-layout:fixed; }
		table#mm_content_checker_results_list tr:not(:last-child) { border-bottom: thin #ddd solid; }
		table#mm_content_checker_results_list td { padding: 13px 0; text-align: center; word-break: break-word; }
		table#mm_content_checker_results_list tr td:first-child { border-right: thin #f3f3f3 solid; text-align: right; padding-right: 10px; }
		table#mm_content_checker_results_list td a { color: #006fbf; text-decoration: none; cursor: pointer; }
		table#mm_content_checker_results_list td a:hover { color: #005694; text-decoration: underline; outline-width: 0; }
		#mm_content_checker_time_taken { position: absolute; right: 0; bottom: -60px; font-size: 12px; }
		.mm-link-checker, .mm-link-checker p { white-space: normal; }
		.mm-checker-text:after { content: ''; clear:both; display:block; }
		.hilite-yellow { background-color: yellow; }
		.hilite-red { background-color: #900000; color: white !important; }
		#MM_content_checker_results { margin-top: 10px; }
	`;
  document.head.appendChild(style);

  String.prototype.hashCode = function () {
    let hash = 0;
    let i;
    let chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
      chr = this.charCodeAt(i);
      hash = (hash << 5) - hash + chr;
      hash |= 0;
    }
    return hash;
  };

  // The row to put the icon in
  const headerRow = document.querySelector('.d2l-page-header-side');

  let bar;
  if (!document.querySelector('div.mm-button-bar')) {
    bar = document.createElement('div');
    bar.classList.add('mm-button-bar');
    bar.style.display = 'inline-block';
    headerRow.insertBefore(bar, headerRow.firstChild);
  } else bar = document.querySelector('div.mm-button-bar');

  bar.hidden = document.body.getAttribute('mm-abort') == 'true';

  // The iFrame selector
  const iframe = document.querySelector('div.d2l-fileviewer iframe');
  if (iframe && iframe.contentDocument) {
    const root = iframe.contentDocument;

    // Added to the document itself. Allows for styles to appear (which won't carry over from the page stylesheet!)
    const style = document.createElement('style');
    style.innerHTML = `
			.hilite-yellow { background-color: yellow; } 
			.hilite-red { background-color: #900000; color: white !important; } 
			@-webkit-keyframes flash { 50% { background-color: white; color: black; } }
			.flash { -webkit-animation-name: flash; -webkit-animation-duration: 1s; -webkit-animation-iteration-count: 3; -webkit-animation-timing-function: ease-in-out; }
		`;
    root.head.appendChild(style);

    const dialog = document.createElement('div');
    dialog.setAttribute('hidden', '');
    dialog.classList.add('mm-link-checker');
    dialog.innerHTML = `
    <div class="closer" title="Hide dialog">&times;</div>
    <div class="mm-checker-text">
      <p>This tool will check the current page for invalid links, including external links that won't open correctly.</p>
      <p>Links that have issues leaving MyLO will be <span class="hilite-yellow">highlighted yellow</span>, and pages that have loading errors (such as not existing) will be <span class="hilite-red">highlighted red</span>.</p>
      <button class="d2l-button" is="d2l-button" primary="" id="MM_content_checker_activate" style="float:right;">Check Links</button>
    </div>
    <div id="MM_content_checker_results">
      <table id="mm_content_checker_results_list">
      </table>
    </div>`;

    const newIcon = document.createElement('d2l-button-icon');
    newIcon.setAttribute('data-testid', 'link-checker-dialog');
    newIcon.setAttribute('icon', 'tier1:link');
    newIcon.classList.add('d2l-inline');
    newIcon.style.margin = '0 10px';
    newIcon.title = 'Check for invalid or broken links.';

    // const newIcon = document.createElement('div');
    // newIcon.setAttribute('data-testid', 'link-checker-dialog');
    // newIcon.setAttribute('mm-icon-type', 'action-select');
    // newIcon.setAttribute('mm-icon-offset', '-6:0');
    // newIcon.classList.add('d2l-inline');
    // newIcon.style.margin = '0 5px';
    // newIcon.innerHTML = `
    // <a class="d2l-dropdown-opener d2l-imagelink mm-link-checker-opener" style="cursor:pointer" title="Check for invalid or broken links.">
    //   <d2l-icon class="mm-link-checker-opener" icon="d2l-tier1:link"></d2l-icon>
    // </a>
    // `;

    document.querySelector('.d2l-page-header').append(dialog);

    bar.appendChild(wrap(newIcon));
    newIcon.addEventListener('click', e => dialog.toggleAttribute('hidden'));
    window.addEventListener('message', ({ data }) => {
      if (data.data == 'check-mylo-links') {
        dialog.toggleAttribute('hidden');
      }
    });

    dialog.addEventListener('click', e => {
      if (e.target.matches('.closer')) {
        dialog.setAttribute('hidden', '');
      }
    });

    const checkErrors = urls =>
      new Promise(resolve => chrome.runtime.sendMessage({ flag: 'checkURL', payload: urls }, resolve));

    const info = {
      '__default__': '',
      '-1': 'Missing link text',
      '0': 'An error occurred while fetching the page. The link may be invalid or there may be a problem with your Internet connection.',
      '404': "Page could not be found, it has probably been moved or deleted. You'll need to replace this link.",
      '403': 'You may not have permission to view this page, or it requires you to be logged in before accessing.',
      '500':
        "This indicates an issue with the web server. It's not possible to determine if the link is temporarily or permanently inaccessible.",
      '999': 'Connection to the page has timed out.',
    };

    const title = {
      '__default__': '',
      '-1': 'Missing link text',
      '0': '0: Error fetching page',
      '404': '404: Page not found',
      '403': '403: Access denied',
      '500': '500: Server error',
      '999': 'Connection timeout',
    };

    const Alert = text =>
      new Promise(res => {
        const alert = document.createElement('mm-alert');
        alert.innerText = text;
        document.body.append(alert);

        alert.addEventListener('ok', res);
        alert.setAttribute('open', '');
      });

    const checkLinks = async () => {
      const button = document.querySelector('#MM_content_checker_activate');

      const loader = document.createElement('mm-loader');
      loader.setAttribute('center', '');
      try {
        button.setAttribute('disabled', '');

        // Do we have permission to check the links?
        let hasPermission = await new Promise(res =>
          chrome.runtime.sendMessage({ request: 'link-checker-has-permissions' }, res)
        );

        if (!hasPermission) {
          // If we don't already have permission, inform the user of what's going on, and why we're asking for permission to everything.
          // Then, request permission.
          await Alert(
            "MyLO MATE doesn't have the necessary permissions to check external links. A dialog will appear asking for permission to access all pages; if you wish to check external links, please click 'Allow'."
          );
          hasPermission = await new Promise(res =>
            chrome.runtime.sendMessage({ request: 'link-checker-get-permission' }, res)
          );
        }

        // This will be whether we can actually check externally or not.
        canCheckExternalPages = hasPermission;

        // If we still don't have permission, then give them the option of just checking internal pages.
        if (!hasPermission) {
          if (
            !confirm(
              'Permission was denied for checking external pages. Please click OK if you wish to just check internal links, or Cancel to stop the process.'
            )
          ) {
            return; // They don't want to check anything anymore.
          }
        }

        const table = dialog.querySelector('#mm_content_checker_results_list');
        table.innerHTML = '';
        dialog.appendChild(loader);

        root.querySelectorAll('a[mm-id]').forEach(a => a.removeAttribute('mm-id'));

        let broken_links = Array.from(root.querySelectorAll('a[href^="http://"]:not([target="_blank"])')).filter(
          a => !a.matches('[href*="browsehappy"]')
        );
        broken_links.forEach(a => {
          const hash = (a.href + Math.random() * Date.now()).hashCode().toString(16);
          a.setAttribute('mm-id', hash);
          a.hashID = hash;
        });

        let links = [];

        const internal = Array.from(
          root.querySelectorAll('a[href^="http"][href*="' + window.location.hostname + '"]:not([mm-id])')
        ).map(a => {
          const hashID = (a.href + Math.random() * Date.now()).hashCode().toString(16);
          a.setAttribute('mm-id', hashID);
          return { hashID, href: a.href, innerText: a.innerText };
        });

        links = links.concat(internal);

        if (canCheckExternalPages) {
          const external = Array.from(
            root.querySelectorAll('a[href^="http"]:not([href*="' + window.location.hostname + '"]):not([mm-id])')
          ).map(a => {
            const hashID = (a.href + Math.random() * Date.now()).hashCode().toString(16);
            a.setAttribute('mm-id', hashID);
            return { hashID, href: a.href, innerText: a.innerText };
          });
          links = links.concat(external);
        }

        const results = await checkErrors(links);
        const bad_results = results
          .map(res => {
            res.status = res.innerText.trim() == '' ? -1 : res.status;
            return res;
          })
          .filter(res => res.status !== 200);
        const bad_stuff = bad_results.concat(Array.from(broken_links));

        bad_stuff.forEach(a => {
          const tr = document.createElement('tr');
          if (a.innerText.trim().length == 0) {
            tr.innerHTML = `<td width="60%">(empty link - edit page to fix)</td><td><span title="${
              info[a.status != null ? a.status : '__default__']
            }">[${
              a.status != null ? title[a.status] : 'Bad target'
            }]</span> <span class="visit"><a class="d2l-link" title="Open link in new tab" target="_blank" href="${
              a.href
            }">visit</a></span></td>`;
          } else {
            tr.innerHTML = `<td width="60%"><a class="mm-scroll-to-issue" data-ref="${
              a.hashID
            }">${a.innerText.trim()}</a></td><td><span title="${info[a.status != null ? a.status : '__default__']}">[${
              a.status != null ? title[a.status] : 'Bad target'
            }]</span> <span class="visit"><a class="d2l-link" title="Open link in new tab" target="_blank" href="${
              a.href
            }">visit</a></span></td>`;
          }
          table.appendChild(tr);
        });

        loader.remove();

        if (bad_stuff.length == 0 && broken_links.length == 0) {
          const tr = document.createElement('tr');
          tr.innerHTML = '<td>No issues found in this page.</td>';
          table.appendChild(tr);
        }

        const hilite = link => {
          link.classList.add('hilite-yellow');
          link.title = 'This link needs to open in a new tab or window.';
        };

        broken_links.forEach(hilite);
        bad_results.forEach(link => {
          if (Object.keys(title).includes(link.status.toString())) {
            const a = root.querySelector(`a[mm-id="${link.hashID}"]`);
            a.classList.add('hilite-red');
            a.title = info[link.status];
          }
        });

        table.addEventListener('click', e => {
          if (e.target.matches('a.mm-scroll-to-issue')) {
            const hash = e.target.getAttribute('data-ref');
            const link = root.querySelector(`a[mm-id="${hash}"]`);
            link.scrollIntoView({ behaviour: 'smooth', block: 'center', inline: 'nearest' });
            link.addEventListener('animationend', () => {
              link.classList.remove('flash');
            });
            link.classList.add('flash');
          }
        });
      } catch (e) {
        console.error(e);
        loader.remove();
        await Alert(
          'Something went wrong while fetching the results. Please refresh and try again.\n\nIf this error continues, please contact MyLO MATE support.'
        );
      } finally {
        button.removeAttribute('disabled');
      }
    };

    dialog.addEventListener('click', e => {
      if (e.target.matches('#MM_content_checker_activate')) {
        checkLinks();
      }
    });
  }
})();
