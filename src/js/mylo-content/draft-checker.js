/**
 * @affects Unit Content
 * @problem It is not always apparent when viewing content whether a page is hidden (in Draft mode).
 * @solution Add a red box in the top right of a page indicating if the page is Drafted.
 * @target Academic and support staff
 * @impact Low
 */

(async function () {
  const [, OrgID, , TopicID] = window.location.pathname.match(/content\/([0-9]+)\/(viewContent|contentfile)\/([0-9]+)\//i);
  const unit = await GetUnitModule({ OrgID, TopicID, Cached: false });
  if (!unit || unit.IsBroken) return;

  document.body.setAttribute('mm-draft', unit.IsHidden);
  let label = document.createElement('p');
  label.innerText = 'DRAFT';
  label.classList.add('mm-draft-label');

  let style = document.createElement('style');
  style.innerHTML = `
      .mm-draft-label {
        position: absolute;
        right: 29px;
        top: 96px;
        color: white;
        font-size: 11px;
        background-color: #80251a;
        padding: 0 5px;
        border: thin solid lightgrey;
        border-radius: 3px;
        font-weight: bold;
        text-transform: uppercase;
        margin: 0 !important;
        height: 18px;
        line-height: 20px;
        display: none;
      }
      
      [mm-draft="true"] .mm-draft-label {
        display: block;
      }
  
      .d2l-page-main-padding {
        position: relative;
      }
    `;

  document.body.appendChild(style);
  document.querySelector('.d2l-page-main-padding').insertAdjacentElement('afterbegin', label);
})();
