export default async function ({ widget, orgid, config, unit }) {
  if (!unit.IsSandpit) {
    widget.querySelectorAll('[widget-link][sandpit-only]').forEach(widget => {
      widget.toggleAttribute('inactive', true);
    });
  }
}
