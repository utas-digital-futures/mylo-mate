(async function () {
  /*
   *	Make sure that this variable is different for each widget you add.
   *	This is what stops it from adding itself multiple times if the script is added more than once (it can happen, not sure why).
   *	Note: This is an issue that aims to be resolved as part of the codebase rebuild.
   */
  if (window.WidgetAdded) return;
  window.WidgetAdded = true;

  const SettingsBanner = `/** Extension settings for MyLO MATE **/\n`;

  // If we want to enable our JS Playground widget.
  const SANDPIT = false;

  // If we're not in a sandpit unit, or we are in a sandpit unit but we're a student, then stop running.
  // if (!document.querySelector('#courseName') || !document.querySelector('#courseName').innerText.includes('Sandpit') || document.body.getAttribute('mm-abort') === 'true') return;

  // If we're in a sandpit, and not a student, make sure we're on the homepage (/d2l/home/{OrgID}). If we're not, stop running here.
  if (!/\/d2l\/home\/[0-9]+/i.test(window.location.pathname)) return;
  if (document.body.getAttribute('mm-abort') === 'true') return;

  function check() {
    setTimeout(function () {
      if (document.body.getAttribute('mm-abort') == null || document.body.getAttribute('mm-unit-code') == null) check();
      else if (document.body.getAttribute('mm-abort') === 'false') ready();
    }, 20);
  }

  check();

  document.addEventListener(
    'click',
    e => {
      if (e.target.matches('[mm-widget-view-state]')) {
        const widget = e.target.closest('[mm-type-is="widget"]');
        const action = e.target.getAttribute('mm-widget-view-state');

        if (action == 'collapse') {
          window.localStorage.setItem(`mm-widget-state-${widget.getAttribute('mm-widget-name')}`, false);
          widget.setAttribute('collapsed', '');
        } else {
          window.localStorage.setItem(`mm-widget-state-${widget.getAttribute('mm-widget-name')}`, true);
          widget.removeAttribute('collapsed');
        }

        e.target.closest('d2l-dropdown-context-menu').removeAttribute('opened');
        e.target.closest('d2l-dropdown-content').removeAttribute('opened');
      }
    },
    true
  );

  async function ready() {
    const _ = (r, all) => (all ? document.querySelectorAll(r) : document.querySelector(r));
    // If we've enabled it in the options, carry on.

    const sandpit = document.body.getAttribute('mm-unit-code').split('_')[1] == 'SP';
    const store = await FetchMMSettings();
    const permissions = store.widgets || defaults.widgets;

    const widgetCave = _('.homepage-col-8');
    const widget_sidebar = _('.homepage-col-4');

    const appendAtIndex = (parent, node, index) =>
      parent.insertBefore(node, parent.querySelector('div[role="region"]:nth-child(' + index + ')'));
    const append = (arr, parent) => arr.map(el => parent.appendChild(el));

    let widget_style = document.createElement('link');
    widget_style.href = chrome.runtime.getURL('js/mylo-widgets/global-widget-styles.css');
    widget_style.rel = 'stylesheet';
    widget_style.type = 'text/css';
    document.head.appendChild(widget_style);

    let scripts = {
      sandpit: {
        body: [],
        sidebar: [],
      },
      all: {
        body: [],
        sidebar: [],
      },
    };
    let res = {
      sandpit: {
        body: [],
        sidebar: [],
      },
      all: {
        body: [],
        sidebar: [],
      },
    };

    // Check for each permission, and add its script name to the relevant array.

    if (permissions.Search) scripts.all.sidebar.push('SearchUnit');
    if (permissions.LinkChecker) scripts.all.sidebar.push('ContentChecker');
    if (permissions.GradesAnalysis) scripts.all.body.push('GradesAnalysis');
    if (permissions.LecturerCSV) scripts.sandpit.sidebar.push('EnrolledUsers');
    if (permissions.AbsentStudents) scripts.sandpit.sidebar.push('GetAbsentStudents');
    if (permissions.StatusReport) scripts.all.sidebar.push('StatusReport');
    if (permissions.UnitOutlineUploader) scripts.all.sidebar.push('UnitOutlineUploader');
    if (store.other.ShowUnitTemplateWidget) scripts.all.sidebar.push('SetUnitTemplate');

    if (permissions.WordCloud) scripts.all.sidebar.push('WordCloud');

    // if(chrome.runtime.getManifest().update_url == null) {
    // 	scripts.sandpit.body.push('TechHelpDiscussionsTool');
    // }

    if (SANDPIT) {
      scripts.sandpit.body.push('Sandpit');
    }

    let sidebar_wrapper = document.createElement('div');
    sidebar_wrapper.setAttribute('role', 'region');

    res.all.body = await getSource(scripts.all.body);
    res.all.sidebar = await getSource(scripts.all.sidebar);

    if (sandpit) {
      res.sandpit.body = await getSource(scripts.sandpit.body);
      res.sandpit.sidebar = await getSource(scripts.sandpit.sidebar);
    }

    if (permissions.GradesAnalysisAtTop) {
      // Add them before the announcements
      widgetCave.prepend(...res.all.body);
      widgetCave.prepend(...res.sandpit.body);
    } else {
      // Add them after the announcements
      widgetCave.append(...res.all.body);
      widgetCave.append(...res.sandpit.body);
    }

    sidebar_wrapper.append(...res.all.sidebar);
    sidebar_wrapper.append(...res.sandpit.sidebar);

    appendAtIndex(widget_sidebar, sidebar_wrapper, 2);

    function getSource(widget_name) {
      const get = name =>
        new Promise((resolve, reject) => {
          let src = chrome.runtime.getURL(`js/mylo-widgets/${name}.widget.html`);
          let xhr = new XMLHttpRequest();
          xhr.open('GET', src);
          xhr.addEventListener('loadend', function () {
            if (this.statusText === 'OK') {
              let d = document.createElement('div');
              d.id = name;
              let html = '';
              html = this.responseText.replace(/mylo-mate:\/\//gi, chrome.runtime.getURL(''));
              html = html.replace(
                /<.*?script.*?purpose.*?=.*?"extension\/settings".*?>.*?<\/script>/gi,
                `<script>${SettingsBanner}const store = ${JSON.stringify(store)};</script>`
              );
              d.innerHTML = html;
              d.querySelectorAll('script[src]').forEach(node => {
                let src = document.createElement('script');
                Array.from(node.attributes).forEach(attr => src.setAttribute(attr.nodeName, attr.nodeValue));
                src.async = false;
                // src.src = node.src;
                node.parentNode.insertBefore(src, node);
                node.remove();
              });
              d.querySelector('[mm-type-is="widget"]').setAttribute('mm-extension-id', chrome.runtime.id);

              const widget = d.querySelector('[mm-type-is="widget"]');
              const isFalse = val => !val || val.toLowerCase() === 'false';
              if (isFalse(window.localStorage.getItem(`mm-widget-state-${widget.getAttribute('mm-widget-name')}`))) {
                widget.setAttribute('collapsed', '');
              }

              resolve(d.childNodes[0]);
            } else reject(this);
          });
          xhr.send();
        });

      if (Array.isArray(widget_name)) {
        return Promise.all(widget_name.map(get));
      } else {
        return get(widget_name);
      }
    }
  }
})();
