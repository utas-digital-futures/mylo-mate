/**
 * @affects MyLO Widgets
 * @problem Identifying page content that contains a specific search phrase requires tediously checking through the content.
 * @solution A widget allows the user to search all content in the unit for a phrase, including HTML pages, PDFs, Word documents, descriptions, checklists and any files found linked in HTML content.
 * @target Academic and support staff
 * @impact High
 * @savings 3 hours per unit
 */

/**
 * @affects MyLO Widgets
 *
 * @problem
 *  With the sudden COVID-19 changes meaning many students were unable to come to Australia,
 *  it was necessary to identify links that may be blocked by the firewall, a tedious process
 *  requiring manually checking all content within a unit.
 *
 * @solution
 *  A widget searches through all content in the unit, including HTML pages, PDFs, Word documents,
 *  descriptions, checklists and any files found linked in HTML content, for known blocked links (such as Twitter, YouTube and Google).
 *
 * @target Academic and support staff
 * @impact High
 * @savings 3 hours per unit
 */

export default async function ({ widget: root, orgid, config: settings }) {
  const widget = {};
  widget.search = root.querySelector('[data-action="general"]');
  widget.findBlocked = root.querySelector('[data-action="blocked"]');
  widget.copy = root.querySelector('[data-action="copy-results"]');
  widget.save = root.querySelector('[data-action="save-docx"]');
  widget.actionsRow = root.querySelector('[data-action="search-result-actions"]');
  widget.loader = root.querySelector('mm-loader');
  widget.resultOrder = root.querySelector('.results-order');
  widget.table = root.querySelector('#MM_search_results');

  const metaTag = document.head.querySelector('meta[name="mylo-mate"]');
  const extensionID = metaTag ? metaTag.getAttribute('content') : '';
  pdfjsLib.GlobalWorkerOptions.workerSrc = `chrome-extension://${extensionID}/lib/pdf.worker.min.js`;

  const { name, path } = await api(`/d2l/api/lp/1.25/enrollments/myenrollments/${orgid}`, {
    first: true,
  }).then(res => ({
    name: res.OrgUnit.Name,
    path: res.OrgUnit.Code,
  }));
  const codes = (name.match(/[A-Z]{3}[0-9]{3}/g) || [name.replace(/\s/g, '-')]).map(code => code.toLowerCase());

  const contains = (target, match, regex, caseSensitive) => {
    if (!Array.isArray(target)) target = [target];
    const check = string => {
      if (!caseSensitive) string = string.toLowerCase();

      if (!regex) {
        if (!caseSensitive) match = match.toLowerCase();
        let results = string.split(match).length - 1;
        return { found: results > 0, count: results };
      } else {
        let r = new RegExp(match, 'gm' + caseSensitive ? '' : 'i');
        let matches = string.match(r) || [];
        return { found: matches.length > 0, count: matches.length };
      }
    };

    return target.map(check).reduce((o, c) => ({
      found: o.found || c.found,
      count: o.count + c.count,
    }));
  };

  const FindAttributeLinksInString = string => {
    let dom = new DOMParser().parseFromString(string, 'text/html');
    return Array.from(dom.querySelectorAll('[href],[src]')).map(el => el.href || el.src);
  };

  // Turns a URL into a zip file
  const unpack = url =>
    new Promise((resolve, reject) =>
      JSZipUtils.getBinaryContent(url, (err, content) => {
        if (err) {
          reject(err);
        } else {
          let z = new JSZip();
          z.loadAsync(content).then(() => resolve(z));
        }
      })
    );

  let PDFStore = new Map();

  class Document {
    static extensions = ['docx', 'pptx', 'pdf', 'html', 'htm'];
    static async _pdf({ src, text = true, links = true } = {}) {
      try {
        if (!PDFStore.has(src)) {
          PDFStore.set(src, pdfjsLib.getDocument(SafeURL(src)).promise);
        }

        const pdf = await PDFStore.get(src);

        let str = '';
        let urls = new Set();

        for (let i = 0; i < pdf.numPages; i++) {
          const page = await pdf.getPage(i + 1);
          // If we want the text, grab it here.
          if (text) str += await page.getTextContent().then(data => data.items.map(item => item.str).join(''));

          // If we want links, grab them here
          if (links) {
            const annots = await page.getAnnotations();
            annots.forEach(anno => {
              if (anno.subtype == 'Link') {
                urls.add(anno.url);
              }
            });
          }
        }

        // Return what we found.
        return { links: Array.from(urls), text: str };
      } catch (e) {
        console.error(src, e);
        return null;
      }
    }

    static async _pptx({ src, text = true, links = true } = {}) {
      try {
        const zip = await unpack(SafeURL(src));

        const rels = new DOMParser().parseFromString(
          await zip.files['ppt/_rels/presentation.xml.rels'].async('string'),
          'text/xml'
        );

        const slidePaths = Array.from(rels.querySelectorAll('Relationship[Type$="relationships/slide"]')).map(rel =>
          rel.getAttribute('Target')
        );

        const output = { text: '', links: [] };
        for (const slide of slidePaths) {
          const slideName = slide.split('/').pop();
          const [content, props] = await Promise.all([
            new DOMParser().parseFromString(await zip.files[`ppt/${slide}`].async('string'), 'text/xml'),
            new DOMParser().parseFromString(
              await zip.files[`ppt/slides/_rels/${slideName}.rels`].async('string'),
              'text/xml'
            ),
          ]);

          const txt = Array.from(content.querySelectorAll('t'))
            .map(el => el.textContent.trim())
            .join(' ');

          const urlsFromText = txt.match(/(http[^\s]*)/gim) || [];
          const urlsFromProps = Array.from(
            props.querySelectorAll('Relationship[Type$="relationships/video"],Relationship[Type$="relationships/hyperlink"]')
          ).map(rel => rel.getAttribute('Target'));

          if (text) output.text += txt;
          if (links) output.links = output.links.concat(urlsFromText, urlsFromProps);
        }

        if (links) output.links = output.links.filter((v, i, a) => a.indexOf(v) === i);

        return output;
      } catch (e) {
        console.error(src, e);
        return null;
      }
    }

    static async _docx({ src, text = true, links = true } = {}) {
      try {
        const zip = await unpack(SafeURL(src));
        // Gets the document text from the zip file.
        let doc = await zip.files['word/document.xml'].async('string');
        let rels = new DOMParser().parseFromString(
          await zip.files['word/_rels/document.xml.rels'].async('string'),
          'text/xml'
        );

        // If we want links, process it and grab it here.
        // Otherwise, just short circuit and return an empty array.
        let urls = links
          ? Array.from(rels.querySelectorAll('Relationship[Type$="relationships/hyperlink"][Target^="http"]')).map(rel =>
              rel.getAttribute('Target')
            )
          : [];

        // Turns the XML doc into a plain string.
        // If we don't want the text, just short circuit and return an empty string.
        let str = text
          ? Array.from(new DOMParser().parseFromString(doc, 'text/xml').querySelectorAll('t'))
              .map(t => t.textContent.trim())
              .join('')
          : '';

        return { links: urls, text: str };
      } catch (e) {
        console.error(src, e);
        return null;
      }
    }

    static async _html({ src, text = true, links = true } = {}) {
      try {
        let dom = new DOMParser().parseFromString(await ajax(src), 'text/html');

        // If we want our text, grab it here.
        // Otherwise, short circuit.
        let str = text ? dom.body.innerText : '';
        // If we want our links, grab them here.
        // Otherwise, short circuit.
        let urls = links
          ? Array.from(dom.querySelectorAll('[href],[src]')).map(el => el.getAttribute('href') || el.getAttribute('src'))
          : [];
        // let urls = links ? Array.from(dom.querySelectorAll('[href],[src]')).map(el => el.href || el.src) : [];

        return { links: urls, text: str };
      } catch (e) {
        console.error(`Error testing page: %s`, src);
        return null;
      }
    }

    static async search(
      { src, search, regex = false, caseSensitive = false, text = true, links = true } = {},
      linksCb = () => {}
    ) {
      try {
        let url = new URL(src, window.location.origin);
        let ext = url.pathname.split('.').pop().toLowerCase();
        if (!this.extensions.includes(ext)) {
          console.error('Unknown file type "%s"', url.pathname.split('.').pop());
          return null;
        }

        let results = null;
        if (ext == 'docx') results = await Document._docx({ src, text, links: true });
        if (ext == 'pptx') results = await Document._pptx({ src, text, links: true });
        if (ext == 'pdf') results = await Document._pdf({ src, text, links: true });
        if (ext == 'html' || ext == 'htm') results = await Document._html({ src, text, links: true });

        if (results == null) {
          console.warn('Something went wrong trying to process the provided document.', src);
          return;
        }

        let checks = { text: null, links: null };
        if (text) {
          checks.text = contains(results.text, search, regex, caseSensitive);
        }

        if (links) {
          checks.links = results.links.filter(link => {
            try {
              const host = new URL(link, window.location.origin).host;
              if (regex) return RegExp(search, 'gmi').test(host);
              else return host.includes(search);
            } catch (e) {
              return null;
            }
          });
        }

        linksCb(results.links);

        return checks;
      } catch (e) {
        console.log(e, src);
        return { text: null, links: null };
      }
    }
  }

  class Checklist {
    config = {
      /** Make search case sensitive */
      caseSensitive: false,
      /** Run search as regular expression */
      regex: false,
    };

    constructor(settings = this.config, files) {
      this.config = {
        ...this.config,
        ...settings,
      };

      this.mf = files;
    }
    async search({ search, links = true, text = true } = {}) {
      let checklists = await this._getChecklists();

      const CheckDescriptionBlock = cl => {
        const desc = cl.Description;
        let match = false;
        const urls = FindAttributeLinksInString(desc.Html).filter(link => {
          const file = new URL(link, window.location.origin);
          if (file.pathname.includes('quickLink/quickLink.d2l')) {
            const { ou, type, fileId } = file.query;
            if (type == 'coursefile' && parseInt(ou) == orgid) {
              const ref = new URL(
                `/content/enforced/${orgid}-${path}/${fileId.replace(/\+/g, ' ')}`,
                window.location.origin
              );
              this.mf.addFile(ref, cl);
            }
          }

          const host = file.host;
          if (this.config.regex) return RegExp(search, 'gmi').test(host);
          else return host.includes(search);
        });

        if (text) match = match || contains(desc.Text, search, this.config.regex, this.config.caseSensitive).found;
        if (links) match = match || urls.length > 0;

        desc.ContainsMatch = match;
        desc.MatchingLinks = urls;
        return cl;
      };

      const results = checklists
        .map(cl => {
          cl = CheckDescriptionBlock(cl);
          cl.Categories = cl.Categories.map(CheckDescriptionBlock);
          cl.Items = cl.Items.map(CheckDescriptionBlock);
          return cl;
        })
        .filter(cl => {
          return (
            cl.Description.ContainsMatch ||
            cl.Categories.find(({ Description }) => Description.ContainsMatch) != null ||
            cl.Items.find(({ Description }) => Description.ContainsMatch) != null
          );
        });

      return results;
    }

    async _getChecklists() {
      let checklists = await api(`/d2l/api/le/1.40/${orgid}/checklists/`, {
        fresh: true,
        first: true,
      }).then(data => data && data.Value && data.Value.Objects);
      return Promise.all(
        (checklists || []).map(async checklist => {
          const [categories, items] = await Promise.all([
            api(`/d2l/api/le/1.40/${orgid}/checklists/${checklist.ChecklistId}/categories/`, {
              first: true,
              fresh: true,
            }).then(data => data && data.Value && data.Value.Objects),
            api(`/d2l/api/le/1.40/${orgid}/checklists/${checklist.ChecklistId}/items/`, { first: true, fresh: true }).then(
              data => data && data.Value && data.Value.Objects
            ),
          ]);

          checklist.Categories = categories;
          checklist.Items = items;
          return checklist;
        })
      );
    }
  }

  class TableOfContents {
    _toc = [];
    config = {
      /** Make search case sensitive */
      caseSensitive: false,
      /** Check element attributes */
      attributes: true,
      /** Check HTML pages only */
      html: false,
      /** Check only visible pages */
      visible: false,
      /** Run search as regular expression */
      regex: false,
      /** Include topic and module descriptions in search criteria */
      descriptions: false,
    };

    constructor(settings = this.config, files) {
      this.config = {
        ...this.config,
        ...settings,
      };

      this.mf = files;
    }

    /** A collection of unit resources found from the content. */
    get linkedFiles() {
      return this.mf;
    }

    async search({ search, links = true, text = true, returnAll = false, each = () => {} } = {}) {
      let moduleDescriptions = [];
      let iterator = () => {};

      if (this.config.descriptions) {
        // Only run our description-grabbing iterator if we have to.
        // This should keep our performance up if we don't want descriptions.
        iterator = item => {
          if (item.ModuleId != null) {
            const dom = new DOMParser().parseFromString(item.Description.Html, 'text/html');
            let links = Array.from(dom.querySelectorAll('[href],[src]')).map(a => a.href || a.src);
            moduleDescriptions.push({
              ModuleId: item.ModuleId,
              Title: item.Title,
              Description: item.Description.Text,
              Links: links,
            });
          }

          item.Description = {
            text: (item.IsBroken ? '' : item.Description.Text) || '',
            links: FindAttributeLinksInString(item.Description.Html || ''),
          };
          return item;
        };
      }

      let files = this._iterate(await this.toc(), iterator);
      if (this.config.visible) {
        // Only run the filter if we need to.
        files = files.filter(topic => !topic.IsHidden);
      }

      const flaggedLink = link => {
        const host = new URL(link, window.location.origin).host;
        if (this.config.regex) return RegExp(search, 'gmi').test(host);
        else return host.includes(search);
      };

      const matchingLinks = links => links.filter(flaggedLink);

      let index = 0;
      const results = await Promise.all(
        files.map(async file => {
          let results = {
            links: [],
            text: { found: false, count: 0 },
          };

          let found = false;

          const bail = () => {
            each(++index, files.length);

            if (found) {
              file.SearchResults = results;
            }
            return file;
          };

          const titleMatch = contains(file.Title, search, this.config.regex, this.config.caseSensitive);
          if (titleMatch.found) {
            results.text = titleMatch;
            found = true;
            return bail();
          }

          if (this.config.descriptions) {
            if (text) results.text = contains(file.Description.text, search, this.config.regex, this.config.caseSensitive);
            if (links) results.links = matchingLinks(file.Description.links);

            if (results.text?.found || results.links?.length > 0) {
              found = true;
              return bail();
            }
          }

          if (file.Url && file.Url.startsWith('/content/enforced')) {
            const ext = file.Url.split('.').pop();
            this.mf.known.add(file.Url);

            // Not a supported document type.
            if (!Document.extensions.includes(ext)) return bail();

            // If we only want to check HTML, skip any other type of extension.
            if (this.config.html && !['.htm', '.html'].includes(ext)) return bail();

            results = await Document.search(
              {
                src: file.Url,
                search,
                text,
                links,
                regex: this.config.regex,
                caseSensitive: this.config.caseSensitive,
              },
              links => {
                const { href } = new URL(file.Url, window.location.href);
                const relative = links
                  .filter(l => l)
                  .map(link => {
                    try {
                      return new URL(link, href);
                    } catch (e) {
                      return null;
                    }
                  })
                  .filter(link => link && link.pathname.startsWith('/content/enforced'));
                const documents = relative.filter(link => link.pathname.endsWith('pdf') || link.pathname.endsWith('docx'));
                documents.forEach(doc => this.mf.addFile(doc, file));
              }
            );
            found = true;
          }

          return bail();
        })
      );

      let matchingModules = moduleDescriptions
        .filter(module => {
          return (
            (text && contains(module.Title, search, this.config.regex, this.config.caseSensitive).found) ||
            (links && matchingLinks(module.Links).length > 0)
          );
        })
        .map(module => {
          module.Links = matchingLinks(module.Links);
          return module;
        });

      let matchingTopics = files.filter(file => file.Url && file.Url.startsWith('http') && flaggedLink(file.Url));
      if (returnAll) return results.concat(matchingModules, matchingTopics);
      return results
        .filter(
          ({ SearchResults }) =>
            SearchResults &&
            ((SearchResults.text && SearchResults.text.found) || (SearchResults.links && SearchResults.links.length > 0))
        )
        .concat(matchingModules, matchingTopics);
    }

    async toc() {
      if (this._toc.length > 0) {
        return this._toc;
      }

      let res = await api(`/d2l/api/le/1.40/${orgid}/content/toc?ignoreModuleDateRestrictions=true`, {
        fresh: true,
        first: true,
      });

      this._toc = res.Modules;
      return this._toc;
    }

    _iterate(base, onEach = () => {}) {
      let output = [],
        start = base;
      const loop = node => {
        onEach(node);
        if (node.Modules.length > 0) node.Modules.forEach(loop);
        if (node.Topics.length > 0)
          output = output.concat(
            node.Topics.map(topic => {
              topic.IsHidden = topic.IsHidden || node.IsHidden;
              topic.ParentModule = {
                ModuleId: node.ModuleId,
                Title: node.Title,
              };

              topic = onEach(topic) || topic;
              return topic;
            })
          );
      };
      if (Array.isArray(start)) start.forEach(loop);
      else loop(start);
      return output;
    }
  }

  class ManageFiles {
    /** A collection of files to check */
    files = new Map();
    known = new Set();

    config = {
      /** Make search case sensitive */
      caseSensitive: false,
      /** Check element attributes */
      attributes: true,
      /** Check HTML pages only */
      html: false,
      /** Run search as regular expression */
      regex: false,
    };

    constructor(settings = this.config) {
      this.config = {
        ...this.config,
        ...settings,
      };
    }

    get untested() {
      let newItems = new Map();
      for (const [url, src] of this.files) {
        if (!this.known.has(url.pathname)) {
          newItems.set(url, src);
        }
      }
      return newItems;
    }

    addFile(path, origin) {
      if (this.files.has(path.pathname)) {
        let stack = this.files.get(path.pathname);
        stack.origins.push(origin);
        this.files.set(path.pathname, stack);
      } else {
        this.files.set(path.pathname, { path, origins: [origin] });
      }
    }

    async search({ search, links = true, text = true, each = () => {} } = {}) {
      return Promise.all(
        Array.from(this.untested.values()).map(async ({ path, origins }, index) => {
          const ext = path.pathname.split('.').pop();

          // Not a supported document type.
          if (!Document.extensions.includes(ext)) return;

          // If we only want to check HTML, skip any other type of extension.
          if (this.config.html && !['.htm', '.html'].includes(ext)) return;

          let results = await Document.search({
            src: decodeURI(path.href),
            search,
            text,
            links,
            regex: this.config.regex,
            caseSensitive: this.config.caseSensitive,
          });

          each(index, this.untested.size);
          return {
            url: path,
            origins,
            results,
          };
        })
      ).then(data =>
        data.filter(
          value =>
            value &&
            value.results &&
            ((value.results.text != null && value.results.text.found) ||
              (value.results.links != null && value.results.links.length > 0))
        )
      );
    }
  }

  root.querySelector('[name="search_case_sensitive"]').addEventListener('change', e => {
    const checked = e.target.checked;
    root.querySelector('.regex_placeholder').setAttribute('data-regex-flags', checked ? 'm' : 'mi');
  });

  root.querySelector('[name="search_regex"]').addEventListener('change', e => {
    const checked = e.target.checked;
    root.querySelector('.regex_placeholder').setAttribute('data-type', checked ? 'regex' : 'plaintext');
  });

  const search = async ({ flags, search } = {}) => {
    try {
      flags = {
        caseSensitive: root.querySelector('[name="search_case_sensitive"]').checked,
        attributes: root.querySelector('[name="search_attributes"]').checked,
        html: root.querySelector('[name="only_html"]').checked,
        visible: root.querySelector('[name="visible_only"]').checked,
        regex: root.querySelector('[name="search_regex"]').checked,
        descriptions: root.querySelector('[name="search_descriptions"]').checked,
        showFoundLinks: false,
        checkLinksOnly: false,
        ...flags,
      };

      let searchText = search || root.querySelector('d2l-input-text').value.trim();
      if (!flags.caseSensitive) searchText = searchText.toLowerCase();
      if (searchText.length == 0) return;
      if (searchText.length < 3) {
        alert('Please enter three characters or more.');
        return;
      }

      let timer = {
        start: Date.now(),
        end: 0,
        get duration() {
          return this.end - this.start;
        },
      };

      widget.table.innerHTML = '';

      root.querySelectorAll('[data-disable-on-searching="true"]').forEach(el => (el.disabled = true));
      widget.loader.show();
      widget.loader.indeterminate = true;
      widget.resultOrder.setAttribute('hidden', true);
      widget.actionsRow.classList.add('hidden');

      const files = new ManageFiles(flags);

      const toc = new TableOfContents(flags, files);
      const checklists = new Checklist(flags, files);

      const [contentResults, checklistResults] = await Promise.all([
        toc.search({
          search: searchText,
          links: flags.attributes,
          text: !flags.checkLinksOnly,
          each: (i, total) => {
            widget.loader.indeterminate = false;
            widget.loader.value = i;
            widget.loader.max = total;
          },
        }),
        checklists.search({
          search: searchText,
          links: flags.attributes,
          text: !flags.checkLinksOnly,
        }),
      ]);

      // const checklistResults = await checklists.search({
      //   search: searchText,
      //   links: flags.attributes,
      //   text: !flags.checkLinksOnly,
      // });

      const filesResults = await files.search({
        search: searchText,
        links: flags.attributes,
        text: !flags.checkLinksOnly,
      });

      let overallResults = contentResults.length + filesResults.length + checklistResults.length;

      if (overallResults == 0) {
        let row = document.createElement('div');
        row.innerHTML = `<p class="no-results">No matches found.</p>`;
        widget.table.appendChild(row);
      } else {
        widget.resultOrder.removeAttribute('hidden');
        widget.actionsRow.classList.remove('hidden');
        timer.end = Date.now();
        widget.resultOrder.querySelector('.timer').innerHTML = `${overallResults} matches found in ${timer.duration}ms.`;

        const container = document.createElement('div');
        /**
         *         const heading = document.createElement('h3');
         *         heading.classList.add('unit-name-heading', 'search-module-heading');
         *         heading.innerText = name;
         *
         *         container.append(heading);
         */
        widget.table.append(container);

        if (contentResults.length > 0) {
          const modules = new Map();
          contentResults.forEach(item => {
            const moduleId = item.ModuleId || item.ParentModule.ModuleId;
            if (modules.has(moduleId)) {
              let stack = modules.get(moduleId);
              stack.push(item);
              modules.set(moduleId, stack);
            } else {
              modules.set(moduleId, [item]);
            }
          });

          const contentHeading = document.createElement('h2');
          contentHeading.classList.add('search-module-heading', 'content-heading');
          contentHeading.innerText = 'Content Results';
          container.append(contentHeading);

          modules.forEach(module => {
            const title = module[0]?.ParentModule.Title || module[0].Title;

            const moduleTitle = document.createElement('h3');
            moduleTitle.classList.add('search-module-heading', 'module-heading');
            moduleTitle.innerText = title;
            container.append(moduleTitle);

            module.forEach(item => {
              const row = document.createElement('div');
              row.classList.add('data-row');
              let type = '';

              const IsModule = item.TopicId == null;

              const LinkType = href => {
                if (/youtu.?be/i.test(href)) return 'YouTube';
                if (/echo360/i.test(href)) return 'Echo360';

                const url = new URL(href, window.location.origin);
                const host = url.host.split('.');
                if (/www/i.test(host[0])) {
                  host.shift();
                }

                return host.join('.');
              };

              if (IsModule) type = 'Module';
              else if (item.TypeIdentifier == 'Link') type = `Topic Item | ${LinkType(item.Url)}`;
              else if (item.Url && item.Url.split('.').pop().startsWith('htm')) type = 'Web page';
              else if (item.Url && item.Url.split('.').pop().length > 0) type = item.Url.split('.').pop();

              if (type.length > 0) type = `<span class="ext"> [${type}]</span>`;

              const links = (IsModule && item.Links) || (item.TopicId && item.SearchResults?.links) || [];

              let sub = '';
              if (flags.showFoundLinks && links.length > 0) {
                sub = `<ul class="sublist">${links
                  .map(link => {
                    if (link.startsWith('//')) link = 'https:' + link;
                    return `<li><a class="d2l-link searched-link-result" title="Visit page in new tab" target="_blank" href="${link}">${link}</a></li>`;
                  })
                  .join('\n')}</ul>`;
              }

              let url = IsModule
                ? `/d2l/le/content/${orgid}/Home?itemIdentifier=D2L.LE.Content.ContentObject.ModuleCO-${item.ModuleId}`
                : `/d2l/le/content/${orgid}/viewContent/${item.TopicId}/View`;

              const link = document.createElement('a');
              link.href = url;
              link.target = '_blank';
              link.innerText = IsModule ? 'Topic Description' : item.Title;

              const results = document.createElement('span');
              results.innerHTML = `${type}${sub}`;

              row.append(link, results);
              container.append(row);
            });
          });
        }

        if (checklistResults.length > 0) {
          const contentHeading = document.createElement('h3');
          contentHeading.classList.add('search-module-heading', 'content-heading');
          contentHeading.innerText = 'Checklist Results';
          container.append(contentHeading);

          checklistResults.forEach(checklist => {
            const generateBlock = (text, url, links) => {
              const row = document.createElement('div');
              row.classList.add('data-row');
              let sub = '';

              if (flags.showFoundLinks && links.length > 0) {
                sub = `<ul class="sublist">
                ${links
                  .map(link => {
                    if (link.startsWith('//')) link = 'https:' + link;
                    return `<li><a class="d2l-link searched-link-result" title="Visit page in new tab" target="_blank" href="${link}">${link}</a></li>`;
                  })
                  .join('\n')}
                </ul>`;
              }

              const link = document.createElement('a');
              link.href = url;
              link.target = '_blank';
              link.innerText = text;

              const results = document.createElement('span');
              results.innerText = `${sub}`;

              row.append(link, results);
              container.append(row);
            };

            if (checklist.Description.ContainsMatch) {
              const contentHeading = document.createElement('h3');
              contentHeading.classList.add('search-module-heading');
              contentHeading.innerText = checklist.Name;
              container.append(contentHeading);

              generateBlock(
                'Checklist Description',
                `/d2l/lms/checklist/checklist.d2l?checklistId=${checklist.ChecklistId}&ou=${orgid}`,
                checklist.Description.MatchingLinks
              );
            }

            const BadCategories = checklist.Categories.filter(cat => cat.Description.ContainsMatch);
            const BadItems = checklist.Items.filter(item => item.Description.ContainsMatch);

            if (BadCategories.length > 0) {
              const contentHeading = document.createElement('h3');
              contentHeading.classList.add('search-module-heading');
              contentHeading.innerText = `${checklist.Name} - Categories`;
              container.append(contentHeading);

              BadCategories.forEach(category => {
                generateBlock(
                  category.Name,
                  `/d2l/lms/checklist/editcategory.d2l?categoryId=${category.CategoryId}&checklistId=${checklist.ChecklistId}&ou=${orgid}`,
                  category.Description.MatchingLinks
                );
              });
            }

            if (BadItems.length > 0) {
              const contentHeading = document.createElement('h3');
              contentHeading.classList.add('search-module-heading');
              contentHeading.innerText = `${checklist.Name} - Items`;
              container.append(contentHeading);

              BadItems.forEach(item => {
                generateBlock(
                  item.Name,
                  `/d2l/lms/checklist/edititem.d2l?itemId=${item.ChecklistItemId}&checklistId=${checklist.ChecklistId}&ou=${orgid}`,
                  item.Description.MatchingLinks
                );
              });
            }
          });
        }

        if (filesResults.length > 0) {
          container.append(
            ...html`<h2 class="search-module-heading">Linked Results</h2>
              <p>These are documents that were found linked within HTML content or item descriptions.</p>`
          );

          filesResults.forEach(file => {
            const row = document.createElement('div');
            row.classList.add('data-row');
            let type = '';

            if (file.url.pathname.split('.').pop().startsWith('htm')) type = 'HTML';
            else if (file.url.pathname.split('.').pop().length > 0) type = file.url.pathname.split('.').pop();

            if (type.length > 0) type = html`<span class="ext"> [${type}]</span>`;

            let sub = '';
            if (flags.showFoundLinks && file.results.links.length > 0) {
              sub = html`<ul class="sublist">
                ${file.results.links
                  .map(link => {
                    if (link.startsWith('//')) link = 'https:' + link;
                    return `<li><a class="d2l-link searched-link-result" title="Visit page in new tab" target="_blank" href="${link}">${link}</a></li>`;
                  })
                  .join('\n')}
              </ul>`;
            }

            let sources = [];
            const makeLink = (link, text) =>
              `<a href="${link}" class="d2l-link" target="_blank" title="Visit page in new tab">${text}</a>`;
            file.origins.forEach(origin => {
              if (origin.ChecklistItemId) {
                // It's a Checklist Item
                sources.push(
                  makeLink(
                    `/d2l/lms/checklist/edititem.d2l?itemId=${origin.ChecklistItemId}&checklistId=${origin.ChecklistId}&ou=${orgid}`,
                    `[CL Item: ${origin.Name}]`
                  )
                );
              } else if (origin.CategoryId && origin.ChecklistId) {
                // It's a Checklist Category
                sources.push(
                  makeLink(
                    `/d2l/lms/checklist/editcategory.d2l?categoryId=${origin.CategoryId}&checklistId=${origin.ChecklistId}&ou=${orgid}`,
                    `[CL Category: ${origin.Name}]`
                  )
                );
              } else if (origin.ChecklistId) {
                // It's a Checklist
                sources.push(
                  makeLink(
                    `/d2l/lms/checklist/checklist.d2l?checklistId=${origin.ChecklistId}&ou=${orgid}`,
                    `[CL: ${origin.Name}]`
                  )
                );
              } else if (origin.TypeIdentifier == 'File' && origin.TopicId) {
                // It's a Content item
                sources.push(
                  makeLink(`/d2l/le/content/${orgid}/viewContent/${origin.TopicId}/View`, `[Content: ${origin.Title}]`)
                );
              } else if (origin.ModuleId) {
                // It's a Module
                sources.push(
                  makeLink(
                    `/d2l/le/content/${orgid}/Home?itemIdentifier=D2L.LE.Content.ContentObject.ModuleCO-${origin.ModuleId}`,
                    `[Module: ${origin.Title}]`
                  )
                );
              }
            });

            const name = decodeURI(file.url.pathname).replace(/\/content\/enforced\/(.*?)\//i, '');

            const link = document.createElement('a');
            link.target = '_blank';
            link.classList.add('manage-files-link');
            link.innerText = name;
            link.href = file.url.href;

            const src = document.createElement('div');
            src.classList.add('manage-files-sources');
            src.innerHTML = sources.join(' ');

            row.append(link, ...type, src, ...sub);
            container.append(row);
          });
        }
      }
    } catch (e) {
      console.error(e);
    } finally {
      root.querySelectorAll('[data-disable-on-searching="true"]').forEach(el => (el.disabled = false));
      widget.loader.hide();
    }
  };

  const config = {
    flags: {
      caseSensitive: false,
      attributes: true,
      html: false,
      visible: false,
      regex: true,
      descriptions: true,
      showFoundLinks: true,
      checkLinksOnly: true,
    },
    search: 'youtu.?be|vimeo|slideshare|google|twitch.tv',
  };

  widget.findBlocked.addEventListener('click', () => search(config));
  widget.search.addEventListener('click', () => search());
  root.querySelector('d2l-input-text').addEventListener('keydown', e => {
    if (e.key == 'Enter') {
      search();
    }
  });

  /** Handle copying to clipboard */
  let clipboard = new ClipboardJS(widget.copy, {
    target: () => {
      widget.table.querySelector('.unit-name-heading').classList.remove('hidden');
      return widget.table;
    },
  });

  clipboard.on('success', e => {
    widget.table.querySelector('.unit-name-heading').classList.add('hidden');
    alert('Search results copied to clipboard.');
    e.clearSelection();
  });

  clipboard.on('error', console.error);

  /** Handle saving to file */
  function saveToWord() {
    const UTF8ized = document.createElement('html');
    const header = document.createElement('head');
    const charset = document.createElement('meta');
    charset.setAttribute('http-equiv', 'Content-Type');
    charset.setAttribute('content', 'text/html; charset=UTF-8');
    header.appendChild(charset);
    UTF8ized.appendChild(header);

    const body = document.createElement('body');
    body.append(root.querySelector('style').cloneNode(true));
    body.append(widget.table.cloneNode(true));
    body.querySelectorAll('[href],[src]').forEach(el => {
      if (el.href && el.getAttribute('href').startsWith('/')) {
        el.setAttribute('href', window.location.origin + el.getAttribute('href'));
      }
      if (el.src && el.getAttribute('src').startsWith('/')) {
        el.setAttribute('src', window.location.origin + el.getAttribute('src'));
      }
    });

    body.querySelector('.unit-name-heading').classList.remove('hidden');
    UTF8ized.appendChild(body);

    const blob = htmlDocx.asBlob(UTF8ized.innerHTML, {
      orientation: 'portrait',
      margins: {
        top: 720,
        bottom: 720,
        left: 720,
        right: 720,
      },
    });

    saveAs(blob, `${codes.join('-')}-search-results.docx`);
  }
  widget.save.addEventListener('click', saveToWord);
}
