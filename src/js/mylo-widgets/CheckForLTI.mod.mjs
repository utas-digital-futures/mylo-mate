const { Subject } = rxjs;

export const CheckPages = (urls, { tags = ['a', 'iframe'], filters = [], ignore = [] }) => {
  if (!Array.isArray(filters)) throw new Error('Filters must be an array.');

  filters = filters.filter(v => v.trim?.().length);
  if (filters.length == 0) throw new Error('There must be at least one filter with a non-zero length.');

  const notHref = ignore.length == 0 ? '' : `:not(${ignore.map(i => `[href*="${i}" i]`).join(',')})`;
  const notSrc = ignore.length == 0 ? '' : `:not(${ignore.map(i => `[src*="${i}" i]`).join(',')})`;
  const filter = tags.flatMap(tag => {
    return filters.flatMap(filter => {
      return [`${tag}[href*="${filter}" i]${notHref}`, `${tag}[src*="${filter}" i]${notSrc}`];
    });
  });

  const subj = new Subject();
  Promise.all(
    urls.map(async ({ key, href }) => {
      href = href.replace(/#/g, '%23');
      try {
        const src = await fetch(href);

        if (!src.ok) {
          console.error(src, href);
          throw new Error('Error fetching page source.', { src, url: href });
        }

        const doc = new DOMParser().parseFromString(await src.text(), 'text/html');
        const matches = Array.from(doc.querySelectorAll(filter.join(',')));

        const result = { key, matches: matches.map(el => el.getAttribute('src') ?? el.getAttribute('href')) };
        subj.next(result);

        return result;
      } catch (e) {
        console.error(e);

        const result = { key, matches: [] };
        subj.next(result);
        return result;
      }
    })
  ).then(data => subj.complete(data));

  return subj;
};
