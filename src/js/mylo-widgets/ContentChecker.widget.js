/**
 * @affects MyLO Widgets
 * @problem Ensuring that all links in a unit are functioning correctly can be an overwhelmingly challenging and error-prone task, with a high chance for human error.
 * @solution A widget automatically checks all links found in the Unit Content, as well as links found in web pages, and reports back any links that have issues.
 * @target Academic and support staff
 * @impact High
 * @savings 3 hours per unit
 */

export default async function ({ widget, orgid, config }) {
  const content_box = widget.querySelector('#results');
  const loader = widget.querySelector('mm-loader');
  const progress_label = widget.querySelector('[mm-id="progress-text"]');
  const result_text = widget.querySelector('#results_text');
  const time_taken = widget.querySelector('#time_taken');
  const button = widget.querySelector('#activate');
  const feedback_visibility = widget.querySelector('.feedback-visibility');
  const timeout_cb = widget.querySelector('[name="displayTimeouts"]');

  let canCheckExternalPages = false;

  const errors = {
    '0': {
      title: 'Error fetching page',
      issue: 'An error occured when fetching the page.',
      fix: 'Check your internet connection, and try again. If the issue continues, please check the page manually.',
    },
    '400': {
      title: '400: Bad request',
      issue: 'The server refuses to process the request.',
      fix: 'Ensure that the URL is correct.',
    },
    '403': {
      title: '403: Access denied',
      issue: `You don't have permission to access this page. Please ensure the link is still correct, and that it doesn't require login to access.`,
      fix: 'Ensure the link is valid.',
    },
    '404': {
      title: '404: Page not found',
      issue: `Page could not be found, it has probably been moved or deleted. You'll need to replace this link.`,
      fix: 'Replace this link with a new URL.',
    },
    '410': {
      title: '410: Gone',
      issue: `Page no longer exists. You'll need to replace this link.`,
      fix: 'Replace this link with a new URL.',
    },
    '500': {
      title: '500: Server error',
      issue: `Something has gone wrong with the remote server. Please check again later, or try a different link.`,
      fix: 'Check again later, or replace the link.',
    },
    '503': {
      title: '503: Service Unavailable',
      issue: `The server is currently unable to handle the request due to a temporary overload or scheduled maintenance, which will likely be alleviated after some delay.`,
      fix: 'Check again later, or replace the link.',
    },
    '522': {
      title: '522: Connection timeout',
      issue: 'The connection timed out while trying to check the page.',
      fix: 'Test the link manually, and replace if needed.',
    },
    '999': {
      title: 'Connection timeout',
      issue: 'The connection timed out while trying to check the page.',
      fix: 'Test the link manually, and replace if needed.',
    },
    '-2': { title: 'Invisible Link', issue: 'A link was found that contains no text.', fix: 'Remove this empty link.' },
    '-3': {
      title: 'Bad Target',
      issue: `This link won't open in a MyLO page, for security reasons.`,
      fix: 'Change the link to open in new tab.',
    },
    '-8': {
      title: 'MyLO Site',
      issue:
        'This is an absolute link to the current site, and while it will work for this delivery, it will not work for the next delivery.',
      fix: 'Change the link to a relative link.',
    },
    '-9': {
      title: 'MyLO Site',
      issue: 'This is a link to another MyLO site that students may not have access to.',
      fix: 'Check the link is valid for your cohort.',
    },
  };

  const updateFeedbackVisibility = () => {
    const value = feedback_visibility.querySelector('input:checked').value;
    if (!value) return;

    const isHidden = value == 'hidden';

    widget.querySelectorAll('.feedback-container .issue,.feedback-container .fix').forEach(el => (el.hidden = isHidden));
  };

  const toggleTimeoutErrors = () => {
    const value = timeout_cb.checked;

    widget.querySelectorAll('#mm_content_checker_results_list tr[data-status="999"]').forEach(el => {
      el.hidden = !value;
    });
  };

  feedback_visibility.addEventListener('change', updateFeedbackVisibility);
  timeout_cb.addEventListener('change', toggleTimeoutErrors);

  const mkdwn = (strings, ...args) => {
    let correct = '';
    for (let i = 0; i < strings.length; i++) {
      correct += strings[i] + (args?.[i] ?? '');
    }

    const patterns = [
      ['\\*\\*(.*?)\\*\\*', '<strong>$1</strong>'],
      ['\\*(.*?)\\*', '<em>$1</em>'],
      ['\\[(.*?)\\]\\((.*?)\\)', '<a href="$2" target="_blank">$1</a>'],
    ];

    for (const [match, replace] of patterns) {
      correct = correct.replace(new RegExp(match, 'gim'), replace);
    }

    const p = document.createElement('p');
    p.innerHTML = correct;
    return p;
  };

  const buildStatus = (error, pageTitle = '') => {
    const container = document.createElement('div');
    container.classList.add('feedback-container');

    let err = errors[error];
    if (err == null) {
      err = {
        title: error,
        issue: 'An error occured when fetching the page.',
        fix: 'Check your internet connection, and try again. If the issue continues, please check the page manually.',
      };
    }

    const { title, issue, fix } = err;

    const header = document.createElement('strong');
    header.innerText = title;

    const desc = document.createElement('p');
    desc.classList.add('issue');
    desc.innerHTML = `<em>Issue:</em> ${issue}`;

    const solution = document.createElement('p');
    solution.classList.add('fix');
    solution.innerHTML = `<em>Fix:</em> ${fix}`;

    container.append(header, desc, solution);

    if (pageTitle?.trim().length > 0) {
      const wrapper = document.createElement('div');
      wrapper.classList.add('wrapper');

      const hdr = mkdwn`**Issue with hyperlinked text in content:** "${pageTitle.trim()}"`;

      wrapper.append(hdr, container);
      return wrapper;
    } else {
      return container;
    }
  };

  function iterate(base) {
    let output = [];
    const start = base;
    const loop = node => {
      if (node.Modules.length > 0) node.Modules.forEach(loop);
      if (node.Topics.length > 0)
        output = output.concat(
          node.Topics.map(topic => {
            topic._parent = node;
            return topic;
          })
        );
    };
    if (Array.isArray(start)) start.forEach(loop);
    else loop(start);
    return output;
  }

  const CheckLinks = {
    Internal(links, increment) {
      if (!Array.isArray(links)) links = [links];
      return Promise.all(
        links.map(async link => {
          try {
            link.status = await fetch(link.href, { method: 'HEAD' }).then(d => d.status);
          } catch (e) {
            /** console.info('Error fetching info for %s: ', link.href, e); */
            link.status = 0;
          }
          if (link.status == 999) link.status = 403;
          if (increment) loader.value++;

          return link;
        })
      );
    },
    External(links, increment) {
      if (!canCheckExternalPages) return [];
      links = Array.isArray(links) ? links : [links];
      return Promise.all(
        links.map(
          link =>
            new Promise(resolve =>
              chrome.runtime.sendMessage(extensionID, { request: 'link-check', data: link }, data => {
                if (increment) {
                  loader.value++;
                }
                resolve(data);
              })
            )
        )
      );
    },
  };

  const check_local_files = async file => {
    const source = await fetch(file.href).then(d => d.text());
    const html = new DOMParser().parseFromString(source, 'text/html');

    const abs = html.querySelectorAll(
      [
        `a[href*="/content/enforced/" i]`, // Any link that's hardcoded to a path in a unit
        `a[href*="/d2l/" i]:not([href*="${orgid}"]):not([href*="/d2l/lor/"])`, // Any link to a mylo page that doesn't appear to be within this unit.
      ].join(',')
    );

    const internal = [];
    let external = [];
    const links_out = Array.from(html.querySelectorAll('a[href^="http"]')).map(a => ({
      href: a.href.trim(),
      Title: a.innerText,
      target: a.target,
    }));
    links_out.forEach(link => {
      if (link.href.startsWith('/') || link.href.includes(window.location.hostname)) {
        internal.push(link);
      } else {
        external.push(link);
      }
    });
    external.forEach(link => {
      link.lead_out = true;
      // If the link matches the description of `won't open`.
      if (link.href.startsWith('http://') && link.target != '_blank') {
        if (link.href.includes('browsehappy')) return;
        link.lead_out = false;
      }
    });

    [, external] = await Promise.all([CheckLinks.Internal(internal, false), CheckLinks.External(external, false)]);

    const unitError = [];
    const unitWarning = [];

    if (abs.length > 0) {
      const hrefs = Array.from(abs).map(a => ({ href: a.href, title: a.innerText }));

      unitWarning.push(...hrefs.filter(v => v.href.includes(`${orgid}-`)));
      unitError.push(...hrefs.filter(v => !v.href.includes(`${orgid}-`)));
    }

    file.links = { internal, external, unitError, unitWarning };

    loader.value++;
    return file;
  };

  const check_links = async () => {
    const toc = await api(`/d2l/api/le/1.28/${orgid}/content/toc?ignoreModuleDateRestrictions=true`, { first: true });
    const to_check = iterate(toc.Modules)
      .filter(
        link =>
          link.TypeIdentifier == 'Link' ||
          (link.TypeIdentifier == 'File' && link.Url != null && link.Url.startsWith('/content/enforced'))
      )
      .map(obj => ({
        TopicId: obj.TopicId,
        ModuleId: obj._parent.ModuleId,
        Title: obj.Title,
        href: obj.Url.trim(),
      }));

    const internal = [];
    let external = [];
    to_check.forEach(link => {
      if (link.href.startsWith('/') || link.href.includes(window.location.hostname)) {
        internal.push(link);
      } else {
        external.push(link);
      }
    });

    const local_html = internal.filter(link => link.href.endsWith('.html'));

    loader.value = 0;
    loader.max = to_check.length + local_html.length;

    progress_label.innerText = 'Checking Content links...';

    // Gets the HTTP STATUS response for each link, and attaches it to the respective item.
    [, external] = await Promise.all([CheckLinks.Internal(internal, true), CheckLinks.External(external, true)]);

    progress_label.innerText = 'Checking links in HTML pages...';
    // Gets the HTML Source for each internal .html file, and attaches it to the respective item.
    await Promise.all(local_html.map(check_local_files));

    /** console.log(local_html, internal); */

    const issues = external.map(link => ({ ...link, Type: 'Link' })).filter(link => link.status >= 400 || link.status == 0);
    internal.forEach(item => {
      if (item.status >= 400 || item.status == 0) issues.push({ ...item, Type: 'Link' });
      else if (item.links?.unitError?.length > 0 || item.links?.unitWarning?.length > 0) {
        issues.push({ ...item, Type: 'File' });
      } else {
        if (
          item.links &&
          item.links.external.filter(link => !link.lead_out || link.status >= 400 || link.status == 0).length > 0
        )
          issues.push({ ...item, Type: 'File' });
        if (item.links && item.links.internal.filter(link => link.status >= 400 || link.status == 0).length > 0)
          issues.push({ ...item, Type: 'File' });
      }
    });

    return issues;
  };

  let _cache = null;
  const render_errors = (issues, { show_timeouts = timeout_cb.checked } = {}) => {
    /**
     * if (content_box.querySelector('#mm_content_checker_results_list')) {
     *   content_box.querySelector('#mm_content_checker_results_list').remove();
     * }
     */

    issues = issues.filter(i => !i.Title.startsWith('/d2l') && !i.href.startsWith('/d2l/common/dialogs'));

    /**
     * if (!show_timeouts) {
     *   issues = issues.filter(
     *     issue =>
     *       issue.status != 999 &&
     *       (issue.links && issue.links.internal ? issue.links.internal.every(l => l.status != 999) : true) &&
     *       (issue.links && issue.links.external ? issue.links.external.every(l => l.status != 999) : true)
     *    );
     * }
     */

    if (issues.length > 0) {
      const links = issues.filter(issue => issue.Type == 'Link');
      const files = issues.filter(issue => issue.Type == 'File');
      const total = links.length + files.length;

      /** console.log({ links, files }); */

      const messages = [];
      if (links.length > 0) messages.push(`${links.length} link${links.length > 1 ? 's' : ''}`);
      if (files.length > 0) messages.push(`${files.length} file${files.length > 1 ? 's' : ''}`);
      result_text.innerText = `Issue${total > 1 ? 's' : ''} found with ${messages.join(' and ')}.`;

      const table = document.createElement('table');
      table.id = 'mm_content_checker_results_list';

      links.forEach(item => {
        const tr = document.createElement('tr');
        tr.dataset.status = item.status;
        const href = `/d2l/le/content/${orgid}/Home?itemIdentifier=D2L.LE.Content.ContentObject.ModuleCO-${item.ModuleId}`;

        tr.innerHTML = `
        <td class="page-name link-error"> 
          <a class="d2l-link" target="_blank" title="Open the parent module" href="${href}">
            <span class="linktext">${item.Title}</span>
          </a> 
        </td>
        <td class="issue-type"> <d2l-icon icon="tier1:broken-link"></d2l-icon>  <span>link</span></td>
        `;

        const msg = document.createElement('td');
        msg.classList.add('error');
        msg.append(buildStatus(item.status));
        tr.append(msg);

        table.appendChild(tr);
      });

      files.forEach(item => {
        let link_list = [];
        if (item.links) link_list = item.links.internal.concat(item.links.external);

        link_list = link_list
          .flatMap(link => {
            let errors = [];
            if (link.Title.trim().length == 0) {
              errors.push('-2');
            }
            if (link.href.startsWith('http:') && link.target != '_blank') {
              errors.push('-3');
            }
            if (link.status < 1 || link.status > 400) {
              errors.push(link.status);
            }

            return { errors, title: link.Title };
          })
          .filter(t => t != null && t.errors.length > 0)
          .concat(
            item.links.unitWarning.map(v => {
              v.errors = [-8];
              return v;
            }),

            item.links.unitError.map(v => {
              v.errors = [-9];
              return v;
            })
          )
          .flat();
        if (link_list.length == 0) return;

        const href = {
          view: `/d2l/le/content/${orgid}/viewContent/${item.TopicId}/View`,
          edit: `/d2l/le/content/${orgid}/contentfile/${item.TopicId}/EditFile?fm=0`,
        };

        const tr = document.createElement('tr');
        tr.dataset.status = item.status;
        tr.innerHTML = `
					<td class="page-name html-error">
            <a target="_blank" title="Open ${item.Title}" href="${href.view}">
              <span class="linktext">${item.Title}</span>
            </a> 
						<a target="_blank" title="Edit ${item.Title}" href="${href.edit}">[edit page]</a>
          </td>

          <td class="issue-type"> <d2l-icon icon="tier1:feed"></d2l-icon> <span>web page</span></td>
        `;

        const msg = document.createElement('td');
        msg.classList.add('error');
        msg.append(...link_list.flatMap(({ errors, title }) => errors.map(error => buildStatus(error, title))));
        tr.append(msg);
        table.appendChild(tr);
      });

      content_box.appendChild(table);
    } else {
      result_text.innerText = 'No issues found.';
    }
  };

  /**
   * timeout_cb.addEventListener('change', e => {
   *   if (_cache == null) return;
   *   render_errors(_cache, { show_timeouts: timeout_cb.checked });
   * });
   */

  const Alert = text =>
    new Promise(res => {
      const alert = document.createElement('mm-alert');
      alert.innerText = text;
      document.body.append(alert);

      alert.addEventListener('ok', res);
      alert.show();
    });

  button.addEventListener('click', async e => {
    try {
      // Do we have permission to check the links?
      let hasPermission = await new Promise(res =>
        chrome.runtime.sendMessage(extensionID, { request: 'link-checker-has-permissions' }, res)
      );

      if (!hasPermission) {
        // If we don't already have permission, inform the user of what's going on, and why we're asking for permission to everything.
        // Then, request permission.
        await Alert(
          "MyLO MATE doesn't have the necessary permissions to check external links. A dialog will appear asking for permission to access all pages; if you wish to check external links, please click 'Allow'."
        );
        hasPermission = await new Promise(res =>
          chrome.runtime.sendMessage(extensionID, { request: 'link-checker-get-permission' }, res)
        );
      }

      // This will be whether we can actually check externally or not.
      canCheckExternalPages = hasPermission;

      // If we still don't have permission, then give them the option of just checking internal pages.
      if (!hasPermission) {
        if (
          !confirm(
            'Permission was denied for checking external pages. Please click OK if you wish to just check internal links, or Cancel to stop the process.'
          )
        ) {
          return; // They don't want to check anything anymore.
        }
      }

      _cache = null;
      /** const timeStart = Date.now(); */
      button.disabled = true;
      loader.show();
      result_text.innerText = '';
      /** time_taken.innerText = ''; */

      /**
       * if (content_box.querySelector('#mm_content_checker_bad_links_results_list')) {
       *   content_box.querySelector('#mm_content_checker_bad_links_results_list').remove();
       * }
       */

      content_box.innerHTML = '';
      loader.show();
      progress_label.innerText = 'Fetching links from Content...';

      const issues = await check_links();
      render_errors(issues);
      _cache = issues;

      updateFeedbackVisibility();
      toggleTimeoutErrors();

      loader.hide();

      const heading = document.createElement('h2');
      heading.innerText = 'Issues found';
      result_text.prepend(heading);

      progress_label.innerText = '';
      const timeEnd = Date.now();
      /** time_taken.innerText = `Request completed in ${Math.ceil(timeEnd - timeStart)}ms`; */
      button.disabled = false;
    } catch (e) {
      button.disabled = false;
      console.error(e);
      await Alert(
        'Something went wrong while fetching the results. Please refresh and try again.\n\nIf this error continues, please contact MyLO MATE support.'
      );
    }
  });
}
