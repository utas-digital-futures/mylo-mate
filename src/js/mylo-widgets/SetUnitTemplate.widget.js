/**
 * @affects MyLO Widgets
 * @problem
 *  It's not obvious how to (or whether one should) set the default template for a unit, with most staff unfamiliar with the process.
 *  This has come to light especially with the release of the new D2L Templates.
 * @solution A widget displays the current default template setting, and allows it to be changed directly from a unit home page.
 * @target Support staff
 * @impact High
 * @savings 5 minutes per unit
 */

(async function () {
  const orgid = window.location.href.split('/').pop();
  const dom = document.querySelector('[mm-widget-name="SetUnitTemplate"]');
  const loader = dom.querySelector('mm-loader');
  const template = dom.querySelector('#template-option');
  const save = dom.querySelector('#save-template-settings');

  const resultText = dom.querySelector('.status');

  const { Path } = await api(`/d2l/api/lp/1.26/courses/${orgid}`, { first: true });

  loader.hidden = false;
  template.hidden = true;

  template.addEventListener('change', e => {
    save.disabled = false;
  });

  async function GetUnitSettings(id, asURLData = false) {
    const dom = new DOMParser().parseFromString(
      await fetch(`/d2l/le/content/${id}/settings/Open`).then(d => d.text()),
      'text/html'
    );
    const form = dom.querySelector('form');
    const fd = new URLSearchParams(new FormData(form));
    if (asURLData) return fd;

    const items = {};
    for (const [k, v] of fd) {
      items[k] = v;
    }
    return items;
  }

  async function SaveUnitSettings(id, changes) {
    const fd = await GetUnitSettings(id, true);
    for (const [k, v] of Object.entries(changes)) {
      fd.set(k, v);
    }

    fd.append('requestId', Date.now() % 10);
    fd.append('d2l_referrer', window.localStorage.getItem('XSRF.Token'));
    fd.append('isXhr', true);

    return fetch(`/d2l/le/content/${id}/settings/Save`, { method: 'post', body: fd });
  }

  const { defaultTemplateIsEnabled: TemplateIsEnabled, defaultTemplatePathSelector: TemplatePath } = await GetUnitSettings(
    orgid
  );

  if (TemplateIsEnabled == '0') {
    // If the template option is disabled
    template.value = 'None';
  } else if (/^\/content\/enforced\/[^\/]+\/template\/$/i.test(TemplatePath)) {
    // If the path matches where the ICB template would live
    template.value = 'ICB';
  } else if (/^\/shared\/content-template\/[^\/]+\/pages\/$/i.test(TemplatePath)) {
    template.value = 'D2L';
  } else if (/^\/content\/enforced\/[^\/]+\/$/i.test(TemplatePath)) {
    template.value = 'None';
  } else {
    template.value = 'Other';
  }

  const RemoteFileExists = url =>
    fetch(url, {
      method: 'head',
      redirect: 'error',
    })
      .then(d => d.status == 200)
      .catch(() => false);

  if (!(await RemoteFileExists(`${Path}template/template.html`))) {
    template.querySelector('option[value="ICB"]').disabled = true;
  }

  save.addEventListener('click', async e => {
    // What should we do if it's Other??
    // Probably nothing. Lets return.
    if (template.value == 'Other') {
      alert('This setting is illogical and cannot be saved.');
      return;
    }

    save.disabled = true;
    resultText.innerText = '';

    const payload = { defaultTemplateIsEnabled: '1', defaultTemplatePathSelector: Path };
    switch (template.value) {
      case 'ICB':
        payload.defaultTemplatePathSelector += 'template/';
        break;
      case 'D2L':
        payload.defaultTemplatePathSelector = '/shared/content-template/v3.0/pages/';
        break;
      case 'None':
        payload.defaultTemplateIsEnabled = '0';
        break;
    }

    await SaveUnitSettings(orgid, payload);

    save.disabled = false;
    resultText.innerText = 'Settings saved successfully.';
  });

  loader.hidden = true;
  template.hidden = false;
})();
