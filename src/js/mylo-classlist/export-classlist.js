(async function () {
  const ou = window.location.searchParams.ou;
  const classlist = api(`/d2l/api/le/1.48/${ou}/classlist/`, { fresh: true }).then(data =>
    data.filter(usr => usr.RoleId == 103)
  );
  const users = api(`/d2l/api/lp/1.29/enrollments/orgUnits/${ou}/users/?roleId=103`, { fresh: true });
  const info = api(`/d2l/api/lp/1.21/enrollments/myenrollments/${ou}`, { first: true });

  let container = document.querySelector('d2l-overflow-group');

  let btn = document.createElement('d2l-button-subtle');
  btn.setAttribute('text', 'Export Student List');
  btn.setAttribute('icon', 'tier1:classlist');
  btn.setAttribute('title', 'Exports a CSV spreadsheet of all students in the classlist');
  btn.setAttribute('mm-icon-render', 'icon');
  btn.setAttribute('mm-icon-type', 'action-navigation');
  btn.setAttribute('mm-icon-offset', '-5:0');

  container.append(btn);

  btn.addEventListener('click', async e => {
    btn.setAttribute('disabled', true);
    try {
      const groupId = prompt('Optional: Enter the Intellilearn Group ID number:');
      const isUsingGroupId = groupId?.trim().length > 0;

      const emails = new Map();
      (await users).forEach(({ User }) => emails.set(User.Identifier, User.EmailAddress));

      const {
        OrgUnit: { Name },
      } = await info;

      const headers = [
        isUsingGroupId ? undefined : 'Student ID',
        'Username',
        isUsingGroupId ? 'Password' : undefined,
        'First Name',
        'Last Name',
        'Email',
        isUsingGroupId ? 'Group ID' : undefined,
        isUsingGroupId ? 'Role' : undefined,
      ].filter(v => v);
      const rows = [];

      (await classlist).forEach(usr => {
        const { FirstName, LastName, Identifier, OrgDefinedId } = usr;
        const Email = emails.get(Identifier);
        if (!Email || Email.trim().length == 0) return;

        rows.push(
          [
            isUsingGroupId ? undefined : OrgDefinedId,
            Email,
            isUsingGroupId ? 'Password1' : undefined,
            FirstName,
            LastName,
            Email,
            isUsingGroupId ? groupId : undefined,
            isUsingGroupId ? 'user' : undefined,
          ]
            .filter(v => v != undefined)
            .map(v => `"${v}"`)
            .join(',')
        );
      });

      const csv = `${headers.map(v => `"${v}"`).join(',')}\r\n${rows.join('\r\n')}`;
      const url = URL.createObjectURL(new Blob([csv]));

      SaveFile(`${Name} - ${ou}.csv`, url);
    } catch (e) {
      console.error(e);
    } finally {
      btn.removeAttribute('disabled');
    }
  });
})();
