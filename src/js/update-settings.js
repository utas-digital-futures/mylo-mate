(async function () {
  const store = async name => (await fetch(chrome.runtime.getURL(`config/${name}.json`))).json();

  // Replaces jQuery's $.extend for JSON objects.
  // function _extend() {
  // 	// Shorthand handler for `arguments` supervar.
  // 	const args = arguments;
  // 	// Go through each argument, ignoring the very first one (the one we're modifying).
  // 	for(let i=1;i<args.length;i++) {
  // 		// For the keys in this argument, go through one by one
  // 		for(let k in args[i]) {
  // 			// If this key is enumerable and useable (instead of, say, a prototype function)
  // 			if(args[i].hasOwnProperty(k)) {
  // 				// If both of these are objects, recurse the function
  // 				if(typeof args[0][k] === 'object' && typeof args[i][k] === 'object') {
  // 					_extend(args[0][k], args[i][k]);
  // 				} else {
  // 					args[0][k] = args[i][k];
  // 				}
  // 			}
  // 		}
  // 	}
  // 	return args[0];
  // }

  chrome.runtime.onInstalled.addListener(async details => {
    chrome.storage.sync.get(null, async res => {
      const defaults = await store('default');
      chrome.storage.sync.set(merge(defaults, res));
    });
  });
})();

