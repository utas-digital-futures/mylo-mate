/**
 * @affects Grades
 * @problem Grade items are commonly hidden from students, usually requiring support staff to adjust the visibility of each item manually.
 * @solution Add a bulk hide and un-hide tool, which almost instantly sets the visibility of the selected grade items.
 * @target Academic and support staff
 * @impact High
 * @savings 1 hour per unit
 */

(async function () {
  const id = document.querySelector('a[href^="/d2l/home"]').href.split('/').pop();
  const store = await new Promise(res => chrome.storage.sync.get(null, res));

  // Find the grade items
  document.querySelectorAll('a[onclick*="gotoNewEditItemProps"]').forEach(a => {
    let tr = a.parentNode.parentNode;
    let id = a.getAttribute('onclick').split('(')[1].split(')')[0].trim();
    let hidden = a.parentNode.querySelector('img[title="Hidden"]') != null;
    tr.setAttribute('data-objid', id);
    tr.setAttribute('data-type', 'item');
    tr.setAttribute('data-hidden', hidden);
  });

  document.querySelectorAll('a[onclick*="gotoNewEditCatProps"]').forEach(a => {
    let tr = a.parentNode.parentNode;
    let id = a.getAttribute('onclick').split('(')[1].split(')')[0].trim();
    let hidden = a.parentNode.querySelector('img[title="Hidden"]') != null;
    tr.setAttribute('data-objid', id);
    tr.setAttribute('data-type', 'cat');
    tr.setAttribute('data-hidden', hidden);
  });

  if (!(store.other.EdTechsMode && store.other.BulkToggleGradeItems)) return;

  const urls = {
    item(obj) {
      return `/d2l/lms/grades/admin/manage/item_rests_edit.d2l?ou=${id}&objectId=${obj}&feedback=2`;
    },
    cat(obj) {
      return `/d2l/lms/grades/admin/manage/category_rests_edit.d2l?ou=${id}&objectId=${obj}&feedback=2`;
    },
  };

  let field = document.querySelector('d2l-overflow-group');

  let hideAll = document.createElement('d2l-button-subtle');
  hideAll.setAttribute('data-testid', 'hide-selected-grades');
  hideAll.setAttribute('text', 'Hide selected');
  hideAll.setAttribute('icon', 'd2l-tier1:visibility-hide');
  hideAll.setAttribute('mm-icon-render', 'icon');
  hideAll.setAttribute('mm-icon-type', 'action-navigation');
  hideAll.setAttribute('mm-icon-offset', '-5:0');
  field.appendChild(hideAll);

  let showAll = document.createElement('d2l-button-subtle');
  showAll.setAttribute('data-testid', 'show-selected-grades');
  showAll.setAttribute('text', 'Show selected');
  showAll.setAttribute('icon', 'd2l-tier1:visibility-show');
  showAll.setAttribute('mm-icon-render', 'icon');
  showAll.setAttribute('mm-icon-type', 'action-navigation');
  showAll.setAttribute('mm-icon-offset', '-5:0');
  field.appendChild(showAll);

  const getSelectedRows = (hidden = false) =>
    Array.from(document.querySelectorAll(`[data-objid][data-hidden=${hidden}] input[name="GradesList_cb"]:checked`)).map(
      cb => cb.parentNode.parentNode
    );

  const processURLs = (urls, value, failIn = 5000) =>
    urls.map(
      url =>
        new Promise(async (res, rej) => {
          MyLOLayer(url, {
            load: async ({ frame, dom, win }) => {
              let handle = setTimeout(rej, failIn);
              if (win.location.href.includes(url)) {
                dom
                  .querySelector(`input[onclick*="hiddenstatuschanged" i]${value == 1 ? ':not(:checked)' : ':checked'}`)
                  .click();
                await new Promise(res => setTimeout(res, 500));
                dom.querySelector('button.d2l-button[primary]').click();
              } else {
                clearTimeout(handle);
                frame.remove();
                res();
              }
            },
          });
        })
    );

  hideAll.addEventListener('click', async () => {
    try {
      let objs = getSelectedRows(false).map(row => urls[row.getAttribute('data-type')](row.getAttribute('data-objid')));
      if (objs.length == 0) {
        return alert('Please select at least one visible grade item to use this feature.');
      }
      document.querySelector('d2l-table-wrapper').classList.add('loading');
      await Promise.all(processURLs(objs, 1, 10 * 1000));
      window.location.reload(true);
    } catch (e) {
      console.error('Remote page timed out while trying to save changes.');
      alert('Something went wrong trying to save the changes.');
    }
  });

  showAll.addEventListener('click', async () => {
    try {
      let objs = getSelectedRows(true).map(row => urls[row.getAttribute('data-type')](row.getAttribute('data-objid')));
      if (objs.length == 0) {
        return alert('Please select at least one hidden grade item to use this feature.');
      }
      document.querySelector('d2l-table-wrapper').classList.add('loading');
      await Promise.all(processURLs(objs, 0, 10 * 1000));
      window.location.reload(true);
    } catch (e) {
      console.error('Remote page timed out while trying to save changes.');
      alert('Something went wrong trying to save the changes.');
    }
  });

  let loader = document.createElement('mm-loader');
  loader.setAttribute('width', '80');
  loader.setAttribute('height', '80');
  document.querySelector('d2l-table-wrapper').appendChild(loader);
})();
