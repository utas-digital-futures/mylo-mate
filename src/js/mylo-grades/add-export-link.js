(async function () {
  await until(
    () =>
      Array.from(document.querySelectorAll('.d2l-buttonmenu-text')).find(t => t.innerText.toLowerCase() == 'more actions') !=
      null
  );
  let dropdown = Array.from(document.querySelectorAll('.d2l-buttonmenu-text')).find(
    t => t.innerText.toLowerCase() == 'more actions'
  );
  dropdown = dropdown.parentNode.parentNode.parentNode;
  dropdown.addEventListener('d2l-dropdown-open', e => {
    let link = document.createElement('d2l-menu-item');
    link.setAttribute('text', 'Export Grades');
    link.setAttribute('role', 'menuitem');
    link.classList.add('d2l-menu-item-last');

    link.addEventListener('click', e => {
      window.open(
        `https://mylo.utas.edu.au/d2l/lms/grades/admin/importexport/export/options_edit.d2l?ou=${window.location.searchParams.ou}`,
        '_blank'
      );
    });

    if (e.target.querySelector('.d2l-menu-item-last')) {
      e.target.querySelector('.d2l-menu-item-last').classList.remove('d2l-menu-item-last');
    }

    e.target.querySelector('d2l-menu').appendChild(link);
  });
})();
