/**
 * @affects Grades
 * @problem When a grade item has a date restriction, MyLO indicates only that it's hidden. The timeframe is only availably by visiting the restrictions tab.
 * @solution Add the start and end date and time to the hover text next to each restricted grade item.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  let rowsWithDateFilter = Array.from(document.querySelectorAll('img[title*="(date restricted)"]'));
  rowsWithDateFilter.forEach(async img => {
    let GradeID = img.parentNode.parentNode
      .querySelector('a[onclick*="gotoNewEditItemProps"]')
      .getAttribute('onclick')
      .split('(')[1]
      .split(')')[0]
      .trim();
    let OrgID = window.location.searchParams.ou;

    let dom = new DOMParser().parseFromString(
      await fetch(`/d2l/lms/grades/admin/manage/item_rests_edit.d2l?ou=${OrgID}&objectId=${GradeID}`).then(d => d.text()),
      'text/html'
    );

    /// Start date info
    let HasStartDateEnabled = dom.querySelector('input[name="RestrictionsDate_cb_sd"]').checked;
    let StartDate = dom.querySelector('#RestrictionsDate_dts_sd').value;
    let StartTime =
      dom.querySelector('input[name="RestrictionsDate_dts_sd$time$hour').value +
      ':' +
      dom.querySelector('input[name="RestrictionsDate_dts_sd$time$minute').value;

    /// End date info
    let HasEndDateEnabled = dom.querySelector('input[name="RestrictionsDate_cb_ed"]').checked;
    let EndDate = dom.querySelector('#RestrictionsDate_dts_ed').value;
    let EndTime =
      dom.querySelector('input[name="RestrictionsDate_dts_ed$time$hour').value +
      ':' +
      dom.querySelector('input[name="RestrictionsDate_dts_ed$time$minute').value;

    let titleParts = [img.title];
    if (HasStartDateEnabled) {
      titleParts.push(`Start date: ${StartDate} ${StartTime}`);
    }
    if (HasEndDateEnabled) {
      titleParts.push(`End date: ${EndDate} ${EndTime}`);
    }

    img.title = titleParts.join('\u000A');
  });
})();
