(async () => {
  /** Make sure we should run this. */
  //const config = await FetchMMSettings();
  //if (!config.mylo.showGradeRenameNotice) return;

  const styles = style`
    .rename-wizard p { margin: 0; }
    .rename-wizard .description { font-size: smaller; line-height: 1.5em; }
    
    .rename-wizard .container {
      display: grid;
      grid-template-columns: 1fr 1fr;
      gap: 1em;
      margin-top: 1em;
    }

    .rename-wizard .container .inputs {
      display: flex;
      flex-direction: column;
      gap: 1em;
      padding: 1em;
      background: #F9FBFF;
      border: solid #CDD5DC 1px;
      border-radius: 4px;
    }

    /** Basic list reset */
    .rename-wizard .container .inputs .ilos { margin: 0; padding: 0; list-style: none; }

    .rename-wizard .container .inputs .ilos:not(:empty) { padding: 1em 0; }
    .rename-wizard .container .inputs .ilos li { 
      font-size: 0.8em; 
      display: flex;
      gap: 1em;
      align-items: center;
    }

    .rename-wizard .container .inputs .ilos li .remove-ilo {
      cursor: pointer; 
      font-size: 1.8em;
    }
    
    .rename-wizard .container .inputs .ilos li:not(:last-of-type) {
      margin-bottom: 1em;
    }

    .rename-wizard .container .preview {}

    .launch-wizard { margin-left: 1em; }

    .small-strong {
      cursor: default;
      display: block;
      font-size: 0.7rem;
      font-weight: 700;
      letter-spacing: 0.2px;
      line-height: 1rem;
      margin: 0px 0px 7px;
      padding: 0px;
    }

    .small-em {
      cursor: default;
      display: block;
      font-size: 0.7rem;
      font-style: italic;
      line-height: 1rem;
      margin: -14px 0px 7px 40px;
    }

    .checkbox-clean {
      padding-left: 0;
      margin: 0;
    }

    input.edit-inline { width: auto; }

    .checkbox-clean label {
      display: inline-flex;
      justify-content: center;
      align-items: center;
      gap: 1em;
      user-select: none;
    }
  `;

  const modal = document.createElement('mm-dialog');
  modal.toggleAttribute('preserve', true);
  modal.setAttribute('heading', 'Generate <em>MyResults</em> Compatible Category Name');
  modal.setAttribute('confirm-text', 'Use this name');
  modal.classList.add('rename-wizard');
  document.body.append(modal);

  const description = document.createElement('p');
  description.innerText = `
    MyResults is a tool which will be used to support the finalisation of most unit results in the University. 
    It requires a grade category to represent each assessment task in the approved curriculum (as per the Unit Outline). 
    This generator can be used to name grade categories in the correct format to import of unit data from MyLO into MyResults.
  `.trim();
  description.classList.add('description');

  modal.append(description);

  const button = document.createElement('button');
  button.toggleAttribute('primary', true);
  button.innerText = 'Name Generator';
  button.classList.add('launch-wizard', 'd2l-button');
  button.setAttribute('mm-icon-type', 'action-button');
  button.setAttribute('mm-icon-offset', '-4:-2');

  const row = document.querySelector('input[name^="name" i]').closest('td');
  row.append(button);

  /** Get an element based on its `data-el-ref` attribute */
  const getRef = (refs, base = document) => {
    if (!Array.isArray(refs)) {
      return base.querySelector(`[data-el-ref="${refs}"]`);
    } else {
      const o = {};
      for (const ref of refs) {
        const el = base.querySelector(`[data-el-ref="${ref}"]`);
        if (el) o[ref] = el;
      }
      return o;
    }
  };

  const container = html`
    <div class="container">
      <div class="inputs" data-el-ref="inputs">
        <div class="row">
          <strong class="small-strong">Assessment task number</strong>
          <input type="number" data-el-ref="taskNumber" class="d2l-edit" />
        </div>
        <div class="row">
          <strong class="small-strong">Assessment task name</strong>
          <input type="text" data-el-ref="taskName" class="d2l-edit" />
        </div>
        <div class="row">
          <strong class="small-strong">ILOs assessed by this task</strong>
          <ul data-el-ref="ilos" class="ilos"></ul>
          <button class="d2l-button" data-el-ref="addIlo">Add ILO</button>
        </div>
        <div class="row">
          <strong class="small-strong">Hurdle task</strong>
          <div class="d2l-checkbox-container checkbox-clean">
            <label>
              <input type="checkbox" class="d2l-checkbox" data-el-ref="hurdleTask" />
              This task is a hurdle task
            </label>
          </div>
          <span class="small-em">
          Checking this option only affects MyResults. It will <br />
          not have an impact on any MyLO grade calculation.            
          </span>
        </div>
      </div>

      <div class="preview">
        <strong class="small-strong">Name preview:</strong>
        <p class="name-preview" data-el-ref="namePreview"></p>
      </div>
    </div>
  `;

  modal.append(container);

  /** Things are added now. Lets start the logic side of things. */
  button.onclick = () => {
    hydrateWithName(row.querySelector('input').value);
    modal.show();
  };

  const generateNamePreview = () => {
    const { taskNumber, taskName, hurdleTask, namePreview } = getRef(
      ['taskNumber', 'taskName', 'hurdleTask', 'namePreview'],
      container
    );

    const ILOs = ListToILOs();

    const text = [
      `AT${taskNumber.value}`,
      taskName.value,
      ILOs.length > 0 ? `ILO-${ILOs.join('-')}` : null,
      hurdleTask.checked ? 'H' : null,
    ]
      .filter(Boolean)
      .join('_');
    namePreview.innerText = text;
    return text;
  };

  const ILOsToList = ILOs => {
    if (!ILOs) return;

    const { ilos } = getRef(['ilos'], container);
    ilos.innerHTML = '';

    const parts = ILOs.split('-')
      .map(v => v.trim())
      .filter(Boolean);

    for (const ILO of parts) {
      const item = html`
        <li>
          ILO # <input type="number" class="d2l-edit edit-inline" data-type="ilo" value="${ILO}" />
          <span class="remove-ilo">&times;</span>
        </li>
      `;
      ilos.append(item);
    }
  };

  const ListToILOs = () => {
    const { ilos } = getRef(['ilos'], container);
    return Array.from(ilos.querySelectorAll('li input[data-type="ilo"]'))
      .map(v => v.value.trim())
      .filter(v => v != '')
      .sort();
  };

  const addILOItem = () => {
    const { ilos } = getRef(['ilos'], container);
    const item = html`
      <li>
        ILO # <input type="number" class="d2l-edit edit-inline" data-type="ilo" />
        <span class="remove-ilo">&times;</span>
      </li>
    `;
    ilos.append(item);
    generateNamePreview();
  };

  const removeILOItem = row => {
    row.remove();
    generateNamePreview();
  };

  const hydrateWithName = name => {
    /**
     * Parses the name out into useful named chunks
     * @see [Named capture groups](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Groups_and_Ranges)
     */
    const name_parser = /^AT(?<ATTask>[0-9]+)(_(?<ATName>[^_]*)(_ILO(?<ILOs>(-[0-9]+)*))?(?<Hurdle>_H)?)?/gim;
    const results = name_parser.exec(name);

    if (!results) return;

    const { ATTask, ATName, ILOs, Hurdle } = results?.groups;
    const { taskNumber, taskName, hurdleTask } = getRef(['taskNumber', 'taskName', 'hurdleTask'], container);

    taskNumber.value = ATTask ?? '';
    taskName.value = ATName ?? '';
    hurdleTask.checked = Hurdle != undefined;

    ILOsToList(ILOs);

    generateNamePreview();
  };

  const { inputs, addIlo, ilos } = getRef(['inputs', 'addIlo', 'ilos'], container);
  inputs?.addEventListener('change', generateNamePreview);
  inputs?.addEventListener('keyup', generateNamePreview);

  addIlo.onclick = () => addILOItem();
  ilos.addEventListener('click', e => {
    if (e.target.matches('.remove-ilo')) {
      removeILOItem(e.target.closest('li'));
    }
  });

  modal.addEventListener('ok', () => {
    row.querySelector('input').value = generateNamePreview();
  });
})();
