(async () => {
  const config = await FetchMMSettings();
  if (!config.mylo.ShowGradeVisibilityChanger) return;

  const btn = document.createElement('button');
  btn.setAttribute('mm-icon-type', 'action-select');
  btn.setAttribute('mm-icon-offset', '-6:0');
  btn.innerText = 'Change Grades Visibility';
  btn.title =
    'Select this option to hide the Points and the Weighted Grade, and only show the Grade Scheme Symbol and Colour';
  btn.classList.add('d2l-button');
  btn.style.float = 'right';

  document.querySelector('#d_page_title').insertAdjacentElement('afterend', btn);

  const orgid = new URL(window.location.href).searchParams.get('ou');
  const url = `/d2l/lms/grades/admin/settings/orgunit_display_options.d2l?d2l_isfromtab=1&ou=${orgid}&feedback=2`;

  // Store the values as FormData
  const body = new FormData();
  // Generate a hitcode and referrer token
  body.append('d2l_hitcode', window.localStorage.getItem('XSRF.HitCodeSeed') + ((new Date().getTime() + 1e8) % 1e8));
  body.append('d2l_referrer', window.localStorage.getItem('XSRF.Token'));
  // Flag that this is an RPC call
  body.append('d2l_action', 'Update');
  body.append('d2l_actionparam', '');
  body.append('artifactTargets', '');

  body.append('DisplaySchemeSymbol', 'True');
  body.append('DisplaySchemeColor', 'True');

  btn.addEventListener('click', async e => {
    btn.disabled = true;
    // Send the request, and return the resulting promise.
    await fetch(url, { method: 'post', body });
    alert('Updated Student View Display Options to only show Grade Scheme Symbol and Colour.');

    btn.disabled = false;

    if (window.location.pathname.endsWith('orgunit_display_options.d2l')) window.location.reload();
  });
})();
