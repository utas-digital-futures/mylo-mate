/**
 * @affects Manage Files
 * @problem Tidying up the Manage Files area of a unit often involves moving files that are not used. Clicking these files one at a time is time consuming and error-prone.
 * @solution Add three buttons to the Manage Files toolbar: Select/Deselect all folders; Select/Deselect all linked files; Select/Deselect all unlinked files.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  if (window.ManageFilesFix) return;

  // Check that we have items to append to!
  //	If we do, then add our no-repeat flag so that the rest of this script will only run once.
  //	If we don't, bail. We're not ready yet.
  const items = document.querySelector('ul.da_a');
  if (items != null) window.ManageFilesFix = true;
  else return;

  const store = await FetchMMSettings();
  if (!store.other.ManageFilesFeatures) return;

  /**
   * Adds an item to the heading actions list
   */
  const addItem = ({ id, icon, title }) => {
    const item = document.createElement('li');
    item.className = 'float_l ' + id;
    item.style.display = 'inline';
    item.setAttribute('mm-icon-type', 'action-button');
    item.setAttribute('mm-icon-offset', '4:10');
    item.innerHTML = `<d2l-button-icon icon="d2l-tier1:${icon}" title="${title}" class="checkbox"></d2l-button-icon>`;
    items.appendChild(item);
    return item;
  };

  // Add folder, linked and unlinked buttons.
  const folder = addItem({ id: 'folder', icon: 'folder', title: 'Select / Deselect all folders' });
  const linked = addItem({ id: 'linked', icon: 'link', title: 'Select / Deselect all linked files' });
  const unlinked = addItem({ id: 'unlinked', icon: 'broken-link', title: 'Select / Deselect all unlinked HTML files' });

  let onlyWarnOnce = false;
  let warned = false;
  const warnUser = () => {
    if ((document.querySelector('input[type="checkbox"][title*="Select all items"]') || { checked: false }).checked) {
      if (onlyWarnOnce && warned) return;

      alert(
        'Notice: This function can only apply to rows that are visible. It will not affect rows that may be loaded by scrolling to the bottom of the list.'
      );
      warned = true;
    }
  };

  folder.addEventListener('click', warnUser);
  linked.addEventListener('click', warnUser);
  unlinked.addEventListener('click', warnUser);

  // Listen for when the folder button is clicked
  folder.addEventListener('click', () => {
    // Find all the rows that have a type of 'Folder'
    const rows = [...document.querySelectorAll('.yui-dt-data tr')].filter(
      row => row.querySelector('.d_dg_col_Type span').innerText === 'Folder'
    );
    // Find all the rows that have a type of 'Folder' and are selected
    const checkedRows = [...document.querySelectorAll('.yui-dt-selected')].filter(
      row => row.querySelector('.d_dg_col_Type span').innerText === 'Folder'
    );

    rows.forEach(row => {
      // If the amount of rows is different to the amount of checked rows (i.e. there are rows to check still)
      if (rows.length != checkedRows.length) {
        // Check the rows checkbox, give it the 'selected' class (this is used for actions, strangely enough, not the check-state of the fields themselves),
        // and then send a manual 'click' event to them so that any other listening scripts can observe the new 'click'.
        row.querySelector('input[type="checkbox"]').checked = true;
        row.classList.add('yui-dt-selected');
        row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
      } else {
        // Uncheck the rows checkbox, remove the 'selected' class and then send a manual 'click' event to them so that any other listening scripts can observe the new 'click'.
        row.querySelector('input[type="checkbox"]').checked = false;
        row.classList.remove('yui-dt-selected');
        row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
      }
    });
  });

  // Listen for when the linked button is clicked
  linked.addEventListener('click', () => {
    // Find all the rows that have a type of 'Folder'
    const rows = [...document.querySelectorAll('.yui-dt-data tr')].filter(
      row => row.querySelectorAll('img[src*="images/tier1/link.svg"]').length === 1
    );
    // Find all the rows that have a type of 'Folder' and are selected
    const checkedRows = [...document.querySelectorAll('.yui-dt-selected')].filter(r =>
      r.querySelector('img[src*="images/tier1/link.svg"]')
    );

    rows.forEach(row => {
      // If the amount of rows is different to the amount of checked rows (i.e. there are rows to check still)
      if (rows.length != checkedRows.length) {
        // Check the rows checkbox, give it the 'selected' class (this is used for actions, strangely enough, not the check-state of the fields themselves),
        // and then send a manual 'click' event to them so that any other listening scripts can observe the new 'click'.
        row.querySelector('input[type="checkbox"]').checked = true;
        row.classList.add('yui-dt-selected');
        row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
      } else {
        // Uncheck the rows checkbox, remove the 'selected' class and then send a manual 'click' event to them so that any other listening scripts can observe the new 'click'.
        row.querySelector('input[type="checkbox"]').checked = false;
        row.classList.remove('yui-dt-selected');
        row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
      }
    });
  });

  // Listen for when the unlinked button is clicked
  unlinked.addEventListener('click', () => {
    // Find all the rows that have a type of 'Folder'
    const rows = [...document.querySelectorAll('.yui-dt-data tr')].filter(
      row =>
        (row.querySelector('.d_dg_col_Name span').innerText.toUpperCase().endsWith('HTM') ||
          row.querySelector('.d_dg_col_Name span').innerText.toUpperCase().endsWith('HTML')) &&
        row.querySelectorAll('img[src*="images/tier1/link.svg"]').length === 0
    );

    // Find all the rows that have a type of 'Folder' and are selected
    const checkedRows = [...document.querySelectorAll('.yui-dt-selected')].filter(
      row =>
        (row.querySelector('.d_dg_col_Name span').innerText.toUpperCase().endsWith('HTM') ||
          row.querySelector('.d_dg_col_Name span').innerText.toUpperCase().endsWith('HTML')) &&
        row.querySelectorAll('img[src*="images/tier1/link.svg"]').length === 0
    );

    if (rows.length == 0) return alert('No unlinked HTML files were found.');

    rows.forEach(row => {
      // If the amount of rows is different to the amount of checked rows (i.e. there are rows to check still)
      if (rows.length != checkedRows.length) {
        // Check the rows checkbox, give it the 'selected' class (this is used for actions, strangely enough, not the check-state of the fields themselves),
        // and then send a manual 'click' event to them so that any other listening scripts can observe the new 'click'.
        row.querySelector('input[type="checkbox"]').checked = true;
        row.classList.add('yui-dt-selected');
        row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
      } else {
        // Uncheck the rows checkbox, remove the 'selected' class and then send a manual 'click' event to them so that any other listening scripts can observe the new 'click'.
        row.querySelector('input[type="checkbox"]').checked = false;
        row.classList.remove('yui-dt-selected');
        row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
      }
    });
  });
})();
