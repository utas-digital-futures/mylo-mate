(async function () {
  const bottom_scrollheight_multiplier = 2; // The scrollheight must be x times larger than the current viewpanel to appear.
  const store = await FetchMMSettings();

  const wrapper = document.querySelector('.dsl_pmv .dsl_p_m_f .dsl_p_m');

  if (store.mylo.BackToTop) {
    const show_bottom = wrapper.scrollHeight > wrapper.clientHeight * bottom_scrollheight_multiplier;

    let container = document.createElement('div');
    container.style.right = '65px';
    container.style.bottom = '55px';
    container.style.position = 'fixed';

    // Create a new button that will act as our "Back to Top",
    // and give it some properties.
    let top = document.createElement('button');
    top.id = 'top';
    top.classList.add('btn');
    if (!show_bottom) top.classList.add('solo');
    top.setAttribute('mm-icon-type', 'action-button');
    top.setAttribute('mm-icon-show', 'false');
    top.title = 'Back to Top';
    top.setAttribute('aria-hidden', 'true');
    top.setAttribute('is', 'd2l-icon-button');
    top.innerHTML = `<d2l-icon class="style-scope d2l-icon-button d2l-icon-0" icon="d2l-tier1:chevron-up"></d2l-icon><span class="style-scope d2l-icon-button"></span>`;
    container.appendChild(top);

    // When the button is clicked, scroll the window back to the top.
    top.addEventListener('click', function () {
      wrapper.scrollTo(0, 0);
    });

    if (show_bottom) {
      // This will be our "Jump to Bottom" button
      let bottom = document.createElement('button');
      bottom.id = 'bottom';
      bottom.className = 'btn';
      bottom.setAttribute('mm-icon-type', 'action-button');
      bottom.setAttribute('mm-icon-show', 'false');
      bottom.title = 'Jump to Bottom';
      bottom.setAttribute('aria-hidden', 'true');
      bottom.setAttribute('is', 'd2l-icon-button');
      bottom.innerHTML = `<d2l-icon class="style-scope d2l-icon-button d2l-icon-0" icon="d2l-tier1:chevron-down"></d2l-icon><span class="style-scope d2l-icon-button"></span>`;
      container.appendChild(bottom);

      // When the button is clicked, scroll the window back to the top.
      bottom.addEventListener('click', function () {
        wrapper.scrollTo(0, wrapper.scrollHeight);
      });
    }

    wrapper.appendChild(container);

    // Boundaries to appear in.
    //	When window.scrollY is above `invisible`px, the button will disappear.
    //  When window.scrollY is between `invisible`px and `fadeIn`px, it will set the opacity
    //  to between 0 and 1, where 0 is 200px and 1 is 300px.
    let invisible = 200;
    let fadeIn = 300;

    // Store the last known value of window.scrollY
    let Y = null;

    // Sets the opacity of the Back to Top button
    const update = () => {
      if (Y < invisible) {
        top.style.opacity = 0;
        return;
      }
      if (Y > fadeIn) {
        top.style.opacity = 1;
        return;
      }
      if (Y > invisible && Y < fadeIn) {
        let scale = ((Y - invisible) * 100) / (fadeIn - invisible);
        top.style.opacity = scale / 100;
      }
    };

    // Runs every 10ms, but will only fire the update function if the scrollY position is different.
    setInterval(function () {
      let oldY = Y;
      Y = wrapper.scrollTop;
      if (oldY !== wrapper.scrollTop) update();
    }, 10);
  }
})();

