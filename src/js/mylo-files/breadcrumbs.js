/**
 * @affects Manage Files
 * @problem The Manage Files tool shows an icon if a file is used, but not where it is shown in the Content. Staff have to tediously look through the content to find references.
 * @solution Add feature that examines the Table of Contents information and displays the exact locations that a file is used when hovering over the "Linked File" icon.
 * @target Academic and support staff
 * @impact High
 * @savings 2 hours per unit
 */

(async function () {
  function iterate(base) {
    let output = [],
      start = base;
    const loop = (node, parent = []) => {
      if (parent.length == 0) parent = [node.Title];
      if (node.Modules.length > 0) {
        node.Modules.forEach(m => {
          let o = [].concat(parent);
          o.push(m.Title);
          loop(m, o);
        });
      }
      if (node.Topics.length > 0) {
        output = output.concat(
          node.Topics.map(topic => {
            topic.parents = [].concat(parent);
            topic.parents.push(topic.Title);
            return topic;
          })
        );
      }
    };
    if (Array.isArray(start)) start.forEach(e => loop(e));
    else loop(start);
    return output;
  }

  const toc = await api(`/d2l/api/le/1.32/${window.location.searchParams.ou}/content/toc`);
  const dataset = iterate(toc[0].Modules).filter(
    v => v.TypeIdentifier == 'File' && v.Url && v.Url.startsWith('/content/enforced')
  );
  const hash = dataset.reduce((o, v) => {
    let r = o[v.Url] || [];
    r.push(v);
    o[v.Url] = r;
    return o;
  }, {});

  const checkForFolders = () => {
    let files = UI.GetControl('dg_files').m_rows.filter(
      r => !r.m_rowKey.endsWith('/') && r.m_domNode.querySelector('[alt*="used in a Content topic"]')
    );
    files.forEach(row => {
      let link = row.m_domNode.querySelector('[alt*="used in a Content topic"]');
      if (!link) return;
      if (link.getAttribute('mm-tooltip-action')) return;
      link.setAttribute('mm-tooltip-action', true);
      const key = row.m_rowKey.substr(2);

      // Resolves bug in comments of issue #266
      if (hash[key] == null) return;

      // Adds a linebreak, ready for our content.
      link.title += '\u000A';
      link.title += hash[key].map(({ parents }) => parents.join(' > ')).join('\u000A');
    });
  };

  let container = UI.GetControl('sptl_main');
  let watcher = new MutationObserver(checkForFolders);
  watcher.observe(container.IDomNode, { childList: true, subtree: true });
  checkForFolders();
})();
