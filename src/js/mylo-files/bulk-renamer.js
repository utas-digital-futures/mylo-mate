/**
 * @affects Manage Files
 * @problem
 *  When setting up a new unit, renaming files in a logical fashion (ie. `week1/page01.html`) is often required.
 *  Doing this manually for each file in a unit can often take hours.
 * @solution
 *  Introduce a Bulk Rename feature which allows all selected files to be renamed at once.
 *  Helper functions are included, such as being able to prefix, suffix or replace all file names with a user-defined pattern
 *    (such as "page[#].html", and "[#]" is automatically replaced with an incrementing number).
 * @target Academic and support staff
 * @impact High
 * @savings 6 hours per unit
 */

(async function () {
  if (window.BulkRenamer) return;

  // Check that we have items to append to!
  //	If we do, then add our no-repeat flag so that the rest of this script will only run once.
  //	If we don't, bail. We're not ready yet.
  const items = document.querySelector('ul.da_a');
  if (items != null) window.BulkRenamer = true;
  else return;

  const pad = (v, total) => (v + '').padStart((total + '').length, '0');
  const isVisible = elem => elem && elem.offsetHeight > 0 && elem.offsetWidth > 0;

  /**
   * Adds an item to the heading actions list
   */
  const addItem = ({ id, icon, title, hide = false }) => {
    const item = document.createElement('li');
    item.classList.add('float_l', id);
    item.style.display = 'inline';
    item.setAttribute('mm-icon-type', 'action-button');
    item.setAttribute('mm-icon-offset', '4:10');
    item.innerHTML = `<d2l-button-icon icon="d2l-tier1:${icon}" title="${title}" class="checkbox"></d2l-button-icon>`;

    if (hide) item.style.display = 'none';

    items.appendChild(item);
    return item;
  };

  let onlyWarnOnce = false;
  let warned = false;
  const warnUser = () => {
    if ((document.querySelector('input[type="checkbox"][title*="Select all items"]') || { checked: false }).checked) {
      if (onlyWarnOnce && warned) return;

      alert(
        'Notice: This function can only apply to rows that are visible. It will not affect rows that may be loaded by scrolling to the bottom of the list.'
      );
      warned = true;
    }
  };

  const startRename = addItem({ id: 'rename', icon: 'edit', title: 'Bulk rename items' });
  const renameText = addItem({ id: 'renameText', icon: 'role-switch', title: 'Change titles', hide: true });

  startRename.addEventListener('click', warnUser);

  const HideAllTheThings = () => {
    renameText.style.display = 'none';
    save_bar.style.display = 'none';
  };

  const buttonAction = e => {
    let action = e.target.getAttribute('mm-action');

    if (action.toLowerCase() == 'save') {
      UI.GetControl('dg_files')
        .IDomNode.querySelectorAll('.deip')
        .forEach(block => block.querySelectorAll('button')[0].click());
    } else if (action.toLowerCase() == 'cancel') {
      UI.GetControl('dg_files')
        .IDomNode.querySelectorAll('.deip')
        .forEach(block => block.querySelectorAll('button')[1].click());
    }
    HideAllTheThings();
  };

  const style = document.createElement('style');
  style.innerHTML = `
  d2l-floating-buttons {
    position: absolute;
    bottom: 0;
    background: white;
    left: 0;
    right: 0;
    padding: 1em 2em;
    border-top: 4px whitesmoke solid;
    box-sizing: border-box;
    box-shadow: 0px -8px 20px whitesmoke;
  }
  `;

  // Adds the floating save/cancel bar
  let save_bar = document.createElement('d2l-floating-buttons');
  save_bar.style.display = 'none';
  document.querySelector('#d_content').insertAdjacentElement('afterend', save_bar);

  let save_btn = document.createElement('button');
  save_btn.innerText = 'Save';
  save_btn.setAttribute('mm-action', 'Save');
  save_btn.classList.add('d2l-button');
  save_btn.addEventListener('click', buttonAction);
  save_btn.setAttribute('primary', true);

  let cancel_btn = document.createElement('button');
  cancel_btn.innerText = 'Cancel';
  cancel_btn.setAttribute('mm-action', 'Cancel');
  cancel_btn.classList.add('d2l-button');
  cancel_btn.addEventListener('click', buttonAction);
  save_bar.append(save_btn, cancel_btn, style);

  const getCheckedRows = () =>
    Array.from(UI.GetControl('dg_files').IDomNode.querySelectorAll('input[type="checkbox"]:checked')).map(
      cb => cb.parentNode.parentNode.parentNode
    );

  const bulkRename = () => {
    const map = UI.GetControl('dg_files').m_rows.reduce((o, r) => {
      o[r.m_id] = r.m_rowKey;
      return o;
    }, {});
    const rows = getCheckedRows();
    if (!rows.length) {
      return alert('No rows selected. Please select some rows to use this function.');
    }

    // Show the items again
    renameText.style.display = '';
    save_bar.style.display = '';

    rows.forEach(row => {
      if (!row.id.startsWith('yui')) return;
      Rename(map[row.id]);
    });
  };

  const message =
    'Text to replace the titles of the selected file with. Use [#] to indicate the selection number, e.g. Page [#]. Please note the file extension (e.g. docx) will NOT be modified by this.';
  renameText.addEventListener('click', e => {
    let rows = getCheckedRows()
      .map(el => el.querySelector('.deip'))
      .filter(isVisible);

    if (rows.length == 0) {
      return alert('Please select at least one row to use this tool.');
    }

    let msg = `${message}\n\nThis will affect ${rows.length} rows.`;

    let val = prompt(msg);
    if (val == null) return;
    while (val.trim().length == 0) {
      alert('No title provided. Please supply a title to continue.');
      val = prompt(msg);
    }

    let exts = {};
    for (let i = 0, j = rows.length; i < j; i++) {
      let v = rows[i].querySelector('input').value;
      if (!v.includes('.')) continue;

      let ext = v.split('.').pop().toLowerCase().trim();
      exts[ext] = (exts[ext] || 0) + 1;
    }

    if (!val.includes('[#]') && Object.values(exts).find(v => v > 1)) {
      return alert(
        'Continuing would cause conflicting file names. Make sure you use [#] to add sequential numbers to your filename, or deselect files with matching extensions.'
      );
    }

    rows.forEach((row, i) => {
      const input = row.querySelector('input');
      let v = val.replace(/\[#\]/g, pad(i + 1, rows.length));
      if (input.value.includes('.')) {
        const ext = input.value.split('.').pop();
        input.value = v + '.' + ext;
      } else {
        input.value = v;
      }
    });
  });

  startRename.addEventListener('click', bulkRename);

  document.addEventListener(
    'click',
    e => {
      if (e.target.matches('.deip .d2l-button')) {
        let remainingButtons = Array.from(document.querySelector('.deip .d2l-button')).filter(isVisible);
        if (remainingButtons.length === 0) {
          HideAllTheThings();
        }
      }
    },
    true
  );

  document.querySelector('html').style.overflow = '';
  document.body.style.overflow = '';

  document.addEventListener('filemanager-navigate', HideAllTheThings);
})();
