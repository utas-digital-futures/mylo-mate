/**
 * @affects Manage Files
 * @problem Using the browser Back and Forward buttons take you out of the Manage Files tool, rather than helping the user navigate through directories (as is standard file manager behaviour).
 * @solution Navigating through directories updates the browser's history, allowing the Back and Forward buttons to help move through their folder history instead of leaving Manage Files entirely.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  let base = UI.GetControl('tv_folders').m_children[0].m_key;

  const evt = new EventTarget();

  let children = [];
  const checkForFolders = () => {
    // This handles the root nodes on the left hand side.
    children = [];
    let root = UI.GetControl('tv_folders').m_children[0].m_childNodes;
    function flatten(node) {
      children.push({ node: node.IDomNode, key: node.m_key.replace(base, '/') });
      if (node.m_childNodes.length > 0) {
        node.m_childNodes.forEach(flatten);
      }
    }

    root.map(flatten);
    children.forEach(child => {
      let el = child.node.querySelector('[title="Navigate to"]');
      el.setAttribute('data-path', child.key);
    });
    document.querySelector('.dt_md [title="Navigate to"]').setAttribute('data-path', '/');

    // This handles the nodes on the RHS
    let dirs = UI.GetControl('dg_files').m_rows.filter(row => row.m_rowKey.endsWith('/'));
    dirs.forEach(dir => {
      dir.m_domNode
        .querySelector('[title="Navigate to"]')
        .setAttribute('data-path', dir.m_rowKey.replace('d_' + base.split('|')[1], '/'));
    });

    evt.dispatchEvent(new Event('treechange'));
  };

  let container = UI.GetControl('sptl_main');
  let watcher = new MutationObserver(checkForFolders);
  watcher.observe(container.IDomNode, { childList: true, subtree: true });
  checkForFolders();

  document.addEventListener(
    'click',
    e => {
      if (!e.isTrusted) return;

      const node = e.composedPath().find(n => n.matches && n.matches('[data-path]'));
      if (!node) return;

      const path = node.getAttribute('data-path');
      history.pushState({ path }, '');
    },
    true
  );

  window.onpopstate = e => {
    let path = null;
    if ((!e.state || !e.state.path) && window.location.href.includes('/manageFiles/main.d2l')) path = '/';
    else if (e.state && e.state.path) path = e.state.path;
    else return;

    const known_node = children.find(child => child.key == decodeURIComponent(path));
    if (known_node) {
      known_node.node.querySelector('[data-path]').click();
    } else {
      let [type, root] = base.split('|');
      DoActivateFolder(type, root + decodeURIComponent(path.substr(1)));

      let rootNode = document.querySelector(`[data-path="/"]`);
      if (rootNode) rootNode.click();
    }
  };

  if (window.location.searchParams.Path) {
    let [type, root] = base.split('|');
    let path = window.location.searchParams.Path.replace(root, '/');

    const known_node = children.find(child => child.key == decodeURIComponent(path));
    if (known_node) {
      known_node.node.querySelector('[data-path]').click();
    } else {
      DoActivateFolder(type, root + decodeURIComponent(path.substr(1)));
      let rootNode = document.querySelector(`[data-path="/"]`);
      if (rootNode) rootNode.click();
    }

    let done = false;
    evt.addEventListener('treechange', e => {
      if (window.location.searchParams.File) {
        const req = decodeURIComponent(window.location.searchParams.File);
        const files = UI.GetControl('dg_files').m_rows;
        const file = files.find(file => file.m_subject == req);

        if (!file) return;
        if (done) return;

        done = true;
        file.m_domNode.click();

        file.m_domNode.scrollIntoView({ block: 'center', behaviour: 'smooth' });
      }
    });
  }
})();
