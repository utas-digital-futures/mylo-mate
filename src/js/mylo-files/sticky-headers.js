(async function () {
  const actionsBar = UI.GetControl('ctl_10').IDomNode.parentNode;
  actionsBar.setAttribute('sticky-header', true);

  const headerRow = UI.GetControl('dg_files').m_yuiDataTable._elThead;
  headerRow.setAttribute('sticky', 'true');
  headerRow.classList.add('sticky-row');
})();
