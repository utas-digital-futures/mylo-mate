(async () => {
  // Get the checked quizzes, and then find the attempts for each quiz.
  // Compare users with attempts against the classlist, and pull out those that aren't in the attempt list.
  //
  // Should we save these to an .xlsx file, with a tab for each quiz?
  // Or should it be saved in a single .csv file?

  const button = ({ text, icon, title = '', parent = null, classes = [], testid = '' }) => {
    const btn = document.createElement('d2l-button-subtle');
    btn.title = title;
    btn.setAttribute('data-testid', testid);
    btn.setAttribute('icon', icon);
    btn.setAttribute('text', text);
    btn.setAttribute('mm-icon-type', 'action-button');
    btn.setAttribute('mm-icon-offset', '6:6');
    btn.classList.add(...classes);
    parent.appendChild(btn);
    return btn;
  };

  const noRowsSelected = 'No rows selected. Please select at least 1 row to use this tool.';
  const parent = document.querySelector('d2l-overflow-group');

  // D2L edit btn.
  const exportButton = button({
    text: 'Export Users Without Attempt',
    testid: 'export-no-attempt',
    title: 'Exports students who have not made an attempt in a quiz',
    icon: 'tier2:quiz-submissions',
    parent,
    classes: ['mm-btn'],
  });

  exportButton.toggleAttribute('disabled', true);

  const ou = new URL(window.location.href).searchParams.get('ou');
  const me = await api('/d2l/api/lp/1.28/users/whoami', { first: true });
  const classlist = await api(`/d2l/api/le/1.48/${ou}/classlist/`, { fresh: true }).then(data =>
    data.filter(usr => usr.RoleId == 103)
  );

  const enrollments = await api(`/d2l/api/lp/1.29/enrollments/orgUnits/${ou}/users/`, { fresh: true });
  const unit = await api(`/d2l/api/lp/1.21/enrollments/myenrollments/${ou}`, { first: true });

  const workbook = new ExcelJS.Workbook();
  workbook.creator = `MyLO MATE`;
  workbook.lastModifiedBy = `${me.FirstName} ${me.LastName}`;
  workbook.modified = new Date();

  document.addEventListener('change', e => {
    const quizzes = Array.from(document.querySelectorAll('input[name="gridQuizzes_cb"]:checked'));
    exportButton.toggleAttribute('disabled', quizzes.length == 0);
  });

  exportButton.addEventListener('click', async () => {
    const quizzes = Array.from(document.querySelectorAll('input[name="gridQuizzes_cb"]:checked')).map(
      v => v.value.split('_')[1]
    );

    if (quizzes.length == 0) return alert(noRowsSelected);

    for (const quizId of quizzes) {
      const name = document.querySelector(`a[href*="qi=${quizId}"]`).innerText;
      const attempts = await api(`/d2l/api/le/1.46/${ou}/quizzes/${quizId}/attempts/`);
      const users = new Set();

      // Make sure that there's only one version of each user id.
      attempts.forEach(att => users.add(att.UserId));

      const notAttempted = classlist.filter(user => !users.has(parseInt(user.Identifier)));

      const sheet = workbook.addWorksheet();
      sheet.name = name.replace(/\W+/g, '_').substr(0, 25) + `${Math.random() * 1e10}`.substr(0, 5);

      // TODO: What columns do we want to record?
      sheet.addRow(['User ID', 'First Name', 'Last Name', 'Email', 'Last Accessed']);

      notAttempted.forEach(clu => {
        const { Identifier, FirstName, LastName, OrgDefinedId, LastAccessed } = clu;
        const { EmailAddress } = enrollments.find(({ User }) => User.Identifier == Identifier).User;

        let lastAccessTime = 'Never';
        if (LastAccessed) {
          const date = new Date(LastAccessed);
          lastAccessTime = `${date.toDateString()} ${date.toLocaleTimeString()}`;
        }
        sheet.addRow([OrgDefinedId, FirstName, LastName, EmailAddress, lastAccessTime]);
      });
    }

    const blob = new Blob([await workbook.xlsx.writeBuffer()]);
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.download = `Students without quiz attempts - ${unit.OrgUnit.Name}.xlsx`;
    a.href = url;
    a.click();
  });
})();
