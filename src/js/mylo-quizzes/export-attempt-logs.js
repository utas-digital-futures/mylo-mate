(async function () {
  const items = parseInt(
    document
      .querySelector('#Detailed .d2l-grid-action-container-count-nosearch')
      .innerText.match(/([\d,]+)/)[0]
      .replace(/[\D]/, '')
  );
  // const pages = 1;
  const pages = Math.ceil(items / 100);

  const container = document.createElement('div');

  let export_logs = document.createElement('d2l-button');
  export_logs.setAttribute('data-testid', 'export-logs');
  export_logs.setAttribute('icon', 'tier1:download');
  export_logs.innerText = 'Export Log';
  export_logs.setAttribute('title', 'Export all log data to file');
  export_logs.setAttribute('mm-icon-type', 'action-button');
  export_logs.setAttribute('mm-icon-offset', '6:6');
  container.append(export_logs);

  document.querySelector('#Detailed .d2l-grid-action-container-count-nosearch').insertAdjacentElement('afterend', container);

  export_logs.addEventListener('click', async e => {
    export_logs.disabled = true;

    const headers = Array.from(document.querySelectorAll('#Detailed table thead th')).map(th =>
      th.innerText.split('\n')[0].trim()
    );

    let data = await Promise.all(
      Array(pages)
        .fill(null)
        .map(async (val, index) => {
          const body = new URLSearchParams();
          body.append('d2l_referrer', window.localStorage.getItem('XSRF.Token'));
          body.append('isXhr', true);
          body.append('gridPartialInfo$_type', 'D2L.LP.Web.UI.Desktop.Controls.GridPartialArgs');
          body.append('gridPartialInfo$SortingInfo$SortField', 'EventTime');
          body.append('gridPartialInfo$SortingInfo$SortDirection', '0');
          body.append('gridPartialInfo$NumericPagingInfo$PageSize', '100');
          body.append('_d2l_prc$headingLevel', '2');
          body.append('_d2l_prc$scope', '');
          body.append('_d2l_prc$childScopeCounters', `${document.querySelector('.d2l-numericpager-container').id}:0`);
          body.append('_d2l_prc$hasActiveForm', 'false');
          body.append('gridPartialInfo$NumericPagingInfo$PageNumber', index + 1);
          try {
            const res = await fetch(`DetailedGridPartial?${body.toString()}`).then(d => d.text());
            const html = /"Html":"(.*)",/gim.exec(res)[1].replace(/\\"/g, '"');

            const dom = document.createElement('div');
            dom.innerHTML = html;

            let rows = [];
            dom.querySelectorAll('tr.d2l-grid-row').forEach(row => {
              rows.push(
                Array.from(row.querySelectorAll('th,td')).map(cell => cell.innerText.replace(/\\r|\\n|\\t/g, '').trim())
              );
            });
            return rows;
          } catch (e) {
            console.error(e);
            return [];
          }
        })
    );

    let csv = `data:text/csv;charset=utf-8,Record Number,${headers.join(',')}\r\n`;
    csv += data
      .flat()
      .map((v, i) => {
        v.unshift(i + 1);
        return v;
      })
      .sort(([, name1], [, name2]) => {
        name1 = name1.toLocaleUpperCase();
        name2 = name2.toLocaleUpperCase();
        return name1 < name2 ? -1 : name1 > name2 ? 1 : 0;
      })
      .map(row => row.map(v => `"${v}"`).join(','))
      .join('\r\n');

    let link = document.createElement('a');
    link.setAttribute('href', encodeURI(csv));
    link.setAttribute(
      'download',
      `Attempt Logs - ${document
        .querySelector('[href*="quiz_mark_users"]')
        .getAttribute('text').replace(/[^\w\s]/, '')
        .replace(/[\s]+/, '-')}.csv`
    );
    link.click();

    export_logs.disabled = false;
  });
})();
