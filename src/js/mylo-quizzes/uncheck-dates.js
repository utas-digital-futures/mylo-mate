/**
 * @affects Quizzes
 * @problem When using a previous delivery, we commonly need to remove all dates on a quiz, a task that requires a lot of clicking and waiting.
 * @solution Add a Bulk Uncheck Dates feature to quizzes, which removes the Start, End and Due dates for all selected quizzes.
 * @target Support staff
 * @impact Moderate
 */

/* eslint-disable no-undef */
/* eslint-disable no-await-in-loop */
(async function () {
  if (window.BulkDateUncheckCreated) return;
  window.BulkDateUncheckCreated = true;

  const store = await new Promise(res => chrome.storage.sync.get(null, res));
  if (!store.other.BulkUncheckQuizDates) return;

  const button = ({ text, icon, title = '', parent = null, classes = [], testid = '' }) => {
    const btn = document.createElement('d2l-button-subtle');
    btn.title = title;
    btn.setAttribute('data-testid', testid);
    btn.setAttribute('icon', icon);
    btn.setAttribute('text', text);
    btn.setAttribute('mm-icon-type', 'action-button');
    btn.setAttribute('mm-icon-offset', '6:6');
    btn.classList.add(...classes);
    parent.appendChild(btn);
    return btn;
  };

  const noRowsSelected = 'No rows selected. Please select at least 1 row to use this tool.';
  const parent = document.querySelector('d2l-overflow-group');

  // D2L edit btn.
  const bulk_edit = button({
    text: 'Bulk Uncheck Dates',
    testid: 'bulk-uncheck-dates',
    title: 'Bulk uncheck Start, End and Due dates of selected quizzes',
    icon: 'd2l-tier1:manage-dates-edit',
    parent,
    classes: ['mm-btn'],
  });

  bulk_edit.toggleAttribute('disabled', true);
  document.addEventListener('change', e => {
    const quizzes = Array.from(document.querySelectorAll('input[name="gridQuizzes_cb"]:checked'));
    bulk_edit.toggleAttribute('disabled', quizzes.length == 0);
  });
  const items = () =>
    Array.from(document.querySelectorAll('d2l-table-wrapper input[type="checkbox"][name*="gridQuizzes"]:checked')).map(
      cb => {
        const url = new URL(cb.closest('tr').querySelector('a[title="Edit Quiz"]').href);
        return `/d2l/lms/quizzing/admin/modify/quiz_newedit_restrictions.d2l?qi=${url.query.qi}&ou=${url.query.ou}`;
      }
    );

  const updatePage = link =>
    new Promise(resolve => {
      MyLOLayer(link, {
        load: ({ frame, dom, win }) => {
          if (win.location.href.includes('quiz_newedit_restrictions.d2l')) {
            const cbs = ['js_startDateEdit', 'js_endDateEdit', 'js_dueDateEdit'];
            cbs.forEach(cb => {
              const elem = dom.querySelector(`.${cb} input[type="checkbox"]`);
              if (elem.checked) elem.click();
            });
            setTimeout(() => dom.querySelector('d2l-floating-buttons button[primary]').click(), 100);
          }
          if (win.location.href.includes('quizzes_manage.d2l')) {
            frame.remove();
            resolve();
          }
        },
      });
    });

  bulk_edit.addEventListener('click', async e => {
    // Do time consuming logic here.
    const rows = items();
    if (rows.length === 0) return alert(noRowsSelected);

    bulk_edit.setAttribute('disabled', '');
    bulk_edit.innerText += ' (Working...)';

    // eslint-disable-next-line no-alert
    await PromisePool(rows.map(url => PromisePoolItem(updatePage, url)));
    window.location.reload();
  });
})();
