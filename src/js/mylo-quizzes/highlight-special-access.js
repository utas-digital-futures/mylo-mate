(async function () {
  const restricted = document.querySelectorAll('img[alt^="special access" i]');

  restricted.forEach(icon => {
    if (icon.title.toLowerCase().includes('is required')) {
      icon.title += ' to view this item';
      icon.classList.add('mm-highly-restricted');
    }
  });

  const GetLastClose = coll => {
    const old = new Date(0);
    const last = coll
      .filter(obj => obj.SpecialAccess.EndDate)
      .reduce((orig, curr) => {
        const d = curr.SpecialAccess.EndDate;
        if (!d) return orig;

        if (new Date(d) > orig) return new Date(d);
        return orig;
      }, old);

    if (last == old) return null;
    return last;
  };

  const QuizSpecialAccess = async href => {
    const url = new URL(href);
    const ou = url.searchParams.get('ou');
    const quiz = url.searchParams.get('qi');

    const access = await api(`/d2l/api/le/1.60/${ou}/quizzes/${quiz}/specialaccess/`);
    return GetLastClose(access);
  };

  const DropboxSpecialAccess = async href => {
    const url = new URL(href);
    const ou = url.searchParams.get('ou');
    const folder = url.searchParams.get('db');

    const access = await api(`/d2l/api/le/1.60/${ou}/dropbox/folders/${folder}/specialaccess/`);
    return GetLastClose(access);
  };

  for (const icon of restricted) {
    if (window.location.pathname.includes('quizzing/admin')) {
      const url = icon.closest('th').querySelector('a[onclick*="setreturnpoint" i]').href;
      const last = await QuizSpecialAccess(url);

      if (!last) continue;
      icon.title += ' | Ends ' + last.toLocaleString();
    }

    if (window.location.pathname.includes('dropbox/admin')) {
      const url = icon.closest('th').querySelector('a[href*="admin/mark/" i]').href;
      const last = await DropboxSpecialAccess(url);

      if (!last) continue;
      icon.title += ' | Ends ' + last.toLocaleString();
    }
  }
})();
