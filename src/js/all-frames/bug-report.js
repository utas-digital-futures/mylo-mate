(async function () {
  const user = await api(`/d2l/api/lp/1.22/users/whoami`, { first: true });

  const MoreInfoDialog = () =>
    new Promise(res => {
      let container = document.createElement('div');
      container.style.width = '100%';
      container.style.height = '100%';
      container.style.zIndex = '10000000000000';
      container.style.backgroundColor = 'rgba(0,0,0,0.4)';
      container.style.position = 'fixed';
      container.style.top = 0;
      container.style.left = 0;
      container.style.right = 0;
      container.style.bottom = 0;
      container.style.display = 'flex';
      container.style.justifyContent = 'center';
      container.style.alignItems = 'center';

      let div = document.createElement('div');
      div.style.width = '600px';
      div.style.maxHeight = '520px';
      div.style.backgroundColor = 'white';
      div.style.padding = '25px';

      let heading = document.createElement('h3');
      heading.innerText = 'MyLO MATE: Report a Bug';
      heading.style.borderBottom = '1px solid #bbc3c7';
      heading.style.marginBottom = '20px';

      let description = document.createElement('p');
      description.innerText = `Thank you for reporting a MyLO MATE bug. This feature will automatically capture some useful information about your current experience, but it would be appreciated if you could provide further details.
		
		This information includes:
		- Your role in this unit (Lecturer, Tutor, Auditor, Support)
		- What is going wrong
		`;
      description.style.fontSize = '0.9em';

      let info = document.createElement('d2l-input-textarea');
      info.setAttribute('rows', 10);
      info.setAttribute('max-rows', 10);
      info.setAttribute('value', `Role in unit:\nIssue:`);

      let bar = document.createElement('d2l-floating-buttons');
      bar.style.marginLeft = 0;
      bar.style.height = '70px';

      let ok = document.createElement('button');
      ok.innerText = 'Save Bug Report';
      ok.classList.add('d2l-button');
      ok.setAttribute('primary', true);

      let cancel = document.createElement('button');
      cancel.innerText = 'Cancel';
      cancel.classList.add('d2l-button');

      bar.appendChild(ok);
      bar.appendChild(cancel);

      div.appendChild(heading);
      div.appendChild(description);
      div.appendChild(info);
      div.appendChild(bar);

      container.appendChild(div);
      document.body.appendChild(container);
      document.body.style.overflowY = 'hidden';

      const closeDialog = () => {
        document.body.style.overflowY = '';
        container.remove();
      };

      const nextDialog = ts => {
        info.remove();
        cancel.remove();
        ok.innerText = 'Close';
        description.innerHTML = `Thank you for reporting an issue with MyLO MATE, ${user.FirstName}. A log file "MyLO-MATE-${user.UniqueName}-Bug-Report-${ts}.json" has just been downloaded.</p>
			<p>Please send this file to <a href="mailto:MyLO.MATE@utas.edu.au?subject=MyLO%20MATE%20Bug%20Report">MyLO.MATE@utas.edu.au</a> and we will help out the best we can.`;
        ok.addEventListener('click', e => {
          closeDialog();
        });
      };

      ok.addEventListener('click', e => {
        let ts = Date.now();
        res({ status: 'ok', data: info.shadowRoot.querySelector('textarea').value, ts });
        nextDialog(ts);
      });

      cancel.addEventListener('click', e => {
        res({ status: 'cancel', data: '' });
        closeDialog();
      });
    });

  const GenerateBugReport = async () => {
    let info = await MoreInfoDialog();
    if (info.status == 'ok') {
      let data = {};
      data.OrgId = document.querySelector('a[href*="/d2l/home"]').href.split('/').pop();
      data.Settings = await FetchMMSettings();
      data.UnitData = await api(`/d2l/api/lp/1.22/enrollments/myenrollments/${data.OrgId}`, { first: true });
      data.CurrentUser = user;
      data.CurrentURL = window.location.href;
      data.MoreInfo = info.data;

      let blob = new Blob([JSON.stringify(data)]);

      let a = document.createElement('a');
      a.href = URL.createObjectURL(blob);
      a.download = `MyLO-MATE-${data.CurrentUser.UniqueName}-Bug-Report-${info.ts}.json`;
      a.click();
      a.remove();
    }
  };

  chrome.runtime.onMessage.addListener(async (req, sender, res) => {
    if (req.request == 'bug-report') GenerateBugReport();
  });
})();
