(async function () {
  const type = 'checklists';
  const titleBreak = '\u000A';
  const ou = window.location.searchParams.ou;

  document.querySelectorAll('img[title="Has Release Conditions"]').forEach(async el => {
    const quizId = new URL(el.closest('th').querySelector('[onclick*="setreturnpoint" i]').href).searchParams.get('qi');
    const restrictions = await getReleaseConditions(ou, type, quizId);
    const text = [el.title].concat(restrictions.map(v => ` - ${v}`));
    el.title = text.join(titleBreak);
  });
})();
