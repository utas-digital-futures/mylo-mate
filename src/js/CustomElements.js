class Help extends HTMLElement {
  static get observedAttributes() {
    return ['heading'];
  }

  get heading() {
    return this.getAttribute('heading');
  }
  set heading(val) {
    this.setAttribute('heading', val);
  }

  constructor() {
    super();

    if (this.shadowRoot != null) return;

    const shadow = this.attachShadow({
      mode: 'open',
    });

    const t = document.createElement('template');
    t.innerHTML = `
      <style> d2l-icon { cursor: help; height: 1em; width: 1em; } </style>
      <d2l-icon icon="tier1:help"></d2l-icon>
      <mm-dialog preserve hide-cancel heading="${this.heading ?? this.title ?? ''}"><slot></slot></mm-dialog>
    `;

    shadow.appendChild(document.importNode(t.content, true));

    shadow.querySelector('d2l-icon').addEventListener('click', e => {
      const dialog = shadow.querySelector('mm-dialog');
      if (dialog.querySelector('slot').innerHTML.trim().length > 0) {
        dialog.open = true;
      }
    });
  }
}

/**
 * class Loader extends HTMLElement {
 *   static get observedAttributes() {
 *     return ['value', 'max', 'hidden', 'width', 'height', 'background', 'center'];
 *   }
 *
 *   // Set the background colour
 *   get background() {
 *     return this.getAttribute('background');
 *   }
 *   set background(val) {
 *     this.setAttribute('background', val);
 *   }
 *
 *   // Set whether the spinner is spinning, or representing a value
 *   get indeterminate() {
 *     return !(this.hasAttribute('value') && this.hasAttribute('max'));
 *   }
 *   set indeterminate(val) {
 *     if (val) {
 *       this.removeAttribute('value');
 *       this.removeAttribute('max');
 *     } else {
 *       this.setAttribute('value', 0);
 *       this.setAttribute('max', 1);
 *     }
 *   }
 *
 *   // Set the currently presented value
 *   get value() {
 *     return parseInt(this.getAttribute('value'));
 *   }
 *   set value(val) {
 *     this.setAttribute('value', val);
 *   }
 *
 *   // Set the max value, for percentage representation
 *   get max() {
 *     return parseInt(this.getAttribute('max'));
 *   }
 *   set max(val) {
 *     this.setAttribute('max', val);
 *   }
 *
 *   // Flag whether the loader is visible
 *   get hidden() {
 *     return this.getAttribute('hidden');
 *   }
 *   set hidden(val) {
 *     this[val ? 'setAttribute' : 'removeAttribute']('hidden', val);
 *   }
 *
 *   // Set the width of the loader
 *   get width() {
 *     return parseInt(this.getAttribute('width'));
 *   }
 *   set width(val) {
 *     this.setAttribute('width', val);
 *   }
 *
 *   // Set the height of the loader
 *   get height() {
 *     return parseInt(this.getAttribute('height'));
 *   }
 *   set height(val) {
 *     this.setAttribute('height', val);
 *   }
 *
 *   // Internal function to hide/show the loader
 *   hide() {
 *     this.hidden = true;
 *   }
 *   show() {
 *     this.hidden = false;
 *   }
 *
 *   constructor() {
 *     super();
 *     if (this.shadowRoot != null) return;
 *     const shadow = this.attachShadow({
 *       mode: 'open',
 *     });
 *
 *     const t = document.createElement('template');
 *     t.innerHTML = `
 *       <style>
 *       @keyframes spin {
 *         0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }
 *         100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }
 *       }
 *
 *       :host {
 *         display: block;
 *         contain: content;
 *         --bg-light: rgb(214, 214, 214);
 *         --bg-dark: rgb(91,91,91);
 *       }
 *
 *       :host([center]) #container {
 *         display: flex;
 *         justify-content: center;
 *         align-items: center;
 *       }
 *
 *       #container > * {
 *         box-sizing: border-box;
 *       }
 *
 *       #loader {
 *         width: 40px;
 *         height: 40px;
 *         border-radius: 100%;
 *         background-color: var(--bg-light);
 *         display: flex;
 *         justify-content: center;
 *         align-items: center;
 *
 *         background-color:var(--bg-dark);
 *         background-image:
 *           linear-gradient(90deg, transparent 50%, var(--bg-light) 50%),
 *           linear-gradient(90deg, var(--bg-light) 50%, transparent 50%);
 *       }
 *
 *       #fill {
 *         display:block;
 *         width:80%;
 *         height:80%;
 *         border-radius: 100%;
 *       }
 *
 *       :host([value]) #loader, :host([max]) #loader {
 *         animation: initial;
 *       }
 *       :host([hidden]) #loader { display: none; }
 *       </style>
 *       <div id="container">
 *         <div id="loader">
 *           <div id="fill" style="background-color: white;"></div>
 *         </div>
 *       </div>
 *     `;
 *
 *     shadow.appendChild(document.importNode(t.content, true));
 *   }
 *
 *   setFill(deg) {
 *     const bg =
 *       deg <= 180
 *         ? `linear-gradient(${
 *             90 + deg
 *           }deg, transparent 50%, var(--bg-light) 50%),linear-gradient(90deg, var(--bg-light) 50%, transparent 50%)`
 *         : `linear-gradient(${
 *             deg - 90
 *           }deg, transparent 50%, var(--bg-dark) 50%),linear-gradient(90deg, var(--bg-light) 50%, transparent 50%)`;
 *
 *     this.shadowRoot.querySelector('#loader').style.backgroundImage = bg;
 *   }
 *
 *   spin() {
 *     this.setFill(45);
 *     this.shadowRoot.querySelector('#loader').style.animation = 'spin 1s infinite linear';
 *   }
 *
 *   attributeChangedCallback(name, oldV, newV) {
 *     if (!this.indeterminate && (name === 'value' || name === 'max')) {
 *       const v = this.value;
 *       const m = this.max;
 *       let i = 0;
 *       if (v === 0 || m === 0) {
 *         if (v === 0) i = 0;
 *         if (m === 0) i = 1;
 *       } else {
 *         i = v / m;
 *       }
 *
 *       this.setFill(360 * i);
 *       this.shadowRoot.querySelector('#loader').style.animation = '';
 *     } else {
 *       this.spin();
 *     }
 *
 *     if (name === 'background') {
 *       this.shadowRoot.querySelector('#fill').style.backgroundColor = this.background;
 *     }
 *
 *     if (name === 'width' || name === 'height') {
 *       if (this.width && !this.height) this.height = this.width;
 *       if (this.height && !this.width) this.width = this.height;
 *
 *       this.shadowRoot.querySelector('#loader').style.width = `${this.width}px`;
 *       this.shadowRoot.querySelector('#loader').style.height = `${this.height}px`;
 *     }
 *   }
 *
 *   connectedCallback() {
 *     if (this.indeterminate) {
 *       this.spin();
 *     }
 *     if (this.background) {
 *       this.shadowRoot.querySelector('#fill').style.backgroundColor = this.background;
 *     }
 *   }
 * }
 */

class Loader extends HTMLElement {
  static get observedAttributes() {
    return ['value', 'max', 'width', 'height', 'show-progress'];
  }

  // Set whether the spinner is spinning, or representing a value
  get indeterminate() {
    return !(this.hasAttribute('value') && this.hasAttribute('max'));
  }

  set indeterminate(val) {
    if (val) {
      this.removeAttribute('value');
      this.removeAttribute('max');
    } else {
      this.setAttribute('value', 0);
      this.setAttribute('max', 1);
    }
  }

  // Set the currently presented value
  get value() {
    return parseInt(this.getAttribute('value'));
  }
  set value(val) {
    this.setAttribute('value', val);
  }

  // Set the max value, for percentage representation
  get max() {
    return parseInt(this.getAttribute('max'));
  }
  set max(val) {
    this.setAttribute('max', val);
  }

  // Set the width of the loader
  get width() {
    return parseInt(this.getAttribute('width'));
  }
  set width(val) {
    this.setAttribute('width', val);
  }

  // Set the height of the loader
  get height() {
    return parseInt(this.getAttribute('height'));
  }
  set height(val) {
    this.setAttribute('height', val);
  }

  // Set the currently presented value
  get showProgress() {
    return this.hasAttribute('show-progress') && this.getAttribute('show-progress') != 'false';
  }
  set showProgress(val) {
    this.toggleAttribute('show-progress', val);
  }

  hide() {
    this.hidden = true;
  }
  show() {
    this.hidden = false;
  }

  constructor() {
    super();
    if (this.shadowRoot != null) return;
    const shadow = this.attachShadow({
      mode: 'open',
    });

    const t = document.createElement('template');
    t.innerHTML = `<canvas height="60" width="60"></canvas>`;

    shadow.appendChild(document.importNode(t.content, true));
  }

  /** Convert degrees to radians */
  D2R(degrees) {
    return ((degrees - 90) * Math.PI) / 180;
  }

  get label() {
    return this.innerHTML?.trim() ?? '';
  }

  #doSpin = false;
  #redraw(arc, offset = 0) {
    const canvas = this.shadowRoot.querySelector('canvas');
    const ctx = canvas.getContext('2d');

    const { width, height } = canvas;

    const hasLabel = this.label.length > 0;

    let x = width / 2;
    let y = height / 2;

    if (hasLabel) y -= 10;

    const radius = Math.max(width / 2 - (hasLabel ? 20 : 10), 10);

    const start = this.D2R(offset);
    const end = this.D2R(offset + arc);

    ctx.clearRect(0, 0, width, height);
    ctx.beginPath();
    ctx.lineWidth = 4;
    ctx.strokeStyle = 'rgb(91, 91, 91)';
    ctx.arc(x, y, radius, start, end);
    ctx.stroke();

    if (!this.indeterminate && this.showProgress) {
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      ctx.fillText(`${this.value ?? 0}/${this.max ?? 0}`, x, y);
    }

    if (hasLabel) {
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      ctx.font = '14px Arial';
      ctx.fillText(this.label, x, y + radius + 20, width - 10);
    }

    if (this.#doSpin) {
      requestAnimationFrame(() => this.#redraw(arc, offset + 8));
    }
  }

  draw(completion) {
    if (completion < 0) {
      // Let's do the indeterminate spin
      this.#doSpin = true;
      this.#redraw(60);
    } else {
      // Let's draw a circle.
      this.#doSpin = false;
      this.#redraw(360 * completion);
    }
  }

  spin() {
    this.draw(-1);
  }

  attributeChangedCallback(name) {
    if (this.isConnected && ['value', 'max'].includes(name)) {
      this.refresh();
    }

    if (['width', 'height'].includes(name)) {
      /** if (this.width && !this.height) this.height = this.width; */
      /** if (this.height && !this.width) this.width = this.height; */
      const width = this.width || this.height;
      const height = this.height || this.width;

      const canvas = this.shadowRoot.querySelector('canvas');

      canvas.width = width;
      canvas.height = height;
    }
  }

  refresh() {
    if (this.indeterminate) {
      this.spin();
    } else {
      const v = this.value || 0;
      const m = this.max || 1;
      const i = v / m;

      this.draw(Math.min(Math.max(i, 0), 1));
    }
  }

  mo = new MutationObserver(() => this.refresh());
  connectedCallback() {
    /** const ctx = this.shadowRoot.querySelector('canvas')?.getContext('2d'); */

    this.refresh();
    this.mo.observe(this, { subtree: true, childList: true, attributes: true });
  }

  disconnectedCallback() {
    this.mo.disconnect();
  }
}

/**
 * The source for the Dialog custom element
 */
const DialogTemplate = document.createElement('template');
DialogTemplate.innerHTML = `
  <style>
    body.hide-scrollbar {
      overflow: hidden;
    }

    :host {
      transition: opacity 0.2s linear, visibility 0.2s linear;
      opacity: 0;
      visibility: hidden;
    }

    :host([open]) {
      visibility: visible;
      opacity: 1;
    }

    [role="wrapper"] [role="inner"] h2,
    [role="wrapper"] [role="inner"] h3 {
      font-weight: bold;
      margin-bottom: 10px;
      border-bottom: thin lightgrey solid;
      font-size: 0.9em;
    }

    [role="wrapper"] [role="inner"] h3 {
      font-size: 0.8em;
    }

    [role="wrapper"] p {
      margin: 0;
    }

    [role="wrapper"] {
      z-index: 1006;
      overflow: hidden;
      position: absolute;
      padding: 3px;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      display: flex;
      justify-content: center;
      align-items: center;
      box-sizing: border-box;
    }

    [role="wrapper"] [role="inner"] {
      display: flex;
      flex-direction: column;
      background-color: #fff;
      border: 1px solid #d3d9e3;
      border-radius: .4rem;
      box-shadow: 0 2px 12px rgba(86,90,92,.25);
      box-sizing: border-box;
      width: auto;
      height: auto;
      position: fixed;
      min-width: 400px;
      min-height: 100px;
      max-height: 90%;
      max-width: 90%;
      top: 5vh;
    }

    [role="wrapper"] [role="inner"]>div {
      padding: 20px;
    }

    [role="wrapper"] [role="inner"] .buttons {
      display: flex;
      justify-content: flex-end;
    }
    
    [role="wrapper"] [role="inner"] .heading {
      padding: 10px 20px;
    }

    [role="wrapper"] [role="inner"] .buttons .d2l-button {
      margin-right: .75rem;
      width: auto;
      border-radius: .3rem;
      border-style: solid;
      border-width: 1px;
      box-sizing: border-box;
      cursor: pointer;
      display: inline-block;
      margin: 0;
      min-height: calc(2rem + 2px);
      outline: 0;
      text-align: center;
      transition: box-shadow .2s;
      user-select: none;
      vertical-align: middle;
      white-space: nowrap;
      box-shadow: 0 0 0 4px transparent;
      font-family: inherit;
      padding: .5rem 1.5rem;
      font-size: .7rem;
      line-height: 1rem;
      font-weight: 700;
      letter-spacing: .2px;
    }

    [role="wrapper"] [role="inner"] .buttons button:not(:last-child) {
      margin-right: 20px;
    }

    [role="wrapper"] [role="inner"] .buttons .d2l-button[primary] {
      background-color: #006fbf;
      border-color: #005694;
      color: #fff;
    }

    :host([hide-cancel]) .buttons button[role="cancel"] { display: none; }

    :host([fullscreen]) [role="inner"] {
      width: 90vw;
      height: 90vh;
    }

    [role="wrapper"] [role="inner"] .content {
      overflow-y: auto;
      height: 100%;
      padding: 0 20px;
    }

    [role="wrapper"] h2:not(:first-child),
    [role="wrapper"] h3:not(:first-child) {
      margin-top: 20px;
    }

    [role="modal"] {
      position: fixed;
      left: 0;
      top: 0;
      bottom: 0;
      width: 100%;
      background-color: rgba(249, 250, 251, 0.7);
      z-index: 1000;
    }

    *[hidden] {
      display: none !important;
    }
  </style>
  <div role="modal">
    <div role="wrapper">
      <div role="inner">
        <div class="heading">
          <h2 role="title"></h2>
        </div>
        <div class="content">
          <slot></slot>
        </div>
        <div class="buttons">
        </div>
      </div>
    </div>
  </div>
`;

class Dialog extends HTMLElement {
  static get observedAttributes() {
    return [
      'width',
      'height',
      'max-height',
      'max-width',
      'heading',
      'hide-cancel',
      'open',
      'preserve',
      'confirm-text',
      'padding',
      'disable-confirm',
      'hide-button-row',
      'align-buttons',
      'fullscreen',
    ];
  }

  get width() {
    return this.getAttribute('width');
  }
  set width(val) {
    this.setAttribute('width', val);
  }

  get height() {
    return this.getAttribute('height');
  }
  set height(val) {
    this.setAttribute('height', val);
  }

  get maxHeight() {
    return this.getAttribute('max-height');
  }
  set maxHeight(val) {
    this.setAttribute('max-height', val);
  }

  get maxWidth() {
    return this.getAttribute('max-width');
  }
  set maxWidth(val) {
    this.setAttribute('max-width', val);
  }

  get heading() {
    return this.getAttribute('heading');
  }
  set heading(val) {
    this.setAttribute('heading', val);
  }

  get padding() {
    return this.getAttribute('padding');
  }
  set padding(val) {
    this.setAttribute('padding', val);
  }

  get confirmText() {
    return this.getAttribute('confirm-text');
  }
  set confirmText(val) {
    this.setAttribute('confirm-text', val);
  }

  get disableConfirm() {
    return this.getAttribute('disable-confirm');
  }
  set disableConfirm(val) {
    this.setAttribute('disable-confirm', val);
  }

  get hideCancel() {
    return this.hasAttribute('hide-cancel');
  }
  set hideCancel(state) {
    this.toggleAttribute('hide-cancel', state);
  }

  get open() {
    return this.hasAttribute('open');
  }
  set open(state) {
    this.toggleAttribute('open', state);
  }

  get preserve() {
    return this.hasAttribute('preserve');
  }
  set preserve(state) {
    this.toggleAttribute('preserve', state);
  }

  get hideButtonBar() {
    return this.hasAttribute('hide-button-row');
  }
  set hideButtonBar(state) {
    this.toggleAttribute('hide-button-row', state);
  }

  get alignButtons() {
    return this.getAttribute('align-buttons') || 'left';
  }
  set alignButtons(state) {
    this.setAttribute('align-buttons', state);
  }

  dismiss() {
    if (!this.preserve) this.remove();
    else this.open = false;

    this.dispatchEvent(new Event('dismissed'));
  }

  show() {
    this.open = true;
  }

  buttons = [
    { primary: true, text: 'OK', role: 'ok' },
    { primary: false, text: 'Cancel', role: 'cancel' },
  ];

  registerNewButton({ primary = false, text, role, index = -1 }) {
    const ctx = { primary, text, role };
    if (!this.shadowRoot) {
      this.buttons = this.buttons.splice(index, 0, ctx);
    } else {
      this._createNewButton(ctx, index);
    }
  }

  _createNewButton(btn, index) {
    const buttons = this.shadowRoot.querySelector('.buttons');
    if (!buttons) return;

    const button = document.createElement('button');
    button.classList.add('d2l-button');
    button.toggleAttribute('primary', btn.primary === true);
    button.setAttribute(
      'role',
      btn.role ??
        btn.text
          .toLowerCase()
          .trim()
          .replace(/[^a-z0-9-_\.]/gi, '-')
    );
    button.innerText = btn.text;

    // If we're working with a negative index, work from the end of the children.
    if (index < 0) index += buttons.children.length;
    // If the index is _still_ less than zero, set it to zero to avoid OOB error;
    if (index < 0) index = 0;

    // Rather than doing clever math and logic, if the requested index is larger than the children, just append.
    if (index > buttons.children.length) buttons.append(button);
    else buttons.insertBefore(button, buttons.children[index]);
  }

  constructor() {
    super();
    if (this.shadowRoot != null) return;
    const shadow = this.attachShadow({ mode: 'open' });
    shadow.appendChild(document.importNode(DialogTemplate.content, true));

    // For queued buttons, append them to the DOM here
    this.buttons.forEach(this._createNewButton.bind(this));

    // Listen to click events, and fire an event matching their role name
    this.shadowRoot.querySelector('.buttons').addEventListener('click', e => {
      // This was a click in the container, not a button.
      if (!e.target.matches('button')) return;

      // Fire the event (events manually dispatched are always synchronous) and wait for the result.
      const shouldClose = this.dispatchEvent(new CustomEvent(e.target.getAttribute('role'), { cancelable: true }));

      // If e.preventDefault() wasn't called, then go ahead and dismiss the dialog.
      if (shouldClose) this.dismiss();
    });
  }

  updateHeading() {
    this.shadowRoot.querySelector('[role="title"]').innerHTML = this.heading;
  }

  updateSize() {
    const elem = this.shadowRoot.querySelector('[role="wrapper"] [role="inner"]');
    if (this.maxHeight) elem.style.maxHeight = `${this.maxHeight}`;
    if (this.maxWidth) elem.style.maxWidth = `${this.maxWidth}`;
    if (this.height) elem.style.height = `${this.height}`;
    if (this.width) elem.style.width = `${this.width}`;
  }

  updatePadding() {
    const elem = this.shadowRoot.querySelector('[role="wrapper"] [role="inner"]');
    elem.style.padding = `${this.padding || ''}`;
  }

  updateConfirmText() {
    this.shadowRoot.querySelector('button[role="ok"]').innerText = this.confirmText ?? 'OK';
  }

  updateConfirmEnable() {
    this.shadowRoot.querySelector('button[role="ok"]').enabled = !this.disableConfirm;
  }

  hideButtons() {
    this.shadowRoot.querySelector('.buttons').hidden = this.hideButtonBar;
  }

  alignButtonRow() {
    let position = '';
    const target = this.alignButtons;

    if (target == 'left') position = 'flex-start';
    else if (['center', 'centre', 'middle'].includes(target)) position = 'center';
    else position = 'flex-end';

    this.shadowRoot.querySelector('.buttons').style.justifyContent = position;
  }

  attributeChangedCallback(name, oldV, newV) {
    if (name === 'heading') this.updateHeading();
    if (name === 'max-height') this.updateSize();
    if (name === 'max-width') this.updateSize();
    if (name === 'height') this.updateSize();
    if (name === 'width') this.updateSize();
    if (name === 'padding') this.updatePadding();
    if (name === 'hide-button-row') this.hideButtons();
    if (name === 'confirm-text') this.updateConfirmText();
    if (name === 'disable-confirm') this.updateConfirmEnable();
    if (name === 'align-buttons') this.alignButtonRow();
    if (name === 'open') this.dispatchEvent(new Event(this.open ? 'open' : 'close'));
  }

  connectedCallback() {
    if (this.height || this.width || this.maxHeight || this.maxWidth) this.updateSize();
    if (this.heading) this.updateHeading();
    if (this.confirmText) this.updateConfirmText();
    if (this.disableConfirm) this.updateConfirmEnable();
    this.hideButtons();
    this.alignButtonRow();
  }
}

/**
 * The source for the Alert custom element
 */
const AlertTemplate = document.createElement('template');
AlertTemplate.innerHTML = `
  <style>
    :host {
      transition: opacity 0.2s linear, visibility 0.2s linear;
      opacity: 0;
      visibility: hidden;
    }

    :host([open]) {
      visibility: visible;
      opacity: 1;
    }

    [role="wrapper"] [role="inner"] h2,
    [role="wrapper"] [role="inner"] h3 {
      font-weight: bold;
      margin-bottom: 10px;
      border-bottom: thin lightgrey solid;
      font-size: 0.9em;
    }

    [role="wrapper"] [role="inner"] h3 {
      font-size: 0.8em;
    }

    [role="wrapper"] p {
      margin: 0;
    }

    [role="wrapper"] {
      z-index: 1006;
      overflow: hidden;
      position: absolute;
      padding: 3px;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      display: flex;
      justify-content: center;
      align-items: center;
      box-sizing: border-box;
    }

    [role="wrapper"] [role="inner"] {
      display: flex;
      flex-direction: column;
      background-color: #fff;
      border: 1px solid #d3d9e3;
      border-radius: .4rem;
      box-shadow: 0 2px 12px rgba(86,90,92,.25);
      box-sizing: border-box;
      width: auto;
      height: auto;
      position: fixed;
      min-width: 400px;
      max-height: 90%;
      max-width: 650px;
      top: 5vh;
    }

    [role="wrapper"] [role="inner"]>div {
      padding: 20px;
    }

    [role="wrapper"] [role="inner"] .buttons .d2l-button {
      margin-right: .75rem;
      width: auto;
      border-radius: .3rem;
      border-style: solid;
      border-width: 1px;
      box-sizing: border-box;
      cursor: pointer;
      display: inline-block;
      margin: 0;
      min-height: calc(2rem + 2px);
      outline: 0;
      text-align: center;
      transition: box-shadow .2s;
      user-select: none;
      vertical-align: middle;
      white-space: nowrap;
      box-shadow: 0 0 0 4px transparent;
      font-family: inherit;
      padding: .5rem 1.5rem;
      font-size: .7rem;
      line-height: 1rem;
      font-weight: 700;
      letter-spacing: .2px;
    }

    [role="wrapper"] [role="inner"] .buttons .d2l-button[primary] {
      background-color: #006fbf;
      border-color: #005694;
      color: #fff;
    }

    [role="wrapper"] [role="inner"] .heading {
      padding-top: 0;
      padding-bottom: 0;
    }

    [role="wrapper"] [role="inner"] .content {
      padding-top: 10px;
      overflow-y: auto;
    }

    [role="wrapper"] h2:not(:first-child),
    [role="wrapper"] h3:not(:first-child) {
      margin-top: 20px;
    }

    [role="modal"] {
      position: fixed;
      left: 0;
      top: 0;
      bottom: 0;
      width: 100%;
      background-color: #f9fafb;
      z-index: 1000;
      opacity: .7;
    }
  </style>
  <div role="modal"></div>
  <div role="wrapper">
    <div role="inner">
      <div class="heading">
        <h2 role="title"></h2>
      </div>
      <div class="content">
        <slot></slot>
      </div>
      <div class="buttons">
        <button class="d2l-button" primary role="ok">OK</button>
      </div>
    </div>
  </div>
`;

class Alert extends HTMLElement {
  static get observedAttributes() {
    return ['heading', 'open'];
  }

  get open() {
    return this.hasAttribute('open');
  }
  set open(state) {
    if (state === true) {
      this.setAttribute('open', '');
    } else {
      this.removeAttribute('open');
    }
  }

  show() {
    this.open = true;
  }

  constructor() {
    super();
    if (this.shadowRoot != null) return;
    const shadow = this.attachShadow({
      mode: 'open',
    });
    shadow.appendChild(document.importNode(AlertTemplate.content, true));

    this.shadowRoot.querySelector('button[role="ok"]').addEventListener('click', e => {
      this.dispatchEvent(new CustomEvent('ok'));
      document.body.style.overflowY = '';
      this.remove();
    });
  }

  updateHeading() {
    this.shadowRoot.querySelector('[role="title"]').innerText = this.heading;
  }

  attributeChangedCallback(name, oldV, newV) {
    if (name === 'heading') this.updateHeading();
    if (name === 'open') {
      this.dispatchEvent(new Event(this.open ? 'open' : 'close'));
      document.body.style.overflowY = this.open ? 'hidden' : '';
    }
  }

  connectedCallback() {
    if (!this.heading) this.heading = 'MyLO MATE Alert';
    this.updateHeading();
  }
}

/**
 * The source for the Dialog custom element
 */
const LoadingModalTemplate = document.createElement('template');
LoadingModalTemplate.innerHTML = `
  <style>
    :host {
      transition: opacity 0.2s linear, visibility 0.2s linear;
      opacity: 0;
      visibility: hidden;
    }

    :host([open]) {
      visibility: visible;
      opacity: 1;
    }

    [role="wrapper"] {
      z-index: 1006;
      overflow: hidden;
      position: fixed;
      padding: 3px;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      display: flex;
      justify-content: center;
      align-items: center;
      box-sizing: border-box;
      flex-direction: column;
    }

    [role="modal"] {
      position: fixed;
      left: 0;
      top: 0;
      bottom: 0;
      width: 100%;
      background-color: #f9fafb;
      z-index: 1000;
      opacity: .9;
    }
  </style>
  <div role="modal"></div>
  <div role="wrapper">
    <mm-loader></mm-loader>
    <p role="text"></p>
  </div>
`;

class LoadingModal extends HTMLElement {
  static get observedAttributes() {
    return ['width', 'height', 'open', 'preserve', 'text', 'value', 'max', 'indeterminate'];
  }

  get width() {
    return this.getAttribute('width');
  }
  set width(val) {
    this.setAttribute('width', val);
  }

  get height() {
    return this.getAttribute('height');
  }
  set height(val) {
    this.setAttribute('height', val);
  }

  get text() {
    return this.getAttribute('text');
  }
  set text(val) {
    this.setAttribute('text', val);
  }

  get value() {
    return this.getAttribute('value');
  }
  set value(val) {
    this.setAttribute('value', val);
  }
  get max() {
    return this.getAttribute('max');
  }
  set max(val) {
    this.setAttribute('max', val);
  }
  get indeterminate() {
    return this.getAttribute('indeterminate');
  }
  set indeterminate(val) {
    this.setAttribute('indeterminate', val);
  }

  get open() {
    return this.hasAttribute('open');
  }
  set open(state) {
    if (state === true) this.setAttribute('open', '');
    else this.removeAttribute('open');
  }

  get preserve() {
    return this.hasAttribute('preserve');
  }
  set preserve(state) {
    if (state === true) this.setAttribute('preserve', '');
    else this.removeAttribute('preserve');
  }

  constructor() {
    super();

    if (this.shadowRoot != null) return;

    const shadow = this.attachShadow({
      mode: 'open',
    });
    shadow.appendChild(document.importNode(LoadingModalTemplate.content, true));
  }

  updateLoader() {
    const loader = this.shadowRoot.querySelector('mm-loader');
    loader.setAttribute('value', this.getAttribute('value'));
    loader.setAttribute('max', this.getAttribute('max'));
    loader.setAttribute('indeterminate', this.getAttribute('indeterminate'));
  }

  updateText() {
    this.shadowRoot.querySelector('[role="text"]').innerText = this.text;
  }

  attributeChangedCallback(name, oldV, newV) {
    if (name === 'height') this.updateSize();
    if (name === 'width') this.updateSize();
    if (name === 'text') this.updateText();
    if (['value', 'max', 'indeterminate'].includes(name)) this.updateLoader();
  }

  connectedCallback() {
    if (this.height || this.width) this.updateSize();
    if (this.text) this.updateText();
  }
}

customElements.define('mm-fullscreen-loading-modal', LoadingModal);
customElements.define('mm-loader', Loader);
/** customElements.define('mm-loader-canvas', Loader2); */
customElements.define('mm-dialog', Dialog);
customElements.define('mm-alert', Alert);
customElements.define('mm-help', Help);
/** customElements.define('mm-file-zone', FileDropZone); */
