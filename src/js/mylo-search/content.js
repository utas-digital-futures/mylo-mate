/**
 * @affects MyLO
 * @problem It's not clear which delivery units in the Waffle Menu refer to.
 * @solution Automatically adds coloured backgrounds to indicate the current status of each unit (adjusting for accessibility where required).
 * @target Academic and support staff
 * @impact High
 * @savings 30 seconds
 */

(async function () {
  // if(window.SearchReady) return;
  // window.SearchReady = true;

  // await until(() => document.body.getAttribute('mm-abort') != null);
  // if(document.body.getAttribute('mm-abort') == "true") return;
  // const store = await new Promise(res => chrome.storage.sync.get(null, res));
  // if (!store.mylo.highlightRowBackgrounds) return;

  let units = {};
  const now = new Date();
  const orange_before = 14;
  const orange_after = 21;

  /*****************************************
   *                                       *
   * Handles autofocusing the search field *
   *                                       *
   *****************************************/

  const dropdown = document.querySelector('d2l-dropdown-content');

  const focus = async () => {
    if (!dropdown.hasAttribute('opened')) return;

    const search = dropdown.querySelector('d2l-input-search');

    await until(() => sdqs.querySelectorDeep('.d2l-input', search));
    const input = sdqs.querySelectorDeep('.d2l-input', search);

    setTimeout(() => input.focus(), 250);
  };

  if (dropdown) {
    const watch_dialog = new MutationObserver(focus);
    watch_dialog.observe(dropdown, { attributes: true });
  }

  /****************************************
   *                                      *
   * Handles applying colours to the rows *
   *                                      *
   ****************************************/

  const applyStyles = () => {
    document.querySelectorAll('.d2l-courseselector-wrapper ul.d2l-datalist div[data-org-unit-id]').forEach(node => {
      let orgID = node.getAttribute('data-org-unit-id');
      let row = node.parent('li');

      let unit = units[orgID];
      if (!unit) return;

      const start = new Date(unit.Access.StartDate); // The date the unit opens
      const end = new Date(unit.Access.EndDate); // The date the unit closes
      end.setTime(end.getTime() - orange_before * 86400000);
      const exam = new Date();
      exam.setTime(end.getTime() + (orange_before + orange_after) * 86400000); // The date the unit closes, plus {exam_days} days.

      let className = '';

      if (start > now) {
        /* This is a future unit. Just leave it as is. */
      } else if (start < now && end > now) {
        /* This is a current unit. */ className = 'mylo-mate-current';
      } else if (end < now && exam > now) {
        /* This is in the exam window. */ className = 'mylo-mate-marking-period';
      } else if (exam < now) {
        /* This is past it's exam window. */ className = 'mylo-mate-ended';
      }

      if (className !== '') row.classList.add(className);
    });
  };

  let m = new MutationObserver(applyStyles);

  (await api('/d2l/api/lp/1.24/enrollments/myenrollments/?canAccess=true&orgUnitTypeId=3')).forEach(unit => {
    units[unit.OrgUnit.Id] = unit;
  });
  if (document.querySelector('.d2l-courseselector-wrapper ul.d2l-datalist')) {
    applyStyles();
    if (document.querySelector('.d2l-courseselector-wrapper ul.d2l-datalist'))
      m.observe(document.querySelector('.d2l-courseselector-wrapper ul.d2l-datalist'), { childList: true, subtree: true });
  } else {
    let mo = new MutationObserver(e => {
      if (document.querySelector('.d2l-courseselector-wrapper ul.d2l-datalist')) {
        applyStyles();
      }
    });
    if (document.querySelector('d2l-dropdown-content'))
      mo.observe(document.querySelector('d2l-dropdown-content'), { childList: true, subtree: true });
  }
})();
