import { LitElement, html, css } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';

@customElement('mm-file-zone')
class FileDropZone extends LitElement {
  static styles = css`
    :host {
      display: block;
    }

    :host([clicktobrowse]) .dropzone {
      background: rgba(255, 255, 255, 0.4);
      width: 100%;
      padding: 20px;
      box-sizing: border-box;
      display: flex;
      justify-content: space-between;
      align-items: center;
      flex-direction: column;
      border: 6px dashed darkgrey;
      border-radius: 6px;
      cursor: pointer;
      text-align: center;
    }

    .dropzone[data-file-dropped='true'][data-file-declined='false'] {
      border-color: green;
      background: rgba(0, 100, 0, 0.4);
    }

    .dropzone[data-file-dropped='true'][data-file-declined='true'] {
      border-color: red;
      background: rgba(100, 0, 0, 0.4);
    }

    :host([file-over]) .dropzone,
    :host([clicktobrowse]) .dropzone:hover {
      background: rgba(100, 100, 100, 0.4);
      border-color: var(--d2l-color-feedback-action);
    }

    :host([selector]) .dropzone {
      display: none;
    }
  `;

  @property()
  selector: string = '';

  @property()
  text: string = 'Drag file here or click to browse';

  @property({ type: Boolean })
  draganddrop: boolean = false;

  @property({ type: Boolean })
  clicktobrowse: boolean = false;

  @property({ type: Boolean })
  showDropZone: boolean = false;

  @property({ type: Boolean })
  disabled: boolean = false;

  @property()
  accept: string = '';

  @property({ reflect: true, type: Boolean, attribute: 'file-over' })
  fileOver = false;

  constructor() {
    super();

    this.addEventListener('dragover', this.OnFileOver);
    this.addEventListener('dragenter', this.OnFileOver);
    this.addEventListener('dragleave', this.OnFileOut);
    this.addEventListener('drop', this.OnFileDrop);
  }

  private droppedFile = false;
  private declinedStatus = false;

  @state()
  message?: string = null;

  render() {
    return html`
      <div
        class="dropzone"
        @click="${this.OnDropZoneClick}"
        ?data-file-dropped=${this.droppedFile}
        ?data-file-declined=${this.declinedStatus}
      >
        <div class="overlay" ?hidden=${!this.showDropZone}>
          <d2l-icon icon="tier1:upload"></d2l-icon>
          <p>${this.message ?? this.text}</p>
        </div>
        <slot></slot>
      </div>
    `;
  }

  private enterTarget = null;
  private OnFileOver = (e: Event) => {
    if (this.disabled) return;
    if (!this.draganddrop) return;
    e.preventDefault();
    e.stopPropagation();

    this.fileOver = true;
    this.enterTarget = e.target;
    this.dispatchEvent(new Event('file-over', { bubbles: true, composed: true }));
  };

  private OnFileOut = (e: Event) => {
    if (this.disabled) return;
    if (!this.draganddrop) return;
    if (this.enterTarget != e.target) return;

    e.preventDefault();
    e.stopPropagation();

    this.fileOver = false;
    this.enterTarget = null;
    this.dispatchEvent(new Event('file-out', { bubbles: true, composed: true }));
  };

  public files: FileList = null;
  private ProcessFiles = (files: FileList) => {
    if (files.length > 1) {
      this.message = 'Please only drop a single file.';
      return;
    }

    const file = files[0];
    const ext = '.' + file.name.split('.').pop().trim().toLowerCase();

    const invalid = this.accept && !this.accept.split(',').includes(ext);

    this.droppedFile = true;
    this.declinedStatus = invalid;

    if (invalid) {
      const formats = this.accept.split(',').join(', ');
      this.message = `File invalid. Please ensure you dropped a file of the following format: ${formats}`;
    } else {
      this.files = files;
      this.message = file.name;
    }
  };

  private OnFileDrop = (e: any) => {
    if (this.disabled) return;
    if (!this.draganddrop) return;
    e.preventDefault();
    e.stopPropagation();

    this.toggleAttribute('file-over', false);
    this.enterTarget = null;

    const files = e.dataTransfer.files;
    const ev = new CustomEvent('file-drop', { detail: { files }, bubbles: true, composed: true });
    this.dispatchEvent(ev);

    this.ProcessFiles(files);
  };

  OnDropZoneClick = (_: any) => {
    if (!this.clicktobrowse) return;

    const file = document.createElement('input');
    file.type = 'file';
    file.accept = this.accept;
    file.click();

    file.onchange = e => {
      this.ProcessFiles(file.files);
    };
  };
}
