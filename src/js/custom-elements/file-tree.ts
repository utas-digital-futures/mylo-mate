import { LitElement, html, css } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { repeat } from 'lit/directives/repeat.js';
import { classMap } from 'lit/directives/class-map.js';
import { styleMap } from 'lit/directives/style-map.js';
import { Ref, createRef, ref } from 'lit/directives/ref.js';

enum CheckboxEnum {
  INDETERMINATE,
  CHECKED,
  UNCHECKED,
}

interface BaseStructure {
  key: string;
  full_path: string;
  label: string;
  checked: CheckboxEnum;
  type: 'folder' | 'file';
}

interface FileStructure extends BaseStructure {
  type: 'file';
}

interface DirStructure extends BaseStructure {
  type: 'folder';
  expanded: boolean;
}

type TreeItemElement = FileStructure | DirStructure;
type TreeMap = Map<string, TreeItemElement>;
type SeperatedTreeMap = { dirs: Map<string, DirStructure>; files: Map<string, FileStructure> };

function childrenOf(target: string, tree: TreeMap, seperateDirectories: true): SeperatedTreeMap;
function childrenOf(target: string, tree: TreeMap, seperateDirectories: false): TreeMap;
function childrenOf(target: string, tree: TreeMap, seperateDirectories: boolean): SeperatedTreeMap | TreeMap {
  const test = `^${target}${target.endsWith('/') ? '' : '/'}[^/]*$`;
  const results = Array.from(tree).filter(([v]) => new RegExp(test, 'mi').test(v));

  if (seperateDirectories) {
    const dirs: Map<string, DirStructure> = new Map();
    const files: Map<string, FileStructure> = new Map();

    for (const [key, el] of results) {
      if (el.type == 'folder') {
        dirs.set(key, el);
      } else {
        files.set(key, el);
      }
    }
    return { dirs, files };
  } else {
    return new Map(results);
  }
}

/**
 *
 * 📆 TODO:
 *    - Provide a [base-path] property that allows certain paths to be squashed together (eg. `/content/enforced/<<path>>/` to prevent multiple unwanted levels)
 *    - Improve UI experience
 *    - Better icons/sizes/colours (definitely going to need Bealeage/Quinnput)
 */

@customElement('mm-file-tree')
export class FileTree extends LitElement {
  static get styles() {
    return [
      css`
        :host {
          display: block;
        }

        ul {
          list-style: none;
          margin: 0;
          padding: 0;
        }
      `,
    ];
  }

  @property()
  data: string[] = [];

  private parsed = new Map<string, TreeItemElement>();

  private toggleCollapse(e: any) {
    const { key, state } = e.detail;

    const el = this.parsed.get(key) as DirStructure;
    el.expanded = state;
    this.parsed.set(key, el);

    // Now that all the data is processed, request an update from the UI.
    this.requestUpdate();
  }

  private onToggle(e: any) {
    const { key, state } = e.detail;

    // Set the child element first
    const el = this.parsed.get(key);
    el.checked = state;
    this.parsed.set(key, el);

    // Update all children to the same state as the current checkbox
    const els = Array.from(this.parsed.entries());
    for (const [k, el] of els) {
      if (k.startsWith(key)) {
        el.checked = state;
        this.parsed.set(k, el);
      }
    }

    // Update the parent checkboxes accordingly
    const parents = key
      // Split the key per path
      .split('/')
      // Don't get the last element, we're only looking at the parents
      .slice(0, -1)
      // Rebuild each item to be the full path to that point
      .map((_, i: number, a: string[]) => a.slice(0, i + 1).join('/'))
      // Make sure we don't have any empty elements
      .filter((v: string) => v.trim().length > 0)
      // Work backwards up the tree, so that parent elements reflect updates from their children
      .reverse();

    /** Test if all immediate children are fully checked (not indeterminate or unchecked) */
    const childrenCheckState = (key: string) => {
      const children = Array.from(childrenOf(key, this.parsed, false).values());
      const checked = children.filter(v => v.checked === CheckboxEnum.CHECKED).length;
      const unchecked = children.filter(v => v.checked === CheckboxEnum.UNCHECKED).length;

      if (checked === children.length) return CheckboxEnum.CHECKED;
      if (unchecked === children.length) return CheckboxEnum.UNCHECKED;

      return CheckboxEnum.INDETERMINATE;
    };

    // For each parent (bubbling away from the target element)
    //  update the check state to be checked (if all children also checked),
    //  or indeterminate (if only some children checked)
    for (const key of parents) {
      const parent = this.parsed.get(key);
      parent.checked = childrenCheckState(key);
      this.parsed.set(key, parent);
    }

    // Now that all the data is processed, request an update from the UI.
    this.requestUpdate();
  }

  willUpdate() {
    this.data.forEach(path => {
      const elements = path.split('/');
      elements.forEach((v, i, a) => {
        const key = elements.slice(0, i + 1).join('/');
        if (key.trim().length == 0) return;

        const obj = {
          key,
          checked: CheckboxEnum.UNCHECKED,
          label: v,
          full_path: key,
        };

        if (i == a.length - 1) {
          // This is the end of the path; in this case, present it as a file.
          if (!this.parsed.has(key)) {
            this.parsed.set(key, {
              ...obj,
              type: 'file',
            });
          }
        } else {
          // Treat this component as a directory.
          if (!this.parsed.has(key)) {
            this.parsed.set(key, {
              ...obj,
              type: 'folder',
              expanded: true,
            });
          }
        }
      });
    });
  }

  render() {
    if (this.parsed.size == 0) return 'Nothing to show';

    return html`<ul @toggle-checkbox=${this.onToggle} @toggle-collapse=${this.toggleCollapse}>
      ${repeat(
        childrenOf('/', this.parsed, false),
        ([key]) => key,
        ([key, value]) => new FileTreeDirectory(this.parsed, value as DirStructure)
      )}
    </ul>`;
  }
}

@customElement('mm-file-tree-directory')
export class FileTreeDirectory extends LitElement {
  static get styles() {
    return [
      css`
        :host {
          display: block;
        }

        ul {
          list-style: none;
          margin: 0;
          margin-left: 1em;
          padding: 0;
          font-weight: normal !important;
        }

        li span {
          cursor: pointer;
          user-select: none;
        }

        li span.icon:before {
          font-family: 'FontAwesome';
          content: '';
          display: inline-block;
          color: hsl(0deg 0% 70%);
          margin-right: 0.4em;
          font-size: 0.8em;
          width: 0.6em;
        }

        li.expanded span.icon:before {
          content: '';
        }
      `,
    ];
  }

  @property()
  private entry: Map<string, TreeItemElement>;

  @property()
  private rendered: DirStructure;

  constructor(entry: TreeMap, rendered: DirStructure) {
    super();
    this.entry = entry;
    this.rendered = rendered;
  }

  private toggleCollapse(e: any) {
    const detail = {
      key: this.rendered.key,
      state: !this.rendered.expanded,
    };

    this.dispatchEvent(new CustomEvent('toggle-collapse', { detail, composed: true, bubbles: true }));
  }

  private toggleCheckbox(e: any) {
    const detail = {
      key: this.rendered.key,
      state: e.target.indeterminate
        ? CheckboxEnum.INDETERMINATE
        : e.target.checked
        ? CheckboxEnum.CHECKED
        : CheckboxEnum.UNCHECKED,
    };

    this.dispatchEvent(new CustomEvent('toggle-checkbox', { detail, composed: true, bubbles: true }));
  }

  private cb: Ref<HTMLInputElement> = createRef();
  updated() {
    const cb = this.cb.value!;
    if (this.rendered.checked === CheckboxEnum.INDETERMINATE) {
      cb.checked = false;
      cb.indeterminate = true;
    }
  }

  render() {
    const title = this.rendered.label;
    const state = this.rendered.checked;
    const expanded = this.rendered.expanded;

    const { dirs, files } = childrenOf(this.rendered.full_path, this.entry, true);

    return html`<li class="${classMap({ expanded })}">
      <input type="checkbox" @change=${this.toggleCheckbox} ?checked=${state === CheckboxEnum.CHECKED} ${ref(this.cb)} />
      <span @click="${this.toggleCollapse}" class="icon"> ${title} </span>
      ${expanded
        ? html`
            <ul>
              ${repeat(
                dirs,
                ([key]) => key,
                ([key, value]) => new FileTreeDirectory(this.entry, value)
              )}
              ${repeat(
                files,
                ([key]) => key,
                ([key, value]) => new FileTreeItem(this.entry, value)
              )}
            </ul>
          `
        : null}
    </li>`;
  }
}

@customElement('mm-file-tree-item')
export class FileTreeItem extends LitElement {
  static get styles() {
    return [
      css`
        :host {
          display: block;
        }

        li {
          padding: 0;
          display: flex;
          align-items: center;
          line-height: 1.4em;
          user-select: none;
        }

        label {
          cursor: pointer;
        }

        li label > span.icon:before {
          font-family: 'FontAwesome';
          content: '';
          display: inline-block;
          color: hsl(0deg 0% 70%);
          margin-right: 0.4em;
          font-size: 0.8em;
          width: 0.6em;
        }
      `,
    ];
  }

  @property()
  entry: TreeMap;

  @property()
  rendered: FileStructure;

  constructor(entry: TreeMap, rendered: FileStructure) {
    super();
    this.entry = entry;
    this.rendered = rendered;
  }

  private toggleCheckbox(e: any) {
    const detail = {
      key: this.rendered.key,
      state: e.target.checked ? CheckboxEnum.CHECKED : CheckboxEnum.UNCHECKED,
    };

    this.dispatchEvent(new CustomEvent('toggle-checkbox', { detail, composed: true, bubbles: true }));
  }

  render() {
    const title = this.rendered.label;
    const state = this.rendered.checked;

    return html`<li>
      <label>
        <input type="checkbox" @change=${this.toggleCheckbox} ?checked=${state === CheckboxEnum.CHECKED} />
        <span class="icon"></span>
        ${title}
      </label>
    </li>`;
  }
}
