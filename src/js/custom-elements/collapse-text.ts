import { LitElement, html, css } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { classMap } from 'lit/directives/class-map.js';
import { styleMap } from 'lit/directives/style-map.js';
import { Ref, ref, createRef } from 'lit/directives/ref.js';

@customElement('mm-collapse-text')
export class CollapseText extends LitElement {
  static get styles() {
    return [
      css`
        :host {
          display: block;
        }

        .collapsed {
          box-shadow: black 0px -19px 17px -30px inset;
        }

        .content {
          transition: max-height 300ms;
          overflow: hidden;
        }

        .read-more {
          text-align: right;
          cursor: pointer;
          color: #006fbf;
          margin: 0.5em 0;
        }

        .read-more:hover {
          text-decoration: underline;
        }
      `,
    ];
  }

  @property()
  collapse = 100;

  @state()
  isCollapsed = true;

  @state()
  showCollapse = true;

  private toggleHeight(e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    e.stopPropagation();

    this.isCollapsed = !this.isCollapsed;
  }

  private containerRef: Ref<HTMLDivElement> = createRef();
  render() {
    const maxHeight = this.isCollapsed ? this.collapse : Math.max(this.containerRef.value?.scrollHeight, this.collapse);

    return html`
      <div
        ${ref(this.containerRef)}
        class="${classMap({ content: true, collapsed: this.isCollapsed })}"
        style=${styleMap({ maxHeight: `${maxHeight}px` })}
      >
        <slot></slot>
      </div>
      <p ?hidden="${!this.showCollapse}" class="read-more" @click=${this.toggleHeight}>
        ${this.isCollapsed ? '+Read more' : '-Read less'}
      </p>
    `;
  }
}
