import { LitElement, html, css } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { styleMap } from 'lit/directives/style-map.js';

interface ButtonItem {
  primary: boolean;
  text: string;
  role?: string;
}

@customElement('mm-prompt')
export class Prompt extends LitElement {
  static styles = css`
    :host {
      transition: opacity 0.2s linear, visibility 0.2s linear;
      opacity: 0;
      visibility: hidden;
    }

    :host([open]) {
      visibility: visible;
      opacity: 1;
    }

    :host([hide-cancel]) .buttons button[role='cancel'] {
      display: none;
    }

    :host([fullscreen]) [role='inner'] {
      width: 90vw;
      height: 90vh;
    }

    [role='wrapper'] [role='inner'] h2,
    [role='wrapper'] [role='inner'] h3 {
      font-weight: bold;
      margin-bottom: 10px;
      border-bottom: thin lightgrey solid;
      font-size: 0.9em;
    }

    [role='wrapper'] [role='inner'] h3 {
      font-size: 0.8em;
    }

    [role='wrapper'] p {
      margin: 0;
    }

    [role='wrapper'] {
      z-index: 1006;
      overflow: hidden;
      position: absolute;
      padding: 3px;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      display: flex;
      justify-content: center;
      align-items: center;
      box-sizing: border-box;
    }

    [role='wrapper'] [role='inner'] {
      display: flex;
      flex-direction: column;
      background-color: #fff;
      border: 1px solid #d3d9e3;
      border-radius: 0.4rem;
      box-shadow: 0 2px 12px rgba(86, 90, 92, 0.25);
      box-sizing: border-box;
      width: auto;
      height: 100%;
      position: fixed;
      min-width: 400px;
      min-height: 100px;
      max-height: 90%;
      max-width: 90%;
      top: 5vh;
    }

    [role='wrapper'] [role='inner'] > div {
      padding: 20px;
    }

    [role='wrapper'] [role='inner'] .buttons {
      display: flex;
      justify-content: flex-end;
    }

    [role='wrapper'] [role='inner'] .heading {
      padding: 10px 20px;
    }

    [role='wrapper'] [role='inner'] .buttons .d2l-button {
      margin-right: 0.75rem;
      width: auto;
      border-radius: 0.3rem;
      border-style: solid;
      border-width: 1px;
      box-sizing: border-box;
      cursor: pointer;
      display: inline-block;
      margin: 0;
      min-height: calc(2rem + 2px);
      outline: 0;
      text-align: center;
      transition: box-shadow 0.2s;
      user-select: none;
      vertical-align: middle;
      white-space: nowrap;
      box-shadow: 0 0 0 4px transparent;
      font-family: inherit;
      padding: 0.5rem 1.5rem;
      font-size: 0.7rem;
      line-height: 1rem;
      font-weight: 700;
      letter-spacing: 0.2px;
    }

    [role='wrapper'] [role='inner'] .buttons button:not(:last-child) {
      margin-right: 20px;
    }

    [role='wrapper'] [role='inner'] .buttons .d2l-button[primary] {
      background-color: #006fbf;
      border-color: #005694;
      color: #fff;
    }

    [role='wrapper'] [role='inner'] .content {
      display: contents;
    }

    [role='wrapper'] h2:not(:first-child),
    [role='wrapper'] h3:not(:first-child) {
      margin-top: 20px;
    }

    [role='modal'] {
      position: fixed;
      left: 0;
      top: 0;
      bottom: 0;
      width: 100%;
      background-color: rgba(249, 250, 251, 0.7);
      z-index: 1000;
    }

    *[hidden] {
      display: none !important;
    }
  `;

  /** The default width of the dialog. */
  @property() public width = 'unset';

  /** The default height of the dialog. */
  @property() public height = 'unset';

  /** The maximum height of the dialog. */
  @property({ attribute: 'max-height' }) public maxHeight = 'unset';

  /** The maximum width of the dialog. */
  @property({ attribute: 'max-width' }) public maxWidth = 'unset';

  /** The heading text that will appear */
  @property() public heading = '';

  /** Hide the cancel button */
  @property({ attribute: 'hide-cancel', reflect: true, type: Boolean }) public hideCancel = false;

  /** Flag whether to open or close the dialog. Reflects internal state. */
  @property({ reflect: true, type: Boolean }) open = false;

  /** Determine if the DOM node should be preserved when the dialog is closed. */
  @property({ type: Boolean }) public preserve = false;

  /** The confirm button text; default is `OK` */
  @property({ attribute: 'confirm-text' }) public confirmText = '';

  /** The default padding of the internal modal */
  @property() public padding = 'unset';

  /** Disable the confirm button */
  @property({ attribute: 'disable-confirm', type: Boolean }) public disableConfirm = false;

  /** Hide the entire button row */
  @property({ attribute: 'hide-button-row', type: Boolean }) public hideButtonRow = false;

  /** Choose the alignment of the buttons */
  @property({ attribute: 'align-buttons' }) public alignButtons = 'left';

  /** Make the dialog appear fullscreen */
  @property({ type: Boolean, reflect: true }) public fullscreen = false;

  @query('.buttons')
  private buttonRow!: HTMLDivElement;

  /** Dismiss the dialog. */
  public dismiss() {
    if (!this.preserve) this.remove();

    this.open = false;
  }

  /**
   * Show the dialog.
   * @deprecated: Use `.open = true` instead
   */
  public show() {
    this.open = true;
  }

  @state()
  private buttons: ButtonItem[] = [
    { primary: true, text: 'OK', role: 'ok' },
    { primary: false, text: 'Cancel', role: 'cancel' },
  ];

  /** Add a new button */
  public registerNewButton({ primary = false, text, role, index = -1 }: ButtonItem & { index: number }) {
    this.buttons.splice(index, 0, { primary, text, role });
    this.requestUpdate();
  }

  /** Clear the button row */
  public clearButtons() {
    this.buttons = [];
    this.requestUpdate();
  }

  private onButtonClick(e: any) {
    // This was a click in the container, not a button.
    if (!e.target.matches('button')) return;

    // Fire the event (events manually dispatched are always synchronous) and wait for the result.
    const shouldClose = this.dispatchEvent(new CustomEvent(e.target.getAttribute('role'), { cancelable: true }));

    // If e.preventDefault() wasn't called, then go ahead and dismiss the dialog.
    if (shouldClose) this.dismiss();
  }

  private get buttonRowAlignment() {
    const target = this.alignButtons;

    if (target == 'left') return 'flex-start';
    else if (['center', 'centre', 'middle'].includes(target)) return 'center';
    else return 'flex-end';
  }

  private role(btn: ButtonItem) {
    return (
      btn.role ??
      btn.text
        .toLowerCase()
        .trim()
        .replace(/[^a-z0-9-_\.]/gi, '-')
    );
  }

  render() {
    const modalStyles = {
      padding: this.padding,

      width: this.width,
      maxWidth: this.maxWidth,

      height: this.height,
      maxHeight: this.maxHeight,
    };

    const buttonStyles = { justifyContent: this.buttonRowAlignment };

    return html`
      <div role="modal" ?open="${this.open}">
        <div role="wrapper">
          <div role="inner" style="${styleMap(modalStyles)}">
            <div class="heading">
              <h2 role="title">${this.heading}</h2>
            </div>
            <div class="content">
              <slot></slot>
            </div>
            <div
              class="buttons"
              @click="${this.onButtonClick}"
              ?hidden="${this.hideButtonRow}"
              style="${styleMap(buttonStyles)}"
            >
              ${this.buttons.map(btn => {
                const role = this.role(btn);
                const isConfirm = role == 'ok';

                return html`<button
                  class="d2l-button"
                  ?primary="${btn.primary}"
                  ?disabled="${isConfirm && this.disableConfirm}"
                  role="${role}"
                >
                  ${!isConfirm ? btn.text : this.confirmText ?? btn.text}
                </button>`;
              })}
            </div>
          </div>
        </div>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'mm-prompt': Prompt;
  }
}
