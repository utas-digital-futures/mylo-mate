import { LitElement, html, css } from 'lit';
import { customElement, property, query, queryAsync, state, eventOptions } from 'lit/decorators.js';
import { cache } from 'lit/directives/cache.js';
import { unsafeSVG } from 'lit/directives/unsafe-svg.js';
import { icon as faIcon } from '@fortawesome/fontawesome-svg-core';
import * as library from './mm-fa-icons';

import { selectStyles } from '@brightspace-ui/core/components/inputs/input-select-styles.js';
import { radioStyles } from '@brightspace-ui/core/components/inputs/input-radio-styles.js';

declare const chrome: any;

let _settings = null;
const getconfig = async () => {
  if (!_settings) {
    const metaTag = document.head.querySelector('meta[name="mylo-mate"]');
    const extId = metaTag ? metaTag.getAttribute('content') : '';
    _settings = await new Promise(res => chrome.runtime.sendMessage(extId, { request: 'settings' }, res));
  }
  return _settings;
};

const GetExtUrl = (path: string) => {
  const meta = document.head.querySelector<HTMLMetaElement>('meta[name="mylo-mate"]').content;
  return `chrome-extension://${meta}/${path}`;
};

export interface WidgetInformation {
  name: string;
  src: string;
  icon: any;
  showAtStart?: boolean;
  id: string;
  key: string;
  unit: UnitInformation;
}

export interface UnitInformation {
  /** The name of the unit, as it appears in the MyLO title */
  Name: string;
  /** The unit code, seperated by underscores */
  Code: string;
  /** The base path of the unit's file storage */
  Path: string;
  /** The Org ID of the unit */
  OrgId: number;
  /** The delivery period of this unit */
  Delivery: {
    Name: string;
    Code: string;
  };
  /** Is this unit a sandpit unit? */
  IsSandpit: boolean;
  /** Is this unit active? */
  IsActive: boolean;
  /** Is this unit expired/a past unit? */
  IsExpired: boolean;
}

class WidgetChildItem extends LitElement {
  static get styles() {
    return [
      css`
        :host {
          display: block;
        }
      `,
    ];
  }
  private observer = new ResizeObserver(this.observe.bind(this));
  private observe(e) {
    const { height } = e[0].contentRect;
    this.dispatchEvent(
      new CustomEvent('height-change', {
        detail: { height },
        bubbles: true,
      })
    );
  }

  connectedCallback() {
    super.connectedCallback();
    this.observer.observe(this);
  }

  disconnectedCallback() {
    this.observer.unobserve(this);
    super.disconnectedCallback();
  }

  render() {
    return html`<slot></slot>`;
  }
}

@customElement('mm-fa-icon')
export class FontAwesomeIcon extends LitElement {
  @property()
  icon: string = '';

  render() {
    const { node } = faIcon(library[this.icon]);
    return node;
  }
}

@customElement('mm-widget-script')
export class WidgetScriptItem extends LitElement {
  @property()
  src: string = '';

  @property({ type: Boolean })
  library = false;

  async connectedCallback() {
    super.connectedCallback();

    const src = this.src.startsWith('http') || this.src.startsWith('//') ? this.src : GetExtUrl(this.src);
    const container = this.closest('mm-widget-item') as WidgetItem;

    if (this.library) {
      const script = document.createElement('script');
      script.src = src;
      script.async = false;

      for (const { name, value } of Array.from(this.attributes)) {
        if (name == 'slot') continue;
        if (name == 'src') continue;
        if (name == 'library') continue;

        script.setAttribute(name, value);
      }

      script.setAttribute('script-type', 'library');
      script.dataset.ready = 'false';
      script.addEventListener('load', () => {
        script.dataset.ready = 'true';
      });

      this.insertAdjacentElement('afterend', script);
    } else {
      const widgetConfig = (this.parentElement as WidgetItem).config;

      const config = {
        widget: container,
        config: await getconfig(),
        orgid: window.location.href.split('/').pop(),
        ...widgetConfig,
      };

      try {
        const im = await import(src);

        await Promise.all(
          Array.from(container.querySelectorAll<HTMLScriptElement>('script[script-type="library"]')).map(script => {
            const ready = JSON.parse(script.dataset.ready);

            if (ready) return true;
            return new Promise(res => {
              script.addEventListener('load', () => res(null));
            });
          })
        );

        im.default(config);
      } catch (e) {
        console.error('Error occurred in %s', container.name, e);
      }
    }
  }
}

@customElement('mm-widget-title')
export class WidgetTitleItem extends WidgetChildItem {}

@customElement('mm-widget-description')
export class WidgetDescriptionItem extends WidgetChildItem {}

@customElement('mm-widget-config-item')
export class WidgetConfigItem extends WidgetChildItem {
  @property({ attribute: 'config-path' }) configPath: string;

  private async save() {
    const base = await getconfig().then(config => {
      config.widgets ??= {};
      config.widgets.configuration ??= {};
      config.widgets.configuration[this.configPath] ??= {};
      return config;
    });

    base.widgets.configuration[this.configPath] = Object.fromEntries(
      Array.from<HTMLInputElement>(this.children as any).map(child => [child.name, child.checked ?? child.value])
    );

    this.querySelectorAll<HTMLInputElement>('input[type="radio"]:checked').forEach(radio => {
      const { name, value } = radio;
      base.widgets.configuration[this.configPath][name] = value;
    });

    const metaTag = document.head.querySelector('meta[name="mylo-mate"]');
    const extId = metaTag ? metaTag.getAttribute('content') : '';
    await new Promise(res => chrome.runtime.sendMessage(extId, { request: 'settingsSave', data: base }, res));
  }

  constructor() {
    super();
    getconfig().then(config => {
      config.widgets ??= {};
      config.widgets.configuration ??= {};
      config.widgets.configuration[this.configPath] ??= {};

      const base = config.widgets.configuration[this.configPath];
      Object.entries(base).forEach(([name, value]: [string, string | boolean]) => {
        const el = this.querySelector<HTMLInputElement>(
          `[name="${name}"]${typeof value == 'string' ? `[value="${value}"]` : ''}`
        );
        if (!el) {
          console.warn(name, value);
          return;
        }

        if (el.matches('[type="radio"]')) el.checked = true;
        else if (el.matches('d2l-input-checkbox,input[type="checkbox"]')) el.checked = value as boolean;
        else el.value = value as string;
      });
    });
  }

  render() {
    return html`<slot @change="${this.save}"></slot>`;
  }
}

@customElement('mm-widget-control')
export class WidgetControlItem extends WidgetChildItem {
  constructor() {
    super();
    this.addEventListener('click', e => {
      if ((e.target as HTMLElement)?.matches('[data-config-opener]')) {
        e.preventDefault();
        const options = this.parentElement.querySelector<WidgetResultConfig>('mm-widget-config');
        if (options) {
          options.open();
        }
      }
    });
  }
}

@customElement('mm-widget-config')
export class WidgetResultConfig extends WidgetChildItem {
  @property({ attribute: 'config-path' })
  configPath: string;

  @state()
  private show = false;
  public open() {
    this.show = true;
  }

  private dismissed() {
    this.show = false;
  }

  private async save() {
    const base = await getconfig().then(config => {
      config.widgets ??= {};
      config.widgets.configuration ??= {};
      config.widgets.configuration[this.configPath] ??= {};
      return config;
    });

    base.widgets.configuration[this.configPath] = Object.fromEntries(
      Array.from<HTMLInputElement>(this.children as any).map(child => [child.name, child.checked ?? child.value])
    );

    this.querySelectorAll<HTMLInputElement>('input[type="radio"]:checked').forEach(radio => {
      const { name, value } = radio;
      base.widgets.configuration[this.configPath][name] = value;
    });

    const metaTag = document.head.querySelector('meta[name="mylo-mate"]');
    const extId = metaTag ? metaTag.getAttribute('content') : '';
    await new Promise(res => chrome.runtime.sendMessage(extId, { request: 'settingsSave', data: base }, res));
  }

  constructor() {
    super();
    getconfig().then(config => {
      config.widgets ??= {};
      config.widgets.configuration ??= {};
      config.widgets.configuration[this.configPath] ??= {};

      const base = config.widgets.configuration[this.configPath];
      Object.entries(base).forEach(([name, value]: [string, string | boolean]) => {
        const el = this.querySelector<HTMLInputElement>(
          `[name="${name}"]${typeof value == 'string' ? `[value="${value}"]` : ''}`
        );
        if (!el) {
          console.warn(name, value);
          return;
        }

        if (el.matches('[type="radio"]')) el.checked = true;
        else if (el.matches('d2l-input-checkbox,input[type="checkbox"]')) el.checked = value as boolean;
        else el.value = value as string;
      });
    });
  }

  render() {
    return html`<mm-dialog
      preserve
      heading="Advanced configuration"
      height="fit-content"
      confirm-text="Save"
      @ok=${this.save}
      @dismissed=${this.dismissed}
      ?open=${this.show}
    >
      <slot></slot>
    </mm-dialog>`;
  }
}

@customElement('mm-widget-result')
export class WidgetResultItem extends WidgetChildItem {}

@customElement('mm-widget-item')
export class WidgetItem extends LitElement {
  static get styles() {
    return [
      css`
        :host {
          flex-direction: column;
          line-height: normal;
        }

        [sticky] {
          position: sticky;
          top: 0;
          z-index: 10;
        }

        [scroll-shadow='bottom'] {
          box-shadow: 0 0 7px hsl(0deg 0% 60%);
        }

        .title,
        .description,
        .controls,
        .results {
          background: white;
          padding: 1em;
        }

        .collapse-container {
          padding: 0 !important;
          margin: 0 !important;
        }

        .title {
          font-weight: bold;
          font-size: 2.5em;
          padding-bottom: 0.2em;
          padding-left: 1rem;
        }

        .description {
          display: flex;
          flex-direction: column;
          padding: 0;
        }

        .description slot {
          display: block;
          overflow: hidden;
          padding: 1em;
        }

        .description:not([no-collapse])[collapsed] slot {
          max-height: 3em;
          box-shadow: inset rgb(226 226 226) 0px -30px 20px -20px;
        }

        .description[no-collapse] .toggle-height {
          display: none;
        }

        .description .toggle-height {
          color: #006fbf;
          cursor: pointer;
          padding: 1em;
          align-self: flex-end;
        }

        .description .toggle-height:after {
          display: inline-block;
          content: '- Read less';
        }

        .description[collapsed] .toggle-height:after {
          content: '+ Read more';
        }

        .controls,
        .results {
          margin-top: 1em;
        }

        div[data-children='0'] {
          display: none;
        }
      `,
    ];
  }

  @property() id = '';
  @property() name = '';
  @property() icon = '';
  @property() src = '';
  @property() config: WidgetInformation;

  @property({ reflect: true, type: Boolean }) loading: boolean;

  @state()
  private html: string = '';

  private loaded = false;

  constructor() {
    super();
    this.loading = true;
  }

  createRenderRoot() {
    const root = super.createRenderRoot();

    root.addEventListener('height-change', (e: Event) => {
      const evt = e as CustomEvent<{ height: number }>;
      const { height } = evt.detail;
      const path = e.composedPath();

      const slotIndex = path.findIndex(el => el instanceof HTMLSlotElement);
      const slotParent = path[slotIndex + 1];
      if (!(slotParent instanceof HTMLDivElement)) return;

      slotParent.classList.toggle('collapse-container', height == 0);
    });

    return root;
  }

  handleSlotChange(e: any) {
    const childNodes = e.target.assignedNodes({ flatten: true });
    for (const node of childNodes) {
      if (node instanceof WidgetTitleItem) {
        node.slot = 'title';
      }

      if (node instanceof WidgetDescriptionItem) {
        node.slot = 'description';
      }

      if (node instanceof WidgetControlItem) {
        node.slot = 'controls';
      }

      if (node instanceof WidgetResultItem) {
        node.slot = 'results';
      }

      if (node instanceof WidgetScriptItem) {
        node.slot = 'scripts';
      }
    }
  }

  async connectedCallback() {
    super.connectedCallback();

    if (!this.loaded) {
      await this.fetch();
      this.loaded = true;
    }

    this.dispatchEvent(new Event('scroll', { composed: true, bubbles: true }));
  }

  get source() {
    return GetExtUrl(this.src);
  }

  private async fetch() {
    if (this.html == '') {
      this.html = await fetch(this.source).then(d => d.text());
    }
    this.loading = false;
    this.innerHTML = this.html;
  }

  @query('div.description')
  private desc: HTMLElement;

  private toggleHeight() {
    this.desc.toggleAttribute('collapsed');
  }

  render() {
    if (this.loading) return html`<mm-loader width="100"></mm-loader>`;
    else
      return html`
        <div sticky scroll-shadow="bottom">
          <div class="title collapse-container">
            <slot name="title"></slot>
          </div>

          <div class="description collapse-container" collapsed no-collapse>
            <slot name="description"></slot>
            <div class="toggle-height" @click="${this.toggleHeight}"></div>
          </div>
        </div>

        <div class="controls collapse-container">
          <slot name="controls"></slot>
        </div>

        <div class="results collapse-container">
          <slot name="results"></slot>
        </div>

        <slot name="scripts collapse-container"></slot>
        <slot @slotchange="${this.handleSlotChange}"></slot>
      `;
  }
}

@customElement('mm-widget-dashboard')
export class WidgetDashboard extends LitElement {
  static get styles() {
    return [
      selectStyles,
      radioStyles,
      css`
        :host {
          width: 100%;
          height: 100%;
          position: fixed;
          top: 0;
          left: 0;
          pointer-events: all;
          z-index: 1000;
          font-family: Lato, Lucida Sans Unicode, Lucida Grande, sans-serif;
        }

        /** The backdrop (darken and prevent clicking through) */
        .backdrop {
          background: rgba(0, 0, 0, 0.8);
          height: 100%;
          width: 100%;
          position: absolute;
          top: 0;
          left: 0;
          pointer-events: all;
          z-index: 1000;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        .close {
          position: absolute;
          top: 1em;
          right: 1em;
          font-size: 0.6rem;
          height: 2.5em;
          width: 2.5em;
          cursor: pointer;
          background: #e2e2e2;
          border-radius: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
          z-index: 11;
        }

        .close:after {
          content: '×';
          font-size: 2em;
        }

        .close:hover {
          box-shadow: #737373 0px 0px 6px -1px;
        }

        /** The white container that things will live in */
        .container {
          width: 80%;
          height: 80%;
          background: white;
          border-radius: 0.5em;
          display: flex;
          overflow: hidden;
          position: relative;
          padding: 0;
        }

        /** The sidebar for widget titles */
        .sidebar {
          width: fit-content;
          border-right: thin solid #e8e8e8;
          position: relative;
          display: grid;
          grid-template-rows: min-content auto;
          padding: 0 0.5em;
        }

        /** The widget panel on the right hand side */
        .widget {
          flex: 1;
          background: #f2f2f2;
          overflow-y: auto;
        }

        .widget mm-widget-item {
          display: flex;
          width: 100%;
          min-height: 100%;
          --alpha: 0;
        }

        .widget mm-widget-item:after {
          --alpha-max: 0.8;
          content: '';
          display: block;
          position: absolute;
          bottom: 0;
          width: 100%;
          height: 2em;
          pointer-events: none;
          background: linear-gradient(180deg, hsla(0, 0%, 60%, 0), hsla(0, 0%, 60%, calc(var(--alpha-max) * var(--alpha))));
        }

        .widget mm-widget-item[loading] {
          justify-content: center;
          align-items: center;
        }

        .widgetContainer {
          display: grid;
          height: 100%;
          grid-template-rows: 3.4em auto;
        }

        .branding {
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: flex-start;
          padding: 2em;
          margin: 0 auto;
        }

        .branding .primary {
          font-size: 1.1rem;
          font-weight: 400;
          display: flex;
          margin-bottom: 0.4em;
          gap: 0.3em;
          justify-content: center;
          align-items: center;
        }

        .branding .secondary {
          font-size: 1.2rem;
          font-weight: 700;
        }

        .branding .icon {
          width: 1.5em;
          height: auto;
        }

        .sidebar .widgets {
          overflow-y: auto;
        }

        .sidebar .button-container {
          display: flex;
          justify-content: center;
          align-items: flex-end;
          padding: 0.5em 0 1em;
        }

        .clean-scrollbar::-webkit-scrollbar {
          width: 12px; /* width of the entire scrollbar */
        }

        .clean-scrollbar::-webkit-scrollbar-track {
          background: transparent; /* color of the tracking area */
        }

        .clean-scrollbar::-webkit-scrollbar-thumb {
          background-color: #d2d2d2; /* color of the scroll thumb */
          border-radius: 20px; /* roundness of the scroll thumb */
          border: 3px solid white; // creates padding around scroll thumb
        }

        .sidebar .widgets ul {
          list-style: none;
          margin: 0;
          padding: 0;
        }

        .sidebar .widgets ul li {
          padding: 0.5em 1em;
          font-size: 0.8rem;
          margin: 0.5em 1em;
          border-radius: 6px;
          cursor: pointer;
          display: flex;
          align-items: center;
          gap: 1em;
          user-select: none;
        }

        .sidebar .widgets ul li svg {
          height: 1.2em;
          width: 1.2em;
        }

        .sidebar .widgets ul li[active],
        .sidebar .widgets ul li:hover {
          background: #f2f2f2;
        }

        .sidebar .widgets ul li[active] {
          border-left: #006fbf 7px solid;
        }

        .jump-to-top {
          position: absolute;
          bottom: 1.5em;
          right: 2em;
          width: 2em;
          background: #dfdfdf;
          line-height: 0;
          padding: 1em;
          height: 2em;
          border-radius: 100%;
          box-shadow: 0 0 26px -10px black;
          border: thin solid hsl(0deg 0% 75%);
          font-size: 0.5rem;
          cursor: pointer;

          transition: opacity 200ms;
        }

        .jump-to-top:hover {
          box-shadow: hsl(200deg 100% 29%) 0px 0px 26px -7px;
        }

        .jump-to-top[hide] {
          opacity: 0;
          pointer-events: none;
        }
      `,
    ];
  }

  private nodes: Map<string, WidgetItem> = new Map();
  private widget(key: string) {
    if (key == null || key == '') return;

    if (this.nodes.has(key)) return this.nodes.get(key);

    const node = new WidgetItem();
    const blueprint = this.widgets.get(key);
    node.id = key;
    node.src = blueprint.src;
    node.name = blueprint.name;
    node.icon = blueprint.icon;
    node.config = blueprint;

    this.nodes.set(key, node);
    return node;
  }

  private getWidgetKey(key: string) {
    for (const [id, widget] of this.widgets.entries()) {
      if (widget.key == key) return id;
    }
    return null;
  }

  @state()
  private widgets: Map<string, WidgetInformation> = new Map();
  public registerWidget(widget: Omit<WidgetInformation, 'id'>) {
    const key = Array.from(crypto.getRandomValues(new Uint8Array(8)))
      .map(v => v.toString(16).padStart(2, '0'))
      .join('');

    this.widgets.set(key, { ...widget, id: key });

    if (widget.showAtStart) {
      this.currentKey = key;
    }
  }

  public GetWidgetConfig() {}

  @state()
  private currentKey: string = '';
  private getWidgetDetails() {
    return Array.from(this.widgets.values());
  }

  private set show(val: boolean) {
    this.hidden = !val;
    document.body.classList.toggle('hide-overflow', val);
  }

  public open() {
    this.show = true;
    this.handleScrollShadow();
  }

  public close() {
    this.show = false;
  }

  private script: HTMLScriptElement;
  constructor() {
    super();

    const style = document.createElement('style');
    style.innerHTML = `.hide-overflow { overflow: hidden !important; overflow-x: hidden !important; overflow-y: hidden !important; }`;
    document.head.append(style);

    this.script = document.createElement('script');
    this.script.src = GetExtUrl('lib/bootstrap.bundle.min.js');
  }

  connectedCallback() {
    super.connectedCallback();
    this.hidden = true;

    this.addEventListener('click', this._onclick);
  }

  disconnectedCallback() {
    this.removeEventListener('click', this._onclick);
    super.disconnectedCallback();
  }

  @eventOptions({ passive: true })
  private _onscroll(e: any) {
    window.requestAnimationFrame(this.handleScrollShadow.bind(this, e));
  }

  private _anymatch(target: string, path: HTMLElement[]): HTMLElement | null {
    for (const el of path) {
      if (el.matches?.(target)) return el;
    }
    return null;
  }

  private _onclick(e: any) {
    const el = this._anymatch('[widget-link]', e.composedPath());
    if (!el) return;

    const id = el.getAttribute('widget-link');
    const key = this.getWidgetKey(id);

    if (!key) return;

    this.currentKey = key;
  }

  @query('.widget')
  private widgetRef;

  @state()
  private scrollTopOffset = 0;

  private handleScrollShadow() {
    const container = this.widgetRef;
    const widget = container.querySelector('mm-widget-item');
    const { scrollHeight, clientHeight, scrollTop } = container;

    this.scrollTopOffset = scrollTop;

    const distanceFromBottom = scrollHeight - (clientHeight + scrollTop);
    const fadeDistance = 30;

    const perc = Math.max(Math.min(distanceFromBottom / fadeDistance, 1), 0);
    widget.style.setProperty('--alpha', perc);
  }

  private jump() {
    this.widgetRef.scrollTo(0, 0);
  }

  render() {
    const items = this.getWidgetDetails();
    return html`
      ${this.script}
      <div class="backdrop">
        <div class="container">
          <div class="sidebar">
            <div class="branding">
              <div class="primary">
                <img src="${GetExtUrl('res/icon128.png')}" class="icon" />
                MyLO MATE
              </div>
              <div class="secondary">Widgets Dashboard</div>
            </div>

            <div class="widgets clean-scrollbar">
              <ul>
                ${items.map(item => {
                  return html`<li
                    title="Show ${item.name}"
                    @click="${() => (this.currentKey = item.id)}"
                    ?active="${this.currentKey == item.id}"
                    key="${item.id}"
                  >
                    ${unsafeSVG(faIcon(item.icon).html[0])} ${item.name}
                  </li>`;
                })}
              </ul>
            </div>
            <div class="button-container">
              <d2l-button @click="${this.close}" class="d2l-button" primary title="Close dashboard">
                Close dashboard
              </d2l-button>
            </div>
          </div>

          <div class="widget clean-scrollbar" @scroll="${this._onscroll}">${cache(this.widget(this.currentKey))}</div>

          <div class="jump-to-top" ?hide=${this.scrollTopOffset <= 5} @click="${this.jump}">
            <mm-fa-icon icon="faArrowUp"></mm-fa-icon>
          </div>

          <div class="close" title="Close dialog" @click="${this.close}"></div>
        </div>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'mm-widget-dashboard': WidgetDashboard;
  }
}
