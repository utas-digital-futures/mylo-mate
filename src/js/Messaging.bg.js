let struct = {
  data: null,
  action: null,
  originalAction: null,
  signature: '',
  target: '',
  motion: '',
};

const uuid = () =>
  ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(16)
  );
const noop = () => {
  console.log('Running noop');
};

let _pool = [];
// Made for the background script
class Message {
  constructor() {
    // Keep a collection of the registered ports
    this.ports = [];
  }

  // Broadcast out to all supplied ports
  Broadcast(data, callback, ports = this.ports) {
    // For each port, send a message with the dataset.
    let id = uuid();
    let packet = {
      ...struct,
      data,
      signature: id,
      motion: 'request',
      target: 'all',
      action: 'broadcast:background',
    };

    const _handle = port => {
      return data => {
        if (data.signature == id && data.motion == 'response') {
          callback(data);
        }
        port.onMessage.removeListener(_handle);
      };
    };

    ports.forEach(({ port, uuid }) => {
      port.onMessage.addListener(_handle(port));
      port.postMessage(packet);
    });
  }

  // Register a port
  Register(port) {
    let _ports = this.ports;
    // Add the _handler as a listener for new messages.
    port.onMessage.addListener(data => {
      // Broadcast the data to all ports that aren't the sender. Ensures that we don't get caught in a loop.
      this.Broadcast(
        data,
        noop,
        this.ports.filter(p => p != port)
      );
      _pool.forEach(cb =>
        cb(data, newData => {
          let packet = {
            ...struct,
            ...data,
            originalAction: data.action,
            action: 'broadcast:background',
            data: newData,
            motion: 'response',
          };
          _ports.forEach(({ port }) => port.postMessage(packet));
        })
      );
    });
    port.onDisconnect.addListener(e => (this.ports = this.ports.filter(p => p.port != port)));
    // Push our port, with its unique ID, to our registered ports pool.
    this.ports.push({ port, uuid: uuid() });
  }

  Listen(cb) {
    _pool.push(cb);
  }
}

// Create a new instance.
const Messaging = new Message();
// Whenever a new port connects, add it to our pool.
chrome.runtime.onConnect.addListener(Messaging.Register.bind(Messaging));

// For debugging purposes only.
// Messaging.Listen(data => {
// 	console.log('[MM DEBUG] Message observed: ', data);
// });

