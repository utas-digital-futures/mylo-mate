if (!document.head.querySelector('meta[name="mylo-mate"]')) {
  const attr = document.createElement('meta');
  attr.name = 'mylo-mate';
  attr.content = chrome.runtime.id;
  document.head.prepend(attr);
}

chrome.runtime.onMessage.addListener((request, sender, respond) => {
  if (request.handshake == 'injectScript') {
    let { url, inHead, type, isBrightspaceComponent } = request.data;

    if (isBrightspaceComponent) {
      const loader = document.createElement('script');
      loader.innerHTML = `
/**
 * Loads the custom component "${url}" 
 * Inserted by MyLO MATE
 */
function safeDecorator(fn) {
  return function(...args) {
    try {
      return fn.apply(this, args);
    } catch (error) {
      if (
        error instanceof DOMException &&
        error.message.includes('has already been used with this registry')
      ) {
        return false;
      }
      throw error;
    }
  };
}

customElements.define = safeDecorator(customElements.define);
D2L.LP.Web.UI.Html.Bsi.LoadScript('${url}', 'module');
`;
      document.body.append(loader);
    } else {
      if (url.includes('?')) url = url.split('?')[0];
      const ext = url.split('.').pop();
      switch (ext) {
        case 'css':
          if (!document.head.querySelector(`link[href="${url}"]`)) {
            const css = document.createElement('link');
            css.type = 'text/css';
            css.rel = 'stylesheet';
            css.href = url;
            document.head.append(css);
          }
          break;

        case 'mjs':
        case 'js':
          if (!document.querySelector(`script[src="${url}"]`)) {
            const script = document.createElement('script');
            script.src = url;
            script.async = false;

            if (type == 'module' || ext == 'mjs') {
              script.type = 'module';
              script.crossOrigin = 'anonymous';
            }

            const elem = inHead ? document.head : document.body;
            elem.append(script);
          }
          break;

        default:
          // console.error('Uncertain how to handle extension of type .%s', ext[ext.length - 1]);
          break;
      }
    }
  }
});

chrome.storage.sync.get({ enabled: false }, ({ enabled }) => {
  document.body.setAttribute('mm-install-status', enabled ? 'installed' : 'disabled');
});
