(async () => {
  const config = FetchMMSettings();

  if (!/NewFile|EditFile/i.test(window.top.location.href)) return;

  const orig = D2L.LP.QuickLinks.Web.Desktop.Controls.QuickLinkSelector.OnSelectItem;
  D2L.LP.QuickLinks.Web.Desktop.Controls.QuickLinkSelector.OnSelectItem = async function (
    id,
    typeKey,
    itemUrl,
    itemTitle,
    itemProperties
  ) {
    if (typeKey == 'coursefile') {
      const settings = await config;
      if (settings.editor.AddLinkDetails) {
        const [, hash] = itemUrl.match(/coursefile-([^_]+)/i) ?? [];
        if (hash) {
          const url = atob(hash);

          // Make sure it's not an HTML page, or derivative thereof.
          if (!/\.x?html?$/gi.test(url)) {
            const response = await fetch(url, { method: 'head' });

            const type = new URL(response.url).pathname.split('.').pop().toLowerCase();
            const size = humanReadableFileSize(response.headers.get('content-length'), true, 1);

            itemTitle += ` (${type}, ${size})`;
          }
        }
      }
    }

    orig(id, typeKey, itemUrl, itemTitle, itemProperties);
  };
})();
