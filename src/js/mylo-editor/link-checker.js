(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  let canCheckExternalPages = false;

  const style = document.createElement('style');
  style.innerHTML = `
		table#mm_content_checker_results_list { width: 100%; table-layout:fixed; }
		table#mm_content_checker_results_list tr:not(:last-child) { border-bottom: thin #ddd solid; }
		table#mm_content_checker_results_list td { padding: 13px 0; text-align: center; word-break: break-word; }
		table#mm_content_checker_results_list tr td:first-child { border-right: thin #f3f3f3 solid; text-align: right; padding-right: 10px; }
		table#mm_content_checker_results_list td a { color: #006fbf; text-decoration: none; cursor: pointer; }
		table#mm_content_checker_results_list td a:hover { color: #005694; text-decoration: underline; outline-width: 0; }
		#mm_content_checker_time_taken { position: absolute; right: 0; bottom: -60px; font-size: 12px; }
		.mm-link-checker, .mm-link-checker p { white-space: normal; }
		.mm-checker-text:after { content: ''; clear:both; display:block; }
		#MM_content_checker_results { margin-top: 10px; }
	`;
  document.head.appendChild(style);

  String.prototype.hashCode = function () {
    let hash = 0;
    let i;
    let chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
      chr = this.charCodeAt(i);
      hash = (hash << 5) - hash + chr;
      hash |= 0;
    }
    return hash;
  };

  // The row to put the icon in
  const headerRow = document.querySelector('input[type="hidden"][name="topicIsHidden"]').closest('div');

  await WaitForTinyMCE();
  const root = tinymce.activeEditor.dom.doc;

  const dialog = document.createElement('div');
  dialog.setAttribute('hidden', '');
  dialog.classList.add('mm-link-checker', 'top-only');
  dialog.innerHTML = `
    <div class="closer" title="Hide dialog">&times;</div>
    <div class="mm-checker-text">
      <p>This tool will check the current page for invalid links, including external links that won't open correctly.</p>
      <button class="d2l-button" is="d2l-button" primary="" id="MM_content_checker_activate" style="float:right;">Check Links</button>
    </div>
    <div id="MM_content_checker_results">
      <table id="mm_content_checker_results_list">
      </table>
    </div>`;

  const button = document.createElement('d2l-button-subtle');
  button.classList.add('mm-link-checker-opener');
  button.title = 'Check for invalid or broken links.';
  button.setAttribute('icon', 'tier1:link');
  button.setAttribute('data-testid', 'link-checker-dialog');
  button.setAttribute('mm-icon-type', 'action-select');
  button.setAttribute('mm-icon-offset', '-6:0');

  const frame = document.querySelector('#topicContent');
  frame.parentElement.insertBefore(dialog, frame);

  headerRow.appendChild(button);
  button.addEventListener('click', e => {
    if (e.target.matches('.mm-link-checker-opener')) {
      if (dialog.hasAttribute('hidden')) dialog.removeAttribute('hidden');
      else dialog.setAttribute('hidden', '');
    }
  });

  dialog.addEventListener('click', e => {
    if (e.target.matches('.closer')) {
      dialog.setAttribute('hidden', '');
    }
  });

  const checkErrors = urls =>
    new Promise(resolve => chrome.runtime.sendMessage(extensionID, { flag: 'checkURL', payload: urls }, resolve));

  const info = {
    '__default__': '',
    '-1': 'Missing link text',
    '0': 'An error occurred while fetching the page. The link may be invalid or there may be a problem with your Internet connection.',
    '404': "Page could not be found, it has probably been moved or deleted. You'll need to replace this link.",
    '403': 'You may not have permission to view this page, or it requires you to be logged in before accessing.',
    '500': "This indicates an issue with the web server. It's not possible to determine if the link is temporarily or permanently inaccessible.",
    '999': 'Connection to the page has timed out.',
  };

  const title = {
    '__default__': '',
    '-1': 'Missing link text',
    '0': '0: Error fetching page',
    '404': '404: Page not found',
    '403': '403: Access denied',
    '500': '500: Server error',
    '999': 'Connection timeout',
  };

  const Alert = text =>
    new Promise(res => {
      const alert = document.createElement('mm-alert');
      alert.innerText = text;
      document.body.append(alert);

      alert.addEventListener('ok', res);
      alert.setAttribute('open', '');
    });

  const checkLinks = async () => {
    const button = document.querySelector('#MM_content_checker_activate');

    const loader = document.createElement('mm-loader');
    loader.setAttribute('center', '');
    try {
      button.setAttribute('disabled', '');

      // Do we have permission to check the links?
      let hasPermission = await new Promise(res =>
        chrome.runtime.sendMessage(extensionID, { request: 'link-checker-has-permissions' }, res)
      );

      if (!hasPermission) {
        // If we don't already have permission, inform the user of what's going on, and why we're asking for permission to everything.
        // Then, request permission.
        await Alert(
          "MyLO MATE doesn't have the necessary permissions to check external links. A dialog will appear asking for permission to access all pages; if you wish to check external links, please click 'Allow'."
        );
        hasPermission = await new Promise(res =>
          chrome.runtime.sendMessage(extensionID, { request: 'link-checker-get-permission' }, res)
        );
      }

      // This will be whether we can actually check externally or not.
      canCheckExternalPages = hasPermission;

      // If we still don't have permission, then give them the option of just checking internal pages.
      if (!hasPermission) {
        if (
          !confirm(
            'Permission was denied for checking external pages. Please click OK if you wish to just check internal links, or Cancel to stop the process.'
          )
        ) {
          return; // They don't want to check anything anymore.
        }
      }

      let collection = new Map();

      const table = dialog.querySelector('#mm_content_checker_results_list');
      table.innerHTML = '';
      dialog.appendChild(loader);

      let links = [];
      let broken_links = [];

      root.querySelectorAll('a[href^="http"]').forEach(a => {
        const hash = (a.href + Math.random() * Date.now()).hashCode().toString(16);
        collection.set(hash, a);

        const url = new URL(a.href);

        // Ignore any non-http/https links (such as mailto:)
        if (!url.protocol.startsWith('http')) return;

        // This is just a bad target error, so no need to save it for checking.
        if (url.protocol === 'http:' && a.target != '_blank') {
          broken_links.push({ hash, href: a.href, innerText: a.innerText });
          return;
        }

        // If we can check external pages, add all the http/https links we find.
        // Otherwise, just add the local ones.
        if (canCheckExternalPages || url.hostname === window.location.hostname) {
          links.push({ hash, href: a.href, innerText: a.innerText });
        }
      });

      const results = await checkErrors(links);
      const bad_results = results
        .map(res => {
          res.status = res.innerText.trim() == '' ? -1 : res.status;
          return res;
        })
        .filter(res => res.status !== 200);
      const bad_stuff = bad_results.concat(Array.from(broken_links));

      bad_stuff.forEach(a => {
        const tr = document.createElement('tr');
        if (a.innerText.trim().length == 0) {
          tr.innerHTML = `
          <td width="60%">
            <a class="mm-scroll-to-issue" data-ref="${
              a.hash
            }" title="Click to jump to the location in the page where this link appears">(empty link)<a>
          </td>
          <td>
          <span title="${info[a.status != null ? a.status : '__default__']}">
            [${a.status != null ? title[a.status] : 'Bad target'}]
          </span> 
          <span class="visit">
            <a class="d2l-link" title="Open link in new tab" target="_blank" href="${a.href}">visit</a>
          </span>
          </td>`;
        } else {
          tr.innerHTML = `
          <td width="60%">
            <a class="mm-scroll-to-issue" data-ref="${
              a.hash
            }" title="Click to jump to the location in the page where this link appears">${a.innerText.trim()}</a>
          </td>
          <td>
            <span title="${info[a.status != null ? a.status : '__default__']}">
              [${a.status != null ? title[a.status] : 'Bad target'}]
            </span> 
            <span class="visit">
              <a class="d2l-link" title="Open link in new tab" target="_blank" href="${a.href}">visit</a>
            </span>
          </td>`;
        }
        table.appendChild(tr);
      });

      loader.remove();

      if (bad_stuff.length == 0 && broken_links.length == 0) {
        const tr = document.createElement('tr');
        tr.innerHTML = '<td>No issues found in this page.</td>';
        table.appendChild(tr);
      }

      table.addEventListener('click', e => {
        if (e.target.matches('a.mm-scroll-to-issue')) {
          const hash = e.target.getAttribute('data-ref');
          const link = collection.get(hash);
          link.scrollIntoView({ behaviour: 'smooth', block: 'center', inline: 'nearest' });
          tinymce.activeEditor.selection.select(link);
        }
      });
    } catch (e) {
      console.error(e);
      loader.remove();
      await Alert(
        'Something went wrong while fetching the results. Please refresh and try again.\n\nIf this error continues, please contact MyLO MATE support.'
      );
    } finally {
      button.removeAttribute('disabled');
    }
  };

  dialog.addEventListener('click', e => {
    if (e.target.matches('#MM_content_checker_activate')) {
      checkLinks();
    }
  });
})();
