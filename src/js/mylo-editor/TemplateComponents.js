const GetYoutubeEmbedCode = async src => {
  const remote = new URL('https://www.youtube.com/oembed');
  remote.searchParams.set('url', src);
  remote.searchParams.set('format', 'json');

  const oembed = await fetch(remote.href).then(d => (d.ok ? d.json() : null));
  if (!oembed) throw new Error(`Error fetching data from YouTube OEmbed API for <${src}>`);

  const fragment = new DOMParser().parseFromString(oembed.html, 'text/html');

  return fragment.querySelector('iframe').src;
};

const IsValidEmbedCode = str => str.trim().startsWith('<') && str.trim().endsWith('>');

class TemplateElement {
  constructor(component, isICB) {
    this.component = component;
    this.isICB = isICB;
  }

  async content(asPreview = false, prefill) {
    try {
      for (const [key, value] of Object.entries(this.component.modifications || {})) {
        const response = asPreview ? value.preview || '' : prompt(value.info);
        if (response == null) {
          this.component.modifications[key].value = '';
          throw '';
        }

        this.component.modifications[key].original = response;
        switch (value.expect) {
          case 'videoId': {
            if (response.includes('vimeo')) {
              const cleaned = response.replace(/manage\/videos\//i, '');
              this.component.modifications[key].original = cleaned;

              const url = new URL('https://vimeo.com/api/oembed.json');
              url.searchParams.set('url', cleaned);

              const vimeo_req = await fetch(url.href);
              if (vimeo_req.ok) {
                const vimeo_res = await vimeo_req.json();
                const embed = `https://player.vimeo.com/video/${vimeo_res.video_id}`;
                this.component.modifications[key].value = embed;
              }
            } else if (response.includes('youtu')) {
              // Get the embed string.
              const embed = await GetYoutubeEmbedCode(response);

              // Grab our full href and save this as our value.
              this.component.modifications[key].value = embed;
            } else if (response.includes('echo360')) {
              if (!asPreview && !/echo360\.(org|net)\.au\/media\/[^/]+\/public/i.test(response)) {
                alert('Please ensure you have provided an Echo360 public link.');
                throw '';
              } else {
                this.component.modifications[key].value = response;
              }
            } else {
              alert('Video ID not recognised as either YouTube, Vimeo or Echo360. Nothing changed.');
              throw '';
            }
            break;
          }

          case 'echoId': {
            // Make sure that we've pasted an actual public link, not just the video URL.
            if (!asPreview && !/echo360\.(org|net)\.au\/media\/[^/]+\/public/i.test(response)) {
              alert('Please ensure you have provided an Echo360 public link.');
              throw '';
            } else {
              this.component.modifications[key].value = response;
            }
            break;
          }

          case 'embedCode': {
            // Make sure the supplied text is valid.
            if (!IsValidEmbedCode(response)) {
              alert('Please ensure the supplied embed code is valid HTML.');
              console.warn(
                'Supplied response does not start with `<`, and/or does not end with `>`. Supplied string: %s',
                response
              );
              throw '';
            } else {
              this.component.modifications[key].value = response;
            }
            break;
          }

          default: {
            this.component.modifications[key].value = response;
            break;
          }
        }
      }

      // SUID: Shared Unique ID.
      //  Can either be shared amoung all references for that object (useful for an accordian panel, for example) by using {{%SUID%}}
      //  or can be indexed to have multiple shared indexes, by using {{%SUID_x}}, where x is the index of the ID.
      //    This group-based shared index is useful in situations such as tab-groups, where the
      //    tab and container need to reference each other, but still be unique in the page.
      let suid = new Map();
      const getSUID = index => {
        if (suid.has(index)) return suid.get(index);
        else {
          let id = uuid();
          suid.set(index, id);
          return id;
        }
      };

      let src = this.component.content;
      // Create a universally-unique ID for everything that requires one.
      src = src.replace(/{{%UUID%}}/gi, uuid);

      // Created a shared unique ID for anything that just wants the one across the board.
      // Note that the difference between the call above and below is that above, `uuid` is passed as a value,
      //  meaning that it will run as a replace function, each time creating a new uuid.
      // Below, it runs once in place, so the one value will be used to replace all occurances (rather than regenerating for each occurance).
      src = src.replace(/{{%SUID%}}/gi, uuid());

      // Replace each group-based SUID with it's relevant indexed ID.
      // If it doesn't exist, it will generate one and save it for next time.
      src = src.replace(/{{%SUID_([\d]+)%}}/gi, (match, val) => getSUID(val));

      if (typeof prefill == 'string' && prefill.trim().length > 0) {
        src = src.replace(/%START_CONTENT%(.*)%END_CONTENT%/i, prefill);
      } else {
        src = src.replace(/%(START|END)_CONTENT%/gi, '');
      }

      // Run over all the additional modifications (such as URLs) and replace them as well.
      Object.entries(this.component.modifications || {}).forEach(([key, obj]) => {
        src = src.replace(new RegExp(`{{%${key}\\|ORIG%}}`, 'gi'), obj.original || orig.value || '');
        src = src.replace(new RegExp(`{{%${key}%}}`, 'gi'), obj.value || '');
      });

      const spacer = this.isICB ? '<br data-mce-bogus="1">' : '&nbsp;';

      // Return the finished source.
      return `<p>${spacer}</p>${src}<p>${spacer}</p>`;
    } catch (e) {
      if (e.message) console.error(e);
      return null;
    }
  }
}
class TemplateElementTree {
  constructor(structure, isICB = false) {
    this.isICB = isICB;
    this.components = new Map();
    this.structure = structure;
    this.structure.forEach(child => this.walk(child));
    this.rootNode = document.createElement('ul');
    this.events = {};
  }

  register(event, target, callback) {
    this.events[event] = this.events[event] || {};
    this.events[event][target] = this.events[event][target] || [];
    this.events[event][target].push(callback);
  }

  bindListeners() {
    Object.keys(this.events).forEach(event => {
      this.rootNode.addEventListener(event, e => {
        Object.keys(this.events[event]).forEach(target => {
          for (const item of e.composedPath()) {
            if (item.matches?.(target)) {
              this.events[event][target].forEach(cb => cb.apply(e, [e]));
            }
          }
        });
      });
    });
  }

  /**
   * A callback that handles how to render each category.
   * It should provide an element with [data-target="children"],
   *  as an indication of where to append the sub-elements.
   * @returns {Element} The HTML element to append.
   */
  renderCategory(data) {
    return document.createElement('ul');
  }

  /**
   * A callback that handles how to render interactive components (ie. final entries).
   * @returns {Element} The HTML element to append.
   */
  renderOption(data) {
    return document.createElement('li');
  }

  walk(node) {
    node.id = uuid();
    if (node.type == 'category') {
      node.children.forEach(child => this.walk(child));
    } else if (node.type == 'option') {
      this.components.set(node.id, new TemplateElement(node, this.isICB));
    }
  }

  render(parent) {
    const buildComponent = (node, root) => {
      if (node.type == 'category') {
        const el = this.renderCategory(node);
        root.append(el);

        const nextRoot = el.querySelector('[data-target="children"]');
        if (nextRoot) nextRoot.removeAttribute('data-target');
        node.children.forEach(child => buildComponent(child, nextRoot || el));
      } else if (node.type == 'option') {
        const el = this.renderOption(node);
        root.append(el);
      }
    };

    (Array.isArray(this.structure) ? this.structure : [this.structure]).forEach(child =>
      buildComponent(child, this.rootNode)
    );

    if (parent instanceof HTMLElement) {
      parent.append(this.rootNode);
    }

    return this.rootNode;
  }
}
