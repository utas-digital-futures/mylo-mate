/**
 * @affects Content Editor
 * @problem The ICB templates system required copy-and-pasting components from another site, one-by-one.
 * @solution Add a sidebar to the Content Editor so ICB Template components can be inserted in context with the click of a button. The sidebar also handles many housekeeping activities, such as ensuring that problematic link types work correctly, and that only certain regions are editable.
 * @target Academic and support staff
 * @impact High
 * @savings 3 hours per page
 */

(async function () {
  const __DEV__ =
    window.location.hash == '#ShowLiveResources' ||
    (await new Promise(res => chrome.runtime.sendMessage(extensionID, { request: 'IsDev' }, res)));
  const icbServer = `https://icb.utas.edu.au/v3.3/common/components`;

  const Paths = {
    actions: __DEV__ ? `chrome-extension://${extensionID}/js/mylo-editor/icb-actions.js` : `${icbServer}/actions.js`,
    library: __DEV__ ? `chrome-extension://${extensionID}/js/mylo-editor/icb.json` : `${icbServer}/library.json`,
  };

  const cachebust = url => {
    let u = new URL(url);
    u.searchParams.append('cachebust', Math.round(Date.now() / (1000 * 60 * 30)));
    return u.href;
  };

  /** The collection of extra actions to show at the top of the sidebar */
  const { default: baseactions } = await import(`chrome-extension://${extensionID}/js/mylo-editor/sidebar-actions.js`);
  const { default: ICBactions } = await import(cachebust(Paths.actions));
  const actions = baseactions.concat(ICBactions);

  /** The ICB component collection */
  const components = await api(cachebust(Paths.library), { fresh: true });

  const config = await FetchMMSettings();

  /** The sidebar container. */
  const container = document.createElement('div');
  container.id = 'icb-content';
  container.classList.add('editor-sidebar');
  container.innerHTML = `<div id="close"></div>
  <div id="heading">
    <a title="Open ICB website in a new tab" href="//utas.edu.au/health/current-staff/learning-and-teaching/interactive-content-builder" target="_blank">info</a>
    <h1 mm-icon-type="view-data" mm-icon-offset="-9:-2">Interactive Content Builder</h1>
  </div>
  <div class="children"></div>`;
  container.querySelector('#close').addEventListener('click', e => {
    container.toggleAttribute('collapsed');
  });

  /**
   * Tests if the current template file contains a reference to the ICB CSS stylesheet.
   * It expands all the `<link>` tags found in the editor documents head, and then filters them down by matching against our CSS comparison string.
   * 		`https://health.utas.edu.au/templates/*.css`
   * This will match anything starting with `https://health.utas.edu.au/templates/` and ending with `.css`.
   * @returns {boolean} Returns true if there is at least one reference to a stylesheet in the given location.
   */
  const ICBFriendlyTemplate = () => {
    // This is currently the CSS sheet being used in templates supporting the ICB elements.
    // It's reasonably safe to assume that this won't change, as they'll have to change all templates using it as well.
    // As such, we can use it to test if our template is ICB-ready.
    const CSSCompare = [
      'https://icb.utas.edu.au/*.css',
      'https://health.utas.edu.au/templates/*.css',
      'https://*.health.utas.edu.au/templates/*.css',
      '*/template/css/*',
    ];

    return Array.from(tinymce.activeEditor.dom.doc.head.querySelectorAll('link')).some(link =>
      CSSCompare.some(css => link.href.match(wildcard(css)))
    );
  };

  /** The ICB components processed into an object with an internally managed state. */
  await WaitForTinyMCE();

  if (!ICBFriendlyTemplate()) return;

  const icb = new TemplateElementTree(components, true);
  const doc = tinymce.activeEditor.dom.doc;

  /**
   * A callback that handles how to render each category.
   * It should provide an element with [data-target="children"],
   *  as an indication of where to append the sub-elements.
   * @returns {Element} The HTML element to append.
   */
  icb.renderCategory = category => {
    const li = document.createElement('li');
    li.setAttribute('data-id', category.id);
    li.setAttribute('data-type', 'category');
    li.setAttribute('data-src', 'icb');
    li.setAttribute('state', 'closed');
    li.classList.add('treeview', 'treeview-parent');
    if (category.description) li.title = category.description;
    li.innerText = category.title;

    const ul = document.createElement('ul');
    ul.setAttribute('data-target', 'children');
    ul.classList.add('treeview-child');
    li.append(ul);
    return li;
  };

  /**
   * A callback that handles how to render interactive components (ie. final entries).
   * @returns {Element} The HTML element to append.
   */
  icb.renderOption = option => {
    const li = document.createElement('li');
    li.setAttribute('data-id', option.id);
    li.setAttribute('data-type', 'option');
    li.setAttribute('insertable', true);
    li.setAttribute('data-src', 'icb');
    li.classList.add('treeview', 'treeview-child');
    if (option.description) li.title = option.description;
    li.innerText = option.title;
    return li;
  };

  /** The target for where to append the sidebar components */
  const base = container.querySelector('.children');

  /**
   * Tweak the root node of the ICB (by default it's a UL element)
   * and tell it to render our components, passing it the base element.
   */
  icb.rootNode.classList.add('treeview');
  icb.rootNode.id = 'treeview';
  icb.render(base);

  /** The range that was selected before we modified anything. */
  let prevRange = null;
  /** The range after we modified things. */
  let newRange = null;
  /** The content that was previously selected. */
  let previousSelection = null;

  /**
   * Register synthetic events, to reduce the overhead of an event on each component.
   * This listener waits for category items to be clicked, to toggle their collapsed state.
   */
  icb.register('click', '[data-type="category"][state]', e => {
    e.stopPropagation();
    const curr = e.target.getAttribute('state');
    e.target.setAttribute('state', curr == 'open' ? 'closed' : 'open');
  });

  /**
   * Register synthetic events, to reduce the overhead of an event on each component.
   * This event listener replaces the current selection with the desired ICB component.
   */
  icb.register('click', '[data-src="icb"][insertable]', async e => {
    e.preventDefault();
    if (doc.getSelection().rangeCount) {
      let previouslySelectedContent = document.createElement('div');
      if (previousSelection) previouslySelectedContent.append(previousSelection);
      const innerContent = previouslySelectedContent.innerHTML;
      const src = await icb.components.get(e.target.getAttribute('data-id')).content(false, innerContent);
      document.dispatchEvent(new CustomEvent('insert-code', { detail: src }));
    }
  });

  // Commenting out to prevent insertion duplication from both ICB and D2L side running.
  // document.addEventListener('insert-code', e => {
  //   const src = e.detail;
  //   tinymce.activeEditor.insertContent(src);
  //   prevRange = null;
  //   newRange = null;
  //   previousSelection = null;
  // });

  /**
   * Register synthetic events, to reduce the overhead of an event on each component.
   * This event listener handles the hover-preview, replacing the placeholder content
   *  of the ICB component if there is content selected before hover.
   */
  icb.register('mouseover', '[data-src="icb"][insertable]', async e => {
    e.preventDefault();
    const range = tinymce.activeEditor.dom.doc.getSelection().getRangeAt(0);
    prevRange = tinymce.activeEditor.dom.doc.getSelection().getRangeAt(0);
    previousSelection = range.cloneContents();

    let div = document.createElement('div');
    div.append(range.cloneContents());
    const src = await icb.components.get(e.target.getAttribute('data-id')).content(true, div.innerHTML);

    const fragment = range.createContextualFragment(src);
    range.deleteContents();
    range.insertNode(fragment);
    newRange = range;
  });

  /**
   * Register synthetic events, to reduce the overhead of an event on each component.
   * This event handles returning the selection back to its original state, without the ICB component.
   *  NOTE: This only runs if the previous range exists. The previous range variable is reset when an ICB component is added,
   *        ensuring that the item won't be removed if added then unhovered.
   */
  icb.register('mouseout', '[data-src="icb"][insertable]', async e => {
    e.preventDefault();
    if (prevRange != null) {
      newRange.extractContents();
      newRange.insertNode(previousSelection);
      tinymce.activeEditor.dom.doc.getSelection().removeAllRanges();
      tinymce.activeEditor.dom.doc.getSelection().addRange(prevRange);
      prevRange = null;
      newRange = null;
      previousSelection = null;
    }
  });

  /** Bind the listeners for the sidebar. */
  icb.bindListeners();

  /** The "extras/devtools" portion of the sidebar. Stick it here. */
  let extras = document.createElement('ul');
  /** Give it the class of the other tree, just to keep everything in form. */
  extras.classList.add('treeview');

  /** For each action we retrieved from our remote object, add it to the sidebar. */
  actions.forEach(action => {
    const { key, title, description, runOnStart } = action;

    const li = document.createElement('li');
    li.setAttribute('data-id', key);
    li.setAttribute('data-type', 'option');
    li.setAttribute('insertable', true);
    li.setAttribute('data-src', 'extras');
    li.hidden = true;
    li.classList.add('treeview', 'treeview-child');
    if (description) li.title = description;
    li.innerText = typeof title == 'string' ? title : title(tinymce.activeEditor);
    extras.append(li);

    if (runOnStart) runOnStart.apply(action, [config, tinymce.activeEditor]);
    action.domNode = li;
  });

  /**
   * Whenever an extras item is clicked, run the appropriate action.
   * These actions are stored in the remote file, so we simply need to pass the editor reference to the '.action()' callback.
   */
  extras.addEventListener('click', e => {
    if (e.target.matches('[data-src="extras"][data-id]')) {
      const block = actions.find(comp => comp.key == e.target.getAttribute('data-id'));
      if (block) {
        block.action(tinymce.activeEditor);
      }
    }
  });

  /** Prepend our 'Extras' block to the sidebar, along with a <hr> to keep things tidy. */
  base.prepend(extras, document.createElement('hr'));

  /** Hide or show the sidebar based on the status of the current template. */
  const checkSidebarVisibility = () => {
    if (ICBFriendlyTemplate()) {
      document.body.append(container);
    } else container.remove();
  };
  const OnTemplateLoad = () => {
    checkSidebarVisibility();
    document.dispatchEvent(new Event('domchange'));
  };
  tinymce.activeEditor.on('SetContent', OnTemplateLoad);
  checkSidebarVisibility();

  const MakeLinksOpenInNewTab = all => {
    // A list of rules to avoid.
    //	To be used in a `:not({{rule}})` fashion.
    const rules = [
      '[target="_blank" i]',
      '[href^="#" i]', // href is an anchor
      '[download]', // Has a download attr
      '[href^="mailto:" i]', // Is a mailto link
      '.leavetarget', // Has a specific class
      '.pdf-link', // Isn't the PDF link in the header
      ':empty', // Make sure it's not an empty container used as an anchor point.
      '[href^="javascript" i]',
      '[href^="Javascript" i]',
      '[href^="function" i]',
      '[href^="onclick" i]',
      '[onclick]', // Skip any href that appeaers to run a javascript function.
    ];

    const targets = rules.map(rule => `:not(${rule})`).join('');
    const links = tinymce.activeEditor.dom.doc.querySelectorAll(`a[href^="http${all ? '' : '://'}"]${targets}`);
    links.forEach(a => {
      a.target = '_blank';
      a.setAttribute('data-target-by', 'mylo-mate');
    });

    /** Fix any Ezyproxy links that we encounter */
    tinymce.activeEditor.dom.doc.querySelectorAll('a[href*="ezyproxy" i]').forEach(a => {
      const url = new URL(a.href);
      if (url.searchParams.has('url')) {
        url.searchParams.set('url', decodeURIComponent(url.searchParams.get('url').trim()));
        a.href = a.dataset.mceHref = url.href;
      }
    });
  };

  document.addEventListener('domchange', e => {
    actions.forEach(action => {
      if (!action.targetNode) {
        action.domNode.hidden = !action.display(config, tinymce.activeEditor);
      }
    });
  });
  document.dispatchEvent(new Event('domchange'));

  document.querySelector('d2l-floating-buttons').addEventListener('click', e => {
    if (e.target.matches('button') && /save/i.test(e.target.innerText)) {
      tinymce.activeEditor.dom.doc.querySelectorAll('.editor-only').forEach(node => node.remove());
      MakeLinksOpenInNewTab(config.editor.allLinksInNewTab);
    }
  });
})();
