/**
 * @affects Content Editor
 * @problem The D2L templates system required staff to have basic HTML skills, making them unsuitable for broad roll-out across the university.
 * @solution Add a sidebar to the Content Editor so D2L Template components can be inserted in context with the click of a button. The sidebar also handles many housekeeping activities, such as ensuring that problematic link types work correctly, and that only certain regions are editable.
 * @target Academic and support staff
 * @impact High
 * @savings 3 hours per page
 */

(async function () {
  const thisurl = new URL(window.location);
  const ForceMode = thisurl.searchParams.get('force-version')?.toUpperCase();
  const version =
    ForceMode ?? (await new Promise(res => chrome.runtime.sendMessage(extensionID, { request: 'GetRelease' }, res)));

  /**
   * Use this one if you want to use remote files
   *
   * const root = {
   *   STABLE: `${window.location.origin}/shared/content-template/v3.0/mylo-mate/stable`,
   *   BETA: `${window.location.origin}/shared/content-template/v3.0/mylo-mate/beta`,
   *   DEV: `chrome-extension://${extensionID}/js/mylo-editor`,
   * };
   */

  /** Use this one if you want to only use local files */
  const root = {
    STABLE: `chrome-extension://${extensionID}/js/mylo-editor`,
    BETA: `chrome-extension://${extensionID}/js/mylo-editor`,
    DEV: `chrome-extension://${extensionID}/js/mylo-editor`,
  };

  const cachebust = url => {
    let u = new URL(url);
    u.searchParams.append('cachebust', Math.round(Date.now() / (1000 * 60 * 30)));
    return u.href;
  };

  /** The collection of extra actions to show at the top of the sidebar */
  /** const { default: baseactions } = await import(`${root[version]}/sidebar-actions.js`); */
  const { default: actions, insertComponents } = await import(`${root[version]}/actions.m.js`);
  const { default: icons } = await import(`${root[version]}/icons.js`);

  /** const actions = baseactions.concat(d2lactions); */
  const config = await FetchMMSettings();

  const orgId = window.location.pathname.match(/\/content\/([\d]+)/)[1];
  const course = await api(`/d2l/api/lp/1.26/courses/${orgId}`, { first: true });

  /** The ICB component collection */
  const components = await api(cachebust(`${root[version]}/library.json`), { fresh: true });
  const orgtree = await api(`chrome-extension://${extensionID}/js/mylo-editor/orgtree.json`);

  await WaitForTinyMCE();

  const doc = tinymce.activeEditor.dom.doc;
  const editorArea = document.querySelector('#topicContent');

  /**
   * Tests if the current template file contains a reference to the ICB CSS stylesheet.
   * It expands all the `<link>` tags found in the editor documents head, and then filters them down by matching against our CSS comparison string.
   * 		`https://health.utas.edu.au/templates/*.css`
   * This will match anything starting with `https://health.utas.edu.au/templates/` and ending with `.css`.
   * @returns {boolean} Returns true if there is at least one reference to a stylesheet in the given location.
   */
  const IsD2LTemplate = () => doc.body.querySelector('[mm-template-style="d2l"]') != null;

  const showSidebar = () => {
    if (!IsD2LTemplate()) return;

    const sidebar = document.querySelector(`#d2l-content.editor-sidebar`);
    const shouldAdd = sidebar ? !sidebar.hasAttribute('collapsed') : false;
    editorArea.classList.toggle('show-sidebar', shouldAdd);
    document.body.classList.toggle('sidebar-visible', shouldAdd);
  };

  const AddStyleToPageIfNotExists = src => {
    if (!doc.head.querySelector(`link[href*="${src}" i]`)) {
      const link = document.createElement('link');
      link.rel = 'stylesheet';
      link.href = src;
      doc.head.append(link);
    }
  };

  const AddTinyMCEOnlyStyle = styleText => {
    tinymce.activeEditor.contentStyles.push(styleText);
    if (!doc.head.querySelector('style#mceDefaultStyles')) {
      let style = document.createElement('style');
      style.id = 'mceDefaultStyles';
      doc.head.prepend(style);
    }
    doc.head.querySelector('style#mceDefaultStyles').innerHTML = tinymce.activeEditor.contentStyles.join('\n');
  };

  /** Add the "Use D2L Basic Template" button */
  const basicTemplateSrc = '/shared/content-template/v3.0/pages/Basic%20Page.html';

  if (window.location.href.includes('authorHtml')) {
    const useD2Lbutton = document.createElement('button');
    useD2Lbutton.classList.add('d2l-button');
    useD2Lbutton.setAttribute('mm-icon-type', 'action-select');
    useD2Lbutton.setAttribute('mm-icon-offset', '-6:0');
    useD2Lbutton.innerText = 'Use D2L Basic Template';

    const res = await fetch(basicTemplateSrc)
      .then(d => d.text())
      .catch(() => '');

    useD2Lbutton.addEventListener('click', async e => {
      if (res.trim().length > 0) {
        const willUpdate =
          tinymce.activeEditor.getBody().innerText.trim().length > 0
            ? confirm(
                'There appears to be content in the editor. ALL THIS CONTENT WILL BE REMOVED. Select OK to overwrite the content with a new template.'
              )
            : true;

        if (willUpdate) {
          const page = res;
          tinymce.activeEditor.setContent(page);
        }
      } else {
        alert('The template could not be found.');
      }
    });
    document.querySelector('form > button, form > d2l-dropdown').insertAdjacentElement('afterend', useD2Lbutton);
  }

  const container = document.createElement('div');
  /** The sidebar container. */
  container.id = 'd2l-content';
  container.classList.add('editor-sidebar');
  container.hidden = true;
  container.innerHTML = `<div id="close"></div>
    <div id="heading">
      <h1 mm-icon-type="view-data" mm-icon-offset="-9:-2">D2L Template Components</h1>
    </div>
    <div class="children"></div>`;
  document.body.append(container);
  container.querySelector('#close').addEventListener('click', e => {
    container.toggleAttribute('collapsed');
  });

  /** The D2L components processed into an object with an internally managed state. */
  const d2l = new TemplateElementTree(components);

  /**
   * A callback that handles how to render each category.
   * It should provide an element with [data-target="children"],
   *  as an indication of where to append the sub-elements.
   * @returns {Element} The HTML element to append.
   */
  d2l.renderCategory = category => {
    const li = document.createElement('li');
    li.setAttribute('data-id', category.id);
    li.setAttribute('data-uuid', category.uuid);
    li.setAttribute('data-type', 'category');
    li.setAttribute('data-src', 'icb');
    li.setAttribute('state', window.localStorage.getItem(`d2l-cat-${category.uuid}`) ?? 'closed');
    /** li.setAttribute('state', config.editor.ExpandAllSidebarItems ? 'open' : 'closed'); */
    li.classList.add('treeview', 'treeview-parent');
    if (category.description) li.title = category.description;
    li.innerText = category.title;

    const ul = document.createElement('ul');
    ul.setAttribute('data-target', 'children');
    ul.classList.add('treeview-child');
    li.append(ul);
    return li;
  };

  /**
   * A callback that handles how to render interactive components (ie. final entries).
   * @returns {Element} The HTML element to append.
   */
  d2l.renderOption = option => {
    const li = document.createElement('li');
    li.setAttribute('data-id', option.id);
    li.setAttribute('data-type', 'option');
    li.setAttribute('insertable', true);
    li.setAttribute('data-src', 'icb');
    li.classList.add('treeview', 'treeview-child');
    if (option.description) li.title = option.description;
    li.innerText = option.title;

    return li;
  };

  /** The target for where to append the sidebar components */
  const base = container.querySelector('.children');

  /**
   * Tweak the root node of the ICB (by default it's a UL element)
   * and tell it to render our components, passing it the base element.
   */
  d2l.rootNode.classList.add('treeview');
  d2l.rootNode.id = 'treeview';

  const treeviewHeading = document.createElement('li');
  treeviewHeading.classList.add('add-content-heading');
  treeviewHeading.innerHTML = '<span>Add Content</span>';
  d2l.rootNode.append(treeviewHeading);

  d2l.render(base);

  const editor = tinymce.activeEditor;
  const sel = { previous: null, current: null };

  /**
   * Register synthetic events, to reduce the overhead of an event on each component.
   * This listener waits for category items to be clicked, to toggle their collapsed state.
   */
  d2l.register('click', '[data-type="category"][state]', e => {
    e.stopPropagation();
    const curr = e.target.getAttribute('state');
    e.target.setAttribute('state', curr == 'open' ? 'closed' : 'open');
    window.localStorage.setItem(`d2l-cat-${e.target.dataset.uuid}`, e.target.getAttribute('state'));
  });

  /**
   * Register synthetic events, to reduce the overhead of an event on each component.
   * This event listener replaces the current selection with the desired ICB component.
   */
  d2l.register('click', '[data-src="icb"][insertable]', async e => {
    e.preventDefault();
    if (doc.querySelectorAll('.callout-icon.focus').length > 0) {
      alert('Cannot insert item while Icon Selector panel is open.');
      return;
    }

    const selection = editor.selection;
    const rangeCount = doc.getSelection().rangeCount;
    const selector = '[block-identifier="content-block"],[block-identifier="content-block"] *';

    const message = () => alert('Please select an area inside the content block to place this component.');

    if (rangeCount) {
      if (!selection.getNode().matches(selector)) return message();

      const template = document.createElement('div');
      if (sel.previous.selection) template.append(sel.previous.selection);
      const innerContent = template.innerHTML;

      const src = await d2l.components.get(e.target.getAttribute('data-id')).content(false, innerContent);

      if (!src) return;
      document.dispatchEvent(new CustomEvent('insert-code', { detail: src }));
    }
  });

  document.addEventListener('insert-code', e => hover.next({ action: 'insert', payload: e.detail }));

  let IsHovered = false;

  const handleHoverQueueItem = async ({ action, id, payload }) => {
    const selector = '[block-identifier="content-block"],[block-identifier="content-block"] *';
    const currently_selected = doc.getSelection();

    const get_selection = () => sel;
    const save_selection = () => {
      const rangeCount = currently_selected.rangeCount;
      const node = tinymce.activeEditor.selection.getNode();

      if (rangeCount && node.matches(selector)) {
        const range = currently_selected.getRangeAt(0).cloneRange();
        sel.previous = {
          range: range.cloneRange(),
          selection: range.cloneContents(),
        };
      }
    };

    const save_preview = range => {
      sel.current = range;
    };

    const clear_selection = () => {
      sel.current = null;
      sel.previous = null;
    };

    const show_preview = async () => {
      const prev = get_selection().previous;
      if (!prev) return;

      const { range } = prev;

      const div = document.createElement('div');
      div.append(range.cloneContents());
      const src = await d2l.components.get(id).content(true, div.innerHTML);

      const fragment = range.createContextualFragment(src);
      range.deleteContents();
      range.insertNode(fragment);

      save_preview(range);
      sel.current = range;
    };

    const hide_preview = () => {
      const { previous, current } = get_selection();

      if (previous?.range != null && current != null) {
        current.extractContents();
        current.insertNode(previous.selection);

        currently_selected.removeAllRanges();
        currently_selected.addRange(previous.range);
      }
    };

    const insert = () => {
      hide_preview();
      tinymce.activeEditor.insertContent(payload);
      clear_selection();
    };

    switch (action) {
      case 'insert':
        insert();
        break;

      case 'hoverstart':
        save_selection();
        break;

      case 'hoverend':
        clear_selection();
        break;

      case 'previewstart':
        await show_preview();
        break;

      case 'previewend':
        hide_preview();
        break;

      default:
        console.warn('Hover action <%s> unknown.', action);
        break;
    }
  };

  const hover = new rxjs.Subject();
  hover.pipe(rxjs.operators.concatMap(handleHoverQueueItem)).subscribe();

  /**
   * Register synthetic events, to reduce the overhead of an event on each component.
   * This event listener handles the hover-preview, replacing the placeholder content
   *  of the ICB component if there is content selected before hover.
   */
  //d2l.register('mouseover', '[data-src="icb"][insertable]', e => preview(true, e.target.getAttribute('data-id'), e));
  d2l.register('mouseover', '#treeview', () => hover.next({ action: 'hoverstart' }));
  d2l.register('mouseover', '[data-src="icb"][insertable]', e =>
    hover.next({ action: 'previewstart', id: e.target.getAttribute('data-id') })
  );

  /**
   * Register synthetic events, to reduce the overhead of an event on each component.
   * This event handles returning the selection back to its original state, without the ICB component.
   *  NOTE: This only runs if the previous range exists. The previous range variable is reset when an ICB component is added,
   *        ensuring that the item won't be removed if added then unhovered.
   */
  //d2l.register('mouseout', '[data-src="icb"][insertable]', e => preview(false, null, e));
  d2l.register('mouseout', '[data-src="icb"][insertable]', e => hover.next({ action: 'previewend' }));
  d2l.register('mouseout', '#treeview', () => hover.next({ action: 'hoverend' }));

  /** Bind the listeners for the sidebar. */
  d2l.bindListeners();

  /** The "extras/devtools" portion of the sidebar. Stick it here. */
  let extras = document.createElement('ul');
  /** Give it the class of the other tree, just to keep everything in form. */
  extras.classList.add('treeview');

  /** For each action we retrieved from our remote object, add it to the sidebar. */
  for (const category of actions) {
    const item = document.createElement('li');
    item.classList.add('extra-label');

    const text = document.createElement('span');
    text.innerText = category.label;

    extras.append(item);

    const children = document.createElement('ul');
    item.append(text, children);

    /** Text or Function? */
    const tf = (obj, ...args) => (typeof obj == 'string' ? obj : obj(...args));

    const render = (action, shouldCollapse = false) => {
      const { key, title, description, runOnStart } = action;

      const li = document.createElement('li');
      li.setAttribute('data-id', key);
      li.setAttribute('data-type', 'option');
      li.setAttribute('insertable', true);
      li.setAttribute('data-src', 'extras');
      li.toggleAttribute('collapsable', shouldCollapse);
      li.hidden = true;
      li.classList.add('treeview', 'treeview-child');
      if (description) {
        li.title = tf(description, tinymce.activeEditor);
      }

      li.innerText = tf(title, tinymce.activeEditor);
      children.append(li);

      if (runOnStart) {
        WaitForTinyMCE().then(() => {
          runOnStart.apply(action, [config, tinymce.activeEditor]);
        });
      }

      action.domNode = li;
    };

    category.children.forEach(action => render(action, false));
    category.collapsed?.forEach(action => render(action, true));

    let count = 0;
    if (category.collapsed) {
      const editor = tinymce.activeEditor;
      for (const child of category.collapsed) {
        const response = await child.display(config, editor);
        if (response) {
          count++;
        }
      }
    }

    if (count > 0) {
      children.toggleAttribute('collapse', JSON.parse(window.localStorage.getItem(`d2l-panel-${category.uuid}`) ?? 'true'));

      const toggle = document.createElement('li');
      toggle.classList.add('toggle-fold');
      toggle.dataset.count = count;
      toggle.innerText = ``;

      toggle.addEventListener('click', e => {
        children.toggleAttribute('collapse');
        window.localStorage.setItem(`d2l-panel-${category.uuid}`, children.hasAttribute('collapse'));
      });

      children.append(toggle);
    }
  }

  if (insertComponents) {
    for (const action of insertComponents) {
      const { key, title, display, description } = action;

      const shouldShow = await display(config, tinymce.activeEditor);

      const li = document.createElement('li');
      li.setAttribute('data-id', key);
      li.setAttribute('data-type', 'option');
      li.setAttribute('insertable', true);
      li.setAttribute('data-src', 'extras');
      li.hidden = !shouldShow;
      li.classList.add('treeview', 'treeview-child');
      if (description) {
        li.title = typeof description == 'string' ? description : description(tinymce.activeEditor);
      }

      li.innerText = typeof title == 'string' ? title : title(tinymce.activeEditor);
      container.querySelector('#treeview').append(li);

      li.addEventListener('click', () => action.action(tinymce.activeEditor, tinymce));

      action.domNode = li;
    }
  }

  /**
   * Whenever an extras item is clicked, run the appropriate action.
   * These actions are stored in the remote file, so we simply need to pass the editor reference to the '.action()' callback.
   */
  extras.addEventListener('click', e => {
    if (e.target.matches('[data-src="extras"][data-id]')) {
      const acts = actions.flatMap(({ children, collapsed }) => [...children, ...(collapsed ?? [])]);
      acts.find(comp => comp.key == e.target.getAttribute('data-id'))?.action(tinymce.activeEditor, tinymce);
    }
  });

  /** Prepend our 'Extras' block to the sidebar. */
  base.prepend(extras);

  doc.addEventListener('selectionchange', e => {
    if (IsHovered) return;

    actions.forEach(category => {
      category.children.forEach(action => {
        action.domNode.innerText = typeof action.title == 'string' ? action.title : action.title(tinymce.activeEditor);

        if (action.targetNode) {
          const node = tinymce.activeEditor.selection.getNode();
          const sel = `${action.targetNode.join(',')}`;

          const matches = node.matches(sel) || node.closest(sel) != null;
          action.domNode.hidden = !matches;

          const parent = action.domNode.closest('.extra-label');
          parent.hidden = parent.querySelectorAll('ul li:not([hidden])').length == 0;
        }
      });
    });
  });

  /** Hide or show the sidebar based on the status of the current template. */
  const checkSidebarVisibility = () => {
    setTimeout(() => {
      container.hidden =
        document.querySelector('.d2l-dialog:not(.d2l-dialog-closing),mm-dialog[open]') != null || !IsD2LTemplate();
    }, 50);
  };

  const MakeLinksOpenInNewTab = all => {
    // A list of rules to avoid.
    //	To be used in a `:not({{rule}})` fashion.
    const rules = [
      '[target="_blank" i]',
      '[href^="#" i]', // href is an anchor
      '[download]', // Has a download attr
      '[href^="mailto:" i]', // Is a mailto link
      '.leavetarget', // Has a specific class
      '.pdf-link', // Isn't the PDF link in the header
      ':empty', // Make sure it's not an empty container used as an anchor point.
      '[href^="javascript" i]',
      '[href^="Javascript" i]',
      '[href^="function" i]',
      '[href^="onclick" i]',
      '[onclick]', // Skip any href that appeaers to run a javascript function.
    ];

    const targets = rules.map(rule => `:not(${rule})`).join('');
    const links = doc.querySelectorAll(`a[href^="http${all ? '' : '://'}"]${targets}`);
    links.forEach(a => {
      a.target = '_blank';
      a.setAttribute('data-target-by', 'mylo-mate');
    });

    /** Fix any Ezyproxy links that we encounter */
    doc.querySelectorAll('a[href*="ezyproxy" i]').forEach(a => {
      const url = new URL(a.href);
      if (url.searchParams.has('url')) {
        url.searchParams.set('url', decodeURIComponent(url.searchParams.get('url').trim()));
        a.href = a.dataset.mceHref = url.href;
      }
    });
  };

  // This handles major changes to the template.
  // In this case, major changes are changes that replace the entire template body with the D2L template body.
  const HandleMajorTemplateChange = async () => {
    const BannerImage = doc.body.querySelector('.banner-img');
    if (!BannerImage) return;

    /** This should hopefully mean that this won't apply if the page has already been handled by MM */
    if (doc.body.querySelector('input[name="mm-configured"]')?.value != '1') {
      // Set the banner to the users default config.
      BannerImage.toggleAttribute('hidden', !config.editor.UseD2LBanner ?? true);

      const DefaultBannerPath = `${window.location.origin}${course.Path}images/banners/default/unit-banner.jpg`;
      const DefaultBannerExists = await fetch(DefaultBannerPath).catch(() => {});
      if (DefaultBannerExists.ok) {
        const img = BannerImage.querySelector('img');

        img.src = DefaultBannerPath;
        img.dataset.mceSrc = DefaultBannerPath;
      }
    }
  };

  const OnPaste = e => {
    const node = tinymce.activeEditor.selection.getNode();

    if (node.matches('.card-title')) {
      const el = document.createElement('span');
      el.innerHTML = e.content;
      e.content = el.innerText;
    }
  };

  const SetCursorLocation = (offset = 0, target = null) => {
    const editor = tinymce.activeEditor;
    const sel = editor.selection;
    const range = new Range();

    target = target ?? editor.dom.doc.body;

    editor.focus();
    range.setStart(target, offset);
    range.setEnd(target, offset);
    sel.setRng(range);
  };

  const OnD2LTemplateLoad = () => {
    tinymce.activeEditor.undoManager.ignore(() => {
      if (!doc.body.querySelector('input[name="mm-configured"]')) {
        const flag = document.createElement('input');
        flag.type = 'hidden';
        flag.name = 'mm-configured';
        flag.value = '1';
        doc.body.append(flag);

        tinymce.activeEditor.on('PastePreProcess', OnPaste);

        const contentblock = tinymce.activeEditor.dom.doc.querySelector('[block-identifier="content-block"]');
        SetCursorLocation(0, contentblock);
      }

      /** Check header styles, make sure college and school is set. */
      doc.body.setAttribute('contenteditable', false);
      doc.body
        .querySelectorAll('[block-identifier="header-block"],[block-identifier="content-block"]')
        .forEach(node => node.setAttribute('contenteditable', true));

      /**
       * Add the editor-specific style.
       * Note, this only adds rules under the "#tinymce" body ID, so can safely be saved into the document.
       */
      const editorCssPath = `/shared/content-template/v3.0/assets/css/utas.css`;
      AddStyleToPageIfNotExists(editorCssPath);

      const EditorOnlyStyles = `
      #tinymce[mm-features*="formatmarks"] [mm-template-style="d2l"] :is(.clearfix,.clear-fix) {
          display: block;
          border: thin dashed rgb(36 134 191);
          background: rgba(36, 134, 191, 0.08);
          text-align: center;
          font-size: 1.1875rem;
          line-height: 1.75rem;
          clear:both;
      }

      #tinymce[mm-features*="formatmarks"] [mm-template-style="d2l"] :is(.clearfix,.clear-fix):before {
          content: 'This block forces text wrapping, and is invisible after saving';
          font-size: 0.6em;
          user-select: none;
          text-transform: uppercase;
          color: #333;
          clear:both;
      }

      #tinymce[mm-features*="formatmarks"] [mm-template-style="d2l"] :is(.clearfix,.clear-fix):after { display: none; }
    .callout-card .card-icon { caret-color: transparent; }
    .callout-icon { cursor: pointer; } 
    .callout-icon.focus { position: relative; }

    .delete-element {
      height: 60px;
      z-index: 10000;
      opacity: 1;
      background: repeating-linear-gradient( 45deg, rgba(255,120,120,0.6), rgba(255,120,120,0.6) 10px, rgba(255,120,120,0.8) 10px, rgba(255,120,120,0.8) 20px );
      position: absolute;
      top: 0;
      width: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      border-bottom: 3px solid rgb(255,120,120);
    }
  
    span.callout-icon,
    .video-youtube .embed-responsive,
    .video-wrapper .embed-responsive {
      outline: #28a745 dashed 1px;
      outline-offset: 10px;
      box-sizing: border-box;
      pointer-events: all;
    }
  
    .callout-icon-container {
      position: absolute;
      padding: 10px;
      background: #ffffff;
      border: 1px solid #878787;
      top: 70px;
      z-index: 10;
      display: grid;
      grid-template-columns: repeat(5, 1fr);
    }
    
    .callout-icon-container:after, .callout-icon-container:before {
      bottom: 100%;
      left: 10%;
      border: solid transparent;
      content: " ";
      height: 0;
      width: 0;
      position: absolute;
      pointer-events: none;
    }
    
    .callout-icon-container:after {
      border-color: rgba(255, 255, 255, 0);
      border-bottom-color: #ffffff;
      border-width: 15px;
      margin-left: -15px;
    }
    
    .callout-icon-container:before {
      border-color: rgba(135, 135, 135, 0);
      border-bottom-color: #878787;
      border-width: 18px;
      margin-left: -17.6px;
    }
    
    .icon-chooser-button {
      cursor: pointer !important;
      display: flex;
      flex-direction: column;
      padding: 10px;
    }
  
    .icon-chooser-button:hover .fas {
        color: grey !important;
    }
  
    .icon-chooser-button .fas {
      font-size: 2.5rem !important;
      min-width: unset !important;
      padding: 10px;
    }
    
    .icon-chooser-button .title {
      font-size: 16px;
      font-family: Arial;
      font-weight: normal;
      color: black;
      margin-top: 5px;
    }
  
    .video-youtube .embed-responsive,.video-wrapper .embed-responsive { pointer-events: all; cursor: pointer; }
    .video-wrapper iframe, .video-youtube iframe { pointer-events: none; }

    img.open-externally {
        border-bottom: #7575ff 3px solid;
        padding-bottom: 3px;
    }

    .remove-callout {
      position: absolute;
      top: 1em;
      right: 1em;
      font-size: 1.4em;
      color: #a9a9a9;
      outline: none !important;
      cursor: pointer !important;
    }

    .remove-callout span {
      outline: none !important;
      cursor: pointer !important;
    }

    .remove-callout span:not(:last-of-type) {
      margin-right: 0.5em;
    }

    .card:hover,.jumbotron:hover {
      position: relative;
    }
    `;

      /** Add the icon handler */
      AddTinyMCEOnlyStyle(EditorOnlyStyles);

      if (!doc.head.querySelector('link#unit-style')) {
        const area = course.Code.split('_')[1];
        const styles = [];

        if (area != 'SP') {
          const findSchool = school => school.OrgUnitCode == area || school.SubAreas.includes(area);
          const college = orgtree.find(college => college.OrgUnitCode == area || college.Schools.find(findSchool));
          if (college) {
            styles.push(`/shared/content-template/v3.0/styles/college/${college.OrgUnitCode}.css`);
            const school = college.Schools.find(findSchool);
            if (school) styles.push(`/shared/content-template/v3.0/styles/school/${school.OrgUnitCode}.css`);
          }
        }
        styles.push(`${course.Path}_design/unit-design.css`);

        doc.head.append(
          ...styles.map(href => {
            const stylesheet = document.createElement('link');
            stylesheet.rel = 'stylesheet';
            stylesheet.href = href;
            stylesheet.id = 'unit-style';
            stylesheet.setAttribute('tinymce-temporary', true);
            return stylesheet;
          })
        );
      }
      document.dispatchEvent(new Event('domchange'));
      tinymce.activeEditor.undoManager.clear();
    });
  };

  const iconChooserContainer = document.createElement('div');
  iconChooserContainer.classList.add('callout-icon-container');
  icons.forEach(({ title, icon }) => {
    const item = document.createElement('div');
    item.classList.add('icon-chooser-button');
    item.setAttribute('data-icon', icon);
    item.contentEditable = false;
    item.title = title;

    const ico = document.createElement('span');
    ico.classList.add('fas', icon);

    const name = document.createElement('span');
    name.innerText = title;
    name.classList.add('title');
    item.append(ico, name);
    iconChooserContainer.append(item);
  });

  const wouldBubbleTo = (path, match) => path.find(el => el?.matches?.(match));
  tinymce.activeEditor.on('Click', e => {
    if (!IsD2LTemplate()) return;
    if (wouldBubbleTo(e.composedPath(), '.delete-element')) return;

    if (wouldBubbleTo(e.composedPath(), '.video-youtube .embed-responsive,.video-wrapper .embed-responsive')) {
      const el = wouldBubbleTo(e.composedPath(), '.video-youtube .embed-responsive,.video-wrapper .embed-responsive');
      const parent = el.closest('.video-youtube,.video-wrapper');
      const frame = el.querySelector('iframe');
      if (!frame) return;
      const src = frame.src;
      const link = parent.querySelector(`a[href*="${new URL(src).pathname}" i]`);
      if (src.includes('youtu')) {
        const videoUrl = `https://youtu.be/${GetYoutubeID(src)}`;
        const path = prompt('Please provide a video URL.', videoUrl);
        if (path) {
          const video = el.querySelector('iframe');
          const url = GetYoutubeEmbedCode(path);
          video.src = url;
          video.setAttribute('data-mce-src', url);
          if (link) {
            link.href = path;
            link.setAttribute('data-mce-href', path);
          }
        }
      } else {
        const path = prompt('Please provide a video URL.', src);
        if (path) {
          frame.src = path;
          frame.setAttribute('data-mce-src', path);
          if (link) {
            link.href = path;
            link.setAttribute('data-mce-href', path);
          }
        }
      }
    }

    if (wouldBubbleTo(e.composedPath(), '.icon-chooser-button')) {
      const el = wouldBubbleTo(e.composedPath(), '.icon-chooser-button');
      const iconNode = el.closest('.focus');
      const oldIconName = Array.from(iconNode.classList.values()).find(c => c.startsWith('fa-'));
      iconNode.classList.replace(oldIconName, el.getAttribute('data-icon'));
    }

    doc.querySelectorAll('.callout-icon.focus').forEach(el => {
      el.classList.remove('focus');
      iconChooserContainer.remove();
    });
    if (e.target.matches('.callout-icon')) {
      e.target.classList.add('focus');
      e.target.append(iconChooserContainer);
    }
  });

  const icon = document.createElement('div');
  icon.contentEditable = false;
  icon.classList.add('delete-element');
  const deleteAction = document.createElement('button');
  deleteAction.innerText = 'Delete this element';
  icon.append(deleteAction);

  const removeContainerIcon = document.createElement('span');
  removeContainerIcon.contentEditable = false;
  removeContainerIcon.classList.add('fas', 'fa-tint-slash');
  removeContainerIcon.title = 'Remove container and keep content';

  removeContainerIcon.onclick = e => {
    const card = e.target.closest('.card');

    const icon = card.querySelector('.card-icon');

    if (icon) {
      const heading = card.querySelector('h1,h2,h3,h4,h5,h6');
      const content = card.querySelector('.card-text,.card-body');

      if (heading) card.insertAdjacentElement('beforebegin', heading);

      card.insertAdjacentElement('beforebegin', icon);
      icon.className = 'icon-standalone';
      if (!icon.parentElement?.hasAttribute('block-identifier')) {
        // If it's a nested panel, lets reset the colour.
        icon.querySelector('.fas').style.color = 'inherit';
      }

      card.replaceWith(...content?.children);
    } else {
      card.querySelectorAll('.media,.media-body').forEach(n => n.replaceWith(...n.children));

      if (card.closest('.two-col-panels')) {
        const panelTop = card.closest('.two-col-panels');
        panelTop.replaceWith(...card.closest('.row')?.children);
      } else if (card.closest('.accordion')) {
        const panelTop = card.closest('.accordion');
        const header = card.querySelector('.card-header');
        header?.querySelector('h2')?.classList.remove('card-title');
        panelTop.replaceWith(...header?.children, ...card.querySelector('.card-text,.card-body')?.children);
      }

      card.replaceWith(...card.querySelector('.card-text,.card-body')?.children);
    }

    callouticons.remove();
  };

  const toggleContainerType = document.createElement('span');
  toggleContainerType.contentEditable = false;
  toggleContainerType.classList.add('fas', 'fa-sync');

  toggleContainerType.onclick = e => {
    const card = e.target.closest('.card');
    const isStandard = card.classList.contains('card-standard');

    if (isStandard) {
      const body = card.querySelector('.card-body');

      const text = document.createElement('div');
      text.classList.add('card-text');
      text.append(...body.children);

      let icon = card.querySelector('.card-icon');
      if (!icon) {
        icon = document.createElement('div');
        icon.classList.add('card-icon');
        icon.innerHTML = `<p><span class="fas fa-book-reader callout-icon"></span></p>`;
      }

      body.append(icon, text);
      card.append(body);

      card.classList.remove('card-standard');
      card.classList.add('card-graphic', 'callout-icon');
    } else {
      const body = card.querySelector('.card-body');
      const content = card.querySelector('.card-text');
      [...body.children].forEach(el => el.remove());
      body.append(...content.children);

      card.classList.add('card-standard');
      card.classList.remove('card-graphic', 'callout-icon');
    }
    toggleContainerType.title = `Change to ${card.classList.contains('card-standard') ? 'icon' : 'standard'} callout`;
  };

  const removeJumbotronIcon = document.createElement('span');
  removeJumbotronIcon.contentEditable = false;
  removeJumbotronIcon.classList.add('fas', 'fa-tint-slash', 'remove-callout');
  removeJumbotronIcon.title = 'Remove container and keep content';

  removeJumbotronIcon.onclick = e => {
    const jtron = e.target.closest('.jumbotron');
    jtron.replaceWith(...jtron.children);

    removeJumbotronIcon.remove();
  };

  const callouticons = document.createElement('div');
  callouticons.classList.add('remove-callout');
  callouticons.append(toggleContainerType, removeContainerIcon);

  deleteAction.addEventListener(
    'click',
    e => {
      e.preventDefault();
      const frame = icon.closest('.video-youtube,.video-wrapper');
      doc.querySelectorAll('.delete-element').forEach(node => node.remove());
      tinymce.activeEditor.undoManager.add();
      frame.remove();
      tinymce.activeEditor.undoManager.add();
    },
    true
  );

  tinymce.activeEditor.on('MouseOver', e => {
    const el = wouldBubbleTo(e.composedPath(), '.video-youtube,.video-wrapper');
    if (el) {
      if (!el.querySelector('.delete-element')) el.querySelector('.embed-responsive').prepend(icon);
    } else {
      doc.querySelectorAll('.delete-element').forEach(node => node.remove());
    }

    if (e.target.matches('.card,.card *')) {
      const card = e.target.matches('.card') ? e.target : e.target.closest('.card');

      if (e.target.closest('.accordion')) {
        toggleContainerType.remove();
      } else {
        toggleContainerType.title = `Change to ${card.classList.contains('card-standard') ? 'icon' : 'standard'} callout`;
        callouticons.append(toggleContainerType);
      }

      card.append(callouticons);
    } else {
      callouticons.remove();
    }

    if (e.target.matches('.jumbotron,.jumbotron *')) {
      const card = e.target.matches('.jumbotron') ? e.target : e.target.closest('.jumbotron');
      card.append(removeJumbotronIcon);
    } else {
      removeJumbotronIcon.remove();
    }
  });

  document.addEventListener('click', e => {
    if (!IsD2LTemplate()) return;
    if (e.isTrusted)
      doc.querySelectorAll('.callout-icon.focus').forEach(el => {
        el.classList.remove('focus');
        iconChooserContainer.remove();
      });
  });

  /**
   * Manage on save events.
   * Great place to clean up anything that shouldn't be available outside the editor.
   */
  document.querySelector('d2l-floating-buttons').addEventListener('click', e => {
    if (!IsD2LTemplate()) return;

    if (e.target.matches('button') && /save/i.test(e.target.innerText)) {
      doc.querySelectorAll('[contenteditable]').forEach(node => node.removeAttribute('contenteditable'));

      doc.querySelectorAll('.remove-callout').forEach(node => node.remove());

      doc.querySelectorAll('.editor-only').forEach(node => node.remove());
      MakeLinksOpenInNewTab(config.editor.allLinksInNewTab);

      iconChooserContainer.remove();
      doc.querySelectorAll('.callout-icon.focus').forEach(node => node.classList.remove('focus'));

      doc.querySelectorAll('[tinymce-temporary]').forEach(el => el.remove());

      // Fixes issue #615
      doc.querySelectorAll('input[type="hidden"]').forEach(el => el.remove());

      // Fix strangely nested tab-containers hiding content
      tinymce.activeEditor.dom.doc.querySelectorAll('.tab-content>.tab-pane .tab-pane').forEach(node => {
        if (!node.parentNode.matches('.tab-content')) {
          node.replaceWith(...node.children);
        }
      });

      /**
       * These are the known, required scripts to make the page run.
       * It's also important that they're in order!
       * As such, go through and make sure that they exist, and have been appended in the correct order.
       */
      const scripts = [
        '/shared/content-template/v3.0/assets/thirdpartylib/jquery/jquery-3.3.1.slim.min.js',
        '/shared/content-template/v3.0/assets/thirdpartylib/popper-js/popper.min.js',
        '/shared/content-template/v3.0/assets/thirdpartylib/bootstrap-4.3.1/js/bootstrap.min.js',
        '/shared/content-template/v3.0/assets/js/scripts.min.js',
        '/shared/content-template/v3.0/assets/js/custom.js',
      ];

      const d = tinymce.activeEditor.dom.doc;
      for (const src of scripts) {
        d.querySelectorAll(`script[data-mce-src][src*="${src}"]`).forEach(el => el.remove());

        const script = document.createElement('script');
        script.src = src;
        script.setAttribute('data-mce-src', src);
        script.type = 'mce-no/type';
        d.body.append(script);
      }

      // Remove empty p tags that are just taking up editor room.
      // Note, this keeps p[&nbsp;] tags, as they have an innerHTML content.
      doc.querySelectorAll('p').forEach(p => {
        if (p.closest('[block-identifier]')) return;

        if (p.innerHTML.trim().length == 0) {
          p.remove();
        }
        if (p.innerHTML.trim() == '<br>') {
          p.remove();
        }
      });
    }
  });

  const OnFirstLoad = () => {
    const dom = tinymce.activeEditor.dom.doc;

    /** Replace the card titles with their plain-text equivalent */
    dom.querySelectorAll('.accordion h2.card-title').forEach(el => {
      el.innerText = el.innerText;
    });
  };

  const OnContentUpdate = ev => {
    checkSidebarVisibility();
    if (IsD2LTemplate()) {
      if (ev) {
        // When there's a content update, check if it's major.
        // In this case, "major" means that the entire template body has changed,
        // rather than just a sub-element.
        const domChange = document.createElement('template');
        domChange.innerHTML = ev.content;
        if (domChange.content?.firstElementChild?.matches('[mm-template-style]')) {
          HandleMajorTemplateChange(ev);
        }
      }

      OnD2LTemplateLoad(ev);
      OnFirstLoad();

      const mo = new MutationObserver(showSidebar);
      document.querySelectorAll('.editor-sidebar').forEach(sidebar => {
        mo.observe(sidebar, {
          attributes: true,
          attributeFilter: ['collapsed'],
        });
      });

      showSidebar();
    } else {
      // sizeMessage.remove();
    }
  };

  document.addEventListener('domchange', e => {
    const acts = actions.flatMap(({ children, collapsed }) => [...children, ...(collapsed ?? [])]);

    acts.forEach(async action => {
      const { title, description, display, domNode, targetNode } = action;

      domNode.innerText = typeof title == 'string' ? title : title(tinymce.activeEditor);
      domNode.title = typeof description == 'string' ? description : description(tinymce.activeEditor);
      if (!targetNode) {
        domNode.hidden = !(await display(config, tinymce.activeEditor));
      }

      const parent = action.domNode.closest('.extra-label');
      if (parent) {
        parent.hidden = parent.querySelectorAll('ul li:not([hidden])').length == 0;
      }
    });
  });

  tinymce.activeEditor.on('SetContent', OnContentUpdate);
  document.dispatchEvent(new Event('domchange'));
  OnContentUpdate();

  const mo = new MutationObserver(showSidebar);
  mo.observe(editorArea, { attributes: true, attributeFilter: ['class'] });

  const bodyWatcher = new MutationObserver(checkSidebarVisibility);
  bodyWatcher.observe(document.body, { childList: true, attributes: true });
  showSidebar();

  tinymce.activeEditor.on('PastePreProcess', e => {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = e.content;

    const sel = tinymce.activeEditor.selection.getNode();
    const isHeader =
      sel && (sel.matches('[block-identifier="header-block"]') || sel.closest('[block-identifier="header-block"]') != null);

    if (isHeader) {
      e.content = wrapper.innerText;
    } else {
      wrapper.querySelectorAll('[block-identifier],[contenteditable]').forEach(el => {
        el.removeAttribute('block-identifier');
        el.removeAttribute('contenteditable');
      });
      e.content = wrapper.innerHTML;
    }
  });

  tinymce.activeEditor.dom.doc.addEventListener(
    'keydown',
    e => {
      if (!IsD2LTemplate()) return;
      if (e.code == 'Backspace' || e.code == 'Delete') {
        const el = e.target.matches('[block-identifier]') ? e.target : e.target.closest('[block-identifier]');
        if (el && el.innerText.trim().length == 0) {
          e.preventDefault();
          e.stopPropagation();
        }
      }
    },
    true
  );

  /** Check that the template is okay. */
  setInterval(() => {
    if (!IsD2LTemplate()) return;

    /** Get a true representation of the dom */
    const dom = new DOMParser().parseFromString(tinymce.activeEditor.getContent({ type: 'html' }), 'text/html');

    /** If the head tag is empty, repopulate it. */
    if (dom.head.innerHTML.trim() == '') {
      dom.head.innerHTML = `
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="/shared/content-template/v3.0/assets/thirdpartylib/bootstrap-4.3.1/css/bootstrap.min.css">
  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="/shared/content-template/v3.0/assets/thirdpartylib/fontawesome-free-5.9.0-web/css/all.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="/shared/content-template/v3.0/assets/css/styles.min.css">
  <link rel="stylesheet" href="/shared/content-template/v3.0/assets/css/utas.css">
  <script target="unit-style" src="/shared/content-template/v3.0/assets/js/stylesheet.js"></script>
  <title>{OrgUnitName}</title>
`;

      tinymce.activeEditor.setContent(dom.documentElement.outerHTML, { format: 'html' });
    }
  }, 1000);
})();
