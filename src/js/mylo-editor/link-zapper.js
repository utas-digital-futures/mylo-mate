/**
 * @affects Content Editor
 * @problem Links that have no visible text are often hard to remove, and bloat the code unnecessarily.
 * @solution Add a link zapper which automatically finds any links with no visible text and offers to remove them.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  const emptyLinkId = 'mm-remove-empty-links';

  await WaitForTinyMCE();

  const plural = (count, singular, plural) => (count === 1 ? singular : plural);
  const getEmptyLinks = () =>
    Array.from(tinymce.activeEditor.dom.doc.querySelectorAll('a[href]')).filter(
      a => a.innerText.trim().length === 0 && !(a.querySelector('img') || a.querySelector('video'))
    );

  const checkEmptyLinks = () => {
    try {
      const emptyLinks = getEmptyLinks();
      const uniqueLinks = emptyLinks
        .reduce((o, c) => {
          if (!o.includes(c.href)) {
            o.push(c.href);
          }
          return o;
        }, [])
        .slice(0, 5);
      if (
        emptyLinks.length > 0 &&
        confirm(
          `There ${plural(emptyLinks.length, 'is', 'are')} ${emptyLinks.length} empty ${plural(
            emptyLinks.length,
            'link',
            'links'
          )} in this document.\n\nThe ${
            uniqueLinks.length > 5 ? 'links include' : plural(uniqueLinks.length, 'link is', 'links are')
          }:\n${uniqueLinks.map(href => `- ${href}`).join('\n')}\n\nWould you like to remove ${plural(
            emptyLinks.length,
            'it',
            'them'
          )}?`
        )
      ) {
        emptyLinks.forEach(link => link.remove());
      }
      doEmptyLinksButtonThings();
    } catch (e) {
      console.warn('Something went wrong trying to delete empty links.');
      console.warn(e);
    }
  };

  const doEmptyLinksButtonThings = () => {
    const emptyLinksButton = document.querySelector(`#${emptyLinkId}`);
    const emptyLinks = Array.from(getEmptyLinks());

    emptyLinksButton.setAttribute('text', `(${emptyLinks.length})`);
    if (emptyLinks.length === 0) {
      emptyLinksButton.setAttribute('disabled', '');
      emptyLinksButton.title = `No empty links were found in this page. This feature allows easy cleanup of links that don't have any text visible to the user.`;
    } else {
      emptyLinksButton.removeAttribute('disabled');
      emptyLinksButton.title = `The following ${plural(
        emptyLinks.length,
        'link has',
        'links have'
      )} no visible text: ${emptyLinks
        .map(a => a.href)
        .join(', ')}. This feature allows easy cleanup of links that don't have any text visible to the user.`;
    }
  };

  /** Create the link zapper button. */
  const linkZapper = document.createElement('d2l-button-subtle');
  linkZapper.id = emptyLinkId;
  linkZapper.addEventListener('click', checkEmptyLinks);
  linkZapper.setAttribute('icon', 'tier1:quicklink');

  linkZapper.setAttribute('mm-icon-type', 'action-select');
  linkZapper.setAttribute('mm-icon-offset', '-6:0');

  const container = document.querySelector('input[type="hidden"][name="topicIsHidden"]').closest('div');
  container.append(linkZapper);

  doEmptyLinksButtonThings();
})();
