(async () => {
  const url = new URL(window.location.href);
  const sel = url.searchParams.get('sx');
  if (!sel) return;

  await WaitForTinyMCE();

  const scrollToElement = () => {
    /** The padding from the top of the page to the element in question */
    const buffer = 20;

    const selector = atob(sel);

    const win = tinymce.activeEditor.getWin();
    const doc = tinymce.activeEditor.getDoc();

    const el = doc.querySelector(selector);
    const box = el.getBoundingClientRect();
    const offset = win.pageYOffset + box.top - buffer;

    win.scrollTo({ behavior: 'smooth', top: offset });
  };

  if (document.readyState === 'complete') scrollToElement();
  else tinymce.activeEditor.iframeElement.addEventListener('load', scrollToElement);
})();
