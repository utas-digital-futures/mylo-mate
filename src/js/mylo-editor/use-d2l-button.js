// (async function () {
//   const config = await FetchMMSettings();

//   const basicTemplateSrc = '/shared/content-template/v3.0/pages/Basic%20Page.html';
//   if (window.location.hash == '#MMSetBasicTemplate') {
//     const res = await fetch(basicTemplateSrc);
//     if (res.status == 200) {
//       const page = await res.text();
//       tinymce.activeEditor.setContent(page);
//     }
//     window.location.hash = '';
//   }

//   const orgId = window.location.pathname.match(/\/content\/([\d]+)/)[1];
//   const course = await api(`/d2l/api/lp/1.26/courses/${orgId}`, { first: true });

//   // const RemoteFileExists = url =>
//   //   fetch(url, {
//   //     method: 'head',
//   //     redirect: 'error',
//   //   })
//   //     .then(d => d.status == 200)
//   //     .catch(() => false);

//   // if (!config.editor.AlwaysShowTemplateChangeButton && (await RemoteFileExists(`${course.Path}template/template.html`))) {
//   //   return;
//   // }

//   async function GetUnitSettings(id, asURLData = false) {
//     const dom = new DOMParser().parseFromString(
//       await fetch(`/d2l/le/content/${id}/settings/Open`).then(d => d.text()),
//       'text/html'
//     );
//     const form = dom.querySelector('form');
//     const fd = new URLSearchParams(new FormData(form));
//     if (asURLData) return fd;

//     const items = {};
//     for (const [k, v] of fd) {
//       items[k] = v;
//     }
//     return items;
//   }

//   async function SaveUnitSettings(id, changes) {
//     const fd = await GetUnitSettings(id, true);
//     for (const [k, v] of Object.entries(changes)) {
//       fd.set(k, v);
//     }

//     fd.append('requestId', Date.now() % 10);
//     fd.append('d2l_referrer', window.localStorage.getItem('XSRF.Token'));
//     fd.append('isXhr', true);

//     return fetch(`/d2l/le/content/${id}/settings/Save`, { method: 'post', body: fd });
//   }

//   const unitSettings = await GetUnitSettings(orgId);
//   let shouldShow = true;

//   // if (!unitSettings.defaultTemplateIsEnabled) shouldShow = true;
//   // if (unitSettings.defaultTemplatePathSelector == course.Path) shouldShow = true;
//   if (unitSettings.defaultTemplatePathSelector.includes('/shared/content-template/')) shouldShow = false;

//   if (shouldShow) {
//     const useD2Lbutton = document.createElement('d2l-button-subtle');
//     useD2Lbutton.style.position = 'absolute';
//     useD2Lbutton.style.top = '10px';
//     useD2Lbutton.style.right = '10px';
//     useD2Lbutton.setAttribute('mm-icon-type', 'action-select');
//     useD2Lbutton.setAttribute('mm-icon-offset', '-6:0');
//     useD2Lbutton.setAttribute('icon', 'tier1:gear');
//     useD2Lbutton.title = 'Set Default to D2L Templates';
//     useD2Lbutton.addEventListener('click', async e => {
//       const update =
//         tinymce.activeEditor.getBody().innerText.trim().length > 0
//           ? confirm(
//               'There appears to be content in the editor. ALL THIS CONTENT WILL BE REMOVED. Select OK to overwrite the content with a new template.'
//             )
//           : true;

//       if (update) {
//         await SaveUnitSettings(orgId, {
//           defaultTemplateIsEnabled: '1',
//           defaultTemplatePathSelector: '/shared/content-template/v3.0/pages/',
//         });
//         window.location.hash = 'MMSetBasicTemplate';
//         window.location.reload();
//       }
//     });
//     const container = document.querySelector('.d2l-page-main');
//     container.style.position = 'relative';
//     container.append(useD2Lbutton);
//   }
// })();
