/**
 * @affects MyLO
 * @problem The `Items per Page` dropdown only appears at bottom of screen, beneath all content, requiring the user to scroll to the bottom to use it.
 * @solution Add a copy of the Items per Page dropdown at the top of the page as well, so it is more accessible.
 * @target Academic and support staff
 * @impact Low
 */

(function () {
  let orig = document.querySelector('select[title="Results Per Page"]');
  if (!orig) return;

  const o = orig.parent('table.d_gact.d_gd');
  o.style.zIndex = 1;
  let clone = o.cloneNode(true);

  clone.querySelectorAll('select').forEach(sel => {
    let wrapper = document.createElement('div');
    sel.parentNode.insertBefore(wrapper, sel);
    wrapper.appendChild(sel);
    wrapper.setAttribute('mm-icon-type', 'action-select');
    wrapper.setAttribute('mm-icon-offset', '-6:0');
  });

  const db = new URL(window.location.href).searchParams.get('db');

  orig.parent('div.d2l-grid-container').insertBefore(clone, document.querySelector('d2l-table-wrapper'));
  clone.querySelectorAll('select').forEach(node =>
    node.addEventListener('change', e => {
      let o = orig.parent('table.d_gact.d_gd').querySelector(`#${node.id}`);
      o.value = node.value;
      o.dispatchEvent(new Event('change'));

      window.localStorage[`pagecount-${db}`] = node.value;
    })
  );

  orig
    .parent('table.d_gact.d_gd')
    .querySelectorAll('td.d_gact')
    .forEach(td => (td.style.paddingRight = 0));
  orig.parent('table.d_gact.d_gd').style.width = '100%';
  // orig.parent('table.d_gact.d_gd').style.float = 'right';

  clone.querySelectorAll('td.d_gact').forEach(td => (td.style.paddingRight = 0));
  clone.style.width = '100%';
  // clone.style.float = 'right';
})();
