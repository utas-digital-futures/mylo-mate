/**
 * @affects Assignment Folders
 * @problem There is no way to know if a rubric is attached to an assignment folder without going into the Properties area of the assignment folder.
 * @solution Add an icon next to each Assignment Folder name with an attached rubric. Clicking the icon automatically opens the editor for the Rubric.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  // const id = document.querySelector('a[href^="/d2l/home"]').href.split('/').pop();
  const id = window.location.searchParams.ou;
  let results = await api(`/d2l/api/le/1.28/${id}/dropbox/folders/`, { fresh: true });

  results.forEach(folder => {
    let link = document.querySelector(`a[href*="folder_submissions"][href*="db=${folder.Id}"]`);
    let parent = link?.closest('.d2l-foldername');

    if (link && folder.Assessment.Rubrics.length > 0) {
      let icon = document.createElement('div');
      icon.style.float = 'left';
      icon.style.marginLeft = '1em';
      icon.classList.add('dco', 'rubric-attached');
      icon.innerHTML = `<div class="dco_c"><a href="/d2l/lp/rubrics/edit_structure.d2l?rubricId=${
        folder.Assessment.Rubrics[0].RubricId
      }&ou=${id}" target="_blank" title="${`${folder.Assessment.Rubrics.length} rubric${
        folder.Assessment.Rubrics.length == 1 ? '' : 's'
      } attached. Click to edit the '${
        folder.Assessment.Rubrics[0].Name
      }' rubric in a new window.`}"><d2l-icon icon="tier1:rubric-graded"></d2l-icon></a></div>`;

      parent.querySelector('div.clear')?.insertAdjacentElement('beforebegin', icon);
    }
  });
})();
