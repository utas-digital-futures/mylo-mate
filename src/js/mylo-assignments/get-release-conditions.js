/**
 * @affects Assignment Folders
 * @problem Conditional Release settings aren't visible without visiting the Restrictions tab for each folder.
 * @solution Add the Conditional Release details for each assignment folder to the hover text of the Release icon.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  const titleBreak = '\u000A';

  document.querySelectorAll('img[alt="Has release conditions" i]').forEach(async icon => {
    let el = icon.closest('th').querySelector('a[href*="admin/mark" i]');
    if (!el) return;

    const link = new URL(el.href);
    const restrictions = await getReleaseConditions(link.query.ou, 'dropboxes', link.query.db);
    const text = [icon.title].concat(restrictions.map(v => ` - ${v}`));
    icon.title = text.join(titleBreak);
  });
})();
