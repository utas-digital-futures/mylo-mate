(async () => {
  const specialAccessUsers = Array.from(document.querySelectorAll('.dcs_r'));
  const url = new URL(window.location.href);

  const db = url.searchParams.get('db');
  const ou = url.searchParams.get('ou');

  const submissions = await api(`/d2l/api/le/1.57/${ou}/dropbox/folders/${db}/submissions/`, { fresh: true });

  specialAccessUsers.forEach(row => {
    const userId = row.querySelector('input[name^="csSpAccess_"]')?.name.split('_')[1];
    if (!userId) return;

    const submission = submissions.find(sub => sub.Entity.EntityId == userId);
    if (!submission) return;

    if (submission.Submissions.length == 0) return;

    const latestSubmission = moment(submission.Submissions.at(-1).SubmissionDate);

    const icon = document.createElement('d2l-icon');
    icon.setAttribute('icon', 'tier1:share-parent-filled');

    const date = document.createElement('div');
    date.setAttribute('style', 'text-align:left;margin-left:1.4em;');
    date.innerText = `Latest submission: ${latestSubmission.format('DD MMMM, YYYY HH:mm')}`;

    date.prepend(icon);
    icon.style.marginRight = '1em';

    date.setAttribute('mm-icon-type', 'action-button');
    date.setAttribute('mm-icon-offset', '-10:-2');

    row.querySelector('.dcs_cf').append(date);
  });
})();
