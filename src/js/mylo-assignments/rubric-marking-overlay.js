const RUBRIC_TYPE = {
  POINTS: 2,
  TEXT_ONLY: 0,
  CUSTOM_POINTS: 3,
};

const RubricBase = window.location.origin.includes('d2ldev2')
  ? '0e78be5b-5af6-4f4b-827b-568374e17a1d'
  : 'c0ef8d21-d21d-4bf1-b68f-054dc1a5e6fc';

const storage = FetchMMSettings();

/* const GetOAuth2Token = async () => {
  const existing = window.localStorage['D2L.Fetch.Tokens'];
  if (existing != null && existing['*:*:*'] != null) {
    const body = JSON.parse(existing['*:*:*']);
    if (body.expires_at < Date.now() / 1000) {
      delete window.localStorage['D2L.Fetch.Tokens'];
    } else return body.access_token;
  }

  if (!window.localStorage['XSRF.Token']) return null;

  const token = await fetch('/d2l/lp/auth/oauth2/token', {
    method: 'post',
    body: 'scope=*:*:*',
    credentials: 'include',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-Csrf-Token': window.localStorage['XSRF.Token'],
    }),
  }).then(data => data.json());

  window.localStorage.setItem('D2L.Fetch.Tokens', JSON.stringify({ '*:*:*': token }));
  return token.access_token;
}; */

const getRubricData = async (rubricId, orgid) => {
  try {
    let token = await GetOAuth2Token();
    if (!token)
      return alert('There was an error getting the necessary authentication. Please refresh the page and try again.');

    // console.log(token);
    // return;

    const opts = {
      token,
      fresh: true,
      first: true,
    };

    const rubricItem = `https://${RubricBase}.rubrics.api.brightspace.com/organizations/${orgid}/${rubricId}`;
    const [details, groups, overallLevels] = await Promise.all([
      api(rubricItem, opts),
      api(`${rubricItem}/groups`, opts),
      api(`${rubricItem}/overallLevels`, opts),
    ]);

    const criteriaGroups = await Promise.all(
      groups.entities.map(async group => {
        const levelsURL = group.links.find(link => link.rel[0].endsWith('/levels')).href;
        const criteriaURL = group.links.find(link => link.rel[0].endsWith('/criteria')).href;
        const [levels, criteria] = await Promise.all([
          (await api(levelsURL, opts)).entities.map(e => e.properties),
          (await api(criteriaURL, opts)).entities.map(e => {
            return {
              id: e.links
                .find(l => l.rel.includes('self'))
                .href.split('/')
                .pop(),
              name: e.properties.name,
              outOf: e.properties.outOf,
              criterions: e.entities.map(en => {
                let crit = en.entities.find(j => j.class.includes('description')).properties;
                return {
                  level: en.properties.levelName,
                  points: en.properties.points || null,
                  html: crit.html,
                  text: crit.text,
                };
              }),
            };
          }),
        ]);
        return { levels, criteria };
      }),
    );

    const rubric = {
      name: details.properties.name,
      description: details.entities.find(e => e.class.includes('description')).properties.html,
      overall: overallLevels.entities.map(lvl => lvl.properties),
      type: details.class.includes('numeric') ? RUBRIC_TYPE.CUSTOM_POINTS : RUBRIC_TYPE.TEXT_ONLY,
      status: details.class.includes('draft') ? 2 : 0,
      ...(details.properties.outOf && { outOf: details.properties.outOf }),
      criteria: criteriaGroups,
    };

    return rubric;
  } catch ([url, xhr, e]) {
    console.error('[ERROR] There was an error trying to get the information.', url, xhr, e);
    alert('There was an error getting the information for this rubric.');
    throw e;
  }
};

const getRubrics = async () => {
  const config = await storage;

  const parseNum = (frac1, frac2, val) => {
    const level = parseFloat(config.other.RubricRangeMakerDecimalPlaces ?? '1');
    const floor = number => {
      const amt = level == 0.5 ? 2 : Math.pow(10, level);
      return Math.floor(number * amt) / amt;
    };

    return `${floor(frac1 * val)} - ${floor(frac2 * val)}`;
  };

  const rubrics = sdqs.querySelectorAllDeep('d2l-rubric');

  for (const rubric of rubrics) {
    const [ou, id] = rubric.href.split('/').slice(-2);
    const res = await getRubricData(id, ou);

    const names = sdqs.querySelectorAllDeep('.criterion-name', rubric);
    for (const name of names) {
      const c = res.criteria[0].criteria.find(c => c.id == name.getRootNode().host.href.split('/').pop());
      if (c?.outOf == null) continue;

      const table = document.createElement('table');
      const thead = document.createElement('thead');
      const tbody = document.createElement('tbody');

      thead.innerHTML = `<tr><th colspan="5">UTAS Standard Rubric</th></tr>`;

      tbody.innerHTML = `
					<tr>
					<th>HD</th>
					<th>DN</th>
					<th>CR</th>
					<th>PP</th>
					<th>NN</th>
					</tr>
					<tr>
					<td>${parseNum(0.8, 1, c.outOf)}</td>
					<td>${parseNum(0.7, 0.7999999999, c.outOf)}</td>
					<td>${parseNum(0.6, 0.6999999999, c.outOf)}</td>
					<td>${parseNum(0.5, 0.5999999999, c.outOf)}</td>
					<td>${parseNum(0, 0.4999999999, c.outOf)}</td>
					</tr>
					`;

      const style = document.createElement('style');
      style.innerHTML = `
				table.standard { width: 100%; border-collapse: collapse; margin: 1em 0; user-select: none; }
				table.standard[collapsed] tbody { display: none; }
				table.standard thead th:after { content: 'Click to collapse'; display: inline; font-size: 0.8em; color: grey; margin-left: 1em; }
				table.standard[collapsed] thead th:after { content: 'Click to expand'; }
				table.standard thead { cursor: pointer; }
				table.standard :is(th,td) { border: thin solid #ccc; padding: 0.3em; font-size: 0.9em; text-align: center; }
				`;

      thead.addEventListener('click', () => {
        table.toggleAttribute('collapsed');
      });

      table.classList.add('standard');
      table.setAttribute('collapsed', true);

      table.append(thead, tbody, style);
      name.append(table);
    }
  }
};

const diff = (prev, curr) => prev.length !== curr.length || prev.some(r => !curr.includes(r));

onTick(async prev => {
  const rubrics = sdqs.querySelectorAllDeep('d2l-rubric');
  if (diff(prev ?? [], rubrics)) getRubrics();

  return rubrics;
});
