(async function () {
  return;

  // const ou = window.location.searchParams.ou;
  // const button = document.createElement('d2l-button-subtle');
  // const container = document.createElement('div');
  // container.classList.add('d2l-grid-action-wrapper', 'd2l-button-group-custom-item');

  // button.setAttribute('text', 'Bulk detach rubrics');
  // button.setAttribute('icon', 'tier1:rubric');
  // button.setAttribute('title', 'Detach all rubrics connected to selected assessment folders.');
  // button.setAttribute('mm-icon-type', 'action-button');
  // button.setAttribute('mm-icon-offset', '-7:-2');

  // container.append(button);
  // document.querySelector('d2l-overflow-group').append(container);

  // let folders = await api(`/d2l/api/le/1.28/${ou}/dropbox/folders/`, { fresh: true });

  // const DisconnectRubrics = async folder => {
  //   const url = `/d2l/lms/dropbox/admin/modify/folder_newedit_properties.d2l?db=${folder}&ou=${ou}`;
  //   const dom = new DOMParser().parseFromString(await fetch(url).then(d => d.text()), 'text/html');

  //   const rubricIds = folders.find(asst => asst.Id == folder).Assessment.Rubrics.map(rub => rub.RubricId);

  //   const body = new URLSearchParams(new FormData(dom.querySelector('form')));
  //   body.set('d2l_action', 'Update');
  //   body.set('d2l_actionparam', '1');
  //   body.set('d2l_hitCode', window.localStorage.getItem('XSRF.HitCodeSeed') + ((new Date().getTime() + 1e8) % 1e8));
  //   body.set('d2l_rf', '');
  //   body.set('d2l_referrer', window.localStorage.getItem('XSRF.Token'));
  //   // body.set('d2l_multiedit', JSON.stringify({ Controls: [] }));
  //   // body.set(
  //   //   'd2l_stateScopes',
  //   //   JSON.stringify({
  //   //     1: ['gridpagenum', 'search', 'pagenum'],
  //   //     2: ['lcs', 'dropboxproperties'],
  //   //     3: ['grid', 'pagesize', 'htmleditor', 'hpg'],
  //   //   })
  //   // );
  //   // body.set('d2l_stateGroups', '');
  //   // body.set('d2l_statePageId', '477');
  //   rubricIds.forEach(id => body.set('z_cu_existingDeleted', id));

  //   await fetch(url + '&dst=1', { method: 'post', body });
  // };

  // button.addEventListener('click', e => {
  //   const selected = document.querySelectorAll('table.d2l-table tr:not(:nth-child(1)) input[type="checkbox"]:checked');
  //   if (selected.length == 0) return;

  //   selected.forEach(async cb => {
  //     const folder = new URL(cb.closest('tr').querySelector('[href*="db="]').href).query.db;
  //     await DisconnectRubrics(folder);
  //   });
  // });
})();
