(async () => {
  await until(() => sdqs.querySelectorDeep('d2l-labs-d2l-grade-result-presentational'));

  class ValueNotDefinedError extends Error {
    constructor(msg, response) {
      super(msg);
      this.response = response;
    }
  }

  const { ou, db } = new URL(
    document.querySelector('d2l-consistent-evaluation').getAttribute('return-href'),
    window.location.origin
  ).query;

  try {
    const assessment = await api(`/d2l/api/le/1.55/${ou}/dropbox/folders/${db}`, { first: true });
    if (assessment?.GradeItemId == null)
      throw new ValueNotDefinedError(`Grade item not found for dropbox folder ${db}`, assessment);

    const gradeItem = await api(`/d2l/api/le/1.55/${ou}/grades/${assessment.GradeItemId}`, { first: true });
    if (gradeItem?.GradeSchemeUrl == null)
      throw new ValueNotDefinedError(`Grade scheme not found for grade item ${assessment.GradeItemId}`, gradeItem);

    const scheme = await api(gradeItem.GradeSchemeUrl, { first: true });
    if (scheme?.Ranges == null)
      throw new ValueNotDefinedError(`Grade scheme ranges not found for grade scheme ${gradeItem.GradeSchemeUrl}`, scheme);

    const GetCorrectGradeScheme = score => {
      const base = assessment.Assessment?.ScoreDenominator;
      if (!base) return null;

      const corrected = (score / base) * 100;

      for (let i = scheme.Ranges.length - 1; i >= 0; i--) {
        const range = scheme.Ranges[i];
        if (range.PercentStart > corrected) continue;
        return range;
      }

      return null;
    };

    const style = document.createElement('style');
    style.innerHTML = `
    .student-preview { 
        position: relative; 
        margin-left: 1em;
        padding-left: 1em;
        border-left: thin solid #ccc;
        display: flex;
        gap: 0.5em;
        justify-content: center;
        align-items: center;
    }

    .student-preview .schemacolour {
        background-color: #FFFFFF;
        border-radius: 3px;
        border: thin solid #ccc;
        width: 1em;
        height: 1em;
    } 

    `;

    const schemaview = document.createElement('div');
    schemaview.classList.add('student-preview');

    const schemacolor = document.createElement('div');
    schemacolor.classList.add('schemacolour');

    const schemaletter = document.createElement('div');

    schemaview.append(style, schemacolor, schemaletter);

    const container = await until(() => sdqs.querySelectorDeep('.d2l-grade-result-presentational-container'));
    container.append(schemaview);

    const score = sdqs.querySelectorDeep('d2l-labs-d2l-grade-result-presentational');

    const RenderScore = () => {
      const value = parseFloat(score.getAttribute('scorenumerator'));
      const scheme = GetCorrectGradeScheme(value);

      schemacolor.style.backgroundColor = scheme.Colour;
      schemaletter.innerText = scheme.Symbol;

      schemaview.style.display = Number.isNaN(value) ? 'none' : '';
    };

    const mo = new MutationObserver(RenderScore);
    mo.observe(score, { attributes: true });
    RenderScore();
  } catch (e) {
    console.warn('Error building student grade view:', e);
  }
})();
