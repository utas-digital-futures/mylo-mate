/**
 * @affects Assignment Submission
 * @problem When marking, rubrics that aren't completed fully are easy to overlook.
 * @solution Add a widget to the top of the submissions page that checks the rubric completion status, highlighting any unattempted or incomplete rubrics.
 * @target Academic and support staff
 * @impact High
 * @savings 20 minutes per folder
 */

(async function () {
  if (
    Array.from(document.querySelectorAll('.d2l-table tr:not([header])')).length == 0 ||
    window.location.searchParams == null
  )
    return;

  const dialogWidth = 600;
  const dialogHeight = 600;
  const { ou, db } = window.location.searchParams;

  const style = document.createElement('style');
  style.innerHTML = `
  .mm-row-count, .mm-incomplete-rubrics {
    float:right;
    overflow: visible;
  }
  
  .mm-incomplete-rubrics { margin-right: 50px; }

  .mm-analytics h2,
  .mm-analytics h3,
  .mm-rubric-report-dialog .d2l-dialog-inner h2,
  .mm-rubric-report-dialog .d2l-dialog-inner h3 {
    font-weight: bold;
    margin-bottom:10px;
    border-bottom: thin lightgrey solid;
  }

  .mm-analytics h3, .mm-rubric-report-dialog .d2l-dialog-inner h3 {
    font-size: 0.8em;
  }

  .mm-rubric-report-dialog p, .mm-analytics p { margin: 0; }
  .d_tabs_tabcontent { position: relative; }

  .mm-rubric-report-dialog {
    top: 70px; 
    width: ${dialogWidth}px; 
    height: ${dialogHeight}px;
    z-index: 1006;
  }
  
  .mm-rubric-report-dialog .d2l-dialog-inner {
    height: 598px;
    display: grid;
    grid-template-rows: 1fr 8fr 1fr;
  }

  .mm-rubric-report-dialog .d2l-dialog-inner > div {padding: 20px;}

  .mm-rubric-report-dialog .d2l-dialog-inner .heading,
  .mm-rubric-report-dialog .d2l-dialog-inner .buttons {
    align-self: center;
    padding-top: 0;
    padding-bottom: 0;
  }

  .mm-rubric-report-dialog .d2l-dialog-inner .heading { align-self: end; }
  .mm-rubric-report-dialog .d2l-dialog-inner .content { overflow-y: auto; }
  .mm-rubric-report-dialog h2:not(:first-child), .mm-rubric-report-dialog h3:not(:first-child) { margin-top: 20px; }
  .mm-rubric-report-dialog .d2l-dialog-inner a.csv-save { font-size: 0.9em; margin-left: 5px; }

  d2l-icon.more-info {
    margin-left: 10px;
    color: grey;
    cursor: pointer;
    transition: color 0.1s linear;
  }

  d2l-icon.more-info:hover { color: black; }
  d2l-icon.more-info[disabled] { color: #ccc; cursor: default; }

  a.csv-save {
    font-size: small;
    display: block;
    text-align: right;
  }
  `;
  document.head.appendChild(style);

  const container = document.createElement('div');
  container.classList.add('mm-analytics');

  const results = {};

  Array.from(document.querySelectorAll(":is(.dlay_r,.d_gn) a[onclick*='SetReturnPoint' i]"))
    .map(a => a.innerText.replace(/[^a-z-\d\s]/gi, ''))
    .forEach(row => (results[row] = (results[row] ?? 0) + 1));

  const wrapper = document.createElement('div');
  wrapper.setAttribute('data-testid', 'on-this-page');
  wrapper.classList.add('mm-row-count');
  wrapper.innerHTML = `
    <h2><span mm-icon-type="view-data" mm-icon-offset="-6"></span>On This Page</h2>
    ${Object.keys(results)
      .map(type => `<p>${type}: ${results[type]}</p>`)
      .join('\n')}
  `;

  container.append(wrapper);

  let submissionsLoading = true;
  const sortedSubmissions = {
    incomplete: [],
    partial: [],
    complete: [],
  };
  // const moreSubmissionsInfo = document.createElement('d2l-button-subtle');
  // moreSubmissionsInfo.setAttribute('icon', 'd2l-tier1:table-of-contents');

  const moreSubmissionsInfo = document.createElement('d2l-icon');
  moreSubmissionsInfo.setAttribute('icon', 'd2l-tier1:table-of-contents');
  moreSubmissionsInfo.setAttribute('title', 'Show report');
  moreSubmissionsInfo.setAttribute('disabled', '');
  moreSubmissionsInfo.classList.add('more-info');

  // const moreSubmissionsInfo = document.createElement('button');
  // moreSubmissionsInfo.classList.add('d2l-button');
  // moreSubmissionsInfo.setAttribute('mm-icon-type', 'action-select');
  // moreSubmissionsInfo.setAttribute('mm-icon-offset', '-6:0');
  // document.querySelector('d2l-button-group.d2l-action-buttons').append(moreSubmissionsInfo);

  let groups = null;
  let unit = null;
  let folder = null;

  const GetUsersGroupEnrollments = userid => {
    const output = [];
    groups.forEach(category => {
      category.Groups.forEach(group => {
        if (group.Enrollments.includes(parseInt(userid))) {
          output.push({
            category,
            group,
          });
        }
      });
    });
    return output;
  };

  // Handle the dialog popup for more info on submission data
  (async function () {
    const dialog = document.createElement('mm-dialog');
    document.body.append(dialog);
    dialog.classList.add('mm-rubric-report-dialog');
    dialog.addEventListener('open', () => {
      document.body.style.overflow = 'hidden';
    });
    dialog.addEventListener('close', () => {
      document.body.style.overflow = '';
    });
    dialog.preserve = true;
    dialog.hideCancel = true;

    dialog.heading = `Incomplete Evaluations`;
    dialog.innerHTML = `<p><mm-loader center></mm-loader></p><p style="text-align:center;">Loading, please wait..</p>`;
    dialog.maxHeight = '90%';

    moreSubmissionsInfo.addEventListener('click', async e => {
      dialog.open = true;

      groups =
        groups ||
        (await Promise.all(
          (
            await api(`/d2l/api/lp/1.22/${ou}/groupcategories/`)
          ).map(async group => {
            const enrollments = await Promise.all(
              group.Groups.map(id => api(`/d2l/api/lp/1.22/${ou}/groupcategories/${group.GroupCategoryId}/groups/${id}`))
            );
            group.Groups = enrollments.map(i => i[0]);
            return group;
          })
        ));

      unit = unit || (await api(`/d2l/api/lp/1.22/enrollments/myenrollments/${ou}`, { first: true }));
      folder = folder || (await api(`/d2l/api/le/1.22/${ou}/dropbox/folders/${db}`, { first: true }));

      await until(() => !submissionsLoading);
      dialog.innerHTML = '';

      const classlist = await api(`/d2l/api/le/1.40/${ou}/classlist/`);

      const downloadSpreadsheet = async (title, data) => {
        let csv = 'data:text/csv;charset=utf-8,Student ID,Student Username,Student,Groups,Assignment,Unit\r\n';
        const rows = [];
        for (let i = 0, j = data.length; i < j; i++) {
          const sub = data[i];
          const groups = GetUsersGroupEnrollments(sub.Entity.EntityId).filter(g => !g.group.Name.includes('Default'));
          const { OrgDefinedId, Username, DisplayName } = classlist.find(cl => cl.Identifier == sub.Entity.EntityId);
          rows.push(
            `"${OrgDefinedId.toString().padStart(6, '0')}","${Username}","${DisplayName}","${groups
              .map(g => g.group.Name)
              .join(', ')}","${folder.Name}","${unit.OrgUnit.Name}"`
          );
        }
        csv += rows.join('\r\n');

        const safer = string => string.toLowerCase().replace(/\s/g, '-');

        const a = document.createElement('a');
        a.href = csv;
        a.download = `${safer(unit.OrgUnit.Name)}-${safer(folder.Name)}-${safer(title)}.csv`;
        a.click();
      };

      const generateTable = (data, title) => {
        const csvSave = document.createElement('a');
        csvSave.classList.add('d2l-link');
        csvSave.classList.add('csv-save');
        csvSave.innerText = '[save as spreadsheet]';
        // dialog.setAttribute('heading', dialog.getAttribute('heading + csvSave);

        csvSave.addEventListener('click', () => downloadSpreadsheet(title, data));

        const table = document.createElement('d2l-table-wrapper');
        table.innerHTML = `<table class="d2l-table"><thead><tr><th>${title}</th></tr></thead><tbody>${data
          .map(
            sub =>
              `<tr><td><a href="/d2l/lms/dropbox/admin/mark/folder_user_mark.d2l?ou=${ou}&db=${db}&etid=2&d2l_itemId=${
                sub.Entity.EntityId
              }" target="_blank" title="Evaluate Submission for ${
                sub.Entity.DisplayName ?? sub.Entity.Name
              }" class="d2l-link">Evaluate Submission: ${sub.Entity.DisplayName ?? sub.Entity.Name}</a></td></tr>`
          )
          .join('\n')}</tbody></table>`;

        return [csvSave, table];
      };

      if (sortedSubmissions.incomplete.length > 0) {
        dialog.append(...generateTable(sortedSubmissions.incomplete, 'Rubric Incomplete'));
      }

      if (sortedSubmissions.partial.length > 0) {
        dialog.append(...generateTable(sortedSubmissions.partial, 'Unpublished Submission'));
      }
    });
  })();

  // Handle the streamlined display
  (async function () {
    const button = document.createElement('button');
    button.classList.add('d2l-button');
    button.innerText = 'Export Submissions by Status';
    button.title = `Export submissions for this group, based on status.`;
    button.setAttribute('mm-icon-type', 'action-button');
    button.setAttribute('mm-icon-offset', '-7:-2');
    button.disabled = true;

    const wrapper = document.createElement('div');
    wrapper.classList.add('mm-incomplete-rubrics');
    wrapper.setAttribute('data-testid', 'incomplete-rubrics');
    wrapper.innerHTML = `
      <h2><span mm-icon-type="view-data" mm-icon-offset="-6"></span>Incomplete Rubrics</h2>
      <mm-loader center></mm-loader>
    `;
    container.append(wrapper);
    wrapper.querySelector('h2').append(moreSubmissionsInfo);

    const folder = await api(`/d2l/api/le/1.22/${ou}/dropbox/folders/${db}`, { fresh: true, first: true });

    if (folder.Assessment.Rubrics.length == 0) {
      wrapper.querySelector('mm-loader').remove();
      const notice = document.createElement('p');
      notice.innerHTML = 'None';
      wrapper.append(notice);
      moreSubmissionsInfo.remove();
      return;
    }

    const criteriaGroupCount = {};
    folder.Assessment.Rubrics.forEach(r => {
      criteriaGroupCount[r.RubricId] = r.CriteriaGroups.reduce((o, cg) => o + cg.Criteria.length, 0);
    });

    const groupsSelect = document.querySelector('select[title="Groups"]');
    let usersPool = [];
    let group = null;
    if (groupsSelect) {
      const td = document.createElement('td');
      td.classList.add('d_tl', 'd_tm', 'd_tn');
      td.append(button);
      document.querySelector('.d_tabs_tabcontent tr').append(td);

      const opt = UI.GetControlMap().GetControlByMappedId(groupsSelect.id).GetSelectedOption();
      const category = opt.Parent().GetKey().replace('GT', '');
      const groupId = opt.GetKey();
      group = await api(`/d2l/api/lp/1.26/${ou}/groupcategories/${category}/groups/${groupId}`, { first: true });
      usersPool = group.Enrollments;
    }

    const submissions = await api(`/d2l/api/le/1.22/${ou}/dropbox/folders/${db}/submissions/?activeOnly=true`, {
      fresh: true,
    }).then(res => (usersPool.length > 0 ? res.filter(sub => usersPool.includes(sub.Entity.EntityId)) : res));

    const total = submissions.length;
    for (let i = 0, j = total; i < j; i++) {
      const sub = submissions[i];
      // If there's no Feedback data, then it was never marked whatsoever (still in "Evaluate" state)
      if (sub.Feedback == null) {
        sortedSubmissions.incomplete.push(sub);
      }
      // If the results of the RubricAssessments are empty, then none of the possible rubrics where ever opened.
      // However, this means that the assessment is now no longer marked as "Evaluate", so we should flag it as a "partially completed" item.
      else if (sub.Feedback.RubricAssessments.length == 0) {
        sortedSubmissions.partial.push(sub);
      }
      // Filter out any Criteria that are marked as null (how unselected items currently appear), and then count against the total number of criteria for that rubric.
      // This will make sure that there are as many scored criteria items as there are criteria items in the original rubric.
      else if (
        sub.Feedback.RubricAssessments.some(
          ra =>
            ra.CriteriaOutcome.filter(co => co.Score != null).length != criteriaGroupCount[ra.RubricId] ||
            (ra.OverallLevel ?? ra.OverallScore) == null
        )
      ) {
        sortedSubmissions.incomplete.push(sub);
      } else {
        // If we got to this stage, it's complete. We're good to go ahead and up the counter.
        sortedSubmissions.complete.push(sub);
      }
    }
    submissionsLoading = false;

    wrapper.querySelector('mm-loader').remove();
    const div = document.createElement('div');
    if (sortedSubmissions.incomplete.length + sortedSubmissions.partial.length === 0) {
      div.innerHTML = '<p>None</p>';
      moreSubmissionsInfo.remove();
    } else {
      div.innerHTML = `
        ${
          sortedSubmissions.incomplete.length > 0 ? `<p>Incomplete: ${sortedSubmissions.incomplete.length}/${total}</p>` : ''
        }
        ${
          sortedSubmissions.partial.length > 0
            ? `<p>Partially complete: ${sortedSubmissions.partial.length}/${total}</p>`
            : ''
        }
      `;
    }
    // <p>Complete: ${sortedSubmissions.complete.length}/${total}</p>
    wrapper.append(div);
    moreSubmissionsInfo.removeAttribute('disabled');

    const SubmissionStatus = {
      0: 'Unsubmitted',
      1: 'Evaluate',
      2: 'Draft',
      3: 'Published',
    };

    if (groupsSelect) {
      // Handle downloading report
      const dialog = document.createElement('mm-dialog');
      dialog.setAttribute('heading', 'Export submissions based on status');
      dialog.setAttribute('preserve', true);

      const selector = document.createElement('select');
      selector.classList.add('vui-input', 'd2l-select');
      Object.entries(SubmissionStatus).forEach(([value, name]) => {
        let opt = document.createElement('option');
        const count = submissions.filter(submission => submission.Status == value).length;
        opt.innerText = `${name} (${count} submission${count == 1 ? '' : 's'})`;
        opt.value = value;
        opt.disabled = count == 0;
        selector.append(opt);
      });
      const blurb = document.createElement('p');
      blurb.innerText = `Please select the submission status you'd like to export below:`;
      dialog.append(blurb, selector);

      document.body.append(dialog);

      const folderInfo = await api(`/d2l/api/le/1.22/${ou}/dropbox/folders/${db}`, { first: true });
      const classlist = await api(`/d2l/api/le/1.40/${ou}/classlist/`);

      button.addEventListener('click', e => dialog.setAttribute('open', true));
      dialog.addEventListener('ok', e => {
        const matching = submissions.filter(submission => submission.Status == selector.value);
        const headers = ['Student ID', 'Student Name', 'Group', 'Submission Folder', 'Submission Status'];
        const content = [];

        matching.forEach(submission => {
          const { DisplayName, OrgDefinedId } = classlist.find(cl => cl.Identifier == submission.Entity.EntityId);
          const GroupName = group.Name;
          const SubmissionFolderName = folderInfo.Name;
          const SubmissionState = SubmissionStatus[submission.Status];
          content.push(
            [OrgDefinedId, DisplayName, GroupName, SubmissionFolderName, SubmissionState].map(v => `"${v}"`).join(',')
          );
        });

        const csv = `data:text/csv;charset=utf-8,${headers.map(v => `"${v}"`).join(',')}\r\n${content.join('\r\n')}`;

        let link = document.createElement('a');
        link.setAttribute('href', encodeURI(csv));
        link.setAttribute(
          'download',
          `${folderInfo.Name} - ${group.Name} - ${SubmissionStatus[selector.value]} submissions.csv`
        );
        link.click();
      });
      button.disabled = false;
    }
  })();

  document.querySelector('#d_content').style.maxWidth = 'unset';

  const cell = document.createElement('td');
  cell.append(container);

  document.querySelector('.d_gsearch tr').append(cell);
})();
