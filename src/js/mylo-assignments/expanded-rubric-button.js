(async () => {
  await until(() => sdqs.querySelectorDeep('d2l-icon.d2l-consistent-evaluation-open-rubrics'));

  const icon = sdqs.querySelectorDeep('d2l-icon.d2l-consistent-evaluation-open-rubrics');
  const rubric = sdqs.querySelectorDeep('d2l-consistent-evaluation-right-panel-rubric');

  const style = document.createElement('style');
  style.innerHTML = `
		.rubric-button {
			display: flex;
			gap: 0.3rem;
			line-height: 1.2rem !important;
			justify-content: center;
		}
  `;

  const button = document.createElement('d2l-button');
  button.title = 'Mark rubric in new window';
	button.style.marginLeft = 'auto';
	button.style.width = 'fit-content';
	button.style.display = 'block';

	button.innerHTML = `
		<div class="rubric-button">
			<d2l-icon style="inline-block" icon="tier1:new-window"></d2l-icon>
			<span>Grade Rubrics</span>
		</div>
	`

  button.onclick = () => rubric._openRubricPopout();
  icon.insertAdjacentElement('beforebegin', style);
  icon.replaceWith(button);
})();
