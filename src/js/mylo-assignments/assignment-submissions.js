/**
 * @affects Assignment Submission
 * @problem Submissions that are in various states of marking get messy and difficult to navigate.
 * @solution Adds a dropdown to the top of the submissions table, allowing staff to filter the on-screen results by status. This allows staff to, for example, only show submissions that have not yet been marked.
 * @target Academic and support staff
 * @impact High
 * @savings 2 minutes per folder
 */

(function () {
  if (window.AssignmentSubs) return;
  window.AssignmentSubs = true;

  if (Global.RoleId !== 103 && Global.RoleId !== 118) {
    const wrapper = document.createElement('div');
    wrapper.style.display = 'inline-block';
    wrapper.style.position = 'relative';
    wrapper.setAttribute('mm-icon-type', 'action-select');
    wrapper.setAttribute('mm-icon-offset', '12:0');

    const has_flags = !window.location.pathname.includes('_observed.d2l');

    const flaggedSelect = document.createElement('select');
    flaggedSelect.setAttribute('data-testid', 'submissions-filter');
    flaggedSelect.className = 'vui-input d2l-select';
    flaggedSelect.setAttribute('title', 'Please note this selection only affects the submissions on this page');
    flaggedSelect.style.marginLeft = '20px';
    flaggedSelect.innerHTML = `
		<option value="1" selected="selected">Show all</option>
		<option value="2">Evaluate</option>
		<option value="3">Draft Saved</option>
		<option value="4">Published</option>
		<option value="5">Feedback Read</option>
		${has_flags ? `<option value="6">Flagged</option> <option value="7">Not Flagged</option>` : ''}
		`;

    flaggedSelect.addEventListener('change', () => {
      /* window.localStorage.setItem('mm-assessment-filter', flaggedSelect.value); */

      const filterText = flaggedSelect.selectedIndex == 0 ? '' : flaggedSelect.options[flaggedSelect.selectedIndex].text;
      let thisVis = 'table-row';
      let rowChunk = [];

      document.querySelectorAll('d2l-table-wrapper table > tbody > tr:has(a[id^="ent"])').forEach(element => {
        // Get associated submission rows.
        const rows = [element];
        const nextRowIsSubmission = () => {
          const nextRow = rows.at(-1).nextElementSibling;
          if (!nextRow) return false;
          return !nextRow.classList.contains('d_ggl2');
        };

        while (nextRowIsSubmission()) {
          rows.push(rows.at(-1).nextElementSibling);
        }

        let show = filterText == '' || element.textContent.includes(filterText);
        if (filterText.includes('Flagged')) {
          const test = row => row.innerHTML.includes('Click to remove flag');

          if (filterText === 'Flagged') show = rows.some(row => test(row));
          else show = rows.every(row => !test(row));
        }

        for (const row of rows) {
          row.style.display = show ? 'table-row' : 'none';
        }
      });

      rowChunk.forEach(element => {
        element.style.display = thisVis;
      });

      const lastElement = document.querySelector('#z_h > tbody > tr.d2l-table-row-last');
      if (lastElement != null) lastElement.classList.remove('d2l-table-row-last');
      const allVisible = Array.prototype.slice
        .call(document.querySelectorAll('#z_h > tbody > tr'))
        .filter(function (item, index) {
          return item.style.display != 'none';
        });
      if (allVisible.length > 0) allVisible[allVisible.length - 1].classList.add('d2l-table-row-last');
    });

    wrapper.appendChild(flaggedSelect);
    const par = document
      .querySelector('d2l-table-wrapper tr[header]')
      ?.querySelector('th:has([data-d2l-table-sort-field="DateSubmitted"]), th.d2l-table-cell-last');
    if (!par) return;
    par.append(wrapper);
    /* par.parentNode.appendChild(wrapper); */

    /* if (window.localStorage.getItem('mm-assessment-filter') != null) {
      flaggedSelect
        .querySelector(`option[value="${window.localStorage.getItem('mm-assessment-filter')}"]`)
        .setAttribute('selected', true);
      flaggedSelect.dispatchEvent(new Event('change'));
    } */
  }

  document
    .querySelectorAll(
      'table[summary="List of users and the submissions they have uploaded to this submission folder."] tbody img[src*="/flag-"]'
    )
    .forEach(flag => {
      const UpdateState = () => flag.parentNode.setAttribute('mm-flagged', flag.src.includes('flag-filled.svg'));
      UpdateState();
      const mo = new MutationObserver(UpdateState);
      mo.observe(flag, { attributes: true });
    });
})();
