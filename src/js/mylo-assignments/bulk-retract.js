/**
 * @affects Assignment Folders
 * @problem There is the bulk "Publish Feedback" option available in the Assessments area, but no way to retract in the same fashion.
 * @solution Add a button that will automatically retract feedback from selected rows.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  const row = document.querySelector('.d2l-grid-container table d2l-overflow-group');
  row.parentNode.style.flexDirection = 'column';

  /** let node; */
  /** if (row.nextElementSibling?.dataset.purpose == 'additional-buttons') { */
  /** node = row.nextElementSibling; */
  /** } else { */
  /** node = row.cloneNode(); */
  /** node.dataset.purpose = 'additional-buttons'; */
  /** row.insertAdjacentElement('afterend', node); */
  /** } */

  const { db, ou } = window.location.searchParams;

  let container = document.createElement('div');
  container.classList.add('d2l-grid-action-wrapper', 'd2l-button-group-custom-item');

  let retract = document.createElement('d2l-button-subtle');
  retract.setAttribute('data-testid', 'bulk-retract');
  retract.setAttribute('text', 'Retract Feedback');
  retract.setAttribute('icon', 'tier1:grade-remove');
  retract.setAttribute('title', 'Retract Feedback from selected submissions');

  retract.setAttribute('mm-icon-type', 'action-navigation');
  retract.setAttribute('mm-icon-offset', '5:10');

  container.appendChild(retract);
  row.append(container);

  const RetractSubmissionPublication = async (OrgUnitId, FolderId, ItemId, IsGroup = false) => {
    const url = `/d2l/api/le/1.52/${OrgUnitId}/dropbox/folders/${FolderId}/feedback/${IsGroup ? 'group' : 'user'}/${ItemId}`;
    const block = await api(url, { fresh: true, first: true });

    delete block.Files;
    block.IsGraded = false;
    block.GradedSymbol = null;

    await apisend(url, block);
  };

  retract.toggleAttribute('disabled', true);
  const results = await api(`/d2l/api/le/1.46/${ou}/dropbox/folders/${db}/submissions/`, { fresh: true });
  retract.toggleAttribute('disabled', false);

  retract.addEventListener('click', async e => {
    const cbs = Array.from(document.querySelectorAll('input[name="gridSubmissions_cb"]:checked'));
    const published = cbs
      .map(cb => {
        const student = cb.value.split('_')[1];
        const result = results.find(row => row.Entity.EntityId == student);
        if (result?.Feedback?.IsGraded) return result;
        return null;
      })
      .filter(v => v);

    if (published.length == 0) return alert('Please select at least one published submission to continue.');

    if (
      !confirm(
        `This will retract the feedback from ${published.length} ${plural(
          published.length,
          'submitter',
          'submitters'
        )}. Do you wish to continue?`
      )
    )
      return;

    retract.toggleAttribute('disabled', true);

    await Promise.all(
      published.map(submission => {
        const id = submission.Entity.EntityId;
        const IsGroup = submission.Entity.EntityType == 'Group';
        return RetractSubmissionPublication(ou, db, id, IsGroup);
      })
    );

    window.location.reload();
    retract.toggleAttribute('disabled', false);
  });

  document.addEventListener('click', e => {
    if (e.target.matches('[chomped="chomped"]')) {
      e.target.firstChild.dispatchEvent(new e.constructor(e.type, e));
    }
  });
})();
