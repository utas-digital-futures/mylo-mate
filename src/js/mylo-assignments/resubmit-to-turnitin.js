/**
 * @affects Assignment Folders
 * @problem Sometimes Turnitin fails, and staff have to click a `Resubmit` button for each failed submission individually.
 * @solution Add a button that will automatically find and click all `Resubmit to Turnitin` buttons on the page.
 * @target Academic and support staff
 * @impact Moderate
 */

(async function () {
  const row = document.querySelector('.d2l-grid-container table d2l-overflow-group');

  /**
   * let node;
   * if (row.nextElementSibling?.dataset.purpose == 'additional-buttons') {
   *   node = row.nextElementSibling;
   * } else {
   *   node = row.cloneNode();
   *   node.dataset.purpose = 'additional-buttons';
   *   row.insertAdjacentElement('afterend', node);
   * }
   */

  let container = document.createElement('div');
  container.classList.add('d2l-grid-action-wrapper', 'd2l-button-group-custom-item');

  let resubmit = document.createElement('d2l-button-subtle');
  resubmit.setAttribute('data-testid', 'turnitin-resubmit');
  resubmit.setAttribute('text', 'Submit files to Turnitin');
  resubmit.setAttribute('icon', 'tier1:turnitin');
  resubmit.setAttribute(
    'title',
    'Submit to Turnitin any files that have not yet been submitted, or have encountered errors'
  );

  resubmit.setAttribute('mm-icon-type', 'action-navigation');
  resubmit.setAttribute('mm-icon-offset', '5:10');

  container.appendChild(resubmit);
  row.append(container);

  // If there's nothing to resubmit, disable the button.
  if (document.querySelectorAll('a[title*="ubmit file to Turnitin"],a[title*="refresh turnitin" i]').length == 0) {
    resubmit.setAttribute('disabled', true);
    resubmit.title += ` [none found]`;
  }

  resubmit.addEventListener('click', e => {
    document
      .querySelectorAll('a[title*="ubmit file to Turnitin"],a[title*="Refresh Turnitin® GradeMark®"]')
      .forEach(n => n.click());
  });
})();
