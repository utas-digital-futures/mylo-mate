(async function () {
  // If this isn't the UTAS website, get out of here.
  // This will stop it running on other sites that we're servicing, but don't have access to the Valence API.
  if (!window.location.href.includes('utas.edu.au')) return;

  // Sometimes there will be a redirect that confuses things.
  // Lets make sure that we have access to the body object before moving on.
  if (!document.body) return;

  // This is just a helper function that adds an attribute to the body tag.
  const shouldAbort = abort => document.body.setAttribute('mm-abort', abort);

  // This is just a helper function that adds an attribute to the body tag.
  const setRole = role => document.body.setAttribute('mm-user-role', role);

  // If we can't grab the OrgUnitId (for example, might not be in a unit), then bail as well.
  if (!document.querySelector('a[href*="/d2l/home/"]')) return;

  // Get the Org Unit ID from the "Unit Home" link.
  const id = document.querySelector('a[href*="/d2l/home/"]').href.split('/').pop();

  // If we can't find an ID, we did through an error. This can happen sometimes where dialogs will appear that don't have a reference
  // to the OrgUnit in the URL. At the moment, it just assumes we're a student, and doesn't do anything then.
  if (id == null) {
    shouldAbort(true);
    return;
  }

  // Get our current role in this unit via the MyEnrollments API
  const enrollment = await api(`/d2l/api/lp/1.31/enrollments/myenrollments/${id}`, { fresh: true, first: true });
  if (!enrollment || !enrollment.Access) {
    shouldAbort(true);
  } else {
    const role = (enrollment.Access.ClasslistRoleName || 'student').toLowerCase();
    shouldAbort(role.includes('student') || role.includes('auditor') || false);
    setRole(role);
  }
})();
