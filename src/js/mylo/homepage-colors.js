/**
 * @affects MyLO Homepage
 * @problem It is not clear at a glance on the MyLO Homepage whether units are currently running or belong to a past delivery.
 * @solution Add colours and status text to each unit title, based on the dates the unit opens and closes (Future | Current | Exam | Closed).
 * @target Academic and Support staff
 * @impact Low
 */

(async function () {
  if (window.DateColoursAdded) return;
  window.DateColoursAdded = true;
  // Summary
  //	This script adds coloured dots next to unit titles in the My Units widget, indicating their current status;
  //	 - Green (or purple) for open
  //	 - Yellow for exam period (21 days after unit close date)
  //	 - Red for closed (more than 21 days after unit close date)
  //	 - White for open date that's in the future

  const now = new Date();
  const exam_days = 21;

  // Get a list of units that we're enrolled in.
  const units = await api('/d2l/api/lp/1.24/enrollments/myenrollments/?isActive=true&orgUnitTypeId=3').then(units => {
    let data = {};
    units.forEach(unit => (data[unit.OrgUnit.Id] = unit));
    return data;
  });

  const StatusType = {
    FUTURE: 0,
    ACTIVE: 1,
    EXAM: 2,
    CLOSED: 3,
  };

  const Colors = {
    [StatusType.FUTURE]: '#ccc',
    [StatusType.ACTIVE]: 'var(--d2l-alert-success-color)',
    [StatusType.EXAM]: 'var(--d2l-color-carnelian)',
    [StatusType.CLOSED]: 'red',
  };

  const Status = {
    [StatusType.FUTURE]: 'not yet open',
    [StatusType.ACTIVE]: 'currently open',
    [StatusType.EXAM]: 'in exam period',
    [StatusType.CLOSED]: 'closed',
  };

  document.addEventListener('course-image-loaded', e => {
    const card = e.composedPath().find(el => el?.matches?.('d2l-card'));
    if (card.hasAttribute('mm-touched')) return;

    card.setAttribute('mm-touched', '');
    if (!card.getAttribute('href')) return;
    const id = card.getAttribute('href').split('/').pop();
    const unit = units[id];
    if (!unit) return;

    const StartDate = new Date(unit.Access.StartDate);
    const ExamDate = new Date(unit.Access.EndDate);
    const EndDate = new Date(ExamDate.getTime() + 1000 * 60 * 60 * 24 * exam_days);

    const status =
      now >= StartDate && now <= ExamDate
        ? StatusType.ACTIVE
        : now >= ExamDate && now <= EndDate
        ? StatusType.EXAM
        : now >= EndDate
        ? StatusType.CLOSED
        : now <= StartDate
        ? StatusType.FUTURE
        : StatusType.FUTURE;

    const header = card.querySelector('[slot="header"]');
    header.style.borderBottom = `5px solid ${Colors[status]}`;
    card.title = `This unit is ${Status[status]}.`;
  });
})();
