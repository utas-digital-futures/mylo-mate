/**
 * @affects Attendance Report
 * @problem On a table with multiple columns, the first two columns (containing the student details) slide off-screen when scrolling horizontally.
 * @solution Make the first two columns sticky so they stay visible when scrolling to the right.
 * @target Academic and support staff
 * @impact Low
 */

(async function () {
  const table = document.querySelector('d2l-table-wrapper');
  table.setAttribute('sticky-columns', 2);
})();
