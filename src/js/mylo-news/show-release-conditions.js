(async function () {
  const type = 'news';
  const titleBreak = '\u000A';
  const ou = window.location.searchParams.ou;

  document.querySelectorAll('[title="Conditional Release"]').forEach(async el => {
    const id = new URL(el.closest('th').querySelector('[href*="newedit.d2l" i]').href).searchParams.get('newsId');
    const restrictions = await getReleaseConditions(ou, type, id);
    if (!restrictions) return;

    const text = [el.title].concat(restrictions.map(v => ` - ${v}`));
    el.title = text.join(titleBreak);
  });
})();
