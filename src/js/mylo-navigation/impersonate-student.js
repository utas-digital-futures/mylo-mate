/**
 * @affects MyLO
 * @problem To preview what a student sees, staff need to go to the Classlist tool and impersonate a View Student from the list of all students in the unit. Tedious and time-consuming.
 * @solution
 *  Add a button to the navigation bar which allows staff to immediately impersonate View Student from anywhere in a unit.
 *  If it detects that a student would have access to that page as well (ie. isn't a privileged page), then it will refresh it with the new role in place.
 *
 *  It also allows impersonating staff to return back to their normal view, and attempts to return them to the page they were just viewing, to reduce retraced steps.
 * @target Academic and support staff
 * @impact High
 * @savings 2 minutes per use
 */

(async function () {
  // If we're in the OER domain, get out of here.
  if (window.location.host.includes('oer.utas.edu.au')) return;
  // If we can't grab the OrgUnitId (for example, might not be in a unit), then bail as well.
  if (!document.querySelector('a[href*="/d2l/home/"]')) return;

  const store = await FetchMMSettings();

  // Get the Org Unit ID from the "Unit Home" link.
  const id = document.querySelector('a[href*="/d2l/home/"]').href.split('/').pop();

  let IsStudentView = false;
  let IsStaff = false;

  const GetBestRedirect = () => {
    let loc = new URL(window.top.location.href);

    if (loc.pathname.includes('grades/admin')) {
      loc.pathname = `/d2l/lms/grades/my_grades/main.d2l`;
    } else if (loc.pathname.includes('grades/my_grades')) {
      loc.pathname = `/d2l/lms/grades/admin/${store.mylo.overrideGrades ? 'manage/gradeslist' : 'enter/user_list_view'}.d2l`;
    } else if (loc.pathname.includes('dropbox/admin')) {
      loc.pathname = `/d2l/lms/dropbox/user/folders_list.d2l`;
    } else if (loc.pathname.includes('dropbox/user')) {
      loc.pathname = `/d2l/lms/dropbox/admin/folders_manage.d2l`;
    } else if (loc.pathname.includes('quizzing/user/')) {
      loc.pathname = `/d2l/lms/quizzing/admin/quizzes_manage.d2l`;
    } else if (loc.pathname.includes('quizzing/admin')) {
      loc.pathname = `/d2l/lms/quizzing/user/quizzes_list.d2l`;
    } else if (loc.pathname.includes('/awards/')) {
      if (loc.hash == '#/roster') loc.hash = '#/myAwards';
    }
    return loc;
  };

  // The title and description of the impersonate confirm dialog.
  const title = 'Impersonate View Student';
  const description = `
  <p style="margin-top: 0; padding-top: 0;">Would you like to suspend your current session and impersonate a sample student in your unit?</p>
  <p>If you are accessing a privileged location, you will be taken back to the unit homepage.</p>
  <p style="margin-bottom: 0; padding-bottom: 0;">Do you wish to continue?</p>
  `;

  // Find out who we're currently looking at MyLO as.
  //  If our classlist role name is "Student View", then we know exactly who we are.
  //  However, if our list of rolenames includes "Instructor", then it will include support staff as well as tutors(?)
  const CurrentUser = await api(`/d2l/api/lp/1.25/enrollments/myenrollments/${id}`, { fresh: true, first: true });
  if (CurrentUser.Access.ClasslistRoleName == 'Student View') {
    IsStudentView = true;
  } else if (CurrentUser.Access.LISRoles.includes('urn:lti:instrole:ims/lis/Instructor')) {
    IsStaff = true;
  }

  // Create the button that will start the impersonate process.
  const btn = document.createElement('d2l-icon');
  btn.setAttribute('data-testid', 'impersonate-student-icon');
  btn.setAttribute('icon', 'd2l-tier1:profile-pic');
  btn.style.cursor = 'pointer';
  btn.title = title;

  // If we're currently viewing with normal privileges, give us the "Impersonate Student" button.
  if (IsStaff) {
    // Prefetch the users profile picture, and cache for 24 hours
    await Promise.all([
      api(`/d2l/api/lp/1.25/profile/myProfile/image`, { fresh: true, type: 'blob', expiry: 60 * 1000 * 60 * 24 }),
      api(`/d2l/api/lp/1.25/users/whoami`, { fresh: true, expiry: 60 * 1000 * 60 * 24 }),
    ]);
    const StudentView = await api(`/d2l/api/lp/1.25/enrollments/orgUnits/${id}/users/?roleId=118`, { first: true });

    const dialog = document.createElement('mm-dialog');
    document.body.append(dialog);
    dialog.addEventListener('open', () => {
      // Remove the body scrollbar and disable our Impersonate button.
      document.body.style.overflow = 'hidden';
      btn.setAttribute('disabled', true);
    });

    dialog.addEventListener('close', () => {
      // Show the body scrollbar and enable our Impersonate button.
      document.body.style.overflow = '';
      btn.setAttribute('disabled', false);
    });

    // Configure the dialog to last and to look good.
    dialog.setAttribute('preserve', true);
    dialog.setAttribute('width', '600px');
    dialog.setAttribute('height', '350px');
    dialog.setAttribute('heading', title);
    dialog.innerHTML = description;

    const status = async url => {
      try {
        const res = await fetch(url, { method: 'head' });
        return res.status;
      } catch (e) {
        console.warn(e);
        return 403;
      }
    };

    // When the dialog 'OK' button is clicked, start the Impersonate process.
    dialog.addEventListener('ok', async () => {
      // The RPC functions file that handles student impersonation.
      const loc = `/d2l/lms/classlist/rpc/rpc_functions.d2lfile?ou=${id}&d2l_rh=rpc&d2l_rt=call`;

      // Send the "Impersonate" request, and wait for it to finish before continuing on.
      const r = await CustomRPC(
        loc,
        'ImpersonateUser',
        `{'param1':'${StudentView.User.Identifier}','param2':${StudentView.Role.Id}}`
      );

      const BestRedirectLocation = GetBestRedirect().href;
      const code = await status(BestRedirectLocation);

      // If we don't have permission to view the page we were on, go back to the Unit Home page.
      // Otherwise, refresh our current page to use our new permissions.
      if (code === 403) {
        window.top.location = `/d2l/lp/homepage/home.d2l?ou=${id}`;
      } else {
        window.top.location = BestRedirectLocation;
        /** setTimeout(() => window.top.location.reload(), 500); */
      }
    });

    // When the Impersonate icon is clicked, show the confirmation prompt.
    btn.addEventListener('click', () => dialog.setAttribute('open', true));

    // Wait until the '#unit-links' bar is available, and then add the icon in.
    await until(() => document.querySelector('#unit-links') !== null);
    const unitLinks = document.querySelector('#unit-links');
    unitLinks.appendChild(btn);
  } else if (IsStudentView) {
    // Handle the logout process.
    const logout = async e => {
      // Just run the 'logout' bit.

      // Sometimes, this bit has a different name (such as in the classlist).
      // Rather than trying to store what those names are (and risk them changing),
      //  just find the one we actually want and grab it out.
      // const TheLogoutChunk = Object.values(D2L.OR)
      //   .map(items => Object.values(items).find(val => val.includes('/impersonation/Restore')))
      //   .find(v => v);
      // D2L.LP.Web.Serialization.Json.Deserialize(JSON.stringify(JSON.parse(TheLogoutChunk).P[0]))();

      const body = new FormData();
      body.append('requestId', Date.now() % 10);
      body.append('d2l_referrer', window.localStorage.getItem('XSRF.Token'));
      body.append('isXhr', true);
      await fetch('/d2l/lp/impersonation/Restore', { method: 'post', body });

      const BestRedirectLocation = GetBestRedirect().href;
      window.top.location = BestRedirectLocation;
      /** window.location.reload(); */
      // After 200ms, reload the page.
      // setTimeout(() => window.location.reload(), 200);
    };

    // Add the "Logout" button to the navbar.
    // We have to manually find the location, as we won't have a #unit-links element.
    // As such, we chose to put it next to the classlist, to match how the Impersonate button is located.
    btn.title = 'Exit Student View';
    btn.addEventListener('click', logout);
    /* const base = document.evaluate('//a[contains(text(), "Classlist")]', document.body).iterateNext(); */

    const base = [...document.querySelectorAll('.d2l-navigation-s-item:not(.d2l-navigation-s-more)')].at(-1);

    const item = document.createElement('div');
    item.classList.add('d2l-navigation-s-item', 'less-rpad');
    item.setAttribute('role', 'listitem');
    base.insertAdjacentElement('beforebegin', item);
    item.append(btn);

    // Get our cached user profile image and details.
    // If they aren't cached for some reason, we'll just default to the "impersonate" icon.
    const [picture, user] = await Promise.all([
      api(`/d2l/api/lp/1.25/profile/myProfile/image`, { cacheOnly: true, first: true, type: 'blob' }),
      api(`/d2l/api/lp/1.25/users/whoami`, { cacheOnly: true, first: true }),
    ]);

    // If we did have a picture cached, then generate it into a blob URL, and replace the icon with our image.
    // Also, if we have the users 'whoami' result stored, then show their name instead of just "View as yourself".
    if (picture != null) {
      const wrapper = document.createElement('span');
      wrapper.setAttribute('mm-icon-type', 'action-button');
      wrapper.setAttribute('mm-icon-offset', '0:0');

      const urlForPic = URL.createObjectURL(picture);
      const img = document.createElement('img');
      img.src = urlForPic;
      img.className = 'exit-impersonate';
      img.title = `View as ${user == null ? 'yourself' : `${user.FirstName} ${user.LastName}`}`;
      img.addEventListener('click', logout);
      wrapper.append(img);
      btn.replaceWith(wrapper);
    } else if (user != null) {
      btn.title = `View as ${user.FirstName} ${user.LastName}`;
    }
  }
})();
