/**
 * @affects MyLO
 * @problem We often need to email active lecturers in a unit, and getting accurate lecturer information is difficult.
 * @solution Retrieves any lecturers who have accessed the unit, and adds their information to an `Email Lecturers` button in the navigation bar.
 * @target Support staff
 * @impact Moderate
 */

(async function () {
  const store = await FetchMMSettings();
  if (!store.mylo.emailLecturers) return;

  const joiner = (array, middle = ', ', final = ' and ') => {
    if (array.length === 1) {
      return array[0];
    }
    const last = array.pop();
    return `${array.join(middle)}${final}${last}`;
  };
  const wait = rule =>
    new Promise(res => {
      const check = r => {
        if (r()) {
          res();
        } else {
          window.requestAnimationFrame(() => check(r));
        }
      };
      check(rule);
    });
  await wait(() => document.querySelector('#unit-links') !== null);

  const id = document.querySelector('a[href*="/d2l/home/"]').href.split('/').pop();
  const [lecturers, unit, users] = await Promise.all([
    api(`/d2l/api/le/1.39/${id}/classlist/`).then(data =>
      data.filter(({ RoleId, LastAccessed }) => RoleId === 106 && LastAccessed != null)
    ),
    api(`/d2l/api/lp/1.24/enrollments/myenrollments/${id}`, { first: true }),
    api(`/d2l/api/lp/1.24/enrollments/orgUnits/${id}/users/?roleId=106`),
  ]);

  const activeLecturers = lecturers.map(lecturer => users.find(({ User }) => User.Identifier == lecturer.Identifier));

  const unitLinks = document.querySelector('#unit-links');

  const link = document.createElement('a');
  const btn = document.createElement('d2l-icon');
  btn.setAttribute('data-testid', 'email-lecturers');
  btn.setAttribute('icon', 'd2l-tier1:email');
  btn.style.margin = '0 10px';
  btn.style.cursor = 'pointer';

  if (activeLecturers.length > 0) {
    link.href = `mailto:${activeLecturers.map(({ User }) => User.EmailAddress).join(',')}?subject=${encodeURI(
      unit.OrgUnit.Name
    )}`;
    btn.title = `Email Lecturer${activeLecturers.length > 1 ? 's' : ''} (${joiner(
      activeLecturers.map(({ User }) => User.DisplayName)
    )})`;
  } else {
    btn.title = `No active lecturers found in this unit. Please visit the Classlist and manually assess.`;
    link.href = `/d2l/lms/classlist/classlist.d2l?ou=${id}`;
  }

  link.append(btn);
  unitLinks.append(link);
})();
