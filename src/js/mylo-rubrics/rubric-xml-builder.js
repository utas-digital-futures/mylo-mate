/**
 * @affects Rubrics
 * @problem The Rubric Generator cannot directly retrieve MyLO rubrics, so exporting from MyLO is normally required.
 * @solution Add a button next to each rubric that automatically transfers it to the Rubric Generator in a new tab, drastically simplifying the process.
 * @target Academic and support staff
 * @impact High
 * @savings 5 minutes per rubric
 */

(async function () {
  await until(() => document.body.getAttribute('mm-abort') != null);
  if (document.body.getAttribute('mm-abort') == 'true') return;

  // await until(() => document.body.getAttribute('mm-unit-status') != null);
  // if (document.body.getAttribute('mm-unit-status') !== "2") return;

  const seconds = 5000;

  const RUBRIC_TYPE = {
    POINTS: 2,
    TEXT_ONLY: 0,
    CUSTOM_POINTS: 3,
  };

  const RubricBase = window.location.origin.includes('d2ldev2')
    ? '0e78be5b-5af6-4f4b-827b-568374e17a1d'
    : 'c0ef8d21-d21d-4bf1-b68f-054dc1a5e6fc';

  const GetOAuth2Token = async () => {
    const existing = window.localStorage['D2L.Fetch.Tokens'];
    if (existing != null && existing['*:*:*'] != null) {
      const body = JSON.parse(existing['*:*:*']);
      if (body.expires_at < Date.now() / 1000) {
        delete window.localStorage['D2L.Fetch.Tokens'];
      } else return body.access_token;
    }

    if (!window.localStorage['XSRF.Token']) return null;

    const token = await fetch('/d2l/lp/auth/oauth2/token', {
      method: 'post',
      body: 'scope=*:*:*',
      credentials: 'include',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Csrf-Token': window.localStorage['XSRF.Token'],
      }),
    }).then(data => data.json());

    window.localStorage.setItem('D2L.Fetch.Tokens', JSON.stringify({ '*:*:*': token }));
    return token.access_token;
  };

  const getRubricData = async (rubricId, orgid, a, mangleData) => {
    try {
      let token = await GetOAuth2Token();
      if (!token)
        return alert('There was an error getting the necessary authentication. Please refresh the page and try again.');

      // console.log(token);
      // return;

      const opts = {
        token,
        fresh: true,
        first: true,
      };

      const rubricItem = `https://${RubricBase}.rubrics.api.brightspace.com/organizations/${orgid}/${rubricId}`;
      const [details, groups, overallLevels] = await Promise.all([
        api(rubricItem, opts),
        api(`${rubricItem}/groups`, opts),
        api(`${rubricItem}/overallLevels`, opts),
      ]);

      const criteriaGroups = await Promise.all(
        groups.entities.map(async group => {
          const levelsURL = group.links.find(link => link.rel[0].endsWith('/levels')).href;
          const criteriaURL = group.links.find(link => link.rel[0].endsWith('/criteria')).href;
          const [levels, criteria] = await Promise.all([
            (await api(levelsURL, opts)).entities.map(e => e.properties),
            (await api(criteriaURL, opts)).entities.map(e => ({
              name: e.properties.name,
              outOf: e.properties.outOf,
              criterions: e.entities.map(en => {
                let crit = en.entities.find(j => j.class.includes('description')).properties;
                return {
                  level: en.properties.levelName,
                  points: en.properties.points || null,
                  html: crit.html,
                  text: crit.text,
                };
              }),
            })),
          ]);
          return { levels, criteria };
        })
      );

      // const GetPointsType = () => a.parentNode.parentNode.nextSibling.nextSibling.nextSibling.innerText.includes('Custom') ? RUBRIC_TYPE.CUSTOM_POINTS : RUBRIC_TYPE.POINTS;

      const rubric = {
        name: details.properties.name,
        description: details.entities.find(e => e.class.includes('description')).properties.html,
        overall: overallLevels.entities.map(lvl => lvl.properties),
        type: details.class.includes('numeric') ? RUBRIC_TYPE.CUSTOM_POINTS : RUBRIC_TYPE.TEXT_ONLY,
        status: details.class.includes('draft') ? 2 : 0,
      };

      if (details.properties.outOf) {
        rubric.outOf = details.properties.outOf;
      }

      const models = [
        'High Distinction',
        'Distinction',
        'Credit',
        'Pass',
        'Fail',
        'HD',
        'DN',
        'CR',
        'PP',
        'NN',
        'Description',
      ].map(c => c.toLowerCase());

      const IsCorrectLength = array => array.length == 5 || array.length == 6;

      const MatchTarget = { START: 0, END: 1, ANYWHERE: 2, EXACT: 3 };
      const matchAgainst = (needle, haystack, target = MatchTarget.ANYWHERE) => {
        needle = needle.map(n => n.toLowerCase());
        switch (target) {
          case MatchTarget.EXACT:
            return IsCorrectLength(haystack.filter(h => needle == h));

          case MatchTarget.START:
            return IsCorrectLength(needle.filter(n => haystack.find(m => n.startsWith(m)) != null));

          case MatchTarget.END:
            return IsCorrectLength(needle.filter(n => haystack.find(m => n.endsWith(m)) != null));

          case MatchTarget.ANYWHERE:
            return IsCorrectLength(needle.filter(n => haystack.find(m => n.includes(m)) != null));

          default:
            return null;
        }
      };

      if (!mangleData) {
        rubric.criteria = criteriaGroups;
      } else {
        const TestStructure = criteriaGroup =>
          matchAgainst(
            criteriaGroup.levels.map(v => v.name),
            models,
            MatchTarget.ANYWHERE
          );
        const MergeCriteriaGroups = groups => {
          const res = { levels: [], criteria: [] };
          groups.forEach(group => {
            group.levels.forEach(l => {
              if (!res.levels.find(rl => rl.name == l.name)) {
                res.levels.push(l);
              }
            });
            res.criteria = res.criteria.concat(group.criteria);
          });

          return res;
        };

        const RecognisedGroups = criteriaGroups.filter(TestStructure);
        if (RecognisedGroups.length == 0) {
          alert('No rubrics found that match the UTAS structure.');
          return null;
        }
        if (RecognisedGroups.length != criteriaGroups.length) {
          console.warn(
            '%s of %s rubric criteria groups did not meet the required HD-NN structure. Please review, and try again later.',
            criteriaGroups.length - RecognisedGroups.length,
            criteriaGroups.length
          );
        }
        rubric.criteria = [MergeCriteriaGroups(RecognisedGroups)];
      }

      return rubric;
    } catch ([url, xhr, e]) {
      console.error('[ERROR] There was an error trying to get the information.', url, xhr, e);
      alert('There was an error getting the information for this rubric.');
      if (a.classList.contains('loading')) a.classList.remove('loading');
      throw e;
    }
  };

  const getRubricXML = (rubricData, returnAsString) => {
    try {
      const replace = ({ src = '', matches = [], flags = 'g' } = {}) =>
        matches.reduce((prev, curr) => prev.replace(new RegExp(curr.from, flags), curr.to), src);

      const doc = document.implementation.createDocument('', '', null);

      const rubrics = doc.createElement('rubrics');
      rubrics.setAttribute('schemaversion', 'v2011');
      doc.appendChild(rubrics);

      const rubric = doc.createElement('rubric');
      rubric.setAttribute('id', '1');
      rubric.setAttribute('name', rubricData.name);
      rubric.setAttribute('type', '1');
      rubric.setAttribute('scoring_method', rubricData.type);
      rubric.setAttribute('display_levels_in_des_order', 'True');
      rubric.setAttribute('state', rubricData.status);
      rubric.setAttribute('usage_restrictions', 'Competency');
      rubrics.appendChild(rubric);

      const description = doc.createElement('description');
      description.setAttribute('text_type', 'text/html');
      description.innerHTML = `<html><![CDATA[ ${rubricData.description} ]]></html>`;
      rubric.appendChild(description);

      const criteriaGroups = doc.createElement('criteria_groups');
      const criteriaGroup = doc.createElement('criteria_group');
      criteriaGroup.setAttribute('name', 'Criteria');
      criteriaGroup.setAttribute('sort_order', '1');
      criteriaGroups.appendChild(criteriaGroup);
      rubric.appendChild(criteriaGroups);

      const levelSet = doc.createElement('level_set');
      const levels = doc.createElement('levels');
      levelSet.appendChild(levels);
      criteriaGroup.appendChild(levelSet);

      rubricData.criteria[0].levels.forEach(({ name, points = null }, i) => {
        const level = doc.createElement('level');
        level.setAttribute('name', name);
        level.setAttribute('sort_order', i + 1);
        level.setAttribute('level_id', i);
        if (points != null && points != '0') level.setAttribute('level_value', points);
        levels.appendChild(level);
      });

      const criteria = doc.createElement('criteria');
      criteriaGroup.appendChild(criteria);

      const matches = [
        { from: '&', to: '&#038;' },
        { from: "'", to: '&apos;' },
        { from: '"', to: '&quot;' },
      ];

      let i = 0;
      rubricData.criteria[0].criteria.forEach(({ name, outOf, criterions }) => {
        const criterion = doc.createElement('criterion');
        criterion.setAttribute('name', replace({ src: name, matches }));
        criterion.setAttribute('sort_order', ++i);
        criterions.forEach((data, i) => {
          const cell = doc.createElement('cell');
          cell.setAttribute('level_id', i);
          if (data.points) cell.setAttribute('cell_value', data.points);
          cell.innerHTML = `<description text_type="text/html"><html><![CDATA[${data.html}]]></html></description><feedback text_type="text/html"><text /></feedback>`;
          criterion.appendChild(cell);
        });
        criteria.appendChild(criterion);
      });

      const overallSet = doc.createElement('overall_level_set');
      const overallLevels = doc.createElement('overall_levels');
      rubricData.overall.forEach(({ name, rangeStart = null }, i) => {
        const level = doc.createElement('overall_level');
        level.setAttribute('name', name);
        level.setAttribute('sort_order', i);
        if (rangeStart) level.setAttribute('range_start_value', rangeStart);
        level.innerHTML =
          '<description text_type="text"><text /></description> <feedback text_type="text"><text /></feedback>';
        overallLevels.appendChild(level);
      });
      overallSet.appendChild(overallLevels);
      rubric.appendChild(overallSet);
      return returnAsString ? new XMLSerializer().serializeToString(doc) : doc;
    } catch ([url, xhr, e]) {
      console.error('[ERROR] There was an error trying to get the information.', url, xhr, e);
      // alert('There was an error getting the information for this rubric.');
      // if (a.classList.contains('loading')) a.classList.remove('loading');
      // throw e;
      return null;
    }
  };

  const zipXML = xml => {
    const zip = new JSZip();
    zip.file('imsmanifest.xml', '');
    zip.file('rubrics_d2l.xml', xml);

    return zip.generateAsync({
      type: 'array', // binarystring
      compression: 'DEFLATE',
      compressionOptions: {
        level: 9,
      },
    });
  };

  const store_data = (id, data) => new Promise(resolve => chrome.storage.local.set({ [id]: data }, resolve));

  const processEvent = async function (e) {
    e.preventDefault();
    this.setAttribute('disabled', true);
    const loader = document.createElement('mm-loader');
    loader.setAttribute('max', (seconds / 1000) * 4);
    loader.setAttribute('value', 0);
    loader.setAttribute('width', 20);
    this.appendChild(loader);
    const id = this.getAttribute('mm-rubric-id');
    const orgid = document.querySelector('a[href^="/d2l/home"]').href.split('/').pop();
    try {
      const data = await getRubricData(id, orgid, this, true);
      if (data != null) {
        const xml = getRubricXML(data, true);
        const zip = await zipXML(xml);
        await store_data(id, zip);
        window.open(this.href, '_blank');
      }
    } catch (e) {
      alert('Something went wrong. Check the console for more details.');
      console.error(e);
    }
    loader.remove();
    this.removeAttribute('disabled');
  };

  const style = document.createElement('style');
  style.innerHTML = `
	a[mm-rubric-id].loading {
		position: relative;
		pointer-events: none;
	}
	a[mm-rubric-id].loading::after {
		display: inline-block;
		position: absolute;
		content: '';
		width: 24px;
		height: 24px;
		border-radius: 50 % ;
		border: 0.25 rem solid rgba(50, 50, 50, 0.2);
		border-top-color: rgba(50, 50, 50, 0.8); 
		-webkit-animation: spin 1 s infinite linear;
		animation: spin 1 s infinite linear;
		margin: 15px auto 25px;
		top: -19px;
		left: 2px;
		background-color: white;
		box-sizing: border-box;
	}

	a[disabled] {
		pointer-events: none;
	}

	a[disabled] d2l-icon {
		pointer-events: none;
		opacity: 0.6;
	}

	a[mm-rubric-id] mm-loader {
		display: inline-block;
		margin: -5px 10px;
	}

	.mm-rubric-export d2l-icon,
	.mm-rubric-download-to-word d2l-icon {
		width: 14px;
		color: #555;
	}

	.mm-rubric-export d2l-icon:hover, 
	.mm-rubric-download-to-word d2l-icon:hover {
		color: #80251a;
	}
`;
  document.head.appendChild(style);

  const rowColors = {
    header: '#ddd',
    odd: 'white',
    even: '#f0f0f0',
  };

  const tableStyles = {
    border: 'thin solid grey',
    borderCollapse: 'collapse',
    fontFamily: 'Calibri',
    fontSize: '11pt',
  };

  const cellStyles = {
    padding: '5px',
    border: 'thin solid grey',
    verticalAlign: 'top',
  };

  async function handleSaveToWord(e) {
    e.preventDefault();
    this.setAttribute('disabled', true);
    const loader = document.createElement('mm-loader');
    loader.setAttribute('max', (seconds / 1000) * 4);
    loader.setAttribute('value', 0);
    loader.setAttribute('width', 20);
    this.appendChild(loader);
    const id = this.getAttribute('mm-rubric-id');
    const orgid = document.querySelector('a[href^="/d2l/home"]').href.split('/').pop();
    try {
      const data = await getRubricData(id, orgid, this, false);
      if (data != null) {
        saveToWordDocument(data);
      }
    } catch (e) {
      alert('Something went wrong. Check the console for more details.');
      console.error(e);
    }
    loader.remove();
    this.removeAttribute('disabled');
  }

  function saveToWordDocument(rubric) {
    const output = document.createElement('div');

    // output.style.position = 'absolute';
    // output.style.left = '-100000000000px';
    // document.body.appendChild(output);

    const { name } = rubric;
    const details = document.createElement('div');
    details.innerHTML = `<h2>${name}</h2><small>${rubric.description}</small>`;
    output.appendChild(details);

    const criteriaGroups = rubric.criteria;
    criteriaGroups.forEach(({ levels, criteria }) => {
      const table = document.createElement('table');
      const headerRow = document.createElement('tr');
      const criteriaHeader = document.createElement('td');
      criteriaHeader.innerHTML = '<strong>Criteria</strong>';
      headerRow.appendChild(criteriaHeader);
      levels.forEach(({ name }) => {
        const cell = document.createElement('td');
        cell.innerHTML = `<strong>${name}</strong>`;
        headerRow.appendChild(cell);
      });

      headerRow.querySelectorAll('td').forEach(td => {
        Object.keys(cellStyles).forEach(key => (td.style[key] = cellStyles[key]));
      });

      headerRow.style.backgroundColor = rowColors.header;
      table.appendChild(headerRow);

      criteria.forEach(({ name, outOf, criterions }, index) => {
        const row = document.createElement('tr');

        const critTitle = document.createElement('td');
        critTitle.innerText = name;
        row.appendChild(critTitle);

        const points = outOf;
        if (points != null) {
          critTitle.innerHTML += `<p>&nbsp;</p><p>${points} point${points == '1' ? '' : 's'}</p>`;
        }

        criterions.forEach(({ level, points, html }) => {
          const td = document.createElement('td');
          td.innerHTML = html;

          td.querySelectorAll('div').forEach(desc => desc.replaceWith(...desc.childNodes));
          row.appendChild(td);
        });

        row.querySelectorAll('td').forEach(td => {
          Object.keys(cellStyles).forEach(key => (td.style[key] = cellStyles[key]));
        });

        // Do an odd/even color pattern
        row.style.backgroundColor = index % 2 == 0 ? rowColors.odd : rowColors.even;

        table.appendChild(row);
      });

      Object.keys(tableStyles).forEach(key => (table.style[key] = tableStyles[key]));

      output.appendChild(table);

      const spacer = document.createElement('p');
      spacer.innerHTML = '&nbsp;';
      output.appendChild(spacer);
    });

    output
      .querySelectorAll('td')
      .forEach(td => td.setAttribute('style', `${td.getAttribute('style')}mso-border-alt:solid windowtext .5pt;`));

    const UTF8ized = document.createElement('html');
    const header = document.createElement('head');
    const charset = document.createElement('meta');
    charset.setAttribute('http-equiv', 'Content-Type');
    charset.setAttribute('content', 'text/html; charset=UTF-8');
    header.appendChild(charset);
    UTF8ized.appendChild(header);

    const body = document.createElement('body');
    body.appendChild(output);
    UTF8ized.appendChild(body);

    const blob = htmlDocx.asBlob(UTF8ized.innerHTML, {
      orientation: 'landscape',
      margins: {
        top: 720,
        bottom: 720,
        left: 720,
        right: 720,
      },
    });

    let filename = prompt('Rubric word document name', `${name}.docx`);
    if (filename == null) return;
    if (!filename.endsWith('.docx')) {
      filename += '.docx';
    }
    saveAs(blob, filename);
  }

  const editable_links = Array.from(document.querySelectorAll('th[scope="row"]')); // .filter(row => !row.querySelector('img[alt*="cannot be edited or deleted"]'));
  editable_links.forEach(row => {
    const link = row.querySelector('a');
    const rubric_id = /rubricId=([0-9]+)/.exec(link.href)[1];

    const toWord = document.createElement('a');
    toWord.setAttribute('data-testid', 'rubric-to-word');
    toWord.setAttribute('mm-icon-type', 'action-link');
    toWord.setAttribute('mm-icon-offset', '2:1');
    toWord.innerHTML = '<d2l-icon icon="d2l-tier1:download" title="Download as Word document"></d2l-icon>';
    toWord.className = 'd2l-link d2l-link-inline mm-rubric-download-to-word';
    toWord.style.paddingLeft = '8px';
    toWord.setAttribute('mm-rubric-id', rubric_id);
    toWord.addEventListener('click', handleSaveToWord, true);
    link.parentNode.insertBefore(toWord, link.nextSibling);

    const edit = document.createElement('a');
    edit.setAttribute('data-testid', 'rubric-to-generator');
    edit.setAttribute('mm-icon-type', 'action-link');
    edit.setAttribute('mm-icon-offset', '2:1');
    edit.innerHTML = '<d2l-icon icon="d2l-tier1:export" title="Export to MyLO Rubric Generator"></d2l-icon>';
    edit.className = 'd2l-link d2l-link-inline mm-rubric-export';
    edit.style.paddingLeft = '8px';
    edit.href = `http://www.utas.edu.au/building-elearning/resources/mylo-rubric-generator/beta?mmrid=${rubric_id}`;
    edit.setAttribute('mm-rubric-id', rubric_id);
    edit.addEventListener('click', processEvent, true);
    link.parentNode.insertBefore(edit, link.nextSibling);
  });
})();
