/*global tinymce*/
(function () {
  if (window.RubricEditor) return;
  window.RubricEditor = true;

  if (!store.mylo.floatingRubricEditors) return;

  // Shorthand elements for getting elements.
  //	Usage:
  //		_('selector')...					Get one
  //		_('selector',true)...				Get all
  //		_('selector')._('sub-selector')...	Get subchild from result.
  var _ = (r, all) => (all ? document.querySelectorAll(r) : document.querySelector(r));
  Element.prototype._ = function (r, all) {
    return all ? this.querySelectorAll(r) : this.querySelector(r);
  };

  // Grab our toolbar, and make sure we don't remove it later by adding a custom class.
  // const tb = _('.d2l-htmleditor .d2l-htmleditor-toolbar');
  // tb.classList.add('d2l-toolbar-autopos');

  // Hide the rest of our heading toolbars (keep the spellcheck, etc).
  _('.d2l-htmleditor .d2l-htmleditor-toolbar:not(.d2l-htmleditor-footer)', true).forEach(r => (r.style.display = 'none'));

  let prev = null;
  tinymce.get().forEach(editor => {
    editor.on('focus', e => {
      if (prev) {
        prev.classList.remove('d2l-toolbar-autopos');
        prev.style.display = 'none';
      }
      let this_bar = e.target.editorContainer.parentNode.previousElementSibling;
      this_bar.classList.add('d2l-toolbar-autopos');
      this_bar.style.display = '';
      prev = this_bar;
    });
  });

  // const checkIfHidden = () => {
  // if (_('.d2l-htmleditor-hide1-hidden', true).length > 0) {
  const remove_tags = (content, allowed = []) => {
    let d = document.createElement('div');
    d.innerHTML = content.getContent().replace(/<p[^>]*?>&nbsp;<\/p>/gi, '');
    let tags = [...d.querySelectorAll(`*`)].filter(node => !allowed.includes(node.nodeName.toLowerCase()));
    tags.forEach(tag => tag.replaceWith(...tag.childNodes));
    return d.innerHTML;
  };

  const remove_styles = editor => {
    editor.dom.doc.querySelectorAll('[style]').forEach(node => node.removeAttribute('style'));
  };

  const ShowAllButtons = bar => {
    bar.querySelectorAll('.d2l-htmleditor-group').forEach(el => {
      el.classList.remove('d2l-htmleditor-hide1');
      el.classList.remove('d2l-htmleditor-hide1-hidden');
    });

    // Remove the 'Hide/Show Extra Components' button.
    if (bar && bar.nextElementSibling) bar.nextElementSibling.remove();
  };

  document.querySelectorAll('.d2l-htmleditor-component-container').forEach(html => {
    html.parentNode.setAttribute('mm-icon-type', 'view-data');
    html.parentNode.setAttribute('mm-icon-offset', '0:5');

    const clearBtn = document.createElement('span');
    clearBtn.className = 'd2l-htmleditor-toolbar-item';
    clearBtn.innerHTML =
      '<span class="d2l-htmleditor-button" data-action="clear-formatting" role="button" tabindex="0" title="Clear Formatting"></span>';
    [...html.querySelectorAll('.d2l-htmleditor-group')].pop().appendChild(clearBtn);

    clearBtn.addEventListener('click', e => {
      tinymce.activeEditor.setContent(remove_tags(tinymce.activeEditor, ['p', 'br']));
      remove_styles(tinymce.activeEditor);
    });

    const all = document.createElement('div');
    all.classList.add('editor-box');
    all.innerHTML = `<span class="legend">Apply to all</span>
		<div class="d2l-htmleditor-group d2l-htmleditor-hide1">
			<span class="d2l-htmleditor-toolbar-item">
				<a class="d2l-htmleditor-button" data-action="all_align_left" role="button" tabindex="0" title="Align Left">
					<img class="d2l-htmleditor-buttonmenuitem-icon" style="background-image: url(https://s.brightspace.com/lib/ilp/d2l/img/0/sprites/D2L.LP.Web.UI.Desktop.Controls.HtmlEditor.16E4A96862AADC16C03A4B213BA6E674.png); background-position: 0 -496px; background-repeat: no-repeat; width: 16px; height: 16px;" src="/d2l/img/lp/pixel.gif" alt="">
				</a>
			</span>
			<span class="d2l-htmleditor-toolbar-item">
				<span class="d2l-htmleditor-button" data-action="all-clear-formatting" role="button" tabindex="0" title="Clear Formatting"></span>
			</span>
			<span class="d2l-htmleditor-toolbar-item">
				<a class="d2l-htmleditor-button mm-remove-style-tags" data-action="all-clear-styles" title="Remove style attribute from tags" role="button" aria-label="Remove style attribute from tags" tabindex="0">
					<img class="d2l-htmleditor-button-icon" src="/d2l/img/lp/pixel.gif" style="background-image: url(https://s.brightspace.com/lib/bsi/10.7.10-daylight.14/images/html-editor/insert-attributes.svg); background-position: 0 0; background-repeat: no-repeat; width: 18px; height: 18px; background-size: 18px 18px;">
				</a>
			</span>
		</div>
		<div class="d2l-clear"></div>`;

    const selected = document.createElement('div');
    selected.classList.add('editor-box');

    const label = document.createElement('span');
    label.classList.add('legend');
    label.innerText = 'Apply to selection';
    selected.appendChild(label);

    const container = html.cloneNode();
    container.appendChild(selected);
    container.appendChild(all);

    html.parentNode.insertBefore(container, html);

    selected.appendChild(html);
  });

  document.body.live('click', '[data-action="all_align_left"]', e => {
    tinymce.get().forEach(editor => {
      editor
        .getBody()
        .querySelectorAll('p')
        .forEach(e => {
          e.style.textAlign = 'left';
          e.setAttribute('data-mce-style', e.getAttribute('style'));
        });
    });
  });

  document.body.live('click', '[data-action="all-clear-formatting"]', e => {
    console.log('Clearing all');
    tinymce.get().forEach(editor => {
      editor.setContent(remove_tags(editor, ['p', 'br']));
      remove_styles(editor);
    });
  });

  document.body.live('click', '[data-action="all-clear-styles"]', e => {
    tinymce.get().forEach(remove_styles);
  });

  document.querySelectorAll('.d2l-htmleditor-component-container').forEach(ShowAllButtons);

  let row = document.querySelector('.d2l-grid-container tr:not([header])');
  let first = row.querySelector('th.d2l-table-cell-first');
  first.style.width = '';
  first.style.minWidth = '200px';
  first.parentNode.querySelector('td:not([class])').style.width = '100%';
  row.querySelector('.d2l-table-cell-last').style.minWidth = '300px';
  const OnEditorsReady = () => {
    let editors = document.querySelectorAll('.d2l-htmleditor');
    editors.forEach(editor => {
      editor.style.width = '';
      editor.querySelector('.d2l-htmleditor-iframecontainer').style.maxHeight = 'unset';

      let view = editor.querySelector('.mce-edit-area');
      let iframe = view.querySelector('iframe').contentDocument;
      const height = Math.max(iframe.body.scrollHeight + 50, 300);
      view.style.width = '';
      view.style.minHeight = height + 'px';

      editor.setAttribute('data-d2l-height', height + 'px');
      // This makes sure that whenever one of the footer buttons are clicked, it's focusing the correct editor first.
      editor
        .querySelectorAll('.d2l-htmleditor-footer-inner .d2l-htmleditor-button')
        .forEach(e => e.addEventListener('click', () => tinymce.get(editor.id + '$html').focus(), true));
    });

    tinymce.get()[0].fire('focus');
  };

  const editor_ready = editor =>
    new Promise((resolve, reject) => {
      editor.on('init', resolve);
    });
  Promise.all(tinymce.get().map(editor_ready)).then(OnEditorsReady);
})();
