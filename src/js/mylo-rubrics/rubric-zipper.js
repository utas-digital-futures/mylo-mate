(async function() {
	const xhr = (url) => new Promise((res, rej) => { let x = new XMLHttpRequest(); x.open('GET', url); x.addEventListener('loadend', (e) => { if(x.status == 200) res(x.response); else rej(e,x); }); x.send(); });
	const UUID = () => ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16));

	// TODO: Find out how to get rubric data out of API.
	// 	Question posted to StackOverflow and Brightspace Community Discussion area:
	//		https://community.brightspace.com/s/question/0D50A000001BIq7SAG/is-there-any-way-to-get-a-rubric-from-the-api
	//		https://stackoverflow.com/questions/50616183/get-rubric-from-d2l-api

	// let xml = await xhr(chrome.runtime.getURL('res/rubrics.xml'));
	// let manifest = await xhr(chrome.runtime.getURL('res/imsmanifest.xml'));

	// let zip = new JSZip();

	// zip.file('imsmanifest.xml', manifest);
	// zip.file('rubrics_d2l.xml', xml);

	// let output = await zip.generateAsync({
	// 	type: 'array', //binarystring
	// 	compression: 'DEFLATE',
	// 	compressionOptions: { level: 9 }
	// });

	// let obj = {};
	// let id = UUID();
	// obj[id] = output;
	// chrome.storage.local.set(obj);
})();