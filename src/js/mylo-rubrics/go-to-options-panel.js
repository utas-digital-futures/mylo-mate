(async () => {
  const url = new URL(window.location.href);

  const rubric = document.querySelector('d2l-rubric-editor');
  if (!rubric) return;

  const accordion = await until(() => sdqs.querySelectorDeep('#accordion-container'));
  accordion.toggleAttribute('expanded', true);

  if (url.searchParams.get('options') != '1') return;
  await until(() => sdqs.querySelectorDeep('d2l-rubric-criteria-editor', rubric));
  setTimeout(() => window.scroll({ top: document.body.scrollHeight, behavior: 'smooth' }), 1000);
})();
