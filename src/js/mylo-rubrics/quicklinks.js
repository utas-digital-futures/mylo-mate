(async () => {
  const quicklink = ({ orgId, rubricId, title, text, options }) => {
    const a = document.createElement('a');
		a.toggleAttribute('quicklink', true);
    a.classList.add('d2l-link');
    a.title = title;
    a.href = `edit_structure.d2l?rubricId=${rubricId}&ou=${orgId}${options ? '&options=1' : ''}`;
    a.innerText = `[${text}]`;

    return a;
  };

  document.querySelectorAll(`[href*="edit_structure.d2l"]:not([mm-generated])`).forEach(el => {
    const wrapper = document.createElement('span');
    wrapper.setAttribute('mm-icon-render', 'icon');
    wrapper.setAttribute('mm-icon-type', 'action-navigation');
    wrapper.setAttribute('mm-icon-offset', '-5:0');
    wrapper.style.marginLeft = '1em';

    const search = new URL(el.href).searchParams; //window.location.search;
    const title = el.innerText;

    const orgId = search.get('ou');
    const rubricId = search.get('rubricId');

    const e = quicklink({ orgId, rubricId, title: `Edit ${title}`, text: 'e', options: false });
    const o = quicklink({ orgId, rubricId, title: `Options for ${title}`, text: 'o', options: true });
    wrapper.append(e, o);

    el.insertAdjacentElement('afterend', wrapper);
  });
})();
