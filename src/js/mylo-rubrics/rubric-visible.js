(async function () {
  const GetOAuth2Token = async () => {
    const existing = window.localStorage['D2L.Fetch.Tokens'];
    if (existing != null && existing['*:*:*'] != null) {
      const body = JSON.parse(existing['*:*:*']);
      if (body.expires_at < Date.now() / 1000) {
        delete window.localStorage['D2L.Fetch.Tokens'];
      } else return body.access_token;
    }

    if (!window.localStorage['XSRF.Token']) return null;

    const token = await fetch('/d2l/lp/auth/oauth2/token', {
      method: 'post',
      body: 'scope=*:*:*',
      credentials: 'include',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-Csrf-Token': window.localStorage['XSRF.Token'],
      }),
    }).then(data => data.json());

    window.localStorage.setItem('D2L.Fetch.Tokens', JSON.stringify({ '*:*:*': token }));
    return token.access_token;
  };

  const token = await GetOAuth2Token();

  const StatusCodes = {
    Visible: 'AlwaysVisible',
    Hidden: 'NeverVisible',
    VisibleAfterFeedback: 'VisibleOnceFeedbackPosted',
  };

  const RubricBase = window.location.origin.includes('d2ldev2')
    ? '0e78be5b-5af6-4f4b-827b-568374e17a1d'
    : 'c0ef8d21-d21d-4bf1-b68f-054dc1a5e6fc';

  document.querySelectorAll('a[href*="edit_structure.d2l"]:not([quicklink])').forEach(async a => {
    const params = new URL(a.href).searchParams;
    const ou = params.get('ou');
    const id = params.get('rubricId');

    const status = await api(`https://${RubricBase}.rubrics.api.brightspace.com/organizations/${ou}/${id}`, {
      token,
      first: true,
      fresh: true,
    }).then(d => d.properties.visibility);

    const icon = document.createElement('d2l-icon');
    icon.style.marginRight = '10px';

    if (status == StatusCodes.Visible) {
      icon.setAttribute('icon', 'tier1:visibility-show');
      icon.title = 'Rubric visible to students';
    } else if (status == StatusCodes.Hidden) {
      icon.setAttribute('icon', 'tier1:visibility-hide');
      icon.title = 'Rubric hidden from students';
    } else if (status == StatusCodes.VisibleAfterFeedback) {
      icon.setAttribute('icon', 'tier1:visibility-conditional');
      icon.title = 'Rubric hidden from students until feedback published';
    }

    a.insertAdjacentElement('beforebegin', icon);
  });
})();
