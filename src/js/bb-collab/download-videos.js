(async () => {
  // Open a communications channel with the background script.
  const p = chrome.runtime.connect(extensionID, { name: 'collaborate' });

  // Post a greeting to open the network on the other side, so it can start sending things back!
  p.postMessage({ flag: 'init' });

  // When we get the request to start downloading..
  p.onMessage.addListener(async msg => {
    // If the request is specifically for a download,
    // then send back a message with the same flag,
    // and with our collection of videos.
    if (msg.request == 'download') {
      const { userContext } = angular.element('[ui-view="content"]').scope().menuController;
      const bearer = userContext.user.$response.config.headers.Authorization;

      const prev = new Date();
      prev.setFullYear(prev.getFullYear() - 10);

      const url = new URLSearchParams();
      url.set('startTime', prev.toISOString());
      url.set('endTime', new Date().toISOString());
      url.set('sort', 'endTime');
      url.set('order', 'desc');
      url.set('limit', 500);
      url.set('offset', 0);

      const recordings = await fetch(`/collab/api/csa/recordings?${url.toString()}`, {
        headers: { Authorization: bearer },
      })
        .then(d => d.json())
        .then(d => d?.results);

      p.postMessage({ recordings, userContext });
    }
  });
})();
