(async function () {
  // The ID of the context-based actions in the popup panel.
  const panel = document.querySelector('#context');
  const wildcard = rule => new RegExp('^' + rule.split('*').join('.*') + '$', 'i');

  // The contextual actions to run, based upon the URL and path of the current tab.
  const actions = [
    {
      url: '*.utas.edu.au',
      exclude: ['mylo.utas.edu.au', 'd2ldev1.utas.edu.au', 'd2ldev2.utas.edu.au', 'mylo-manager.utas.edu.au'],
      paths: ['/*'],
      actions: [
        {
          title: 'Check page links',
          data: 'check-utas-links',
        },
      ],
      description: 'Check all page links for validity.',
    },
    {
      url: 'echo360.net.au',
      paths: ['/section/*/home'],
      actions: [
        {
          title: 'Download Clips',
          data: 'echo360-download',
        },
      ],
      description: 'Download the currently visible clips to your local machine.',
    },
    {
      url: 'au-lti.bbcollab.com',
      paths: ['/collab/ui/*'],
      actions: [
        {
          title: 'Download Recordings',
          data: 'collab-download',
        },
      ],
      description: 'Download the currently visible clips to your local machine.',
    },
    {
      url: 'mylo.utas.edu.au',
      paths: ['/d2l/le/content/*/viewContent/*/View'],
      actions: [
        {
          title: 'Check page links',
          data: 'check-mylo-links',
        },
      ],
      description: 'Check all page links for validity.',
    },
  ];
  /**
   * Return the appropriate action set based on the given URL.
   * @param {string} url The URL to compare against.
   * @memberof Chrome/Actions#
   */
  function getAction(url) {
    const u = new URL(url);

    return actions.filter(action => {
      const urls = Array.isArray(action.url) ? action.url : [action.url];
      const excludes = action.exclude != null ? (Array.isArray(action.exclude) ? action.exclude : [action.exclude]) : [];

      if (urls.find(url => u.hostname.match(wildcard(url)))) {
        if (excludes.some(bl => wildcard(bl).test(u.hostname))) return false;
        if (action.paths.find(path => u.pathname.match(wildcard(path)))) return true;
      }
    });
  }

  const getActions = urls => urls.flatMap(getAction);

  // Queries the current tab, and attempts to get an action set for it.
  // If it does, then it will populate the contextual panel of the popup and unhide it.
  const [currentTab] = await new Promise(res => chrome.tabs.query({ active: true, currentWindow: true }, res));
  const frames = await new Promise(res => chrome.webNavigation.getAllFrames({ tabId: currentTab.id }, res));

  const results = getActions(frames.map(f => f.url));
  if (results !== null && results.length > 0) {
    for (const action of results) {
      for (const item of action.actions) {
        const li = document.createElement('li');
        li.setAttribute('action', 'context');
        li.setAttribute('function', item.data);
        li.innerHTML = `${item.title} ${
          action.description ? `<i aria-hidden="true" class="fa-help" title="${action.description}"></i>` : ''
        }`;
        panel.querySelector('ul').append(li);
      }
    }
    panel.classList.remove('hidden');
  }
})();
