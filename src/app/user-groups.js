const collection = {
  groups: document.querySelector('#usergroups_groups'),
  users: document.querySelector('#usergroups_users'),
};

const actions = {
  groups: document.querySelector('#group-actions'),
  users: document.querySelector('#user-actions'),
};

(async function () {
  const manager = new UserGroups();

  // Rather than just replace the entire HTML, lets check for each group item and see if it exists.
  //    If it doesn't, create it.
  //    If it does, update it.
  // This means that adding new groups (or users!) won't change the current selection and make things weird.
  const FillGroupsList = async () => {
    const groups = await manager.get();
    console.log(groups);
    groups.forEach(group => {
      const existing = collection.groups.querySelector(`li[data-group-id="${group.ID}"]`);
      if (existing) {
        existing.querySelector('label span').innerText = group.Name;
        existing.setAttribute('is-favourite', group.Favourite == true);
        if (group.Favourite == true) {
          existing.title = 'Favourited. This group will be selected by default in MyLO Manager.';
        }
      } else {
        let item = document.createElement('li');
        let label = document.createElement('label');
        item.setAttribute('data-group-id', group.ID);
        item.setAttribute('is-favourite', group.Favourite == true);
        if (group.Favourite == true) {
          item.title = 'Favourited. This group will be selected by default in MyLO Manager.';
        }

        let text = document.createElement('span');
        text.innerText = group.Name;

        let radio = document.createElement('input');
        radio.type = 'radio';
        radio.value = group.ID;
        radio.name = 'group';

        label.append(radio, text);
        item.append(label);
        collection.groups.append(item);
      }
    });

    if (groups.length !== collection.groups.querySelectorAll(`li[data-group-id]`).length) {
      let entries = collection.groups.querySelectorAll(`li[data-group-id]`);
      entries.forEach(item => {
        if (!groups.find(group => group.ID == item.getAttribute('data-group-id'))) {
          item.remove();
        }
      });
    }

    if (!collection.groups.querySelector('input[name="group"]:checked')) {
      // There's always the chance that there's no checked radio because there's no groups.
      // So we'll make sure there is something before we try to check it.
      const radio = collection.groups.querySelector('input[name="group"]');
      if (radio) radio.checked = true;
    }

    actions.groups.querySelector('.edit').setAttribute('disabled', groups.length == 0);
    actions.groups.querySelector('.favourite').setAttribute('disabled', groups.length == 0);
    actions.groups.querySelector('.delete').setAttribute('disabled', groups.length == 0);
    actions.groups.querySelector('.save').setAttribute('disabled', groups.length == 0);

    collection.groups.dispatchEvent(new Event('change'));
  };

  const FillUsersList = async () => {
    const selectedGroup = collection.groups.querySelector('input[name="group"]:checked');
    collection.users.innerHTML = '';

    if (!selectedGroup) {
      collection.users.dispatchEvent(new Event('change'));
      return;
    }
    const id = selectedGroup.value;
    const { Users } = (await manager.get(id)) ?? { Users: [] };

    const entries = Users.map(user => {
      let opt = document.createElement('option');
      opt.value = user.Username;
      opt.innerHTML = `${user.PreferredName} ${user.Surname} (${user.Username}) [MyLO ${user.Role || 'Support'}${
        user.EchoRole != '2' ? '' : ' | Echo Instructor'
      }]`;
      return opt;
    });

    collection.users.append(...entries);
    collection.users.dispatchEvent(new Event('change'));
  };

  const UserListButtonStatus = async e => {
    const groups = await manager.get();
    actions.users.querySelector('.add').setAttribute('disabled', groups.length == 0);
    actions.users.querySelector('.edit').setAttribute('disabled', e.target.selectedOptions.length == 0);
    actions.users.querySelector('.delete').setAttribute('disabled', e.target.selectedOptions.length == 0);
  };

  const AddGroup = async () => {
    let name = await OptionsUI.prompt(
      'Group name',
      'Please insert a group name. This can modified later in the Options panel.',
      'Group name..'
    );
    if (name != null) {
      name = name.trim();
      const id = name
        .toLowerCase()
        .replace(/[^\w\s\d]/gi, '')
        .replace(/[\s]/g, '-');

      const exists = (await manager.get(id)) != null;
      if (exists) {
        return OptionsUI.alert('Group Exists', 'A group already exists with that name. Please try again.');
      } else {
        let groupsCount = await manager.size();
        await manager.save(id, { ID: id, Name: name, Users: [], Favourite: groupsCount === 0 });
      }
    }
  };

  const EditGroup = async () => {
    const selected = collection.groups.querySelector('input[name="group"]:checked').value;
    const group = await manager.get(selected);

    const newName = await OptionsUI.prompt('Edit', 'Please enter a new name for this group.', '', group.Name).then(name =>
      name ? name.trim() : null
    );

    if (newName == null) return;
    if (newName != group.Name && newName.length > 0) {
      const id = newName
        .toLowerCase()
        .replace(/[^\w\s\d]/gi, '')
        .replace(/[\s]/g, '-');

      await manager.save(selected, { ...group, ID: id, Name: newName }, true);
      // OptionsUI.notify('Success', `Group "${group.Name}" renamed to "${newName}"`, {
      //   timeout: 2,
      //   parent: document.body,
      //   classes: 'center'
      // });
    }
  };

  const FavouriteGroup = async () => {
    const selected = collection.groups.querySelector('input[name="group"]:checked').value;
    const groups = await manager.get();

    groups.forEach(group => {
      group.Favourite = group.ID == selected ? !group.Favourite : false;
    });

    const group = groups.find(group => group.ID == selected);
    await manager.saveAll(groups);

    // OptionsUI.notify(
    //   'Success',
    //   `Group "${group.Name}" ${
    //     group.Favourite ? 'starred. It will now be selected by default in MyLO Manager' : 'unstarred'
    //   }`,
    //   {
    //     timeout: 2,
    //     parent: document.body,
    //     classes: 'center'
    //   }
    // );
  };

  const DeleteGroup = async () => {
    const selected = collection.groups.querySelector('input[name="group"]:checked').value;
    const group = await manager.get(selected);

    const confirm = await OptionsUI.confirm(
      'Are you sure?',
      `Are you sure you want to delete group "${group.Name}"? <br><strong>This action cannot be undone.</strong>`
    );
    if (confirm) {
      await manager.delete(selected);
      // OptionsUI.notify('Success', `Group "${group.Name}" deleted.`, {
      //   timeout: 2,
      //   parent: document.body,
      //   classes: 'center'
      // });
    }
  };

  const AddUser = async () => {
    if (!(await UserGroups.SignedIn)) {
      return OptionsUI.alert(
        'Not Signed In',
        'You need to be signed into MyLO Manager to use this. Please visit <a href="https://mylo-manager.utas.edu.au/Home/Units" target="_blank">MyLO Manager</a>, and try again.'
      );
    }

    const response = await OptionsUI.promptUserAdd(
      'Add Users',
      'Insert a list of usernames to add, separated by a comma.<br>Example: <pre>username1, username2, username3</pre>',
      'Usernames to add',
      '',
      'Role for these users: '
    );

    if (response != null) {
      if (!collection.groups.querySelector('input[name="group"]:checked')) return;
      const selected = collection.groups.querySelector('input[name="group"]:checked').value;
      const group = await manager.get(selected);

      const existing = group.Users.map(u => u.Username);
      const newUsers = response.users
        .split(',')
        .map(u => u.trim())
        .filter((v, i, a) => v.length > 0 && a.indexOf(v) === i && !existing.includes(v));

      const errors = [];
      const userData = await Promise.all(
        newUsers.map(async username => {
          try {
            const data = await UserGroups.SearchForUser(username);
            if (data != null) {
              return { ...data, Role: response.role, EchoRole: response.echoRole };
            } else {
              errors.push(`Could not find user "${username}"`);
              return null;
            }
          } catch (e) {
            console.error(e);
            return null;
          }
        })
      ).then(data => data.filter(v => v)); // Get rid of any null values.

      group.Users = group.Users.concat(userData);
      await manager.save(selected, group);

      let messages = [];
      // if (userData.length > 0) messages.push(`Successfully added: ${userData.map(user => `'${user.Username}'`).join(', ')}`);
      if (errors.length > 0) messages.push(`Failed to add: ${errors.map(user => `'${user}'`).join(', ')}`);
      if (messages.length > 0) {
        OptionsUI.notify('Notice', messages.join('<br>'), {
          timeout: 2,
          parent: document.body,
          classes: 'center',
        });
      }
    }
  };

  const EditUser = async () => {
    const selected = collection.groups.querySelector('input[name="group"]:checked').value;
    const group = await manager.get(selected);

    const selectedUsers = Array.from(collection.users.selectedOptions).map(opt =>
      group.Users.find(user => user.Username == opt.value)
    );

    await OptionsUI.promptRoles('Edit User Roles', 'Edit user roles here', selectedUsers);
    await manager.save(selected, group);
    // OptionsUI.notify('Success', 'User roles updated successfully.', {
    //   timeout: 2,
    //   parent: document.body,
    //   classes: 'center'
    // });
  };

  const DeleteUser = async () => {
    const selected = collection.groups.querySelector('input[name="group"]:checked').value;
    const group = await manager.get(selected);

    const selectedUsers = Array.from(collection.users.selectedOptions).map(opt =>
      group.Users.find(user => user.Username == opt.value)
    );

    const usernames = selectedUsers.map(user => user.Username);
    const lastUser = selectedUsers.pop();

    const names =
      selectedUsers.map(user => `${user.PreferredName} ${user.Surname}`).join(', ') +
      (selectedUsers.length > 0 ? ' and ' : '') +
      `${lastUser.PreferredName} ${lastUser.Surname}`;

    const confirmed = await OptionsUI.confirm(
      'Are you sure?',
      `Are you sure you want to delete ${names} from "${group.Name}"? <br><strong>This action cannot be undone.</strong>`
    );

    if (confirmed) {
      group.Users = group.Users.filter(user => !usernames.includes(user.Username));
      await manager.save(selected, group);
      // OptionsUI.notify('Success', `${names} removed from "${group.Name}".`, {
      //   timeout: 2,
      //   parent: document.body,
      //   classes: 'center'
      // });
    }
  };

  /** Fill the groups list */
  await FillGroupsList();
  manager.addEventListener('update', FillGroupsList);

  collection.groups.addEventListener('change', FillUsersList);
  collection.groups.dispatchEvent(new Event('change'));

  collection.users.addEventListener('change', UserListButtonStatus);
  collection.users.dispatchEvent(new Event('change'));

  // When the usergroup-group add button is clicked, add a new group.
  actions.groups.querySelector('.add').addEventListener('click', AddGroup);

  // When the usergroup-group edit button is clicked, edit the selected group.
  actions.groups.querySelector('.edit').addEventListener('click', EditGroup);

  // When the usergroup-group favourite button is clicked, favourite the selected group.
  actions.groups.querySelector('.favourite').addEventListener('click', FavouriteGroup);

  // When the usergroup-group delete button is clicked, remove the selected group.
  actions.groups.querySelector('.delete').addEventListener('click', DeleteGroup);

  // When the usergroup-user add button is clicked, add a list of users.
  actions.users.querySelector('.add').addEventListener('click', AddUser);

  // When the usergroup-user edit button is clicked, edit the selected users.
  actions.users.querySelector('.edit').addEventListener('click', EditUser);

  // When the usergroup-user delete button is clicked, remove the selected users.
  actions.users.querySelector('.delete').addEventListener('click', DeleteUser);

  const checkMyloManagerStatus = async () => {
    const online = await UserGroups.CanConnect;
    document
      .querySelectorAll('[data-purpose="mylo-manager-online"]')
      .forEach(el => (el.style.display = online ? 'none' : 'block'));

    actions.users.setAttribute('data-online', online);
  };

  // Every 30 seconds, check the MyLO Manager Server Status
  setInterval(checkMyloManagerStatus, 30000);
  checkMyloManagerStatus();
  document.querySelector('[data-purpose="recheck-online-status"]').addEventListener('click', checkMyloManagerStatus);
})();
