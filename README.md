# MyLO MATE

MyLO MATE is an extension for Google Chrome, [available here][download]. It is an extension that enhances MyLO, MyLO Manager and Echo360. For detailed information on how to use the different features available, please consult the [User Guide][user guide].

# Licensing
This source code is licensed under GPLv3. For more information, please see [LICENSE](./LICENSE).

# Daylight Icon Sets
Daylight icon sets can be found here: https://github.com/BrightspaceUI/icons

[download]: https://chrome.google.com/webstore/detail/mylo-mate/bbmpfljhlpdmialnpoakplledongdcom
[user guide]: http://www.utas.edu.au/building-elearning/resources/mylo-mate/user-guide