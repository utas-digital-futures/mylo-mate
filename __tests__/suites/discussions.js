const { UNIT_ID, TOPIC_ID } = process.env;
const { testid } = require('../config/util.js');

describe('Discussions List', () => {
  beforeAll(async () => {
    await page.goto(`/d2l/le/${UNIT_ID}/discussions/List`);
    await page.waitForSelector('[mm-abort]');
  });

  it('has quicklinks', async () => {
    expect(await page.testid('discussion-quicklinks')).toBeTruthy();
  });

  it('has staff role colours', async () => {
    await page.waitForSelector('.d2l-user-profile-handle[mm-staff]', { timeout: 15000 });
    expect(await page.$('.d2l-user-profile-handle[mm-staff]')).toBeTruthy();
  });
});

describe('Topic View', () => {
  let content = null;
  beforeAll(async () => {
    await page.goto(`/d2l/lms/discussions/messageLists/frame.d2l?ou=${UNIT_ID}&tId=${TOPIC_ID}`);
    await page.waitForSelector('[mm-abort]');
    content = page.frames().find(frame => frame.name() == 'FRAME_list');
  });

  it('has a post', async () => {
    expect(await content.$('a[id^="post_"]')).toBeTruthy();
  });

  it('has the Restore Deleted Posts button', async () => {
    await content.waitForSelector(testid("restore-deleted-posts"), { timeout: 15000 });
    expect(await content.$(testid("restore-deleted-posts"))).toBeTruthy();
  });

  it('has Content Previews', async () => {
    await content.waitForSelector(testid("post-content"), { timeout: 15000 });
    expect(await content.$(testid("post-content"))).toBeTruthy();
  });

  it('has Hide All / Show All buttons', async () => {
    await content.waitForSelector(testid("show-all-messages"), { timeout: 15000 });
    expect(await content.$(testid("show-all-messages"))).toBeTruthy();
    expect(await content.$(testid("hide-all-messages"))).toBeTruthy();
  });

  it('has individual Hide/Show Content icons', async () => {
    await content.waitForSelector(testid("show-hide-post-content"), { timeout: 15000 });
    expect(await content.$(testid("show-hide-post-content"))).toBeTruthy();
  });
});