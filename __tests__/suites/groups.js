const moment = require('moment');
const { testid } = require('../config/util.js');

describe('Manage Groups', () => {
  beforeAll(async () => {
    const { UNIT_ID, CATEGORY_ID } = process.env;
    await page.goto(`/d2l/lms/group/group_list.d2l?ou=${UNIT_ID}&categoryId=${CATEGORY_ID}`);
    await page.waitForSelector('[mm-abort]');
    await page.waitFor(1000);
  });

  it('has all Group Renaming buttons', async () => {
    expect(await page.testid('bulk-edit')).toBeTruthy();
    expect(await page.testid('prefix-text')).toBeTruthy();
    expect(await page.testid('suffix-text')).toBeTruthy();
    expect(await page.testid('replace-text')).toBeTruthy();
  });

  it('has Unique Group IDs button', async () => {
    expect(await page.testid('unique-group-ids')).toBeTruthy();
  });

  it('renames groups successfully', async () => {
    const datestamp = moment().format('hh:mm:ssa Do MMM YYYY');
    page.once('dialog', async dialog => dialog.accept(`Group [#] - ${datestamp}`));
    await page.click('.d2l-grid-container d2l-table-wrapper > table > tbody > tr:not([header]) input');

    const rows = await page.$$('tr[selected]');

    await page.waitForSelector(testid('bulk-edit'), { timeout: 15000 });
    await page.waitFor(500);
    await page.click(testid('bulk-edit'));
    await page.click(testid('replace-text'));
    await page.waitFor(50);
    await page.click(testid('save-group-rename'));
    await page.waitForSelector(testid('group-save-button-bar') + '[mm-visible="false"]')

    const changed = await page.$$(`tr[selected] a[title*="${datestamp}"]`);

    expect(rows.length).toEqual(changed.length);
  }, 60 * 1000);
});