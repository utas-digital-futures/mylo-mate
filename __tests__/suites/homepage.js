beforeAll(async () => {
  const { UNIT_ID } = process.env;
  await page.goto(`/d2l/home/${UNIT_ID}`);
  await page.waitForSelector('.d2l-body');
  await page.waitFor(1000);
});

describe('Unit Homepage', () => {
  it('should load', async () => {
    const body = await page.$('.d2l-body');
    expect(body).toBeTruthy();
  });

  it('should have coloured rows', async () => {
    await page.click('d2l-navigation-button-notification-icon[text*="select a unit" i]');
    await page.waitForSelector('.d2l-courseselector-wrapper ul li[class*="mylo-mate" i]', { timeout: 30000 });

    const items = await page.$$('.d2l-courseselector-wrapper ul li[class*="mylo-mate" i]');
    expect(items.length).toBeGreaterThan(0);
  });

  it('should have a coloured unit dot', async () => {
    expect(await page.$('.mm-status-checker')).toBeTruthy();
  });

  it('supports accessibility mode', async () => {
    const classes = (await page.$eval('body', e => e.getAttribute('class'))).trim().split(' ');
    expect(classes).toContain('mm-access-friendly');
  });

  it('puts an `mm-abort` attribute on the <body>', async () => {
    expect(await page.$('body[mm-abort]')).toBeTruthy();
  });
});

describe('Navbar', () => {
  it('has Impersonate student icon', async () => {
    expect(await page.testid('impersonate-student-icon')).toBeTruthy();
  });

  it('has Manage Files icon', async () => {
    expect(await page.testid('navbar-manage-files')).toBeTruthy();
  });

  it('has Unit Admin icon', async () => {
    expect(await page.testid('navbar-unit-admin')).toBeTruthy();
  });
});

describe('Unit Widgets', () => {
  beforeAll(async () => {
    await page.waitFor(1000);
  });

  const expandWidget = async id => {
    return page.$eval(`[mm-widget-name="${id}"]`, el => el.removeAttribute('collapsed'));
  }

  const collapseWidget = async id => {
    return page.$eval(`[mm-widget-name="${id}"]`, el => el.setAttribute('collapsed', ''));
  }

  describe('Search Unit', () => {
    const ID = 'SearchUnit';
    it('exists', async () => {
      await expandWidget(ID);
      expect(await page.$(`[mm-widget-name="${ID}" i]`)).toBeTruthy();
    });

    it('finds things', async () => {
      await page.click('[data-testid="search-box"]');
      await page.type('[data-testid="search-box"]', 'puppeteer');
      await page.click('[data-testid="search-submit"]');
      await page.waitForSelector('[data-testid="search-results"] td');
      expect(await page.$('[data-testid="search-results"] td.no-results')).toBeFalsy();
    });
  });

  describe('Link Checker', () => {
    const ID = 'ContentChecker';
    it('exists', async () => {
      await expandWidget(ID);
      expect(await page.$(`[mm-widget-name="${ID}" i]`)).toBeTruthy();
    });
  });

  describe('Status Report', () => {
    const ID = 'StatusReport';
    it('exists', async () => {
      await expandWidget(ID);
      expect(await page.$(`[mm-widget-name="${ID}" i]`)).toBeTruthy();
    });
  });

  describe('Unit Outline Uploader', () => {
    const ID = 'UnitOutlineUploader';
    it('exists', async () => {
      await expandWidget(ID);
      expect(await page.$(`[mm-widget-name="${ID}" i]`)).toBeTruthy();
    });
  });

  describe('Word Cloud Generator', () => {
    const ID = 'WordCloud';
    it('exists', async () => {
      await expandWidget(ID);
      expect(await page.$(`[mm-widget-name="${ID}" i]`)).toBeTruthy();
    });
  });

  describe('Get Lecturers from Units', () => {
    const ID = 'EnrolledUsers';
    it('exists', async () => {
      await expandWidget(ID);
      expect(await page.$(`[mm-widget-name="${ID}" i]`)).toBeTruthy();
    });
  });

  describe('Get Absent Students', () => {
    const ID = 'GetAbsentStudents';
    it('exists', async () => {
      await expandWidget(ID);
      expect(await page.$(`[mm-widget-name="${ID}" i]`)).toBeTruthy();
    });
  });
});
