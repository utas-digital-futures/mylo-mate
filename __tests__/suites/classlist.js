const { UNIT_ID } = process.env;

describe('Content page', () => {
  beforeAll(async () => {
    await page.goto(`/d2l/lms/classlist/classlist.d2l?ou=${UNIT_ID}`);
    await page.waitForSelector('[mm-abort]');
  });

  it('has Impersonate Student button', async () => {
    expect(await page.testid('impersonate-student')).toBeTruthy();
  });

  it('has pagination at top of page', async () => {
    expect(await page.testid('cloned-pagination')).toBeTruthy();
  });

  it('has User Progress settings dialog', async () => {
    expect(await page.testid('user-progress-settings-dialog')).toBeTruthy();
  });

  it('has Email all students button', async () => {
    expect(await page.testid('email-students')).toBeTruthy();
  });
});