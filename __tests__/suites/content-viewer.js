beforeAll(async () => {
  const { UNIT_ID, CONTENT_ID } = process.env;
  await page.goto(`/d2l/le/content/${UNIT_ID}/viewContent/${CONTENT_ID}/View`);
  await page.waitForSelector('[mm-abort]');
  await page.waitFor(2000);
});

describe('Content page', () => {
  it('should load', async () => {
    const body = await page.$('.d2l-body');
    expect(body).toBeTruthy();
  });

  it('has [draft] badge', async () => {
    expect(await page.$('.mm-draft-label')).toBeTruthy();
  });

  it('has a quick Edit icon', async () => {
    expect(await page.testid('quick-edit-link')).toBeTruthy();
  });

  it('has the quick link checker', async () => {
    expect(await page.testid('link-checker-dialog')).toBeTruthy();
  });
});