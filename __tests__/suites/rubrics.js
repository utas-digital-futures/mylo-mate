describe('Rubrics', () => {
  beforeAll(async () => {
    const { UNIT_ID } = process.env;
    await page.goto(`/d2l/lp/rubrics/list.d2l?ou=${UNIT_ID}`);
    await page.waitForSelector('[mm-abort]');
    await page.waitFor(1000);
  });

  it('has Rubric Generator button', async () => {
    expect(await page.testid('rubric-generator-link')).toBeTruthy();
  });

  it('has Import Rubric button', async () => {
    expect(await page.testid('import-rubric-button')).toBeTruthy();
  });

  it('has export buttons', async () => {
    expect(await page.testid('rubric-to-word')).toBeTruthy();
    expect(await page.testid('rubric-to-generator')).toBeTruthy();
  });

  // TODO:
  //  Figure out how we can test the export process??
  //  Given it either downloads to Word or opens in a new, completely different layout?
  //  Perhaps we can just check for console errors or network errors??
  it.todo('exports rubrics correctly');
});