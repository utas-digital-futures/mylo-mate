const chrome = require('chrome-cookies-secure');
const puppeteer = require('puppeteer');
const { setup: setupPuppeteer } = require('jest-environment-puppeteer');

const path = require('path');
const dotenv = require('dotenv');
dotenv.config({ path: path.join(__dirname, `./${process.env.TESTENV}.env`) });

const FetchCookies = async (urls, profile = 'Default') => {
  if (!Array.isArray(urls)) throw new TypeError('Expected `urls` to be array, not ' + typeof urls);
  const d = await Promise.all(
    urls.map(
      url =>
        new Promise((res, rej) => {
          // chrome.getCookies(url, 'header', (err, c) => (err ? console.error(err) : console.log(c)), profile);
          chrome.getCookies(
            url,
            'puppeteer',
            (err, c) =>
              err
                ? rej(err)
                : res(
                    c.map(d => {
                      d.session = false;
                      d.expires = Date.now() / 1000 + 10 * 60;
                      return d;
                    })
                  ),
            profile
          );
        })
    )
  );
  return d.flat();
};

// Connect to the browser created in jest-environment-puppeteer
// so we can perform browser actions once before all tests are run.
module.exports = async function globalSetup(globalConfig) {
  await setupPuppeteer(globalConfig);
  const { TEST_PLATFORM, PUPPETEER_WS_ENDPOINT } = process.env;

  const cookies = await FetchCookies([TEST_PLATFORM]);
  const browser = await puppeteer.connect({ browserWSEndpoint: PUPPETEER_WS_ENDPOINT });

  // Open a new page
  const page = await browser.newPage();
  await page.setCookie(...cookies);
  await page.close();
};
