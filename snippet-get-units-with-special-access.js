const enrollments = await api('/d2l/api/lp/1.24/enrollments/myenrollments/?isActive=true&orgUnitTypeId=3')
  .then(data => data.filter(({ OrgUnit }) =>
    OrgUnit.Code.includes('19S2') &&
    (
      OrgUnit.Name.startsWith('CNA') ||
      OrgUnit.Name.startsWith('CXA') ||
      OrgUnit.Name.startsWith('CRH')
    )
  ));

const hasSpecialAccess = async orgId => {
  const page = await fetch('/d2l/lms/dropbox/admin/folders_manage.d2l?ou=' + orgId)
  const dom = new DOMParser().parseFromString(await page.text(), 'text/html');
  return dom.querySelectorAll('img[title="Special Access is required" i]').length > 0;
}

const locked = await Promise.all(enrollments.map(async ({ OrgUnit }) => await hasSpecialAccess(OrgUnit.Id) ? OrgUnit : null)).then(res => res.filter(r => r));
locked.forEach(OrgUnit => console.log(`${OrgUnit.Name}: https://mylo.utas.edu.au/d2l/lms/dropbox/admin/folders_manage.d2l?ou=${OrgUnit.Id}`))