const path = require('path');
const ext = path.join(process.cwd(), 'src');
const config = {
  launch: {
    headless: false,
    args: [
      `--disable-extensions-except=${ext}`,
      `--load-extension=${ext}`,
      '--disable-background-timer-throttling',
      '--disable-backgrounding-occluded-windows',
      '--disable-renderer-backgrounding',
      '--dom-automation'
    ]
  }
};

if (process.env.TEST_EDGE == 'true') {
  config.launch.executablePath = 'C:\\Program Files (x86)\\Microsoft\\Edge\\Application\\msedge.exe';
}

module.exports = config;
