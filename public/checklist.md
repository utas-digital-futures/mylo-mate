# MyLO
## General
- `Jump to Top|Bottom` buttons working correctly.
- Automatically resizes popup dialogs to a larger height (max being window height - `280px`).
- Homepage dot colours appearing correctly.
- `<body>` element getting `mm-abort` attribute.
- All TinyMCE instances have `Remove style attributes from tags` button appearing in footer.
- Subtitle and colour circle appears next to unit headings.

## Advanced Search
- Row colouring appears correctly.

## Assignments
### Assignment Folders
- `Go to rubric` button appears beside linked assignment folders.
- Quick links appear next to assignment folders.
### View Submissions
- Assignment status dropdown filters shows expected values.
- `On this page` shows correct types and counts.
- `Resubmit to Turnitin` resubmits expected files.
### Evaluate Submission
- Page scrolls instead of components.

## Classlist
- Adds pagination to the top right corner.
- Impersonate Student View button appears in top left corner.

## Content Viewer
- `[DRAFT]` appears to the right above drafted content.
- **[HTML only]** Pencil icon appears in a button above the content, with a direct link to the Edit page.
- **[HTML only]** Link icon apperas in a button above the content, with a dropdown panel to run the link checker and return results.
  - Link checker works correctly, finding issues and providing accurate feedback.
  - Links found with issues are highlighted in the page content.

## Discussions
### Topic View
- Quick links appear beside discussion topics.
### Post View [Grid View]
- Discussion posts load in beneath post titles.
- Post content can be hidden and shown by clicking the eye icon.
- `Show all posts` & `Hide all posts` toggle all post visibilities.
- Poster role appears beside name, where applicable.
### Misc
- Bulk delete `Select All` checkbox enables button.

## HTML Editor
### ICB bar
- ICB bar appears in MyLO and OER/MOOC.
- ICB elements preview and insert correctly.
- YouTube videos urls are processed and embedded correctly.
- Other types of videos are correctly embedded.
- `Strip tags` removes undesired tags, as well as `style` attributes from content within `div#content`.

### General UI
- **[Draft]** `[DRAFT]` appears above top right of editor when document in draft state.
- **[Draft]** `Save as draft and close` appears in front of Publish, and Publish is grey instead of blue.
- **[Draft]** `Save` button saves page without returning to viewer.
- Editor resizes to fill more of the page.

## Manage Files
- Navigating directories will add to Back/Forward history stack.
- Navigating Back/Forward through browser history will change the currently navigated directory (and auto-select the right element in the left sidebar).
- `Select all x` buttons in the toolbar work correctly.
- `Jump to Top|Bottom` work as expected.

## Grades Area
- Adds `[r]` next to Grade item, that leads to that items `Restrictions` tab.
- Checks user progress settings and displays status in the top right corner.
- User progress box has a link to [User progress settings](#userprogress-settings)
- Pagination duplicated in top right of page.
- Grades table fills width of screen.
- Examines grades and warns of rounding errors.

## Groups tool
- Bulk edit names tool renames groups properly.
- Pagination duplicated in top right of page.
- `Select all` checkbox appears above each group in `Enrol Users`
- `Select all` only selects its column.

## Import/Export Components
- <a id="importfromfile-auto"></a> Import dialog automatically appears when navigated to from [Rubric `Import from file` button](#rubric-importfromfile)
- `View Rubrics` button appears at end of import component process.

## Navbar
- `Manage Files`, `Unit Admin` and `Impersonate Student` icons appears in navbar.
- `Groups` icon appears when `Groups` not available in navbar.
- `Unit Outline` appears in navbar.
  - Dropdown menu appears in units with multiple Unit Outlines.

## Quizzes
- Checks all checkboxes when `Question used in different quizzes` page appears.
- Quick links appear next to quiz items.

## Rubrics
### Listing
- <a id="rubric-importfromfile"></a>`Import from file` button navigates to `Import/Export components` and [automatically opens `Import` dialog](#importfromfile-auto).
- Button appears that links to Rubric Generator.
- Icon appears beside each editable rubric to exports to Rubric Generator.
### Editor
- Floating, consolidated toolbar at top of page, and removed from each editor box.
- Toolbar also includes three `Apply to all` buttons:
  - Align left
  - Clear formatting
  - Remove style attributes from tags

## Table of Contents
- Unit Builder button appears in top right of `Content` page.
- **[HTML files only]** `[e]` appears to the right of `Web Page` text, with direct link to page edit.
- Drafted modules `Draft` text is italicised in sidebar.

## User Progress
- <a id="userprogress-settings"></a> Button available to toggle Common Hide Settings, in accordance with UTAS policies.
- Modal appears to offer more information.

## Waffle Menu
- Units are coloured correctly.
- Unit colours change when accessability mode is enabled.
- Unit colours continue to apply when list adds more elements.
  - This can occur both from scrolling further, or running a search and clicking 'Load more units'

## Widgets
### Link Checker
- Gracefully handles bad / timing out links.
- Correctly showing units with bad links.
- Accurate count of pages with issues.
- Accurate count of issues found in pages.
- Details provided about issues are accurate and understandable.

### Search Unit
- Regex and case insensitivity works correctly.
- Cannot search for anything under 4 non-whitespace characters long.
- Handles PDF, .docx and HTML files without errors.
- Successfully fetches information from content titles and descriptions.
- Handles bad links found in unit content without failure.

### Grades Analysis
- Graphs appear correctly,
- Students fetched from API successfully.
- Data processed correctly and showing accurate values.

### Get Lecturers from Unit
- Unit name and run period (name and semester/term) are processed correctly.
- `.csv` file built and downloaded successfully.

# MyLO Manager
## Groups tool
- Is the groups tool still visible in the `Staff` tab?
- Can groups still be added to the page?
- Do users double up if they already added to the page?
- Can new users still be added to groups?

## Homepage
- Are the quicklinks still appearing correctly?

# Echo360
## Downloader
- Do the files still download?
- Do the files download with the correct names?