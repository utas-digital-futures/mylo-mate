MyLO MATE is a browser extension designed for Google Chrome. It modifies and improves upon MyLO and OER, MyLO Manager and Echo360, and includes bug fixes and brand new features.
  
Throughout this document, the term MyLO will be used to describe both the MyLO and OER instances of Brightspace.

### Immediate Changes
When first installed, you will notice a few changes appear within MyLO, including an improved content layout, and the Interactive Content Builder (ICB) sidebar in the content editor.

### The ICB sidebar
The ICB sidebar appears in the MyLO content editor, when you're editing something that uses a template. Some of the features made available by this sidebar include (but aren't limited to):
  
- Page Headings
- Block quotes
- Task and Activity blocks (Web Resource, Reading, etc)
- Image blocks
- Stylised tables
- YouTube video embed tool
- Interactive components

This sidebar will appear on the right hand side of your content editor. When you hover over an insertable item (denoted by a **+** on the left hand side of the entry), it will show a preview of that element in the content editor.

### Options
There are several options available within MyLO MATE, allowing you to customise your experience, and enable new features.  
You can access new features by right-clicking on the link icon (![link icon](/etc/images/icon16.png)) located in the top bar, beside your web address bar, and selecting "Options".  
![MyLO MATE Options](https://shs-utas.gitlab.io/mylo-mate/etc/images/main-options1.png)  
  
Once in the Options panel, there will be several tabs available to you, with checkboxes in each one. The headings below will explain what options are available to you in each tab.

/--

/^^ MyLO Page
<!-- #### MyLO Page -->

##### Add extra admin buttons in navigation bar

Adds a button for `Unit Outline`, `Manage Files`, `Unit Admin`, `Groups` and `Impersonate Student View` in the navigation bar beside `Admin & Help`. `Group` will only appear if a link to the Groups tool does not already exist in the navigation bar.

##### Override Grades link

Redirects the Grades link in the navigation bar to `Manage Grades` instead of `Enter Grades`.

##### Highlight unit row backgrounds by date

Highlights the row backgrounds based on their start and end date, with green for current units, red for past units and white for future units.

##### Add Back to Top and Jump to Bottom buttons

Adds a 'Back to Top' and a 'Jump to Bottom' button in the bottom right when you begin to scroll long pages.

/^^ MyLO 
<!-- #### MyLO  -->

![MyLO Options](https://shs-utas.gitlab.io/mylo-mate/etc/images/options-mylo.png)

##### Always show archived rubrics

Automatically shows rubrics that have been Archived. Note, this disables the ability to toggle this selection in the Advanced Search area of Rubrics.

##### Quizzes, Assignment Folders, Grades link enhancements

Direct links to highly trafficked tabs in each tool will be added next to the titles. 

##### Default to 'Show everyone' in Assignment Submissions

Similar to the above "Always show archived rubrics", this shows all students in an assignment folder, even if they haven't submitted anything. Again, this disables the ability to toggle this search option in-page, so you will need to disable this option if you no longer wish to see all students.

##### Use Back and Forward in Manage Files

Allows you to use your browser Back and Forward buttons to move through your folder navigation history. 

##### Enhanced Discussions [Grid View only]

When viewing a discussion forum in Grid View, this inserts the post content directly under the heading, without having to open the post in a new window. This option controls whether posts are visible by default.

/^^ MyLO Widgets
<!-- #### MyLO Widgets -->

##### Search Unit

Search unit content for the given word or phrase. This will check any HTML, PDF and DOCX content, title, and optionally description. Advanced search options (called regular expressions) are available if needed.

##### Invalid Link Checker

Checks unit content for any links with potential issues. Most used for checking **Page Not Found** errors, but will report back on any issues that occur.

##### Grades Analysis

// Kevin should write about this.

##### Status Report

Runs a generalised Status Report on the unit. Some of the checks include:
- Making sure a Unit Outline exists
- Confirm that the Unit Outline isn't a placeholder (currently checks that "unit outline unavailable" doesn't appear in the content)
- There is an ICB template installed in this unit
- The ICB template installed is the same one used within the content (to prevent issues of using another units template)
- Check for announcements that were published more than one month before the unit is set to open
- Check for any students that haven't been assigned to a group
- Check for hidden assessment folders
- Check for reading lists that are unavailable (if unit uses reading lists)

##### Unit Outline Uploader

Provides a user-friendly interface for uploading a Unit Outline to a unit, including automatic file naming.

##### Word Cloud Generator

Generate a word cloud from the Unit Content. The widget allows you to include Discussion posts, and provide a custom list of ignored words. Once generated, you can then save a high-resolution image of the word cloud to your computer.

##### Get Lecturers from Units **[SANDPIT ONLY]**

By providing a unit code (or partial unit code), and a run period (year and semester), a list of lecturers will be returned (with email addresses). This list can then be copied to your clipboard or saved to a file.

##### Get Absent Students **[SANDPIT ONLY]**

By providing a unit code (or partial unit code), and a run period (year and semester), this will create a list of all students (name and email addresses) who haven't logged in to the unit. This will soon be able to also support a timeframe, allowing you to list students who haven't logged in the last x days.

/^^ MyLO Editor
<!-- #### MyLO Editor -->

![MyLO Editor Options](https://shs-utas.gitlab.io/mylo-mate/etc/images/options-mylo-editor.png)

##### Show ICB Sidebar on open

Automatically show the ICB sidebar when you visit the content editor.

##### Enable Developer Tools

Add additional developer support tools to the sidebar, including:
- Edit Class: Modify the classes of the element selected in the content editor.
- Toggle Underrule: Toggles the underrule of the element currently selected in the editor. Mostly used for page headings, but can be applied to most components.
- Strip Tags: Removes unnecessary tags and attributes from the HTML content, usually carried in from Microsoft Word.
- Open All Links in New Tab: Force all links found in the page to open in a new tab (or window, depending on user settings).

##### Enable additional ICB features

Add additional editor features to the sidebar, including **Format Markers** and **Create Hover Text**.

##### Turn Format Markers on by default

Shows the in-editor **Format Markers** by default.

##### Enable Secondary Headings

When adding a Page Heading from the sidebar, this option toggles whether it's added with a secondary smaller heading.

##### Enable Preview

Toggle whether to show the in-editor preview when you hover over elements in the sidebar.

##### Make All Links Open in New Tab

When this option is checked, all links in the page will be set to open in a new tab when the page is saved.

/^^ User Groups
<!-- #### User Groups -->

![User Groups Options](https://shs-utas.gitlab.io/mylo-mate/etc/images/options-user-groups.png)

Please note this options tab is only available if the `Enable user groups` option is enabled in the `MyLO Manager` tab.

##### Creating a new group

Select the `Add` group button:

![Add New Group](https://shs-utas.gitlab.io/mylo-mate/etc/images/options-user-groups-new-group1.png)

Type a name for your new group and select `OK`:

![Add New Group](https://shs-utas.gitlab.io/mylo-mate/etc/images/options-user-groups-new-group2.png)

##### Add users to a group

Select a group from the drop-down list, then select the `Add` users button:

![Add Users](https://shs-utas.gitlab.io/mylo-mate/etc/images/options-user-groups-add-users1.png)

Type in the usernames of the users you want to add to the selected group (separated by commas), select the role (Support, Lecturer, etc.) and then select OK:

![Add Users](https://shs-utas.gitlab.io/mylo-mate/etc/images/options-user-groups-add-users2.png)

/^^ MyLO Manager
<!-- #### MyLO Manager -->

![MyLO Manager Options](https://shs-utas.gitlab.io/mylo-mate/etc/images/options-mylo-manager.png)

##### Units per page

Sets the default number of units per page to display. Changing the records-per-page in MyLO Manager will update this setting automatically.

##### Add Bulk User Management (BUM) CSV Export button at bottom of table

Adds a Bulk User Management (BUM) CSV export button to the bottom of the unit list table. This can be used in conjunction with ITS for bulk enrolment of staff or students for audit purposes.

##### Enable user groups

Enable User Groups to be defined for bulk adding of staff to units.

##### Override Existing users Roles

The User Groups tool will automatically add a user-defined collection of users and roles to the Staff tab of a unit in MyLO Manager. By default, it will ignore any overlapping users between the current Staff and the user group, including their roles. When this checkbox is enabled, it will override the existing users role in the Staff tab with the one defined in the user group.

##### Use Advanced MyLO Manager

There is a link to MyLO Manager in the "popdown" when you click the MyLO MATE link icon (![link icon](/etc/images/icon16.png)) in the URL bar.  
When this option is enabled, it will toggle this to enforce opening the "Advanced" view of MyLO Manager.

/^^ Other
<!-- #### Other -->

![Other Options](https://shs-utas.gitlab.io/mylo-mate/etc/images/options-other.png)

##### Show DEV2 link

Adds a link to the Dev2 MyLO instance to the MyLO MATE popdown.

##### Accessibility mode

Some features of MyLO MATE utilise colours to convey information, such as colouring the "waffle menu" rows based on their open status, or adding coloured circles next to unit titles (to convey the same information as the waffle menu). With accessibility mode enabled, these colours change to utilise higher contrasting colours, and circles next to unit titles will change shape and colour depending on the unit status.

##### Advanced Ed Tech features

These are a subset of features that are designed to provide increased functionality for Ed Techs, but come with increased risk. We encourage you to only use these features if you are aware of the consequences, and have the capacity to resolve the issues that may arise from using these tools.

**Bulk Hide/Show Grade Items**  
This allows Grade items to be hidden or unhidden en masse, which can be extremely useful for pre-semester unit preperations.

**Bulk Unchecking Date in Assignment area**  
When using the "Bulk Edit" feature in the Assignments area, this feature will appear, allowing you to uncheck all Start/End/Due date checkboxes at once.

**Bulk Student emailing**  
Provides a button that adds all students from a unit into a single 'Compose Email' dialog window. Use with caution!

**Additional Manage Files features**  
Adds some new features to the Manage Files area, including feature-specific 'Select All' buttons (directories, linked files, unlinked files), and the capacity to Bulk Rename files.  
Bulk Rename note: this process cannot be undone, without using the Bulk Rename tool again. Proceed with care.
  
--/