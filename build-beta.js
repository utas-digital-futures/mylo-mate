const path = require('path');
const archiver = require('archiver');
const fs = require('fs');

const folder = 'src';
const zipName = 'beta-ext.zip';
const manifest = require(`./${folder}/manifest.json`);

const betaPermissions = ['management'];

Date.prototype.getSecondsSinceMidnight = function () {
  return Math.floor((new Date(this) - new Date(this).setHours(0, 0, 0, 0)) / 1000);
};
Date.prototype.isLeapYear = function () {
  var year = this.getFullYear();
  if ((year & 3) != 0) return false;
  return year % 100 != 0 || year % 400 == 0;
};

// Get Day of Year
Date.prototype.getDOY = function () {
  var dayCount = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
  var mn = this.getMonth();
  var dn = this.getDate();
  var dayOfYear = dayCount[mn] + dn;
  if (mn > 1 && this.isLeapYear()) dayOfYear++;
  dayOfYear++;
  return dayOfYear < 10 ? `00${dayOfYear}` : dayOfYear < 100 ? `0${dayOfYear}` : dayOfYear;
};

const d = new Date();

const v = manifest.version;
const [major, minor] = v.split('.');
const beta = `${d.getFullYear() - 2000}${d.getDOY()}.${Math.round(d.getSecondsSinceMidnight() / 2)}`;
manifest.version = `${major}.${minor}.${beta}`;
manifest.version_name = `${v} beta ${beta}`;
manifest.name += ' Beta';
manifest.short_name += ' Beta';
manifest.permissions = betaPermissions.concat(manifest.permissions);

class Package {
  static async Zip(dir, name) {
    return new Promise((resolve, reject) => {
      const output = fs.createWriteStream(path.join(__dirname, name));
      const zip = archiver('zip', {
        zlib: { level: 9 },
      });

      output.on('close', resolve);
      output.on('end', resolve);
      zip.on('warning', e => {
        if (e.code === 'ENOENT') console.log('Warning: ', e);
        else reject(e);
      });
      zip.on('error', reject);

      zip.pipe(output);
      zip.glob('**/!(*manifest*)', { cwd: dir });
      zip.append(JSON.stringify(manifest), { name: 'manifest.json' });
      zip.finalize();
    });
  }
}

(async function () {
  try {
    await Package.Zip(folder, zipName);
    console.log('Zip file "%s" created from "%s/"', zipName, folder);
  } catch (e) {
    console.log('Error: ', e);
  }
})();
